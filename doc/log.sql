-- ALTER TABLE 17 Juni
ALTER TABLE  `grv_article` CHANGE  `ShortDescription`  `ShortDescription` VARCHAR( 300 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE  `grv_article` CHANGE  `Tag`  `Tag` VARCHAR( 350 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE  `grv_article` CHANGE  `Quote`  `Quote` VARCHAR( 450 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL