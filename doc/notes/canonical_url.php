canonical URL adalah metode untuk memudahkan google mendeteksi konten yang sama dari URL yang berbeda agar page ranking website tidak dianggap sebagai duplikat oleh google.

contoh:
website A memiliki url yang menampilkan halaman yang sama
- http://blabla.com/category=apparel&gender=mens
- http://blabla.com/apparel/mens
- http://blabla.com/tags/apparel

ketiganya menampiulkan konten yang sama, tapi anda ingin agar semua diarahkan ke :
- http://blabla.com/apparel/mens

maka yang harus dilakukan adalah memberikan tag element canonical dengan url tsb. di bagian header seperti : 

<link rel="canonical" href="http://blabla.com/apparel/mens" />

dan memberikan permanent redirect (301) ke URL selain diatas tsb, untuk memastikan semua pengunjung ke URL canonical.