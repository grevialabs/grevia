-- mapping server name
-- friday 14 september 2018

DROP TABLE grv_server; 
CREATE TABLE grv_server(
server_id mediumint NOT NULL AUTO_INCREMENT,
name varchar(120) NULL DEFAULT NULL,
domain varchar(120) NULL DEFAULT NULL,
ip varchar(20) NULL DEFAULT NULL,
location enum('idr','sqp') NULL DEFAULT NULL,
repo enum('git','svn') NULL DEFAULT NULL,
repo_url varchar(20) NULL DEFAULT NULL,
description text NULL DEFAULT NULL,
creator_date date NULL DEFAULT NULL,
editor_date date NULL DEFAULT NULL,
PRIMARY KEY(server_id)
) CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `grv_server`
	ADD UNIQUE INDEX `name` (`name`);
	
INSERT INTO `grv`.`grv_server` (`name`, `domain`, `repo`, `location`, `ip`) VALUES ('diablos', 'grevia.com', 'svn', 'sgp', '128.199.223.13');
INSERT INTO `grv`.`grv_server` (`name`, `domain`, `repo`, `location`, `ip`) VALUES ('patriot', 'grevialabs.com', 'git', 'sgp', '178.128.49.192');

INSERT INTO grv_server(name) VALUES('Agate'),
('bandit'),
('cerberus'),
('pandora'),
('poseidon'),
('mojito'),
('fenix'),
('renux'),
('fira'),
('meltdown'),
('gravija'),
('doomtrain'),
('rebel'),
('comet'),
('meteora'),
('stingray'),
('mako'),
('excalibur'),
('scimitar'),
('aurora'),
('cyclone'),
('dagger'),
('hexagon'),
('rainbow'),
('juniper'),
('lancelot'),
('lumina'),
('luminaire'),
('pegasus'),
('tornado'),
('vector'),
('thunderbird'),
('deacon'),
('timberwolf'),
('lancer'),
('volcano'),
('onyx'),
('monix'),
('merlin'),
('magnum'),
('eureka'),
('evangelist'),
('desperado'),
('puma'),
('wolverine'),
('stellar'),
('gyona'),
('Kamui'),
('nikita');
	
--------------------------------------------------------------------------

-- 10 januari 2018
CREATE TABLE grv_todo
(
todo_id int AUTO_INCREMENT NOT NULL,
title varchar(255) NULL DEFAULT NULL,
details text NULL DEFAULT NULL,
priority tinyint NULL DEFAULT 1 COMMENT '0 = not important, 1 = normal, 2 = high',
reminder_date datetime NULL DEFAULT NULL,
status tinyint NULL DEFAULT 0,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(todo_id)
)

-- 20 desember 2017
ALTER TABLE `grv_abtest_view`
	ADD COLUMN `trigger_name` VARCHAR(100) NULL AFTER `abtest_detail_id`,
	DROP COLUMN `is_click`;


-- Dumping data for table grv_baru.grv_abtest: ~0 rows (approximately)
/*!40000 ALTER TABLE `grv_abtest` DISABLE KEYS */;
INSERT INTO `grv_abtest` (`abtest_id`, `title`, `modulename`, `quota`, `description`, `startdate`, `enddate`, `status`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 'Ordertesting', 'ordernew', 30, 'ini adalah ab testing pertama grevia', '2017-12-18', '2017-12-20', 1, NULL, NULL, NULL, NULL, NULL, NULL);



-- Dumping data for table grv_baru.grv_abtest_detail: ~2 rows (approximately)
/*!40000 ALTER TABLE `grv_abtest_detail` DISABLE KEYS */;
INSERT INTO `grv_abtest_detail` (`abtest_detail_id`, `abtest_id`, `codename`, `filename`, `creator_id`, `creator_ip`, `creator_date`, `editor_id`, `editor_ip`, `editor_date`) VALUES
	(1, 1, 'A', 'a', 1, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'B', 'b', 1, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 'C', 'c', 1, NULL, NULL, NULL, NULL, NULL);

	
-- 18 desember 2017
-- ab testing / a/b testing
CREATE TABLE grv_abtest(
abtest_id BIGINT NOT NULL AUTO_INCREMENT,
title varchar(150) NULL DEFAULT NULL,
modulename varchar(50) NULL DEFAULT NULL COMMENT 'nama module ab test',
quota mediumint NULL DEFAULT 0 COMMENT 'jumlah quota orang',
description text NULL DEFAULT NULL,
startdate datetime NULL,
enddate datetime NULL,
status tinyint NULL DEFAULT 0,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(abtest_id)
);

CREATE TABLE grv_abtest_detail(
abtest_detail_id BIGINT NOT NULL AUTO_INCREMENT,
abtest_id BIGINT NOT NULL,
codename varchar(25) NULL DEFAULT NULL,
filename varchar(25) NULL DEFAULT NULL,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(abtest_detail_id,abtest_id)
);

CREATE TABLE grv_abtest_view(
abtest_view_id BIGINT NOT NULL AUTO_INCREMENT,
abtest_detail_id BIGINT NOT NULL,
is_click tinyint NULL DEFAULT 0,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(abtest_view_id)
);

ALTER TABLE `grv_abtest` COLLATE='utf8_general_ci' ENGINE=InnoDB;
ALTER TABLE `grv_abtest_detail` COLLATE='utf8_general_ci' ENGINE=InnoDB;
ALTER TABLE `grv_abtest_view` COLLATE='utf8_general_ci' ENGINE=InnoDB;

ALTER TABLE `grv_abtest_detail`
	ADD CONSTRAINT `FK_grv_abtest_id1` FOREIGN KEY (`abtest_id`) REFERENCES `grv_abtest` (`abtest_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `grv_abtest_view`
	ADD CONSTRAINT `FK_grv_abtest_view_id1` FOREIGN KEY (`abtest_detail_id`) REFERENCES `grv_abtest_detail` (`abtest_detail_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- 22 november 2017
-- add rating and total count rating for article star
ALTER TABLE grv_article
ADD average_rating DECIMAL(5,1) DEFAULT '5.0' AFTER view,
ADD total_rating smallint DEFAULT 0 AFTER average_rating;

UPDATE grv_article a SET a.total_rating = (FLOOR(RAND() * (200 - 1 + 1)) + 1), a.average_rating = CAST(CONCAT((FLOOR(RAND() * (4 - 3 + 1)) + 3),'.',(FLOOR(RAND() * (9 - 1 + 1)) + 1)) as DECIMAL(5,1));
SELECT average_rating, total_rating FROM grv_article LIMIT 50;

-- 12 october 2017
CREATE TABLE grv_order(
order_id int NOT NULL AUTO_INCREMENT,
order_number varchar(50) NULL,
is_paid tinyint DEFAULT 0,
cust_name varchar(200) NULL,
cust_email varchar(200) NULL,
cust_phone varchar(30) NULL,
payment_code smallint NULL DEFAULT 0,
`status` TINYINT(4) NULL DEFAULT '0',
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(order_id)
);

CREATE TABLE grv_order_detail(
order_detail_id int NOT NULL AUTO_INCREMENT,
order_id int NOT NULL,
product_id int NULL,
price int NULL,
PRIMARY KEY(order_detail_id,order_id)
);

CREATE TABLE grv_product(
product_id int NOT NULL AUTO_INCREMENT,
name varchar(100) NULL, 
description text NULL,
price int NULL DEFAULT 0,
`status` TINYINT(4) NULL DEFAULT '0',
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(product_id)
);

-- 3 october 2017
CREATE TABLE grv_comment(
comment_id int AUTO_INCREMENT NOT NULL,
url_source varchar(455) NULL DEFAULT NULL,
user_name varchar(155) NULL DEFAULT NULL,
user_comment varchar(255) NULL DEFAULT NULL,
user_web varchar(255) NULL DEFAULT NULL,
vote_count mediumint NULL DEFAULT 0,
status tinyint DEFAULT 1,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(comment_id)
)

-- Jumat 8 september 2017
ALTER TABLE `bok_booking_detail`
	ADD COLUMN `price` MEDIUMINT NOT NULL DEFAULT '0' AFTER `seat_id`;

ALTER TABLE `bok_booking`
	ADD COLUMN `book_code` VARCHAR(15) NULL DEFAULT NULL COMMENT 'kode booking' AFTER `book_date`;

-- Kamis 31 agustus 2017
-- Buat booking studio
CREATE TABLE bok_location(
location_id BIGINT NOT NULL AUTO_INCREMENT,
name varchar(150) NULL,
address varchar(255) NULL,
status tinyint DEFAULT 1,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(location_id)
)COLLATE='utf8_general_ci',
ENGINE=InnoDB;

CREATE TABLE bok_studio(
studio_id BIGINT NOT NULL AUTO_INCREMENT,
location_id BIGINT NOT NULL,
code varchar(10) COMMENT 'ex: 1, 2, 3, etc',
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(studio_id)
)COLLATE='utf8_general_ci',
ENGINE=InnoDB;

CREATE TABLE bok_seat(
seat_id BIGINT NOT NULL AUTO_INCREMENT,
studio_id BIGINT NOT NULL,
name varchar(10) COMMENT 'ex: A, B, C, etc',
pos varchar(10) COMMENT 'ex: 1, 2, 3, etc ',
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(seat_id)
)COLLATE='utf8_general_ci',
ENGINE=InnoDB;

CREATE TABLE bok_booking(
booking_id BIGINT NOT NULL AUTO_INCREMENT,
movie_schedule_id BIGINT NOT NULL,
book_date date NULL,
admin_fee INT NULL,
grandtotal INT NULL,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(booking_id)
)COLLATE='utf8_general_ci',
ENGINE=InnoDB;

CREATE TABLE bok_booking_detail(
booking_detail_id BIGINT NOT NULL AUTO_INCREMENT,
booking_id BIGINT NOT NULL,
seat_id BIGINT NOT NULL,
PRIMARY KEY(booking_detail_id,booking_id),
CONSTRAINT `FK_bbd_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `bok_booking` (`booking_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
CONSTRAINT `FK_bbd_seat_id` FOREIGN KEY (`seat_id`) REFERENCES `bok_seat` (`seat_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

CREATE TABLE bok_movie(
movie_id BIGINT NOT NULL AUTO_INCREMENT,
code varchar(15) NOT NULL,
title varchar(255) NULL,
keyword varchar(255) NULL,
description varchar(255) NULL,
status tinyint NULL DEFAULT 1,
image_url varchar(255) NULL,
trailer_url varchar(255) NULL,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(movie_id)
)COLLATE='utf8_general_ci',
ENGINE=InnoDB;

CREATE TABLE bok_movie_schedule(
movie_schedule_id BIGINT NOT NULL AUTO_INCREMENT,
movie_id BIGINT NOT NULL,
start_date date NULL,
start_time time NULL,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(movie_schedule_id),
CONSTRAINT `FK_movie_id` FOREIGN KEY (`movie_id`) REFERENCES `bok_movie` (`movie_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

CREATE TABLE bok_studio_type(
studio_type_id BIGINT NOT NULL AUTO_INCREMENT,
studio_id BIGINT NOT NULL,
name varchar(120) NULL COMMENT 'Nama type studio. ex: velvet, VIP, lovesuit',
description varchar(250) NULL,
price_weekday int NULL DEFAULT 0,
price_jumat int NULL DEFAULT 0,
price_holiday int NULL DEFAULT 0,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(studio_type_id),
CONSTRAINT `FK_bs_studio_id` FOREIGN KEY (`studio_id`) REFERENCES `bok_studio` (`studio_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

CREATE TABLE bok_studio_calendar(
studio_calendar_id BIGINT NOT NULL AUTO_INCREMENT,
studio_id BIGINT NULL,
input_date date NULL,
description varchar(255) NULL,
is_closed tinyint NULL DEFAULT 0,
is_price_weekday tinyint NULL DEFAULT 0,
is_price_jumat tinyint NULL DEFAULT 0,
is_price_holiday tinyint NULL DEFAULT 0,
creator_id INT(11) NULL DEFAULT NULL,
creator_ip VARCHAR(15) NULL DEFAULT NULL,
creator_date DATETIME NULL DEFAULT NULL,
editor_id INT(11) NULL DEFAULT NULL,
editor_ip VARCHAR(15) NULL DEFAULT NULL,
editor_date DATETIME NULL DEFAULT NULL,
PRIMARY KEY(studio_calendar_id),
CONSTRAINT `FK_bsc_studio_id` FOREIGN KEY (`studio_id`) REFERENCES `bok_studio` (`studio_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

ALTER TABLE `bok_studio`
	ADD CONSTRAINT `FK_bok_location_id` FOREIGN KEY (`location_id`) REFERENCES `bok_location` (`location_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
	
ALTER TABLE `bok_seat`
	ADD CONSTRAINT `FK_studio_id` FOREIGN KEY (`studio_id`) REFERENCES `bok_studio` (`studio_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `bok_booking`
	ADD CONSTRAINT `FK_bb_movie_schedule_id` FOREIGN KEY (`movie_schedule_id`) REFERENCES `bok_movie_schedule` (`movie_schedule_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
	
ALTER TABLE `bok_booking`
	ADD UNIQUE INDEX `seat_id_book_date` (`seat_id`, `book_date`);
	
ALTER TABLE `bok_movie_schedule`
	ADD CONSTRAINT `FK_ms_studio_id` FOREIGN KEY (`studio_id`) REFERENCES `bok_studio` (`studio_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `bok_movie`
	ADD COLUMN `duration` TINYINT NULL DEFAULT NULL COMMENT 'in minutes' AFTER `trailer_url`;
	
INSERT INTO `bok_seat` (`studio_id`, `name`, `pos`) VALUES 
('1', 'B', '1'),('1', 'B', '2'),('1', 'B', '3'),('1', 'B', '4'),('1', 'B', '5'),('1', 'B', '6'),('1', 'B', '7'),('1', 'B', '8'),
('1', 'C', '1'),('1', 'C', '2'),('1', 'C', '3'),('1', 'C', '4'),('1', 'C', '5'),('1', 'C', '6'),('1', 'C', '7'),('1', 'C', '8'),
('1', 'D', '1'),('1', 'D', '2'),('1', 'D', '3'),('1', 'D', '4'),('1', 'D', '5'),('1', 'D', '6'),('1', 'D', '7'),('1', 'D', '8');
----------------------------------------------------------

-- 10 agustus 2017
CREATE TABLE `grv_video` (
	`video_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`videolist_id` BIGINT(20) NULL DEFAULT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`author` VARCHAR(255) NULL DEFAULT NULL,
	`description` TEXT NULL,
	`url` VARCHAR(500) NULL DEFAULT NULL,
	`status` TINYINT(4) NULL DEFAULT '0',
	`creator_id` SMALLINT(4) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` SMALLINT(4) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`video_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
AUTO_INCREMENT=7
;

CREATE TABLE `grv_videolist` (
	`videolist_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`description` TEXT NULL,
	`status` TINYINT(4) NULL DEFAULT '0',
	`creator_id` SMALLINT(4) NULL DEFAULT NULL,
	`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
	`creator_date` DATETIME NULL DEFAULT NULL,
	`editor_id` SMALLINT(4) NULL DEFAULT NULL,
	`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
	`editor_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`videolist_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;


-- 2 Sept 2016
ALTER TABLE `grv_calendar`
	ADD COLUMN `start_time` TIME NULL AFTER `input_date`,
	ADD COLUMN `end_time` TIME NULL AFTER `start_time`,
	ADD COLUMN `bg_color` TEXT NULL AFTER `notes`,
	ADD COLUMN `is_global_calendar` TINYINT NULL DEFAULT '0' COMMENT 'untuk penanggalan calendar global user, untuk tgl merah' AFTER `bg_color`;
	
-- 31 Aug 2016
CREATE TABLE grv_calendar(
calendar_id BIGINT NOT NULL AUTO_INCREMENT,
input_date date NOT NULL,
notes text NULL DEFAULT NULL,
`creator_id` SMALLINT(4) NULL DEFAULT NULL,
`creator_ip` VARCHAR(20) NULL DEFAULT NULL,
`creator_date` DATETIME NULL DEFAULT NULL,
`editor_id` SMALLINT(4) NULL DEFAULT NULL,
`editor_ip` VARCHAR(20) NULL DEFAULT NULL,
`editor_date` DATETIME NULL DEFAULT NULL,
PRIMARY KEY(calendar_id)
)

-- 8 Juli 2016
ALTER TABLE `grv_tracker`
	CHANGE COLUMN `TrackerID` `tracker_id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Name` `name` VARCHAR(200) NULL DEFAULT NULL COMMENT 'child element ex: hot-threads-1, sidebar-2, ' AFTER `tracker_id`,
	CHANGE COLUMN `Url` `url` VARCHAR(500) NULL DEFAULT NULL COMMENT 'url target' AFTER `name`,
	CHANGE COLUMN `Source` `source` VARCHAR(100) NULL DEFAULT NULL COMMENT 'page source ex: home, article, category' AFTER `url`,
	CHANGE COLUMN `Segment` `segment` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Parent element ex: hot-thread-10' AFTER `source`,
	CHANGE COLUMN `CreatorDate` `creator_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `segment`,
	CHANGE COLUMN `CreatorIP` `creator_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `creator_date`;


-- 9 April 2016

ALTER TABLE `grv_statistic`
	CHANGE COLUMN `StatisticID` `statistic_id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Source` `source` VARCHAR(155) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `statistic_id`,
	CHANGE COLUMN `Campaign` `campaign` VARCHAR(155) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `source`,
	CHANGE COLUMN `Url` `url` VARCHAR(500) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `campaign`,
	CHANGE COLUMN `CreatorDate` `creator_date` DATE NULL DEFAULT NULL AFTER `Hit`;

ALTER TABLE `grv_search`
	CHANGE COLUMN `SearchID` `search_id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Keyword` `keyword` VARCHAR(150) NULL DEFAULT NULL AFTER `search_id`,
	CHANGE COLUMN `CreatorDate` `creator_date` DATETIME NULL DEFAULT NULL AFTER `keyword`,
	CHANGE COLUMN `CreatorIP` `creator_ip` VARCHAR(15) NULL DEFAULT NULL AFTER `creator_date`;

ALTER TABLE `grv_reference`
	CHANGE COLUMN `ReferenceID` `reference_id` BIGINT(20) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Ref` `ref` VARCHAR(150) NULL DEFAULT NULL AFTER `reference_id`,
	CHANGE COLUMN `FromUrl` `from_url` VARCHAR(255) NULL DEFAULT NULL AFTER `ref`,
	CHANGE COLUMN `UtmSource` `utm_source` VARCHAR(150) NULL DEFAULT NULL AFTER `from_url`,
	CHANGE COLUMN `UtmMedium` `utm_medium` VARCHAR(150) NULL DEFAULT NULL AFTER `utm_source`,
	CHANGE COLUMN `UtmContent` `utm_content` VARCHAR(150) NULL DEFAULT NULL AFTER `utm_medium`,
	CHANGE COLUMN `UtmCampaign` `utm_campaign` VARCHAR(150) NULL DEFAULT NULL AFTER `utm_content`,
	CHANGE COLUMN `CreatorDate` `creator_date` DATETIME NULL DEFAULT NULL AFTER `utm_campaign`,
	CHANGE COLUMN `CreatorIP` `creator_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `creator_date`;

ALTER TABLE `grv_member`
	ALTER `Name` DROP DEFAULT,
	ALTER `Password` DROP DEFAULT;
ALTER TABLE `grv_member`
	CHANGE COLUMN `MemberID` `member_id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Name` `name` VARCHAR(30) NOT NULL AFTER `member_id`,
	CHANGE COLUMN `Email` `email` VARCHAR(150) NULL DEFAULT NULL AFTER `name`,
	CHANGE COLUMN `Password` `password` VARCHAR(100) NOT NULL AFTER `email`,
	CHANGE COLUMN `FullName` `full_name` VARCHAR(100) NULL DEFAULT NULL AFTER `password`,
	CHANGE COLUMN `DOB` `dob` DATE NULL DEFAULT NULL AFTER `full_name`,
	CHANGE COLUMN `Gender` `gender` TINYINT(4) NULL DEFAULT '0' AFTER `dob`,
	CHANGE COLUMN `Facebook` `facebook` VARCHAR(100) NULL DEFAULT NULL AFTER `gender`,
	CHANGE COLUMN `Twitter` `twitter` VARCHAR(50) NULL DEFAULT NULL AFTER `facebook`,
	CHANGE COLUMN `Linkedin` `linkedin` VARCHAR(170) NULL DEFAULT NULL AFTER `twitter`,
	CHANGE COLUMN `Website` `website` VARCHAR(170) NULL DEFAULT NULL AFTER `linkedin`,
	CHANGE COLUMN `AboutMe` `about_me` VARCHAR(500) NULL DEFAULT NULL AFTER `website`,
	CHANGE COLUMN `Image` `image` VARCHAR(100) NULL DEFAULT NULL AFTER `about_me`,
	CHANGE COLUMN `SecureNotes` `secure_notes` TEXT NULL AFTER `image`,
	CHANGE COLUMN `ValidationCode` `validation_code` VARCHAR(30) NULL DEFAULT NULL COMMENT 'kode validasi global' AFTER `secure_notes`,
	CHANGE COLUMN `ForgotPassExp` `forgot_pass_exp` DATETIME NULL DEFAULT NULL COMMENT 'deadline waktu submit kode lupa password' AFTER `validation_code`,
	CHANGE COLUMN `ForgotCode` `forgot_code` VARCHAR(30) NULL DEFAULT NULL COMMENT 'kode konfirmasi untuk lupa password' AFTER `forgot_pass_exp`,
	CHANGE COLUMN `ActiveCode` `active_code` VARCHAR(50) NULL DEFAULT NULL AFTER `forgot_code`,
	CHANGE COLUMN `IsAdmin` `is_admin` TINYINT(4) NULL DEFAULT '0' AFTER `active_code`,
	CHANGE COLUMN `IsSubscribe` `is_subscribe` TINYINT(4) NULL DEFAULT '1' AFTER `is_admin`,
	CHANGE COLUMN `IsActive` `is_active` TINYINT(4) NULL DEFAULT '0' AFTER `is_subscribe`,
	CHANGE COLUMN `IsDeleted` `is_deleted` TINYINT(4) NULL DEFAULT '0';

ALTER TABLE `grv_inbox`
	CHANGE COLUMN `InboxID` `inbox_id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Fullname` `fullname` VARCHAR(120) NULL DEFAULT NULL AFTER `inbox_id`,
	CHANGE COLUMN `Email` `email` VARCHAR(120) NULL DEFAULT NULL AFTER `fullname`,
	CHANGE COLUMN `Subject` `subject` VARCHAR(120) NULL DEFAULT NULL AFTER `email`,
	CHANGE COLUMN `Message` `message` VARCHAR(500) NULL DEFAULT NULL AFTER `subject`;

ALTER TABLE `grv_content`
	CHANGE COLUMN `ContentID` `content_id` SMALLINT(2) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Name` `name` VARCHAR(30) NULL DEFAULT NULL AFTER `content_id`,
	CHANGE COLUMN `Url` `url` VARCHAR(100) NULL DEFAULT NULL AFTER `name`,
	CHANGE COLUMN `Content` `content` VARCHAR(1000) NULL DEFAULT NULL AFTER `url`;

ALTER TABLE `grv_subscribe`
	CHANGE COLUMN `SubscribeID` `subscribe_id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Name` `name` VARCHAR(150) NULL DEFAULT NULL AFTER `subscribe_id`,
	CHANGE COLUMN `Email` `email` VARCHAR(200) NULL DEFAULT NULL AFTER `name`,
	CHANGE COLUMN `IsSubscribe` `is_subscribe` SMALLINT(6) NULL DEFAULT '1' AFTER `email`,
	CHANGE COLUMN `CreatorDateTime` `creator_date` DATETIME NULL DEFAULT NULL AFTER `is_subscribe`,
	CHANGE COLUMN `EditorDateTime` `editor_date` DATETIME NULL DEFAULT NULL AFTER `creator_date`;

ALTER TABLE `grv_articlecategory`
	CHANGE COLUMN `ArticleCategoryID` `article_category_id` INT(4) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `CategoryName` `category_name` VARCHAR(50) NULL DEFAULT NULL AFTER `article_category_id`,
	CHANGE COLUMN `Description` `description` VARCHAR(100) NULL DEFAULT NULL AFTER `category_name`,
	CHANGE COLUMN `Slug` `slug` VARCHAR(50) NULL DEFAULT NULL AFTER `description`,
	CHANGE COLUMN `CreatorID` `creator_id` SMALLINT(4) NULL DEFAULT NULL AFTER `slug`,
	CHANGE COLUMN `CreatorIP` `creator_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `creator_id`,
	CHANGE COLUMN `CreatorDateTime` `creator_date` DATETIME NULL DEFAULT NULL AFTER `creator_ip`,
	CHANGE COLUMN `EditorID` `editor_id` SMALLINT(4) NULL DEFAULT NULL AFTER `creator_date`,
	CHANGE COLUMN `EditorIP` `editor_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `editor_id`,
	CHANGE COLUMN `EditorDateTime` `editor_date` DATETIME NULL DEFAULT NULL AFTER `editor_ip`;

ALTER TABLE `grv_tracker`
	CHANGE COLUMN `TrackerID` `tracker_id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Name` `name` VARCHAR(200) NULL DEFAULT NULL COMMENT 'child element ex: hot-threads-1, sidebar-2, ' AFTER `tracker_id`,
	CHANGE COLUMN `Url` `url` VARCHAR(500) NULL DEFAULT NULL COMMENT 'url target' AFTER `name`,
	CHANGE COLUMN `Source` `source` VARCHAR(100) NULL DEFAULT NULL COMMENT 'page source ex: home, article, category' AFTER `url`,
	CHANGE COLUMN `Segment` `segment` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Parent element ex: hot-thread-10' AFTER `source`,
	CHANGE COLUMN `CreatorDate` `creator_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `segment`,
	CHANGE COLUMN `CreatorIP` `creator_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `creator_date`;

ALTER TABLE `grv_article`
	CHANGE COLUMN `ArticleID` `article_id` MEDIUMINT(8) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `ArticleCategoryID` `article_category_id` VARCHAR(50) NULL DEFAULT NULL AFTER `article_id`,
	CHANGE COLUMN `Title` `title` VARCHAR(255) NULL DEFAULT NULL AFTER `article_category_id`,
	CHANGE COLUMN `Slug` `slug` VARCHAR(500) NULL DEFAULT NULL AFTER `Title`,
	CHANGE COLUMN `Content` `content` MEDIUMTEXT NULL AFTER `slug`,
	CHANGE COLUMN `ShortDescription` `short_description` VARCHAR(300) NULL DEFAULT NULL AFTER `content`,
	CHANGE COLUMN `Quote` `quote` VARCHAR(450) NULL DEFAULT NULL AFTER `short_description`,
	CHANGE COLUMN `Tag` `tag` VARCHAR(350) NULL DEFAULT NULL AFTER `quote`,
	CHANGE COLUMN `View` `view` INT(8) NULL DEFAULT NULL AFTER `tag`,
	CHANGE COLUMN `Image` `image` VARCHAR(200) NULL DEFAULT NULL AFTER `view`,
	CHANGE COLUMN `ImageDescription` `image_description` VARCHAR(150) NULL DEFAULT NULL AFTER `image`,
	CHANGE COLUMN `PublishDate` `publish_date` DATETIME NULL DEFAULT NULL AFTER `image_description`,
	CHANGE COLUMN `IsPublish` `is_publish` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `publish_date`,
	CHANGE COLUMN `IsHot` `is_hot` TINYINT(4) NULL DEFAULT '0' AFTER `is_publish`,
	CHANGE COLUMN `ShortUrl` `short_url` VARCHAR(50) NULL DEFAULT NULL AFTER `is_hot`,
	CHANGE COLUMN `CreatorID` `creator_id` SMALLINT(4) NULL DEFAULT NULL AFTER `short_url`,
	CHANGE COLUMN `CreatorIP` `creator_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `creator_id`,
	CHANGE COLUMN `CreatorDateTime` `creator_date` DATETIME NULL DEFAULT NULL AFTER `creator_ip`,
	CHANGE COLUMN `EditorID` `editor_id` SMALLINT(4) NULL DEFAULT NULL AFTER `creator_date`,
	CHANGE COLUMN `EditorIP` `editor_ip` VARCHAR(20) NULL DEFAULT NULL AFTER `editor_id`,
	CHANGE COLUMN `EditorDateTime` `editor_date` DATETIME NULL DEFAULT NULL AFTER `editor_ip`;

/* ------------------------------------------------------------------------------------------------------ */
-- 28 Okt 2015
ALTER TABLE grv_member ADD SecureNotes text NULL AFTER Image

/* ------------------------------------------------------------------------------------------------------ */
-- 4 sept
ALTER TABLE `grv_member`
	CHANGE COLUMN `AboutMe` `AboutMe` VARCHAR(500) NULL DEFAULT NULL AFTER `Website`;
	
/* ------------------------------------------------------------------------------------------------------ */
-- 3 sept
ALTER TABLE grv_member ADD Linkedin varchar(170) DEFAULT NULL AFTER Twitter;
ALTER TABLE grv_member ADD Website varchar(170) DEFAULT NULL AFTER Linkedin;

/* ------------------------------------------------------------------------------------------------------ */
-- 2 September 2015
ALTER TABLE grv_article ADD PublishDate datetime AFTER ImageDescription;
UPDATE grv_article SET PublishDate = CreatorDateTime;
/* ------------------------------------------------------------------------------------------------------ */