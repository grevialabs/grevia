// component that contains all the logic and other smaller components
// that form the Read Products view

// var apiUrl = 'http://localhost/grevia.com/';
var baseUrl = 'http://www.grevia.com/';
var apiUrl = 'http://www.grevia.com/api/';

window.ReadProductsComponent = React.createClass({
    getInitialState: function() {
        return {
            products: []
        };
    },
 
    // on mount, fetch all products and stored them as this component's state
    componentDidMount: function() {
 
        this.serverRequest = $.get(apiUrl + "api/product", function (products) {
            this.setState({
                products: products.data
            });
        }.bind(this));
    },
 
    // on unmount, kill product fetching in case the request is still pending
    componentWillUnmount: function() {
        this.serverRequest.abort();
    },
 
    // render component on the page
    render: function() {
        // list of products
        var filteredProducts = this.state.products;
        $('.page-header h1').text('Read Products');
 
        return (
            <div className='overflow-hidden'>
                <TopActionsComponent changeAppMode={this.props.changeAppMode} />
 
                <ProductsTable
                    products={filteredProducts}
                    changeAppMode={this.props.changeAppMode} />
            </div>
        );
    }
});