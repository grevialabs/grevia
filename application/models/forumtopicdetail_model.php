<?php

class Forumtopicdetail_model extends MY_Model 
{
	public function get($attr = NULL) 
	{
		$sql = 'SELECT * FROM grv_forumtopicdetail WHERE 1';
		if (isset($attr['ForumTopicDetailID'])) 
		{
			$sql.= ' AND ForumTopicDetailID = ' . $attr['ForumTopicDetailID'];
		}
		$result = $this->db->query($sql)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT * 
		FROM grv_forumtopic ft
		INNER JOIN grv_forumtopicdetail ftd USING(ForumTopicID)
		INNER JOIN grv_members m ON m.MemberID = ftd.CreatorID
		WHERE 1';

		if (isset($attr['ForumTopicID'])) 
		{
			$query.= ' AND ftd.ForumTopicID = ' . $attr['ForumTopicID'];
		}
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY IsCreator DESC, IsHelpful DESC, ftd.ForumTopicDetailID ASC, ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY IsCreator DESC, IsHelpful DESC, ftd.ForumTopicDetailID ASC';
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data) 
	{
		$sql = $this->db->insert('');
		//$save = $this->sql;
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update() 
	{
		$sql = $this->db->insert('');
		//$update = $this->sql;
		if ($update) return TRUE; else return FALSE;	
	}
	
	public function delete() 
	{
		$sql = $this->db->insert('');
		$delete = $this->sql;
		if ($delete) return TRUE; else return FALSE;	
	}
	
}