<?php

Class Reference_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = '
		SELECT *
		FROM grv_reference 
		WHERE 1';
		
		if (isset($attr['month']) && isset($attr['year'])) 
		{
			$query.= ' AND MONTH(input_date) = '.$attr['month'].' AND YEAR(input_date) = '.$attr['year'];
		}
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		
		if (isset($attr['last'])) 
		{
			$query.= ' ORDER BY creator_date DESC LIMIT 1';
		}
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT *
		FROM grv_reference 
		WHERE 1
		';
		if (isset($attr['reference_id'])) 
		{
			$query.= ' AND reference_id = ' . $attr['reference_id'];
		}
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = $list_update = '';
		$query = 'INSERT INTO grv_reference ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= $this->db->escape($val);
			// $list_update.= ' '.$key.' = VALUES('.$key.')';
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
				// $list_update.= ' ,';
			}
			$i++;
		}
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		// $query.= ' ON DUPLICATE KEY UPDATE '.$list_update;
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	// public function update($id, $data)
	// {
		// $query = 'UPDATE grv_reference SET';
		// $i = 1;
		// foreach($data as $key => $val)
		// {
			// $query.= ' '.$key .' = ' . replace_quote($val);
			// if ($i != count($data)) $query.= ' ,';
			// $i++;
		// }
		// $query.= ', editor_date = '.replace_quote(getDatetime());
		// $query.= ' WHERE reference_id = '. replace_quote($id,'num');
		// $update = $this->db->query($query);
		// if ($update) return TRUE; else return FALSE;
	// }
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_reference WHERE reference_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}