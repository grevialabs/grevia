<?php
class Order_Model extends MY_Model
{
	public function generate_random()
	{
		$random = rand(1000000,9999999);
		return $random;
	}
	
	function redirect_to_cart_if_cart_empty()
	{
		$cookie_order_id = NULL;

		if (isset($_COOKIE['order_id']) && is_numeric($_COOKIE['order_id'])) $cookie_order_id = $_COOKIE['order_id'];

		if ($cookie_order_id)
		{
			$is_item_exist = $this->get_detail(array(
				'order_id' => $cookie_order_id
			));
			$is_item_exist = $is_item_exist['data'];
			if (empty($is_item_exist)) redirect(base_url().'cart');
		}
		else
		{
			// NO COOKIES ORDER ID
			redirect(base_url().'cart');
		}
	}

	public function get_new_order_code()
	{
		// GENERAETE NEW ORDER CODE NOT EXIST IN DATABASE
		$try = false;
		do
		{
			$random = $this->generate_random();
			$query = "SELECT * FROM grv_order WHERE order_id = ".$random;
			$result = $this->db->query($query)->row_array();

			// IF RECORD NOT EXIST
			if (empty($result))
			{
				$try = TRUE;
				break;
			}
		} while($try == false);

		if ($try) return $random;
	}
	
	public function get($attr = NULL)
	{
		$query = "
		SELECT *
		FROM grv_order
		WHERE 1
		";
		
		if (isset($attr['book_code']) && $attr['book_code'] != NULL) 
		{
			$query.= " AND book_code = " . $this->db->escape($attr['book_code']);
		}
		
		if (isset($attr['order_id']) && $attr['order_id'] != NULL) 
		{
			$query.= " AND order_id = " . $attr['order_id'];
		}
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_detail($attr = NULL)
	{
		$query = "
		SELECT o.*, p.name as product_name, p.teaser, p.description, p.price as price, p.promo_price, 
		IF(p.promo_price > 0, p.promo_price, o.price) as final_price
		FROM grv_order o
		LEFT JOIN grv_product p USING(product_id)
		WHERE 1
		";
		
		// if (isset($attr['book_code']) && $attr['book_code'] != NULL) 
		// {
			// $query.= " AND book_code = " . $this->db->escape($attr['book_code']);
		// }
		
		if (isset($attr['order_id']) && $attr['order_id'] != NULL) 
		{
			$query.= " AND o.order_id = " . $attr['order_id'];
		}
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = '
		SELECT * 
		FROM grv_order v
		WHERE 1';
		
		if (isset($attr['order_id']) && $attr['order_id'] != NULL)
		{
			$query.= ' AND order_id = ' . $attr['order_id'];
		}

		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY order_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		
		// Return Last Insert ID if needed
		$is_return = FALSE;
		if (isset($data['return'])) 
		{
			unset($data['return']);
			$is_return = TRUE;
		}
		
		$query = 'INSERT IGNORE INTO grv_order ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$query.= ' ON DUPLICATE KEY UPDATE price = VALUES(price), product_id = VALUES(product_id), domain = VALUES(domain), notes = VALUES(notes)';
		$save = $this->db->query($query);
		
		// Show last insert id
		if ($is_return) 
		{
			$callback = NULL;
			$callback['last_id'] = $this->db->insert_id();
			$callback['query_status'] = 'success';
			return $callback;
		}
		
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE grv_order SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ' WHERE order_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_order WHERE order_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}