<?php
class Studio_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT *
		FROM bok_studio
		WHERE 1
		";
		
		if (isset($attr['studio_id']) && $attr['studio_id'] != NULL) 
		{
			$query.= " AND studio_id = " . $attr['studio_id'];
		}
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = '
		SELECT * 
		FROM bok_studio
		WHERE 1';
		
		if (isset($attr['videolist_id']) && $attr['videolist_id'] != NULL)
		{
			$query.= ' AND videolist_id = ' . $attr['videolist_id'];
		}

		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY studio_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO bok_studio ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE bok_studio SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ' WHERE studio_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM bok_studio WHERE studio_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}