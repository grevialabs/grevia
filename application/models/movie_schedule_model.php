<?php
class Movie_schedule_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT *
		FROM bok_movie_schedule
		WHERE 1
		";
		
		if (isset($attr['movie_schedule_id']) && $attr['movie_schedule_id'] != NULL) 
		{
			$query.= " AND movie_schedule_id = " . $attr['movie_schedule_id'];
		}
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_detail($attr = NULL)
	{
		$query = '
		SELECT ms.movie_schedule_id, l.location_id, l.name as location_name, st.name as studio_type_name, l.address, s.code as studio_code, m.title as movie_title,
		st.price_weekday, st.price_jumat, st.price_holiday,
		ms.start_date,
		GROUP_CONCAT(ms.start_time ORDER BY ms.movie_schedule_id ASC) as list_start_time,
		GROUP_CONCAT(ms.movie_schedule_id ORDER BY ms.movie_schedule_id ASC) as list_movie_schedule_id
		FROM bok_movie_schedule ms
		LEFT JOIN bok_movie m USING(movie_id)
		LEFT JOIN bok_studio s USING(studio_id)
		LEFT JOIN bok_studio_type st USING(studio_id)
		LEFT JOIN bok_location l USING(location_id)
		WHERE 1';
		
		// $query = "
		// SELECT ms.movie_schedule_id, l.location_id, l.name as location_name, st.name as studio_type_name, l.address, s.code as studio_code, m.title as movie_title, ms.start_date, ms.start_time
		// FROM bok_movie_schedule ms
		// LEFT JOIN bok_movie m USING(movie_id)
		// LEFT JOIN bok_studio s USING(studio_id)
		// LEFT JOIN bok_studio_type st USING(studio_id)
		// LEFT JOIN bok_location l USING(location_id)
		// WHERE 1";
		
		if (isset($attr['movie_schedule_id']) && $attr['movie_schedule_id'] != NULL) 
		{
			$query.= " AND movie_schedule_id = " . $attr['movie_schedule_id'];
		}
		$query.= " ORDER BY ms.start_date ASC, ms.start_time ASC";
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list_detail($attr = NULL)
	{
		$query = '
		SELECT ms.movie_schedule_id, l.location_id, l.name as location_name, st.name as studio_type_name, l.address, s.code, m.title as movie_title, ms.start_date, ms.start_time
		FROM bok_movie_schedule ms
		LEFT JOIN bok_movie m USING(movie_id)
		LEFT JOIN bok_studio s USING(studio_id)
		LEFT JOIN bok_studio_type st USING(studio_id)
		LEFT JOIN bok_location l USING(location_id)
		WHERE 1';
		
		if (isset($attr['location_id']) && $attr['location_id'] != NULL)
		{
			$query.= ' AND l.location_id = ' . $attr['location_id'];
		}
		
		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY ms.start_date ASC, ms.start_time ASC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	// 
	public function get_list_group($attr = NULL)
	{
		$query = '
		SELECT ms.movie_schedule_id, l.location_id, l.name as location_name, st.name as studio_type_name, l.address, s.code as studio_code, m.title as movie_title,
		st.price_weekday, st.price_jumat, st.price_holiday,
		ms.start_date,
		GROUP_CONCAT(ms.start_time ORDER BY ms.movie_schedule_id ASC) as list_start_time,
		GROUP_CONCAT(ms.movie_schedule_id ORDER BY ms.movie_schedule_id ASC) as list_movie_schedule_id
		FROM bok_movie_schedule ms
		LEFT JOIN bok_movie m USING(movie_id)
		LEFT JOIN bok_studio s USING(studio_id)
		LEFT JOIN bok_studio_type st USING(studio_id)
		LEFT JOIN bok_location l USING(location_id)
		WHERE 1';
		
		if (isset($attr['location_id']) && $attr['location_id'] != NULL)
		{
			$query.= ' AND l.location_id = ' . $attr['location_id'];
		}
		
		if (isset($attr['group_by']) && $attr['group_by'] != NULL)
		{
			$query.= ' GROUP BY '.$attr['group_by'];
		}

		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY ms.start_date ASC, ms.start_time ASC';
		}
		// debug($query);
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO bok_movie_schedule ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE bok_movie_schedule SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ' WHERE movie_schedule_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM bok_movie_schedule WHERE movie_schedule_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}