<?php
class Abtest_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT a.*
		FROM grv_abtest a
		WHERE 1
		";
		if (isset($attr['abtest_id']) && $attr['abtest_id'] != NULL) 
		{
			$query.= " AND a.abtest_id = " . $attr['abtest_id'];
		}
		
		if (isset($attr['last']) && $attr['last'] != NULL) 
		{
			$query.= " ORDER BY a.abtest_id DESC LIMIT 1";
		}
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = "
		SELECT a.* 
		FROM grv_abtest a 
		WHERE 1
		";
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL) 
		{
			$query.= " AND a.creator_id = " . replace_quote($attr['creator_id']);
		}
		
		if (isset($attr['is_hot']) && $attr['is_hot'] != NULL) 
		{
			$query.= " AND is_hot = " . replace_quote($attr['is_hot']);
		}
		
		if (isset($attr['title']) && $attr['title'] != NULL) 
		{
			$query.= " AND title = " . replace_quote($attr['title']);
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND content LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR title LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR short_description LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\'';
		}
		
		if (isset($attr['keyword'])) 
		{
			//$sql.= ' AND m.FullName LIKE "%' . $attr['author'] . '%"';
			$query.= ' AND tag LIKE \'%'.$attr['keyword'].'%\' OR title LIKE \'%'.$attr['keyword'].'%\'';
		}
		
		if (isset($attr['author'])) 
		{
			$query.= ' AND a.tag REGEXP ' . $attr['author'] . '';
		}
		
		if (isset($attr['status'])) 
		{
			if ($attr['status'] == 'all')
				$query.= '';
			else
				$query.= ' AND a.status = ' . $attr['status'];
		}
		else
		{
			// $query.= ' AND a.status = 1';
		}
		
		if (isset($attr['not_abtest_id']))
		{
			$query.= ' AND a.abtest_id NOT IN (' . $attr['not_abtest_id'].')';
		}

		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY abtest_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function get_list_detail($attr = NULL)
	{
		$query = "
		SELECT ab.abtest_id, ab.title, ab.modulename, 
		( 
			SELECT CAST((ab.quota / COUNT(tmp.abtest_id)) as unsigned)
			FROM grv_abtest_detail tmp 
			WHERE tmp.abtest_id = ab.abtest_id 
		) as quota_each_detail,
		(
			SELECT COUNT(abv.abtest_view_id) as totview
			FROM grv_abtest_view abv
			WHERE abv.abtest_detail_id = abd.abtest_detail_id
		) as quota_each_detail_used, 
		CONCAT(ab.modulename,'_',abd.filename) as fullmodulename,
		ab.description, 
		abd.*,
		( 
			SELECT COUNT(abv.abtest_detail_id) 
			FROM grv_abtest_view abv
			WHERE abv.abtest_detail_id = abd.abtest_detail_id
		) as total_pageview,
		( 
			SELECT COUNT(abv.abtest_detail_id) 
			FROM grv_abtest_view abv
			WHERE abv.abtest_detail_id = abd.abtest_detail_id AND abv.trigger_name IS NOT NULL
		) as total_pageview_click,
		FORMAT((
			(
				( 
					SELECT COUNT(abv.abtest_detail_id) 
					FROM grv_abtest_view abv
					WHERE abv.abtest_detail_id = abd.abtest_detail_id AND abv.trigger_name IS NOT NULL
				) 
				/ ab.quota
				-- (
				-- 	SELECT COUNT(abv.abtest_detail_id) 
				-- 	FROM grv_abtest_view abv
				-- 	WHERE abv.abtest_detail_id = abd.abtest_detail_id 
				-- )
			) * 
			100
		),1) as ratio_click_percent
		FROM grv_abtest ab 
		LEFT JOIN grv_abtest_detail abd ON ab.abtest_id = abd.abtest_id
		WHERE 1
		";
		
		if (isset($attr['abtest_id']) && $attr['abtest_id'] != NULL) 
		{
			$query.= " AND ab.abtest_id = " . replace_quote($attr['abtest_id']);
		}
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL) 
		{
			$query.= " AND a.creator_id = " . replace_quote($attr['creator_id']);
		}
		
		if (isset($attr['title']) && $attr['title'] != NULL) 
		{
			$query.= " AND title = " . replace_quote($attr['title']);
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND content LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR title LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR short_description LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\'';
		}
		
		if (isset($attr['keyword'])) 
		{
			//$sql.= ' AND m.FullName LIKE "%' . $attr['author'] . '%"';
			$query.= ' AND tag LIKE \'%'.$attr['keyword'].'%\' OR title LIKE \'%'.$attr['keyword'].'%\'';
		}
		
		if (isset($attr['author'])) 
		{
			$query.= ' AND a.tag REGEXP ' . $attr['author'] . '';
		}
		
		if (isset($attr['status'])) 
		{
			if ($attr['status'] == 'all')
				$query.= '';
			else
				$query.= ' AND ab.status = ' . $attr['status'];
		}
		else
		{
			// $query.= ' AND a.status = 1';
		}
		
		if (isset($attr['not_abtest_id']))
		{
			$query.= ' AND ab.abtest_id NOT IN (' . $attr['not_abtest_id'].')';
		}
		
		if (isset($attr['quota_not_full']))
		{
			$query.= ' HAVING quota_each_detail_used <= quota_each_detail';
		}

		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY ab.abtest_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO grv_abtest ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ',creator_id, creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(member_cookies('member_id'));
		$list_value.= ','.replace_quote(get_ip());
		$list_value.= ','.replace_quote(get_datetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function save_view($data)
	{
		$list_field = $list_value = '';
		
		// Return Last Insert ID if needed
		$is_return = FALSE;
		if (isset($data['return'])) 
		{
			unset($data['return']);
			$is_return = TRUE;
		}
		
		$query = 'INSERT INTO grv_abtest_view ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		// $list_field.= ',creator_id, creator_ip, creator_date';
		
		// $list_value.= ','.replace_quote(member_cookies('member_id'));
		// $list_value.= ','.replace_quote(get_ip());
		// $list_value.= ','.replace_quote(get_datetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) 
		{
			if ($is_return)
			{
				$return['last_id'] = $this->db->insert_id();
			}
			$return['query_status'] = 'success';
		} 
		else 
		{
			$return['query_status'] = 'failed';
		}
		return $return;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE grv_abtest SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		// $query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		// $query.= ', editor_ip = '.replace_quote(get_ip());
		// $query.= ', editor_date = '.replace_quote(get_datetime());
		$query.= ' WHERE abtest_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function update_view($id, $data)
	{
		$query = 'UPDATE grv_abtest_view SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		// $query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		// $query.= ', editor_ip = '.replace_quote(get_ip());
		// $query.= ', editor_date = '.replace_quote(get_datetime());
		$query.= ' WHERE abtest_view_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_abtest WHERE abtest_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}