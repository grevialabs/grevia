<?php

Class Daftar_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = '
		SELECT *
		FROM grv_daftar 
		WHERE 1';
		
		if (!empty($attr['daftar_id'])) 
		{
			$query.= ' AND daftar_id = ' . $attr['daftar_id'];
		}
		
		if (!empty($attr['no'])) 
		{
			$query.= ' AND no = ' . $attr['no'];
		}
		
		if (isset($attr['month']) && isset($attr['year'])) 
		{
			$query.= ' AND MONTH(input_date) = '.$attr['month'].' AND YEAR(input_date) = '.$attr['year'];
		}
		
		if (isset($attr['creator_id'])) 
		{
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		
		if (isset($attr['last'])) 
		{
			$query.= ' ORDER BY creator_date DESC LIMIT 1';
		}
		
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT d.*, COUNT(df.daftar_file_id) as total_file
		FROM grv_daftar d
		LEFT JOIN grv_daftar_file df USING(daftar_id)
		WHERE 1
		';
		
		if (isset($attr['daftar_id']) && $attr['daftar_id'] != "") 
		{
			$query.= ' AND daftar_id = ' . $attr['daftar_id'];
		}
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != "")
		{
			$query.= ' AND creator_id = ' . $attr['creator_id'];
		}
		
		if (isset($attr['creator_date']) && $attr['creator_date'] != "") 
		{
			$creator_date = date("Y-m-d",strtotime($attr['creator_date']));
			$query.= ' AND creator_date BETWEEN ' . $this->db->escape($creator_date.' 00:00:00') . ' AND '.$this->db->escape($creator_date.' 23:59:59');
		}
		
		$query.= ' GROUP BY d.daftar_id';
		
		if (isset($attr['order']) && $attr['order'] != "") 
		{
			$query.= ' ORDER BY '.$attr['order'];
		}
		else 
		{
			$query.= ' ORDER BY creator_date DESC';
		}
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		// die;
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = $list_update = '';
		$query = 'INSERT INTO grv_daftar ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= $this->db->escape($val);
			// $list_update.= ' '.$key.' = VALUES('.$key.')';
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
				// $list_update.= ' ,';
			}
			$i++;
		}
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		// $query.= ' ON DUPLICATE KEY UPDATE '.$list_update;
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	// public function update($id, $data)
	// {
		// $query = 'UPDATE grv_daftar SET';
		// $i = 1;
		// foreach($data as $key => $val)
		// {
			// $query.= ' '.$key .' = ' . replace_quote($val);
			// if ($i != count($data)) $query.= ' ,';
			// $i++;
		// }
		// $query.= ', editor_date = '.replace_quote(getDatetime());
		// $query.= ' WHERE daftar_id = '. replace_quote($id,'num');
		// $update = $this->db->query($query);
		// if ($update) return TRUE; else return FALSE;
	// }
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_daftar WHERE daftar_id = ' . $id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;	
	}
	
}