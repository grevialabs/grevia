<?php

class Forumtopic_model extends MY_Model 
{
	public function get($attr = NULL) 
	{
		$sql = 'SELECT * FROM grv_forumtopic WHERE 1';
		if (isset($attr['ForumTopicID'])) 
		{
			$sql.= ' AND ForumTopicID = ' . $attr['ForumTopicID'];
		}
		$result = $this->db->query($sql)->row_array();
		return $result;
	}
	
	public function get_list($attr = NULL) 
	{
		$query = '
		SELECT *,(SELECT COUNT(*)-1 FROM grv_forumtopicdetail ftd WHERE ft.ForumTopicID = ftd.ForumTopicID) as TotalReply ,
		(SELECT COUNT(IsHelpful)as IsHelpful FROM grv_forumtopicdetail ftd WHERE ft.ForumTopicID = ftd.ForumTopicID AND IsHelpful = 1 ) as IsHelpfulExist 
		FROM grv_forumtopic ft
		WHERE 1';
		if (isset($attr['ForumTopicID'])) 
		{
			$query.= ' AND ForumTopicID = ' . $attr['ForumTopicID'];
		}
		
		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY ForumTopicID DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		} 
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO grv_forumtopic ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ',CreatorID, CreatorIP, CreatorDateTime';
		
		$list_value.= ','.replace_quote(member_cookies('MemberID'));
		$list_value.= ','.replace_quote(getIP());
		$list_value.= ','.replace_quote(getDatetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE grv_forumtopic SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', EditorID = '.replace_quote(member_cookies('MemberID'));
		$query.= ', EditorIP = '.replace_quote(getIP());
		$query.= ', EditorDateTime = '.replace_quote(getDatetime());
		$query.= ' WHERE ForumTopicID = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_forumtopic WHERE ArticleID = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
}