<?php
class Booking_Model extends MY_Model
{
	public function gen_new_book_code($attr = NULL)
	{
		// Generate 5 random number
		
		$numb = array(0,1,2,3,4,5,6,7,8,9);
		$max_num = 5;
		$newcode = NULL;
		for ($i = 1; $i <= 5; $i++) {
			$newcode.= $numb[MT_RAND(0,9)];
		}
		
		// $query = "
		// SELECT *
		// FROM gen
		// WHERE 1
		// ";
		
		// $return = $this->db->query($query)->row_array();
		$param['book_code'] = $newcode;
		$return = $this->get($param);
				
		// recursive when newcode not valid
		if (isset($return['booking_id'])) {
			$this->gen_new_book_code();
		}
		return $newcode;
	}
	
	public function get($attr = NULL)
	{
		$query = "
		SELECT *
		FROM bok_booking
		WHERE 1
		";
		
		if (isset($attr['book_code']) && $attr['book_code'] != NULL) 
		{
			$query.= " AND book_code = " . $this->db->escape($attr['book_code']);
		}
		
		if (isset($attr['booking_id']) && $attr['booking_id'] != NULL) 
		{
			$query.= " AND booking_id = " . $attr['booking_id'];
		}
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = '
		SELECT * 
		FROM bok_booking v
		WHERE 1';
		
		if (isset($attr['videolist_id']) && $attr['videolist_id'] != NULL)
		{
			$query.= ' AND videolist_id = ' . $attr['videolist_id'];
		}

		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY booking_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		
		// Return Last Insert ID if needed
		$is_return = FALSE;
		if (isset($data['return'])) 
		{
			unset($data['return']);
			$is_return = TRUE;
		}
		
		$query = 'INSERT INTO bok_booking ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		
		// Show last insert id
		if ($is_return) 
		{
			$callback = NULL;
			$callback['last_id'] = $this->db->insert_id();
			$callback['query_status'] = 'success';
			return $callback;
		}
		
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE bok_booking SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ' WHERE booking_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM bok_booking WHERE booking_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}