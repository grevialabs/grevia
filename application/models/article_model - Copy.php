<?php
class Article_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT a.*, a.image as image, m.full_name, m.facebook, m.twitter, m.linkedin, m.website, m.about_me, m.image as author_image
		FROM grv_article a
		LEFT JOIN grv_member m ON m.member_id = a.creator_id
		WHERE 1
		";
		if (isset($attr['article_id']) && $attr['article_id'] != NULL) 
		{
			$query.= " AND a.article_id = " . $attr['article_id'];
		}
		
		if (isset($attr['last']) && $attr['last'] != NULL) 
		{
			$query.= " ORDER BY a.article_id DESC LIMIT 1";
		}
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = "
		SELECT m.email, ac.category_name, ac.slug as category_slug, a.* 
		FROM grv_article a 
		LEFT JOIN grv_articlecategory ac ON ac.article_category_id = a.article_category_id
		LEFT JOIN grv_member m ON m.member_id = a.creator_id
		WHERE 1
		";
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL) 
		{
			$query.= " AND a.creator_id = " . replace_quote($attr['creator_id']);
		}
		
		if (isset($attr['is_hot']) && $attr['is_hot'] != NULL) 
		{
			$query.= " AND is_hot = " . replace_quote($attr['is_hot']);
		}
		
		if (isset($attr['title']) && $attr['title'] != NULL) 
		{
			$query.= " AND title = " . replace_quote($attr['title']);
		}
		
		if (isset($attr['keyword']) && $attr['keyword'] != NULL)
		{
			$query.= ' AND content LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR title LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\' OR short_description LIKE \'%' . $this->db->escape_like_str($attr['keyword']) . '%\'';
		}
		
		if (isset($attr['article_category_id'])) 
		{
			// FIND article_category_id in , value
			$query.= ' AND FIND_IN_SET (\''.$attr['article_category_id'].'\',a.article_category_id)';
		}
		
		if (isset($attr['keyword'])) 
		{
			//$sql.= ' AND m.FullName LIKE "%' . $attr['author'] . '%"';
			$query.= ' AND tag LIKE \'%'.$attr['keyword'].'%\' OR title LIKE \'%'.$attr['keyword'].'%\'';
		}
		
		if (isset($attr['author'])) 
		{
			$query.= ' AND a.tag REGEXP ' . $attr['author'] . '';
		}
		
		if (isset($attr['is_publish'])) 
		{
			if ($attr['is_publish'] == 'all')
				$query.= '';
			else
				$query.= ' AND a.is_publish = ' . $attr['is_publish'];
		}
		else
		{
			$query.= ' AND a.is_publish = 1';
		}
		
		if (isset($attr['not_article_id']))
		{
			$query.= ' AND a.article_id NOT IN (' . $attr['not_article_id'].')';
		}

		if(isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY article_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset; 
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO grv_article ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$list_field.= ',creator_id, creator_ip, creator_date';
		
		$list_value.= ','.replace_quote(member_cookies('member_id'));
		$list_value.= ','.replace_quote(get_ip());
		$list_value.= ','.replace_quote(get_datetime());
		
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE grv_article SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ', editor_id = '.replace_quote(member_cookies('member_id'));
		$query.= ', editor_ip = '.replace_quote(get_ip());
		$query.= ', editor_date = '.replace_quote(get_datetime());
		$query.= ' WHERE article_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function update_view($id, $data)
	{
		$query = 'UPDATE grv_article SET';
		$query.= ' view = '.replace_quote($data['view'],'num');
		$query.= ' WHERE article_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_article WHERE article_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}