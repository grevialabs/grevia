<?php

Class Search_model extends MY_Model 
{
	public function __construct() {
        parent::__construct();
    }
	
	public function get($attr = NULL) 
	{
		$query = 'SELECT * FROM grv_search WHERE 1';
		if (isset($attr['keyword'])) 
		{
			$query.= ' AND keyword = ' . $attr['keyword'];
		}
		$result = $this->db->query($query)->row_array();
		return $result;
	}
	
	public function get_list_report($attr = NULL) 
	{
		$query = '
		SELECT *, COUNT(keyword) as hit
		FROM grv_search
		GROUP BY keyword';
		
		if (isset($attr['hit']))
		{
			$query .=' HAVING hit > '.$attr['hit'];
		}
		
		$query .=' ORDER BY hit DESC';
		
		
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}

	public function get_list($attr = NULL) 
	{
		$query = 'SELECT * FROM grv_search WHERE 1';
		if (isset($attr['search_id'])) 
		{
			$query.= ' WHERE search_id = ' . $attr['search_id'];
		}
		$result['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE) 
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		$result['data'] = $this->db->query($query)->result_array();
		return $result;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO grv_search ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE grv_search SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ' WHERE search_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id) 
	{
		$query = 'DELETE FROM grv_search WHERE search_id = '.$id;
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
}