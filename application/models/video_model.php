<?php
class Video_Model extends MY_Model
{
	public function get($attr = NULL)
	{
		$query = "
		SELECT v.*, vl.name as videolist_name
		FROM grv_video v
		LEFT JOIN grv_videolist vl ON v.videolist_id = vl.videolist_id
		WHERE 1
		";
		if (isset($attr['video_id']) && $attr['video_id'] != NULL) 
		{
			$query.= " AND video_id = " . $attr['video_id'];
		}
		
		if (isset($attr['slug']) && $attr['slug'] != NULL) 
		{
			$query.= " AND slug = " . replace_quote($attr['slug']);
		}
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL) 
		{
			$query.= " AND v.creator_id = " . $attr['creator_id'];
		}
		
		if (isset($attr['status']) && $attr['status'] != NULL)
		{
			$query.= " AND v.status = " . $attr['status'];
		}
		
		if (isset($attr['url']) && $attr['url'] != NULL) 
		{
			$query.= " AND v.url = " . replace_quote($attr['url']);
		}
		
		$return = $this->db->query($query)->row_array();
		return $return;
	}
	
	public function get_list_prev_next($attr = NULL)
	{
		// require:
		// video_id, creator_id
		//
		// return:
		// array: prev, next
		if ( ! isset($attr['video_id']) OR ! isset($attr['creator_id'])) return 'creator_id and video id required ';
		$query = '
		SELECT * 
		FROM grv_video v
		WHERE v.video_id != '.$attr['video_id'].' AND v.video_id < '.$attr['video_id'].'
		AND creator_id = '.$attr['creator_id'].' AND v.status = 1
		ORDER BY video_id DESC
		LIMIT 1;';
		// debug($query);
		$data['prev'] = $this->db->query($query)->row_array();
		
		$query = '
		SELECT * 
		FROM grv_video v
		WHERE v.video_id != '.$attr['video_id'].' AND v.video_id > '.$attr['video_id'].'
		AND creator_id = '.$attr['creator_id'].' AND v.status = 1
		ORDER BY video_id ASC
		LIMIT 1;';
		// debug($query);
		$data['next'] = $this->db->query($query)->row_array();
		
		
		$return = $data;
		return $return;
	}
	
	public function get_list($attr = NULL)
	{
		$query = '
		SELECT * 
		FROM grv_video v
		WHERE 1';
		
		if (isset($attr['videolist_id']) && $attr['videolist_id'] != NULL)
		{
			$query.= ' AND videolist_id = ' . $attr['videolist_id'];
		}
		
		if (isset($attr['uncategorized']) && $attr['uncategorized'] != NULL)
		{
			$query.= ' AND videolist_id IS NULL OR videolist_id = 0';
		}
		
		if (isset($attr['category_name']) && $attr['category_name'] != NULL)
		{
			$query.= ' AND category_name LIKE "%' . $attr['category_name'] . '%"';
		}
		
		if (isset($attr['creator_id']) && $attr['creator_id'] != NULL) 
		{
			$query.= " AND v.creator_id = " . $attr['creator_id'];
		}
		
		if (isset($attr['status']) && $attr['status'] != NULL) 
		{
			$query.= " AND v.status = " . $attr['status'];
		}
		
		if (isset($attr['url']) && $attr['url'] != NULL) 
		{
			$query.= " AND v.url = " . replace_quote($attr['url']);
		}

		if (isset($attr['order']) && $attr['order'] != NULL)
		{
			$query.= ' ORDER BY ' . $attr['order'];
		}
		else
		{
			$query.= ' ORDER BY video_id DESC';
		}
		
		$return['total_rows'] = $this->db->query($query)->num_rows();
		if (isset($attr['paging']) && $attr['paging'] == TRUE)
		{
			$limit = 0;
			$offset = OFFSET;
			
			if (isset($_GET['per_page']) && ($_GET['per_page']==10 || $_GET['per_page']==30 || $_GET['per_page']==50))
			{
				$offset = $_GET['per_page'];
			}
			
			if (isset($attr['limit'])) $limit = $attr['limit'];
			if (isset($attr['offset'])) $offset = $attr['offset'];
			if (isset($_GET['page']) && $_GET['page'] > 0) $limit = ($_GET['page']-1) * $offset;
			
			$query.= ' LIMIT '.$limit.','.$offset;
		}
		// debug($query);
		$return['data'] = $this->db->query($query)->result_array();
		return $return;
	}
	
	public function save($data)
	{
		$list_field = $list_value = '';
		$query = 'INSERT INTO grv_video ';
		$i = 1;
		foreach($data as $key => $val)
		{
			$list_field.= $key;
			$list_value.= replace_quote($val);
			if ($i != count($data)) {
				$list_field.= ' ,';
				$list_value.= ' ,';
			}
			$i++;
		}
		$query.= '('.$list_field.') VALUES('.$list_value.')';
		$save = $this->db->query($query);
		if ($save) return TRUE; else return FALSE;
	}
	
	public function update($id, $data)
	{
		$query = 'UPDATE grv_video SET';
		$i = 1;
		foreach($data as $key => $val)
		{
			$query.= ' '.$key .' = ' . replace_quote($val);
			if ($i != count($data)) $query.= ' ,';
			$i++;
		}
		$query.= ' WHERE video_id = '. replace_quote($id,'num');
		$update = $this->db->query($query);
		if ($update) return TRUE; else return FALSE;
	}
	
	public function delete($id)
	{
		$query = 'DELETE FROM grv_video WHERE video_id = ' . replace_quote($id,'num');
		$delete = $this->db->query($query);
		if ($delete) return TRUE; else return FALSE;
	}
	
	public function curl_get_list($attr = NULL) 
	{
		$return = $this->get_list($attr);
		$return = json_encode($return);
		return $return;
	}
	
}