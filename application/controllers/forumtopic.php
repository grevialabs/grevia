<?php 

class Forumtopic extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('forumtopic_model');
		$this->load->model('article_model');
		//$this->load->model('forumtopic_model');
		$this->load->model('forumtopicdetail_model');
	}
	public function index()
	{
		$params['paging'] = TRUE;
		
		//if (isset($_GET['page'])) $params['page'] = $_GET['page'];
		
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Forumtopic');
		$data['data'] = $this->forumtopic_model->get_list();
		$data['PAGE_HEADER'] = 'Forum';
		$data['CONTENT'] = $this->load->view('forumtopic/v_forumtopic',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function indexa()
	{
		// $data['data'] = $this->member_model->get_list();
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Member');
		// $data['PAGE_HEADER'] = 'Admin Member';
        // $data['CONTENT'] = $this->load->view('member/v_adm_member',$data,TRUE);
        // $this->load->view('index', $data);
	}

}