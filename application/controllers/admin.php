<?php

class Admin extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_model');
		$this->load->model('articlecategory_model');
		$this->load->model('forumtopic_model');
		$this->load->model('forumtopicdetail_model');
		$this->load->model('member_model');
		$this->load->model('subscribe_model');
		$this->load->model('search_model');
		$this->load->model('tracker_model');
		
		if (!is_member() || member_cookies('is_admin') != 1) redirect(base_url().'login');
	}
	
	private function _breadcrumb()
	{
		$MODULE = $this->uri->segment(2);
		$return = $this->common_model->breadcrumb(
			array(
			'admin' => 'admin', 
			'admin/'.$MODULE => $MODULE
			)
		);
		return $return;
	}
	
	public function index()
	{
		redirect(base_url().'admin/dashboard');
	}
	
	public function dashboard()
	{
		$data = NULL;
		
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_dashboard',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function articlecategory()
	{	
		$data = NULL;
		
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_articlecategory',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function article()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_article',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function member()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_member',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function forum()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_forum',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function subscribe()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Admin '.$MODULE;
		$data['PAGE_HEADER'] = 'Admin '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Admin '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/adm_subscribe',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function report_search()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Report '.$MODULE;
		$data['PAGE_HEADER'] = 'Report '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Report '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/rpt_search',$data,TRUE);
        $this->load->view('index_admin', $data);
	}
	
	public function report_tracker()
	{
		$data = NULL;
		$MODULE = $data['MODULE'] = $this->uri->segment(2);
		$data['PAGE'] = 'Report '.$MODULE;
		$data['PAGE_HEADER'] = 'Report '.$MODULE.'<hr>';
		$data['PAGE_TITLE'] = 'Report '.$MODULE;
		$data['BREADCRUMB'] = $this->_breadcrumb();
		
		$data['SIDEMENUBAR'] = $this->load->view('admin/adm_sidemenubar',NULL,TRUE);
		$data['CONTENT'] = $this->load->view('admin/rpt_tracker',$data,TRUE);
        $this->load->view('index_admin', $data);
	}

}