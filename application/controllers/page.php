<?php

class Page extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data['PAGE_HEADER'] = 'Page';
		$data['CONTENT'] = $this->load->view('page/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function track()
	{
		$data['PAGE_HEADER'] = 'Track';
		$data['CONTENT'] = $this->load->view('page/track',$data,TRUE);
		$this->load->view('index', $data);
	}
		
	public function anniv()
	{
		$data['PAGE_HEADER'] = 'Anniv';
		$data['CONTENT'] = $this->load->view('page/anniv',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function prewed()
	{
		$data['PAGE_HEADER'] = 'Prewed';
		$data['CONTENT'] = $this->load->view('page/prewed',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function instagram()
	{
		$this->load->view('page/instagram');
	}

}