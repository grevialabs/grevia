<?php

class Tools extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data['PAGE_HEADER'] = 'Page';
		$data['CONTENT'] = $this->load->view('tools/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function json_array()
	{
		$data['PAGE_HEADER'] = 'Json';
		$data['CONTENT'] = $this->load->view('tools/json_array',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function listmall()
	{
		$data['PAGE_HEADER'] = 'Page';
		$data['CONTENT'] = $this->load->view('tools/listmall',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	public function poster()
	{
		$data['PAGE_HEADER'] = 'Poster Page';
		$data['CONTENT'] = $this->load->view('tools/poster',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	// public function testing()
	// {
		// $this->load->view('m/menu');
	// }
	

}