<?php 

class Api extends MY_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('member_model');
		$this->load->model('article_model');
		$this->load->model('forumtopic_model');
		$this->load->model('member_model');
		$this->load->model('forumtopicdetail_model');
		$this->load->model('journey_model');
		
		// header("Content-Type: application/json; charset=UTF-8");
		header("Access-Control-Allow-Origin: *");
		
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header("Access-Control-Allow-Headers: X-Requested-With");
		
		header("Access-Control-Allow-Credentials: true");
		// header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, Secretkey');
		
		$get = $post = NULL;
		if ($_GET) $get = $_GET;
		if ($_POST) $post = $_POST;
		
		$header = getallheaders();
		// debug($header,1);
		
		// Auth api key
		$is_auth_valid = false;
		
		// valid only when have url get => secretkey=grevia or contain header with key 
		if (isset($get['secretkey']) && $get['secretkey'] == 'grevia') {
			$is_auth_valid = true;
		}
		
		if (isset($post['secretkey']) && $post['secretkey'] == 'grevia') {
			$is_auth_valid = true;
		}
		
		if (isset($header['Secretkey']) && $header['Secretkey'] == 'grevia') {
			$is_auth_valid = true;
		}
		
		// allow spesific route: api.curl_with_header
		if (strpos(current_full_url(), 'curl_with_header') != FALSE) $is_auth_valid = true;
		
		if (! $is_auth_valid)  
		{
			$message['is_error'] = 1;
			$message['message'] = 'Secretkey required';
			$message['target_data'] = $_REQUEST;
			$message['target_url'] = current_full_url();
			$message['header_debug'] = $header;
			echo json_encode($message);
			die;
		}
	}
	
	public function index()
	{
		//echo $this->common_model->curl('api/forumtopic');
		//echo ' jalan';die;
		
	}
	
	public function notif()
	{
		// Project ID 	: fir-notification-ffc8f
		// API 			: AIzaSyBhxB6nLXNl4PHyb2nmEiJ2cgWOo54avm8
		
		//echo $this->common_model->curl('api/forumtopic');
		//echo ' jalan';die;
		
	}
	
	// public function member()
	// {
		// if ($this->input->get())
		// {
			// $params = $this->input->get();
		// }
		// $params['paging'] = TRUE;
		// if (isset($_GET['page'])) $params['page'] = $_GET['page'];
			
		// if ($this->input->get('member_id')) 
		// {
			// $params['member_id'] = $this->input->get('member_id');
			// $json = json_encode($this->member_model->get($params));
		// }
		// else 
		// {
			// $json = $this->member_model->get_list($params);
			// unset($json['total_rows']);
			// $json = json_encode($json);
			// // $json = json_encode($this->member_model->get_list($params));
		// }
		// echo $json;
	// }
	
	// public function curl()
	// {
		// $url = $config = NULL;
		// $url = 'http://localhost/grevia.com/api/member';
		// // $config[] = 'token:ayam';
		// // $hit = $this->curl->simple_get($url,NULL,array(CURLOPT_HTTPHEADER => $config));
			
		// $ch = curl_init($url);
		// curl_setopt_array($ch, array(
			// CURLOPT_HTTPHEADER  => array('X-User: admin', 'X-Authorization: 123456', 'token: ayam'),
			// CURLOPT_RETURNTRANSFER  =>true,
			// CURLOPT_VERBOSE     => 1
		// ));
		// $hit = curl_exec($ch);
		// curl_close($ch);
		
		// debug($hit);
		// die;
	// }
	
	public function member()
	{
		$get = $data = NULL;
		if ($_GET)
		{
			$get = $_GET;
			$params = $_GET;
			// $params = $this->input->get();
		}
		
		$params['paging'] = TRUE;
		if (isset($_GET['page'])) $params['page'] = $_GET['page'];

		/*
		$header = getallheaders();
		// Check validation here
		if (isset($get['token']) && $get['token'] == 'ayam') 
		{
			// $get['token']
		} else if (isset($header['Token'])) {
			// debug($header);
			// die;
			
			if ($header['Token'] == 'ayam') {
				// valid
			} else {
				$json['status'] = 'error';
				$json['message'] = 'token key invalid';
				echo json_encode($json);
				die;
			}
			// echo json_encode($header);
			// debug($header);
		} else {
			$json['status'] = 'error';
			$json['message'] = 'token key required';
			echo json_encode($json);
			die;
		}
		*/
	
		if (isset($get)) 
		{
			if (isset($get['member_id'])) $params['member_id'] = $get['member_id'];
			if (isset($get['password'])) 
			{
				if (isset($get['email'])) $params['email'] = $get['email'];
				$params['password'] = $get['password'];
			}
			
			$data = $this->member_model->get($params);
			
			if ( ! empty($data)) 
			{
				$json = $data;
			}
			else
			{
				// $json['response'] = "201";
				// $json['message'] = "data not found";
				$json = array();
			}
		}
		else 
		{
			$temp = $this->member_model->get_list($params);
			$json['total_rows'] = $temp['total_rows'];
			$json['MemberModel'] = $temp['data'];
		}
		$json = json_encode($json);
		echo $json;
		die;
	}
	
	public function product()
	{
		$this->load->model('product_model');
		
		$get = $data = NULL;
		if ($_GET)
		{
			$get = $_GET;
			$params = $_GET;
			// $params = $this->input->get();
		}
		
		$params['paging'] = TRUE;
		if (isset($_GET['page'])) $params['page'] = $_GET['page'];

		/*
		$header = getallheaders();
		// Check validation here
		if (isset($get['token']) && $get['token'] == 'ayam') 
		{
			// $get['token']
		} else if (isset($header['Token'])) {
			// debug($header);
			// die;
			
			if ($header['Token'] == 'ayam') {
				// valid
			} else {
				$json['status'] = 'error';
				$json['message'] = 'token key invalid';
				echo json_encode($json);
				die;
			}
			// echo json_encode($header);
			// debug($header);
		} else {
			$json['status'] = 'error';
			$json['message'] = 'token key required';
			echo json_encode($json);
			die;
		}
		*/
	
		if (isset($get)) 
		{
			if (isset($get['product_id'])) $params['product_id'] = $get['product_id'];
			// if (isset($get['password'])) 
			// {
				// if (isset($get['email'])) $params['email'] = $get['email'];
				// $params['password'] = $get['password'];
			// }
			
			$data = $this->product_model->get($params);
			
			if ( ! empty($data)) 
			{
				$json = $data;
			}
			else
			{
				// $json['response'] = "201";
				// $json['message'] = "data not found";
				$json = array();
			}
		}
		else 
		{
			$temp = $this->product_model->get_list($params);
			$json['total_rows'] = $temp['total_rows'];
			$json['data'] = $temp['data'];
		}
		$json = json_encode($json);
		echo $json;
		die;
	}
	
	public function curl_with_header()
	{
		$url = $headers = $param = NULL;
		// $url = 'http://localhost/grevia.com/api/member/get?email=rusdi.karsandi@gmail.com';		
		$url = 'http://www.grevia.com/api/member/get?email=rusdi.karsandi@gmail.com';		
		// $url = 'http://www.grevia.com/api/member?secretkey=grevia';		
		$headers = array();
		// $headers[] = "Content-Type: application/json";
		// $headers[] = "Accept: application/json";
		
		// Add Token
		// $headers[] = "Secretkey:grevia";
		
		// Add parameter if needed
		// $param['member_id'] = 1;
		
		// $hit = $this->curl->simple_get($url,$param,array(CURLOPT_HTTPHEADER => $headers));
			
		$ch = curl_init($url);
		curl_setopt_array($ch, array(
			CURLOPT_HTTPHEADER  => array('X-User: admin', 'X-Authorization: 123456', 'token: ayam', 'Secretkey: grevia'),
			CURLOPT_RETURNTRANSFER  =>true,
			CURLOPT_VERBOSE     => 1
		));
		$hit = curl_exec($ch);
		curl_close($ch);
		//---------------------------------------
		
		// $process = curl_init($url);
		// curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', $headers));
		// curl_setopt($process, CURLOPT_HEADER, 1);
		// // curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
		// curl_setopt($process, CURLOPT_TIMEOUT, 30);
		// curl_setopt($process, CURLOPT_GET, 1);
		// // curl_setopt($process, CURLOPT_POSTFIELDS, $payloadName);
		// curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		// $hit = curl_exec($process);
		// curl_close($process);
		
		//--------------------------
		// $ch = curl_init();
		// $bbm_staging_url = $url;
		// curl_setopt($ch, CURLOPT_URL, $bbm_staging_url);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
		// // curl_setopt($ch, CURLOPT_GET, 1);
		
		debug($hit);
		die;
	}
	
	// Hit mua
	public function curl_mua()
	{
		$url = $headers = $param = NULL;
		// $url = 'http://localhost/grevia.com/api/member';		
		$url = 'http://localhost/muahunter_api/api/v1/user/get/';
		// http://localhost/muahunter_api/api/v1/user/get?secretkey=mu4hunt3r&user_id=1
		
		$headers = array();
		// $headers[] = "Content-Type: application/json";
		// $headers[] = "Accept: application/json";
		
		// Add Token
		$headers[] = "Secretkey: mu4hunt3r";
		
		// Add parameter if needed
		$param['user_id'] = 1;
		
		$hit = $this->curl->simple_get($url,$param,array(CURLOPT_HTTPHEADER => $headers));
		
		debug($hit);
		die;
	}
	
	public function member_post()
	{
		if ($this->input->post())
		{
			$params = $this->input->post();
		}
					
		if ($this->input->post('name')) 
		{
			$params['name'] = $this->input->post('name');
			$save = json_encode($this->member_model->save($params));
			
			// $json['MemberModel'] = $temp;
			if ($save) 
				$json['response'] = "success";
			else 
				$json['response'] = "error";
			
			$json = json_encode($json);
		}
		echo $json;
		die;
	}
	
	public function article()
	{
		if ($this->input->get())
		{
			$params = $this->input->get();
		}
		$params['paging'] = TRUE;
		if (isset($_GET['page'])) $params['page'] = $_GET['page'];
			
		if ($this->input->get('article_id')) 
		{
			$params['article_id'] = $this->input->get('article_id');
			$json = json_encode($this->article_model->get($params));
		}
		else 
		{
			$json = json_encode($this->article_model->get_list($params));
		}
		echo $json;
		die;
	}
	
	public function ambildatakota()
	{
		$emparray[0]['kota'] = 'ayam';
		$emparray[1]['kota'] = 'cibai';
		$emparray[2]['kota'] = 'lekong';
		$akhir = array(
			// 'DataKota' => $emparray
			'data' => $emparray
		);

		echo json_encode($akhir);
		die;
	}
	
	public function journey()
	{
		if ($this->input->post())
		{
			$params = $this->input->post();
		}
		$params['paging'] = TRUE;
		if (isset($_GET['page'])) $params['page'] = $_GET['page'];
			
		if ($_GET) 
		{
			foreach ($_GET as $key => $val)
			{
				$params[$key] = $val;
			}
			
			$temp = $this->journey_model->get_list($params);
			
			if (isset($temp['data']))
			{
				// $remove_editor = array("editor_id","editor_ip","editor_date");
				
				// $temp['data'] = (array) json_decode($temp['data']);
				// $temp['data'] = array_diff($temp['data'], $remove_editor);
				// $temp
				
				$json['total_rows'] = $temp['total_rows'];
				$json['JourneyModel'] = $temp['data'];
			}
			else 
			{
				$json['error_message'] = "parameter must filled";
			}
			
		}
		else 
		{
			$json['error_message'] = "parameter must filled";
		}
		$json = json_encode($json);
		echo $json;
		die;
	}
	
	public function journey_post()
	{
		$json = NULL;
		if ($this->input->post())
		{
			$params = $this->input->post();
		}

		if (isset($params['lat']) && isset($params['lng'])) 
		{
			if ( ! isset($params['creator_ip'])) $params['creator_ip'] = get_ip();
			if ( ! isset($params['creator_date'])) $params['creator_date'] = get_datetime();
			
			$save = json_encode($this->journey_model->save($params));
			
			if ($save) 
				$json['response'] = "success";
			else 
				$json['response'] = "error";
			
		}
		else 
		{
			$json['error_message'] = "lat & lng must filled";
		}
		$json = json_encode($json);
		echo $json;
		die;
	}
	/*
	public function data()
	{
		$json = '
		[
			{
				id: 1,
				firstName: "Andrew",
				lastName: "McGivery",
			},
			{
				id: 2,
				firstName: "John",
				lastName: "Smith",
			}
		]
		';
		echo $json;
	}
	*/
}