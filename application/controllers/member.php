<?php 

class Member extends MY_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('article_model');
		// $this->load->model('forumtopic_model');
		// $this->load->model('forumtopicdetail_model');
		$this->load->model('member_model');
		
		//$this->load->helper('directory');
		$current_url = str_replace(base_url(),'',current_url());
		if (!member_cookies("member_id")) redirect(base_url().'login?url='.urlencode($current_url));
	}
	
	public function index()
	{
		// $data = NULL;
		// $PAGE_TITLE = HOME;
		// $data['PAGE_TITLE'] = $PAGE_TITLE;
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		// $data['PAGE_HEADER'] = NULL;
		// $data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
        // $data['CONTENT'] = $this->load->view('member/adm_member',$data,TRUE);
        // $this->load->view('index', $data);
		redirect(base_url().'member/profile');
	}
	
	public function profile()
	{
		$data = NULL;
		$PAGE_TITLE = SETTING;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_profile',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function article()
	{
		$data = NULL;
		$PAGE_TITLE = ARTICLE;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_article',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function forum()
	{
		$data = NULL;
		$PAGE_TITLE = FORUM;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_forum',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function changepass()
	{
		$data = NULL;
		$PAGE_TITLE = CHANGE_PASSWORD;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_changepass',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function secure_notes()
	{
		$data = NULL;
		$PAGE_TITLE = "Secure Notes";
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_secure_notes',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function setting()
	{
		$data = NULL;
		$PAGE_TITLE = SETTING;
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_setting',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function video()
	{
		$this->load->model('video_model');
		$this->load->model('videolist_model');
		
		$data = NULL;
		$PAGE_TITLE = "Video";
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_video',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function todo()
	{
		$this->load->model('todo_model');
		// $this->load->model('videolist_model');
		
		$data = NULL;
		$PAGE_TITLE = "Todo list";
		$data['PAGE_TITLE'] = $PAGE_TITLE;
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, $PAGE_TITLE);
		$data['PAGE_HEADER'] = NULL;
		$data['SUBPAGE_HEADER'] = $PAGE_TITLE;
		
		$data['SIDEMENUBAR'] = $this->load->view('member/mbr_sidemenubar',$data,TRUE);
		$data['CONTENT'] = $this->load->view('member/mbr_todo',$data,TRUE);
        $this->load->view('index', $data);
	}

}