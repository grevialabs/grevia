<?php

class Mockup extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		// debug("berak",1);
		// $this->load->model('member_model');
		// $this->load->model('product_model');
		// $this->load->model('product_size_model');
		// $this->load->model('product_category_model');
		// $this->load->model('slideshow_model');
		// $this->load->model('order_model');
		
		// $this->load->model('log_model');
		// debug(cookie_order_id());
		// die;
	}

	public function index()
	{
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, REGISTER);
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_user',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function user()
	{
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, REGISTER);
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_user',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function login()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_login',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function role()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_role',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function vehicle()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_vehicle',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function do()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_do',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function dashboard()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_dashboard',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function customer()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_customer',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function po()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_po',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function vendor()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_vendor',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function product()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_product',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function finance()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_finance',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	public function tracking()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_tracking',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	public function log()
	{
		$data = NULL;
        $data['SIDEBAR'] = $this->load->view('mockup/sidebar',NULL,TRUE);
        $data['CONTENT'] = $this->load->view('mockup/v_log',$data,TRUE);
        $this->load->view('index_mockup', $data);
	}
	
	// public function product_category()
	// {	
		// $data['PAGE'] = 'Product Category';
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Product Category');
		// $data['PAGE_HEADER'] = 'Search';
		// $data['CONTENT'] = $this->load->view('modular/v_product_category',$data,TRUE);
        // $this->load->view('index', $data);
	// }
	
	// public function curl()
	// {	
		// $this->load->view('modular/curl');
	// }
	

}