<?php

class Tutorial extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data['PAGE_HEADER'] = 'Tutorial Index - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function nginx_load_balancer()
	{	
		$data['PAGE'] = 'Tutorial Nginx Load Balancing - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/nginx_load_balancer', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function memcached()
	{	
		$data['PAGE'] = 'memcached - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/memcached', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function subversion()
	{	
		$data['PAGE'] = 'Tutorial Subversion - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/subversion', $data, TRUE);
        $this->load->view('index', $data);
	}

	public function template_variable()
	{	
		$data['PAGE'] = 'Tutorial Template Variable - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/template_variable', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function notes()
	{	
		$data['PAGE'] = 'Tutorial Notes - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/notes', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function nodejs()
	{	
		$data['PAGE'] = 'Tutorial NodeJS - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/nodejs', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function mysql()
	{	
		$data['PAGE'] = 'Tutorial Mysql - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/mysql', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function setup_centos6()
	{	
		$data['PAGE'] = 'Tutorial Setup Centos 6 - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/setup_centos6', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function setup_centos7()
	{	
		$data['PAGE'] = 'Tutorial Setup Centos 7 - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/setup_centos7', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function setup_ssl()
	{	
		$data['PAGE'] = 'Tutorial Setup SSL - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/setup_ssl', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function redis()
	{	
		$data['PAGE'] = 'Tutorial Redis - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/redis', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function redis_centos7()
	{	
		$data['PAGE'] = 'Tutorial Redis di Centos 7 - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/redis_centos7', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function crop_image()
	{	
		$data['PAGE'] = 'Tutorial Crop Image - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/crop_image', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function roundcube_postfix()
	{	
		$data['PAGE'] = 'Tutorial Memperbesar Limit attachment di roundcube dan postfix - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/roundcube_postfix', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function mpdf()
	{	
		include_once __DIR__.'/../libraries/mpdf/mpdf.php';
		$data['PAGE'] = 'Tutorial MPDF - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/mpdf', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function install_git_centos()
	{	
		$data['PAGE'] = 'Tutorial Install Git di Centos - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/install_git_centos', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function ansible()
	{	
		$data['PAGE'] = 'Tutorial Ansible - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/ansible', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function setup_ubuntu_16_lemp()
	{	
		$data['PAGE'] = 'Tutorial Install Ubuntu 16.04 dan LEMP - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/setup_ubuntu_16_lemp', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function setup_ubuntu_16_munin()
	{	
		$data['PAGE'] = 'Tutorial Install Ubuntu 16.04 Munin Monitoring - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/setup_ubuntu_16_munin', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function setup_ubuntu_16_jenkins()
	{	
		$data['PAGE'] = 'Tutorial Install Ubuntu 16.04 Jenkins - Grevia';
		$data['CONTENT'] = $this->load->view('tutorial/setup_ubuntu_16_jenkins', $data, TRUE);
        $this->load->view('index', $data);
	}

	public function angular_1()
	{
		$data['PAGE'] = 'Angular 1 tutorial';
		$data['CONTENT'] = $this->load->view('tutorial/angular_1', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	public function git()
	{
		$data['PAGE'] = 'Git tutorial';
		$data['CONTENT'] = $this->load->view('tutorial/git', $data, TRUE);
        $this->load->view('index', $data);
	}

	public function centos75_virtualmin()
	{
		$data['PAGE'] = 'Centos 75 installing Virtualmin tutorial';
		$data['CONTENT'] = $this->load->view('tutorial/centos75_virtualmin', $data, TRUE);
        $this->load->view('index', $data);
	}
	
	

}