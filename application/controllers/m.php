<?php

class M extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data['PAGE_HEADER'] = 'Page';
		$data['CONTENT'] = $this->load->view('page/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	// public function testing()
	// {
		// $this->load->view('m/menu');
	// }
	
	// public function articlecategory()
	// {
		// echo "berak";die;
	// }
	
	public function articlecategory()
	{	
		$this->load->model('articlecategory_model');
		$this->load->model('article_model');
		/* start paging */
		$page = 1;
		if (isset($_GET['page'])) $page = $_GET['page'];
		if (OFFSET) $offset = OFFSET;
		$offset = 6;
		
		$data['page'] = $page;
		$data['offset'] = $offset;
		
		$param['page'] = $page;
		$param['paging'] = TRUE;
		$param['offset'] = $offset;
		/* end paging */
		
		$segment = '';
		if ($this->uri->segment(2))
		{
			$segment = $this->uri->segment(2);
		}
		else
		{
			$segment = 'all';
		}
		
		$article = $this->articlecategory_model->get(array('slug' => $segment));
		
		if (isset($article['article_category_id'])) $param['article_category_id'] = $article['article_category_id'];
		//if ($segment == 'all') $param = NULL;
		
		$data['data'] = $this->article_model->get_list($param);
		$title = '';
		if (isset($article['category_name'])) $title = ' '.$article['category_name'];
		
		$data['PAGE'] = 'Artikel kategori'.$title.' - Grevia';
		
		if ($this->uri->segment(2)) 
		{
			$segment = str_replace('-',' ',$this->uri->segment(2));
			$bread = array('articlecategory' => 'article category');
			$data['BREADCRUMB'] = $this->common_model->breadcrumb($bread, $segment);
		}
		else
		{
			$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'article category');
		}
		$data['PAGE_HEADER'] = 'Artikel'.$title;
		$data['CONTENT'] = $this->load->view('m/v_articlecategory',$data,TRUE);
        $this->load->view('index_mobile', $data);
	}

}