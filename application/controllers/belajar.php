<?php

class Belajar extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data['PAGE_HEADER'] = 'Page';
		$data['CONTENT'] = $this->load->view('page/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function menu()
	{
		$this->load->view('belajar/menu');
	}
	
	public function recursive()
	{
		$this->load->view('belajar/recursive');
	}
	
	public function calendar()
	{
		$this->load->view('belajar/calendar');
	}
	
	// gcalendar via php
	public function gcalendarphp()
	{
		$this->load->view('belajar/gcalendarphp');
	}
	
	// gcalendar via js
	public function gcalendarjs()
	{
		$this->load->view('belajar/gcalendarjs');
	}
	
	// gcalendar via js
	public function deal_schedule()
	{
		$this->load->view('belajar/deal_schedule');
	}
	
	public function crop()
	{
		$config['image_library'] = 'imagemagick';
		$config['library_path'] = '/usr/X11R6/bin/';
		$config['source_image'] = '/path/to/image/mypic.jpg';
		$config['x_axis'] = 100;
		$config['y_axis'] = 60;

		$this->image_lib->initialize($config);

		if ( ! $this->image_lib->crop())
		{
			echo $this->image_lib->display_errors();
		}
	}
	
	public function watermark()
	{
		$config['source_image'] = './asset/images/bannertest.jpg';
		$config['wm_text'] = 'Copyright 2006 - John Doe';
		$config['wm_type'] = 'text';
		$config['wm_font_path'] = '././system/fonts/texb.ttf';
		$config['wm_font_size'] = '24';
		$config['wm_font_color'] = 'ffffff';
		$config['wm_vrt_alignment'] = 'bottom';
		$config['wm_hor_alignment'] = 'center';
		$config['wm_padding'] = '10';

		$this->image_lib->initialize($config);

		echo $this->image_lib->watermark();
		die;
	}

	public function mobiledetect()
	{
		$this->load->view('belajar/mobiledetect');
	}
	
	public function ci_calendar()
	{
		$this->load->model('calendar_model');
		
		// day_type : long = Sunday, short = Sun, abr = Su.
		
		$config_calendar = array(
			'start_day' => 'monday',
			'show_next_prev' => TRUE,
			'next_prev_url' => base_url().'belajar/ci_calendar',
			'day_type' => 'long',
			'show_other_days' => TRUE,
		);
		
		$events = array(
			// '2016,06,28' => 'birthday',
			31 => 'current day',
		);
		$config_calendar['template'] = '

        {table_open}<table class="calendar" border="0" cellpadding="0" cellspacing="0">{/table_open}

        {heading_row_start}<tr>{/heading_row_start}

        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

        {heading_row_end}</tr>{/heading_row_end}

        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td>{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_start_today}<td>{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

        {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
        {cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

        {cal_cell_no_content}{day}{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_other}{day}{/cal_cel_other}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</table>{/table_close}';
		
		$year = $month = NULL;
		if ($this->uri->segment(3)) $year = $this->uri->segment(3);
		if ($this->uri->segment(4)) $month = $this->uri->segment(4);
		
		// $data[''] = ;
		$this->load->library('calendar',$config_calendar);
		$data['calendar'] = $this->calendar->generate($year,$month,$events);
		
		$data['CONTENT'] = $this->load->view('belajar/ci_calendar', $data, TRUE);
		$this->load->view('index',$data);
	}
	
	public function tags()
	{
		$this->load->view('belajar/tags');
	}
	
	public function json()
	{
		$this->load->view('belajar/json');
	}
	
	public function tags_json()
	{
		// $this->load->view('belajar/tags');
		$json = array(
			"Amsterdam",
			"London",
			"Paris",
			"Washington",
			"New York",
			"Los Angeles",
			"Sydney",
			"Melbourne",
			"Canberra",
			"Beijing",
			"New Delhi",
			"Kathmandu",
			"Cairo",
			"Cape Town",
			"Kinshasa"
		);
		$json = json_encode($json);
		// echo $json;
		die;
	}
	
	public function unveil()
	{
		$this->load->view('belajar/unveil');
	}
	
	public function ai()
	{
		$this->load->view('belajar/ai');
	}
	
	public function scrollbawahchat()
	{
		$this->load->view('belajar/scrollbawahchat');
	}
	
	public function handlebar()
	{
		$this->load->view('belajar/handlebar');
	}
	
	public function encrypt_ci()
	{
		$this->load->library('encrypt');
		
		if (1000000 < 950000) {
			echo "kena true";
			
		} else {
			echo "kena false";
		}

		die;
		
		$string = 'Terima kasih Anda telah menggunakan fasilitas Internet Banking BCA.
Berikut ini adalah informasi transaksi yang telah Anda lakukan di Internet Banking BCA :
 
 	Tanggal	 : 	08/06/2017
 	Jam	 : 	09:51:20
 	Jenis Transaksi	 : 	TRANSFER KE REKENING BANK LAIN - DALAM NEGERI
 	Rekening Tujuan	 	 
 	 	
Nomor Rekening
 : 	6250100034188
 	 	
Kota
 : 	JAKARTA
 	 	
Bank
 : 	PT. BANK CIMB NIAGA TBK
 	Nominal	 : 	Rp.25.000.000,00
 	Biaya	 : 	Rp.5.000,00
 	Berita	 : 	CIMB NIAGA
 	 	 	-
 	Layanan Transfer	 : 	LLG
 	Jenis Transfer	 : 	SEKARANG
 	No. Referensi	 : 	E7CE5A7B-C918-EECF-B371-C8E395883E62
 	Status	 : 	BERHASIL
 
Semoga informasi ini bermanfaat bagi Anda.
Terima kasih.';
		$encode = $this->encrypt->encode($string);
		debug($encode);
		$decode = $this->encrypt->decode($encode);
		debug($decode);
		
		echo strlen('82BsoseUHPMAF/phuXOAXnJIDZZkNVG2gW7kr2nrY7tDb5713evlmm48YANSPKcUVeg7oLCCPD4a6LXjCkQ/NdpoJLUAnLXilzKquDjJ+ELFjZe44ndCEYVJKIzJ8pnL7IzYidBcF2C8Lh3o+Hz5PQILGdg1EwiLNO8OTWQ033TFvH/m74hkhm5uDMgj2NJAESJqniXuxfAoRh9405xVOLlW9+uLOhUjrhJl9lUMnQ8rQZL+vyE+KJKFs09KbbkD95HM0ZbyL2q9aWle2C9jk4W9dS7KJ3gPWFkncL6oqGLAisFzsdc0yFEn0VWebghUQRc2K98S86wfxMQ7DRGGFy4KLakSJCO3Kdm0W5Z1hkx86eOCC31oJYe+Fy4W5O6lwMg5mH6RFcT6ce2Xe7hbEQTi2G936YM04Ns58pwJBGNT81WMOaIcLuE7I13T9R/8jDKDW6U/TuV6k0ELpibNK15A97kdzeAWme511dV2oqUSM1rNIJENwLdMBsJSZEgaIr6JJlQ8qVI86G0MP7hlQV9Qpf3phKW5MVEkJ7VvtHMlGJcv1VlSGWwLEaauLbw3LmU3FJdN2spB3zateZbb3SnzTEu3ddIVD5PPa0zG3f/wl3okw25AwOr9ddPBz49u/fBYxdezl7+4Dtrogu+lGcBXRKZ4qcB6iFmDYIOoO/kRIjxj1I/ZgWrT1nkug9nJb6ar1qvj0G9z/GMV6P5/7niKEOceWd2r1kQk28v5Tka3aHnLGJzlsgPryTTJUq3iYlycdDW8CH00it57f0QGVYqeQ14KxRln4KIfehw/xfA2JgAS5Vwd0D1wOATds7V+4Vktqxd9nbHhUp3Z8CBe6kYTih9j2qHSj+gST5WxLrie9yaC0ltpQ9ZIkWCK/fcGbu05Ni8SJhBfye26sN8f9EIm0PPk5U6s7D9e/oSstjo=');
		echo ' '.strlen($string);
		die;
	}
	
	public function twilio()
	{
		// $dame = 'SUGOI';
		// // if (strpos($dame,'ERROR') >= 0) echo "kena";
		// // else echo "ga kena";
		
		// // Versi
		// // if (strpos($dame,'ERROR') !== FALSE) echo "kena";
		// // else echo "ga kena";
		
		// if (strpos($dame,'ERROR') === TRUE) echo "kena";
		// else echo "ga kena";
		
		
		// die;
		
		// Get the PHP helper library from twilio.com/docs/php/install 
		require_once './application/libraries/twilio/autoload.php'; // Loads the library 
		require_once './application/libraries/twilio/Rest/Client.php'; // Loads the library 
		
		// $mytoken = 'c661f72d289ebb5eeb677d1dc7ae0402';
		$mytoken = '6d6feaa6e5ec67e6ad554542a8dfcb23';
		$mysecret = 'TX6oUeYfWVMJbqtYlvk2MvLPFdOfrx3R'; // Master // SID SK093891116b33610704cf37318bbdf1f4
		// use Twilio\Rest\Client; 
		 
		// $account_sid = 'ACc35ffc51d59223e7b7969febec94085b'; 
		$account_sid = 'ACbaeeebf133008a4f1bd99f7577b2caa7'; 
		$auth_token = $mytoken; 
		$client = new Client($account_sid, $auth_token); 
		 
		$messages = $client->accounts($account_sid) 
		  ->messages->create("+6283891998825", array( 
				'From' => "+6285574677944",  
				'Body' => "testing aja bro",      
		  ));	
		
		debug($messages);
		die;
	}
	
	public function google_authenticator()
	{
		require_once './application/libraries/PHPGangsta/GoogleAuthenticator.php';
		$this->load->view('belajar/google_authenticator');
	}
	
	public function readcsv()
	{
		$this->load->view('belajar/readcsv');
	}
 
	public function sample_mutasi_bca()
	{
		$this->load->view('belajar/sample_mutasi_bca');
	}
	
	public function scrape()
	{
		$this->load->view('belajar/scrape');
	}
	
	public function scraper()
	{
		$this->load->view('belajar/scraper');
	}
	
	public function curl_mutasi_bca()
	{
		$this->load->library('BCAParser');
		$this->load->view('belajar/curl_mutasi_bca');
	}
	
	// Image animation rotate
	public function rotate_image()
	{
		$this->load->view('belajar/rotate_image');
	}
	
	public function palindrome()
	{
		$this->load->view('belajar/palindrome');
	}
	
	public function pass_validation()
	{
		$this->load->view('belajar/pass_validation');
	}

}