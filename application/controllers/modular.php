<?php

class Modular extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_model');
		$this->load->model('articlecategory_model');
		$this->load->model('forumtopic_model');
		$this->load->model('forumtopicdetail_model');
		$this->load->model('member_model');
		$this->load->model('inbox_model');
		$this->load->model('search_model');
		$this->load->model('tracker_model');
	}
	
	public function index()
	{		
		/* START CACHE */
		// $cache_name = 'index';
		// if ( ! $this->cache->get($cache_name))
		// {
			// Save into the cache for 5 minutes
			 $CONTENT = $this->load->view('modular/v_index', NULL, TRUE);
			 // $this->cache->save($cache_name, $CONTENT, 300);
		// } 
		// else 
		// {
			// $CONTENT = $this->cache->get($cache_name);
		// }
		/* END CACHE */
		
		$data['PAGE_TITLE'] = DEFAULT_PAGE_DESCRIPTION;
		$data['OG_TITLE'] = $data['PAGE_TITLE'];
		$data['OG_IMAGE'] = base_url().'asset/images/logo-grevia.jpg';
		$data['OG_DESCRIPTION'] = META_DESCRIPTION;
		
		$data['CONTENT'] = $CONTENT;
        $this->load->view('index_landing', $data);
	}
	
	public function logtesting()
	{
		// $url
		// $log = $this->log();
		// echo $log;
		// die;
		$this->load->library('curl_async');
		
		$url = base_url().'log?1';
		$this->curl_async->process($url);
		
		$url2 = base_url().'log?2';
		$this->curl_async->process($url2);
		
		$url3 = base_url().'log?3';
		$this->curl_async->process($url3);
		
		echo "ok";
		die;
	}
	
	public function log()
	{
		$this->load->model('log_model');
		// $url = ''
		$curl = '';
		
		$param = NULL;
		$param['source'] = 'testingbro';
		$param['subject'] = current_full_url();
		$param['creator_date'] = get_datetime();
		
		$save = $this->log_model->save($param);
	}
	
	// public function index_slide()
	// {
		// /* START CACHE */
		// $cache_name = 'index';
		// if ( ! $this->cache->get($cache_name))
		// {
			// // Save into the cache for 5 minutes
			 // $CONTENT = $this->load->view('modular/v_index_slide_home', NULL, TRUE);
			 // $this->cache->save($cache_name, $CONTENT, 300);
		// } 
		// else 
		// {
			// $CONTENT = $this->cache->get($cache_name);
		// }
		// /* END CACHE */
		
		// $data['PAGE_TITLE'] = DEFAULT_PAGE_DESCRIPTION;
		// $data['OG_TITLE'] = $data['PAGE_TITLE'];
		// $data['OG_IMAGE'] = base_url().'asset/images/logo-grevia.jpg';
		// $data['OG_DESCRIPTION'] = META_DESCRIPTION;
		
		// $data['CONTENT'] = $CONTENT;
        // $this->load->view('index_landing', $data);
	// }
	
	public function admin()
	{
		redirect('admin/dashboard');
	}
	
	public function sitemap()
	{
		$this->load->view('modular/v_sitemap');
	}
	
	public function redir() 
	{
		if (isset($_GET['url'])) {
			$url = urldecode($_GET['url']);
			redirect($url);
		} else {
			redirect(base_url());
		}
	}
	
	public function logout()
	{
		session_start();
		session_destroy();
		unset($_SESSION['hash']);
		setcookie('hash', '', time()-3600, base_url());
		unset($_COOKIE['hash']);
		redirect(base_url());
	}
	
	// public function article()
	// {	
		// /* start paging */
		// $page = 1;
		// if (isset($_GET['page'])) $page = $_GET['page'];
		// if (OFFSET) $offset = OFFSET;
		// $offset = 30;
		
		// $data['page'] = $page;
		// $data['offset'] = $offset;
		
		// $param['paging'] = TRUE;
		// $param['offset'] = $offset;
		// /* end paging */
		
		// $data['data'] = $this->article_model->get_list($param);
		// $data['PAGE'] = 'Artikel';
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Article');
		// $data['PAGE_HEADER'] = 'Article';
		// $data['CONTENT'] = $this->load->view('modular/v_article',$data,TRUE);
        // $this->load->view('index', $data);
	// }
	
	public function articlecategory()
	{	
		$this->load->model('articlecategory_model');
		/* start paging */
		$page = 1;
		if (isset($_GET['page'])) $page = $_GET['page'];
		if (OFFSET) $offset = OFFSET;
		$offset = 6;
		
		$data['page'] = $page;
		$data['offset'] = $offset;
		
		$param['page'] = $page;
		$param['paging'] = TRUE;
		$param['offset'] = $offset;
		/* end paging */
		
		$segment = '';
		if ($this->uri->segment(2))
		{
			$segment = $this->uri->segment(2);
		}
		else
		{
			$segment = 'all';
		}
		
		$article = $this->articlecategory_model->get(array('slug' => $segment));
		
		if (isset($article['article_category_id'])) $param['article_category_id'] = $article['article_category_id'];
		//if ($segment == 'all') $param = NULL;
		
		$data['data'] = $this->article_model->get_list($param);
		$title = '';
		if (isset($article['category_name'])) $title = ' '.$article['category_name'];
		
		$data['PAGE'] = 'Artikel kategori'.$title.' - Grevia';
		
		if ($this->uri->segment(2)) 
		{
			$segment = str_replace('-',' ',$this->uri->segment(2));
			$bread = array('articlecategory' => 'article category');
			$data['BREADCRUMB'] = $this->common_model->breadcrumb($bread, $segment);
		}
		else
		{
			$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'article category');
		}
		$data['SIDEBAR_RIGHT'] = $this->load->view('sidebar',NULL,TRUE);
		$data['PAGE_HEADER'] = 'Artikel'.$title;
		$data['CONTENT'] = $this->load->view('modular/v_articlecategory',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function forum()
	{
		$param['paging'] = TRUE;
		if (isset($_GET['page'])) $param['page'] = $_GET['page'];
		
		/* start paging */
		$page = 1;
		if (isset($_GET['page'])) $page = $_GET['page'];
		if (OFFSET) $offset = OFFSET;
		//$offset = 30;
		$param['offset'] = $offset;
		
		$data['page'] = $page;
		$data['offset'] = $offset;
		/* end paging */
		
		$data['data'] = $this->forumtopic_model->get_list($param);
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Forum');
		$data['PAGE_HEADER'] = 'Forum';
		$data['CONTENT'] = $this->load->view('modular/v_forum',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function showforumdetail()
	{
		if (isset($_GET['page'])) $param['page'] = $_GET['page'];
		
		$data['data'] = '';
		if ($this->uri->segment(2)) 
		{ 
			$param['paging'] = TRUE;
			$param['ForumTopicID'] = $this->uri->segment(2);
			
			/* start paging */
			$page = 1;
			if (isset($_GET['page'])) $page = $_GET['page'];
			if (OFFSET) $offset = OFFSET;
			$offset = 20;
			$param['offset'] = $offset;
			
			$data['page'] = $page;
			$data['offset'] = $offset;
			/* end paging */			
			
			$data['data'] = $this->forumtopicdetail_model->get_list($param);
		}
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(array('forum' => 'Forum'), 'Forum Detail');
		$data['PAGE_HEADER'] = 'Forum';
		$data['CONTENT'] = $this->load->view('modular/v_showforumdetail',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function error404()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Error 404');
		$data['PAGE_TITLE'] = 'Halaman tidak ditemukan.';
		$data['PAGE_HEADER'] = br(2).'<div class="talCnt">Error 404<br/>Halaman tidak ditemukan.</div>';
		$data['CONTENT'] = $this->load->view('modular/v_error404',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function about()
	{
        $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'About');
		$data['PAGE_HEADER'] = '';
		$data['CONTENT'] = $this->load->view('modular/v_about',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function deal_schedule()
	{
        $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Deal Schedule');
		$data['PAGE_HEADER'] = 'Deal schedule';
		$data['CONTENT'] = $this->load->view('modular/deal_schedule',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function contact()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Forum');
		$data['PAGE_HEADER'] = 'Hubungi kami';
        $data['CONTENT'] = $this->load->view('modular/v_contact',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function order()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Order');
		$data['PAGE_HEADER'] = 'Pesan website';
        $data['CONTENT'] = $this->load->view('modular/v_order',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function cara_order()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Order');
		$data['PAGE_HEADER'] = 'Pesan website';
        $data['CONTENT'] = $this->load->view('modular/v_cara_order',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function career()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Career');
		$data['PAGE_HEADER'] = 'Karir';
        $data['CONTENT'] = $this->load->view('modular/v_career',$data,TRUE);
        $this->load->view('index', $data);
	}

	public function help()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Artikel Bantuan');
		$data['PAGE_HEADER'] = 'Artikel Bantuan';
        $data['CONTENT'] = $this->load->view('modular/v_help',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	//abtesting page
	public function ordernew()
	{
		$this->load->model('abtest_model');
		
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Order AB testing');
		$data['PAGE_HEADER'] = 'Artikel Order AB testing';
		
		// Check if postback
		if ($_POST) {
			$post = $_POST;
			// debug($post);
			// die;
			
			if (isset($post['abtest_view_id']) && isset($post['trigger_name'])) {
				// Save log ab_view by ip - insert ignore
				$param_view = NULL;
				$param_view['trigger_name'] = $post['trigger_name'];
				$param_view['editor_ip'] = get_ip();
				$param_view['editor_date'] = get_datetime();
				$update = $this->abtest_model->update_view($post['abtest_view_id'],$param_view);
				
				if ($update) {
					// kasi redirect
					echo "trimakasih karena telah berpartisipasi";
					die;
				} else {
					// ngapain bro
					// redirect ke next page / home
				}
				
			}
			
		}
		
		// Check if abtest exist AND abtest_detail quota valid
		$param['status'] = 1;
		$param['quota_not_full'] = 1;
		$obj = $this->abtest_model->get_list_detail($param);
		$obj = $obj['data'];
		// debug($check_active);
		// die;
		if (count($obj) > 0) {
			
			// get module with quota not full yet
			$arr_pos_module = mt_rand(0,count($obj)-1);
			$get_module = $obj[$arr_pos_module]['fullmodulename'];
			
			// debug($arr_pos_module);
			// debug($get_module);
			// debug($check_active);
			// die;
			
			// Check quota from detail
			if ($obj[$arr_pos_module]['quota_each_detail_used'] < $obj[$arr_pos_module]['quota_each_detail']) {
			
				// Save log ab_view by ip - insert ignore
				$param_view = NULL;
				$param_view['abtest_detail_id'] = $obj[$arr_pos_module]['abtest_detail_id'];
				$param_view['creator_ip'] = get_ip();
				$param_view['creator_date'] = get_datetime();
				$param_view['return'] = 1;
				// debug($param_view);
				// die;
				$save = $this->abtest_model->save_view($param_view);
				
				// pass to view
				$data['abtest_view_id'] = $save['last_id'];
				$data['abtest_detail_id'] = $obj[$arr_pos_module]['abtest_detail_id'];
				// debug($data);
				// die;
				
				$data['CONTENT'] = $this->load->view('modular/'.$get_module,$data,TRUE);
				
			} else {
				$data['CONTENT'] = $this->load->view('modular/order_default',$data,TRUE);
			}
			
		} else {
			// Default page ab testing
			// $data['CONTENT'] = $this->load->view('modular/v_help',$data,TRUE);
			$data['CONTENT'] = $this->load->view('modular/order_default',$data,TRUE);
		}
        $this->load->view('index', $data);
	}
	
	public function article()
	{
		$data['article_id'] = 0;
		if ($this->uri->segment(2) && is_numeric($this->uri->segment(2))) $data['article_id'] = $this->uri->segment(2);
		$data['slug'] = $this->uri->segment(3);
		
		$data['SIDEBAR'] = $this->load->view('sidebar',NULL,TRUE);
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Article');
		
		if (isset($data['article_id'])) 
		{ 
			$obj_article = $this->article_model->get(array('article_id' => $data['article_id']));
			$data['data'] = $obj_article;
			
			if (isset($obj_article))
			{
				$data['PAGE_TITLE'] = $obj_article['title'].' - Grevia';
				$data['OG_TITLE'] = $obj_article['title'].' - Grevia';
				$data['OG_IMAGE'] = base_url().'asset/images/article/'.$obj_article['image'];
				$data['OG_DESCRIPTION'] = $obj_article['short_description'];
				
				$TWITTER_URL = current_url();
				if (isset($obj_article['ShortUrl'])) $TWITTER_URL = $obj_article['short_url']; 
				$data['TWITTER_URL'] = $TWITTER_URL;
				$data['META_AUTHOR'] = $obj_article['full_name'];

				if (isset($obj_article['facebook'])) $data['META_ARTICLE_AUTHOR'] = $obj_article['facebook'];
				
				if ($data['article_id'] != $obj_article['article_id'] && $data['slug'] != $obj_article['slug'] ) 
				{	
					redirect(base_url().'article/'.$obj_article['article_id'].'/'.$obj_article['slug']);
				}
				
				// UPDATE VIEW
				$tmp['view'] = $obj_article['view'] + 1;
				$update = $this->article_model->update_view($data['article_id'], $tmp);
				
				/* START CACHE */
				$cache_name = $obj_article['slug'];
				if ( ! $this->cache->get($cache_name))
				{
					// Save into the cache for 5 minutes
					 $CONTENT = $this->load->view('modular/v_article', $data, TRUE);
					 $this->cache->save($cache_name, $CONTENT, 300);
				} 
				else 
				{
					$CONTENT = $this->cache->get($cache_name);
				}
				/* END CACHE */
			}
		}
		
		$data['CONTENT'] = $CONTENT;
        $this->load->view('index', $data);
	}
	
	public function login()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Login / Register');
		$data['PAGE_HEADER'] = NULL;
        //$data['CONTENT'] = $this->load->view('modular/v_login',$data,TRUE);
		//$this->load->view('index', $data);
		
		$this->load->view('modular/v_login',$data);
	}

	public function register()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Register');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_underconstruct',$data,TRUE);
		$this->load->view('index', $data);
		
		//$this->load->view('modular/v_register',$data);
	}
	
	public function activation()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Aktivasi akun');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_activation',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function forgotpassword()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Lupa Password');
		$data['PAGE_HEADER'] = NULL;
		// $data['CONTENT'] = $this->load->view('modular/v_forgotpassword',$data,TRUE);
        $data['CONTENT'] = $this->load->view('modular/v_underconstruct',$data,TRUE);
		
        $this->load->view('index', $data);
	}
	
	public function landing1()
	{
		$data[''] = '';
		$data['CONTENT'] = $this->load->view('modular/v_landing1',$data,TRUE);
		$this->load->view('index_landing',$data);
	}
	
	public function search()
	{	
		if (isset($_GET['q'])) 
		{	
			$param['keyword'] = $_GET['q'];
			$param['creator_date'] = get_datetime();
			$param['creator_ip'] = get_datetime();
			$this->search_model->save($param);
		}

		$data['SIDEBAR_RIGHT'] = $this->load->view('sidebar',NULL,TRUE);
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Article');
		$data['CONTENT'] = $this->load->view('modular/v_search',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function track()
	{
		$data['PAGE_HEADER'] = 'Track';
		$data['CONTENT'] = $this->load->view('modular/track',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function webdesign()
	{
		//$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Career');
		$data['PAGE_HEADER'] = 'Webdesign';
		$data['CONTENT'] = $this->load->view('modular/v_webdesign',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	public function promo_jasa_pembuatan_web_murah()
	{
		//$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Career');
		$data['PAGE_HEADER'] = 'Promo jasa web murah';
		$data['CONTENT'] = $this->load->view('modular/v_promo_jasa_pembuatan_web_murah',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	public function portofolio()
	{
		//$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Career');
		$data['PAGE_HEADER'] = 'Portofolio';
		$data['CONTENT'] = $this->load->view('modular/v_portofolio',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	public function faq()
	{
		// $data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'FAQ');
		$data['PAGE_HEADER'] = 'FAQ';
		$data['CONTENT'] = $this->load->view('modular/v_faq',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	public function aturan_pengguna()
	{
		//$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Career');
		$data['PAGE_HEADER'] = 'Aturan Pengguna';
		$data['CONTENT'] = $this->load->view('modular/v_aturan_pengguna',$data,TRUE);
		$this->load->view('index_landing', $data);
	}
	
	public function anniv()
	{
		$data['PAGE_HEADER'] = 'Anniv';
		$data['CONTENT'] = $this->load->view('modular/anniv',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function prewed()
	{
		$data['PAGE_HEADER'] = 'Prewed';
		$data['CONTENT'] = $this->load->view('modular/prewed',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function product()
	{
		$this->load->model('product_model');
		
		$data['PAGE_HEADER'] = 'Product';
		$data['CONTENT'] = $this->load->view('modular/v_product',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function template()
	{
		//$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Career');
		$data['PAGE_HEADER'] = 'Themes';
		$data['CONTENT'] = $this->load->view('modular/v_template',$data,TRUE);
		$this->load->view('index_landing', $data);
	}

	public function monitor()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Monitoring server');
		$data['PAGE_HEADER'] = NULL;
        $data['CONTENT'] = $this->load->view('modular/v_monitor',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function cron_backup_database()
	{
		echo "Starting...<hr/>";
		
		$this->load->library('email');

		$this->email->from('noreply@grevia.com', 'Grevia Autobot');
		$this->email->to('rusdi.karsandi@gmail.com');
		
		// $this->email->to('rusdi.karsandi@gmail.com');
		// $this->email->cc('another@another-example.com');
		// $this->email->bcc('them@their-example.com');

		$this->email->subject('Email Cron database backup server Grevia on '.date('D d-m-Y'));
		$this->email->message('Autobot create database backup on server GREVIA.');
		
		$this->email->attach('/home/backup/dam.sql.gz', 'attachment', 'dam'.date('d_m_y').'.sql.gz'); // dapuramoy
		$this->email->attach('/home/backup/grevia_roundcube.sql.gz', 'attachment', 'grevia_roundcube'.date('d_m_y').'.sql.gz'); // grevia
		$this->email->attach('/home/backup/grevia.sql.gz', 'attachment', 'grevia'.date('d_m_y').'.sql.gz'); // grevia
		$this->email->attach('/home/backup/hrprimesolution_com_roundcube.sql.gz', 'attachment', 'hrprimesolution_com_roundcube'.date('d_m_y').'.sql.gz'); // hrprimesolution
		$this->email->attach('/home/backup/hrp.sql.gz', 'attachment', 'hrp'.date('d_m_y').'.sql.gz'); // hrprimesolution
		$this->email->attach('/home/backup/job.sql.gz', 'attachment', 'job'.date('d_m_y').'.sql.gz'); // jobtalento
		$this->email->attach('/home/backup/scm.sql.gz', 'attachment', 'scm'.date('d_m_y').'.sql.gz'); // scm.grevia.com
		$this->email->attach('/home/backup/shp.sql.gz', 'attachment', 'shp'.date('d_m_y').'.sql.gz'); // shop.grevia.com
		$this->email->attach('/home/backup/wal.sql.gz', 'attachment', 'wal'.date('d_m_y').'.sql.gz'); // wallet.grevia.com
		// $this->email->attach('/home/backup/sukseselektronik.sql.gz', 'attachment', 'sukseselektronik'.date('d_m_y').'.sql.gz'); // sukseselektronik.com
		$this->email->attach('/home/backup/aer.sql.gz', 'attachment', 'aer'.date('d_m_y').'.sql.gz'); // aerox.grevia.com
		$this->email->attach('/home/backup/uzin.sql.gz', 'attachment', 'uzin'.date('d_m_y').'.sql.gz'); // uzinretailindonesia.com

		$send = $this->email->send();
		
		if ($send)
		{
			echo "berhasil horee<br/>";
		}
		else 
		{
			echo "gagal <br/>";
		}
		
		debug($send);
		
		echo "<hr/>End...";
		die;
	}
	
	public function native_email()
	{
		// the message
		$msg = "First line of text\nSecond line of text Lorem ipsum /r/n dolor sit amet";

		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email
		mail("rusdi@grevia.com","Subjectnya testing ya",$msg);
		// mail("rusdi.karsandi@gmail.com","Subjectnya testing ya",$msg);

	}
	
	public function instagram()
	{
		$this->load->view('modular/instagram');
	}
	
	public function instagram2()
	{
		$this->load->view('modular/instagram2');
	}
	
	public function demo()
	{
		/* START CACHE */
		$cache_name = 'demo';
		if ( ! $this->cache->get($cache_name))
		{
			// Save into the cache for 5 minutes
			 $CONTENT = $this->load->view('modular/v_index', NULL, TRUE);
			 $this->cache->save($cache_name, $CONTENT, 300);
		} 
		else 
		{
			$CONTENT = $this->cache->get($cache_name);
		}
		/* END CACHE */
		
		$data['PAGE_TITLE'] = DEFAULT_PAGE_DESCRIPTION;
		$data['OG_TITLE'] = $data['PAGE_TITLE'];
		$data['OG_IMAGE'] = base_url().'asset/images/logo-grevia.jpg';
		$data['OG_DESCRIPTION'] = META_DESCRIPTION;
		
		$data['CONTENT'] = $CONTENT;
        $this->load->view('index_landing', $data);
	}
	
	public function undangan()
	{	
		$data['PAGE'] = 'Undangan';
		$data['CONTENT'] = $this->load->view('modular/v_undangan',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	public function redis()
	{
		$this->load->driver('cache', array('adapter' => 'redis', 'backup' => 'file'));
		
		// $this->load->driver('cache');
		
		$this->cache->redis->save('foo', 'bar', 10);
		$data = $this->cache->redis->get('foo');
		echo $data.' data ';
		die;
	}

	public function abtest_report()
	{
		
		$this->load->model('abtest_model');
		
		$data['PAGE'] = 'Abtest Report';
		$data['CONTENT'] = $this->load->view('modular/abtest_report',$data,TRUE);
        $this->load->view('index', $data);
	}
	
	/* GENERAL AJAX */
	public function ajax()
	{
		$this->load->model('subscribe_model');
		$data = null;
		$data['CONTENT'] = $this->load->view('modular/v_ajax',$data,TRUE);
		//$this->load->view('index', $data);
	}
	
	// notif automatic payment
	public function lakupon_notif()
	{
		$sample = '
		{
		  "masked_card": "441111-1118",
		  "approval_code": "1491898366780",
		  "bank": "bni",
		  "eci": "06",
		  "transaction_time": "2017-04-11 15:12:46",
		  "custom_field1": "Dimas Ragil T,test@test.com,09876544321",
		  "custom_field2": "65c5b14d-af1e-40ef-9b45-1d2c7b854dba,65c5b14d-af1e-40ef-9b45-1d2c7b854dba",
		  "custom_field3": "",
		  "gross_amount": "119000.00",
		  "order_id": "TirtaStg-1491898301733",
		  "payment_type": "credit_card",
		  "signature_key": "ad09ccf56bb4f3ac1bc4b88ffd78bb47414f95c7f6375cfc951c7a663b3a4227b142edf01cff916b8d717391e70856ad0f9a1d8dbe35665b0751fc25cfdb89ab",
		  "status_code": "200",
		  "transaction_id": "06f00904-5b1e-4582-a906-038a74b78fba",
		  "transaction_status": "capture",
		  "fraud_status": "accept",
		  "status_message": "Veritrans payment notification"
		}
		';
		
		//-----------------------------------------------------------------------------------------------------------------------------------------------------
		// PAYMENT ACKNOWLEDGE FROM SPRINT
		$post = array();
		$post = file_get_contents("php://input");
		
		// $post = (array) json_decode($help);
		$callback = (array) json_decode($post);
		
		if ($callback) {
			// if ( ! is_array($post)) $post = (array) json_decode($post);
			
			echo "<br/>---------------------------------------------------------------------------<br/>datapost response<br/>";
			echo "<pre>URL datapostjson";
			print_r($callback);
			echo "</pre>";
			echo "<br/>---------------------------------------------------------------------------";
			echo "<br/>masked_card ".$callback['masked_card'];
			
			$param = NULL;
			$param['source'] = get_ip();
			$param['subject'] = 'callback lakupon automatic';
			$param['content'] = $post;
			$param['notes'] = print_r($callback,1);
			$param['creator_date'] = get_datetime();
			$insert = $this->db->insert('grv_log', $param);
			
			// if acc
			if ($callback['transaction_status'] == 'capture') 
			{
				// Get cart by order_id 
				
				// update status card & issued 
				
				// insert log 
			}
			
		} else {
			// echo "<br/>---------------------------------------------------------------------------<br/>datapost response<br/>";
			echo "no data bro";
			// echo "<br/>---------------------------------------------------------------------------";
		}
		die;
	}
	
	public function lakupon_notif_manual()
	{
		/* 
		CREATE TABLE grv_log(
			log_id BIGINT AUTO_INCREMENT,
			source varchar(150) NULL,
			subject varchar(150) NULL,
			content LONGTEXT NULL,
			creator_date datetime NULL,
			PRIMARY KEY(log_id)
		)
		*/
		
		// PAYMENT ACKNOWLEDGE FROM SPRINT
		$post = $param = $insert = array();
		$post = file_get_contents("php://input");
		$post = (array) json_decode($post);
		
		// $post = '{"payment_types":["CREDIT_CARD","MANUAL_TRANSFER"],"manual_transfer":{"manual_transfer_accounts":[{"number":"123456","holder":"PT LAKUPON BBM BCA","branch":"Jakarta","bank":"BCA"},{"number":"808089090","holder":"PT LAKUPON BBM MANDIRI","branch":"Jakarta","bank":"MANDIRI"}],"webhook_url":"http:\/\/localhost\/bbmkupon\/manual_transfer","unique_payment":{"substract":false,"value":783},"custom_expiry":10,"custom_instruction":"Mohon lakukan pembayaran sebelum 2 x 24 jam"},"customer":{"email":"tester@lakupon.com","firstname":"ayam","lastname":"tester","phone":"081146587110"},"items":[{"id":"65c5b14d-af1e-40ef-9b45-1d2c7b854dba","name":"Aqua","price":34500,"quantity":1},{"id":"65c5b14d-af1e-40ef-9b45-1d2c7b854dba","name":"bebek","price":20500,"quantity":1}],"environment":"DEVELOPMENT","redirect_url":"http:\/\/localhost\/bbmkupon\/finish","is_for":"bbm"}';
		// $post = (array) json_decode($post);
		// if ($_POST)
		if ($post)
		{
			// $post = $_POST;
			// $post['ayam'] = 'sugoi';
			// $post['bebek'] = 'dame';
			$param['source'] = get_ip();
			$param['subject'] = 'callback lakupon';
			$param['content'] = print_r($post,1);
			$param['creator_date'] = get_datetime();
			$insert = $this->db->insert('grv_log', $param);
		}
		
		if ($insert) echo "save success";
		else echo "else nih";
		die;
		// if ()
		// {
			// $save
		// }
	}
	
	function contoh()
    {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Imie', 'required|min_length[5]|max_length[25]|required|alpha'); 
		// $this->form_validation->set_rules('surname', 'Nazwisko', 'required|min_length[5]|max_length[25]|required|alpha'); 
		// $this->form_validation->set_rules('surname', 'Nazwisko', 'required|min_length[5]|max_length[25]|required|alpha'); 
		// $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
		// $this->form_validation->set_rules('number', 'Numer telefonu', 'required|alpha_numeric|max_length[10]'); 
		// $this->form_validation->set_rules('brand', 'Marka', 'required|alpha'); 
		// $this->form_validation->set_rules('model', 'Model', 'required|alpha'); 
		// $this->form_validation->set_rules('year', 'Rok produkcji', 'required|alpha_numeric|max_length[5]'); 
		// $this->form_validation->set_rules('km', 'Ilosc KM', 'required|alpha_numeric|max_length[5]'); 
		// $this->form_validation->set_rules('licenceseplate', 'Tablica rejestracyjna', 'required|max_length[15]'); 
		// $this->form_validation->set_rules('description', 'Opis', 'required|max_length[300]'); 
		// $this->form_validation->set_rules('city', 'Miasto', 'required|max_length[30]'); 

        $param = NULL;
		if ($_POST && $this->form_validation->run() == FALSE)
        {
            $param['message_warning'] = 'anda gagal dalam menentukan error';
        }
        else
        {
            
			/*$this->load->view('formsuccess');*/
            // $this->load->model('form');
            // $this->form->save($data);
        }
		
		$this->load->view('modular/contoh',$param);
    }
	
	function firebase()
	{
		$apikey = 'AAAAXqjICbI:APA91bHmhRTPWdeuB0yo9OUlavInFHQ4Kh7-QRKfzL04aBuSqpbax_gYFUv-oliqmdALQg6eOp5rKTUNGI62uuBtdX9dTy_LSJIPrx2SxywCvgft0LoIUtaYeeHsK0b2E2r_blVldEZn';
		// $apikey = 'AIzaSyCjnSWh_I5v6NMUurl1kA8jEA24V4WldBQ';
		// $apikey = 'GoGdfsflknEFñslknaglksjfnklj';
		
		$ch = curl_init("https://fcm.googleapis.com/fcm/send");
		$header = array('Content-Type: application/json',"Authorization: key=".$apikey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		// curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_ENCODING, true);
		
		$data = $data_notif = NULL;
		$data['title'] = 'Ini notif';
		$data['message'] = 'testing senpai';
		$data['text'] = 'testing senpai';
		// $data['to'] = '';
		
		$data_notif['to'] = '0WsucQ3Mf4Tb9fGvdk2NCBFmm3W2';
		// $data_notif['to'] = 'AAAAXqjICbI:APA91bHmhRTPWdeuB0yo9OUlavInFHQ4Kh7-QRKfzL04aBuSqpbax_gYFUv-oliqmdALQg6eOp5rKTUNGI62uuBtdX9dTy_LSJIPrx2SxywCvgft0LoIUtaYeeHsK0b2E2r_blVldEZn';
		$data_notif['notification'] = $data;
		$data_notif = json_encode($data_notif);
		
		// debug($data_notif);
		// die;
		
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, "{ \"notification\": {    \"title\": \"Test desde curl\",    \"text\": \"Otra prueba\"  },    \"to\" : \"SGferg-qWEFWbI:dflñkndfakllvakrgalkgjdgjslfkgjdglksdjflksjglkjlkñerhTHDFSHFZDHzdfakjsdhskjhgkjashfdasjdkf\"}");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_notif);

		$return = curl_exec($ch);
		curl_close($ch);
		
		debug($return);
		die;

	}
	
	// QC checked, berhasil di sabtu 3 juni 2017
	function fire()
	{
		// API access key from Google API's Console
		// ini server key
		define( 'API_ACCESS_KEY', 'AAAAXqjICbI:APA91bHmhRTPWdeuB0yo9OUlavInFHQ4Kh7-QRKfzL04aBuSqpbax_gYFUv-oliqmdALQg6eOp5rKTUNGI62uuBtdX9dTy_LSJIPrx2SxywCvgft0LoIUtaYeeHsK0b2E2r_blVldEZn' );
		
		// ini user registration key bukan userid
		// $_GET['id'] = 'dw166gboz6g:APA91bEZyRmpAg-57S8nenkCRdKTTCRcya-Zq5QnBwWx70kl1NctqDo8X0rXjZ9mSiSeeEj9lHkCTLVbDZEXRwDpI-EQthEa4XFEhDl5lFeFd_rWC-np-SSDI-hKTAajIgGto5GNq-9C';
		
		// abis install baru
		$_GET['id'] = 'c5R6t_R2Ihs:APA91bHUY12Iht9fyLNk4Jb40mGFZU-U1xCDki3QLI-RVqS9uVlVjLFBb7oDkQk-wXAIKJB-KIM9z8Fm1rb36uXPkm-ap3EQvecFkmmf-fH3q77AI-cy9uF6tE4ht7EL9h4BqZ4y9oCo';
		
		$registrationIds = array( $_GET['id'] );

		// prep the bundle
		$msg = array
		(
			'message' 		=> 'here is a message. message',
			'title'			=> 'This is a title. title',
			'subtitle'		=> 'This is a subtitle. subtitle',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			// 'vibrate'		=> 1,
			// 'sound'			=> 1,
			'icon'		=> 'large_icon',
			// 'largeIcon'		=> 'large_icon',
			// 'smallIcon'		=> 'small_icon'
		);
		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		
		if (curl_errno($ch)) {
			echo 'GCM error: ' . curl_error($ch);
		}
		
		curl_close( $ch );
		
		echo $result;
		die;
	}
	
	function uploadfile()
	{
		$this->load->view('modular/v_uploadfile');
	}
	
	function curl_wave()
	{
		/*
		SELECT s.transaction_id, dpa.name as package_name, c.* 
		FROM shopping_carts s
		LEFT JOIN deal_purchases dpu ON dpu.id = s.id
		LEFT JOIN coupons c ON c.deal_purchase_id = dpu.id
		LEFT JOIN deal_package_prices dpp ON dpp.id = dpu.deal_package_price_id
		LEFT JOIN deal_packages dpa ON dpa.id = dpp.package_id
		LEFT JOIN deals d ON d.id = dpa.deal_id
		WHERE d.id = 890 AND s.`status` = 4 AND c.deal_purchase_id IS NOT NULL 
		AND s.transaction_id = '331gR.7517773247732040825'
		LIMIT 10
		*/
		
		$url_bima = 'http://localhost/bima/coupon_manager/get_list_coupon_waterpark?transaction_id=331gR.7517773247732040825';
		$data = $this->curl->simple_get($url_bima,NULL);
		$data = json_decode($data,TRUE);
		if ( ! empty($data)) 
		{
			// Loop each weekday weekend, max 2 data
			$masak = $param_product = $param_voucher = NULL;
			foreach ($data as $k => $rs)
			{
				// Testing for dummy
				if (strpos(strtolower($rs['package_name']),'dummy') !== FALSE) {
					$param_product[] = 'Weekdays';
					$param_voucher['Weekdays'] = explode(',',$rs['list_coupon']);
				}
				
				// If package name contain weekday
				if (strpos(strtolower($rs['package_name']),'weekday') !== FALSE) {
					$param_product[] = 'Weekdays';
					$param_voucher['Weekdays'] = explode(',',$rs['list_coupon']);
				}
				
				// If package name contain weekend
				if (strpos(strtolower($rs['package_name']),'weekend') !== FALSE) {
					$param_product[] = 'Weekend';
					$param_voucher['Weekend'] = explode(',',$rs['list_coupon']);
				}
				
				// $data
				
				$param['Ordercode'] = '110012';
				$param['FullName'] = 'Laku';
				$param['IdentityType'] = '987654321';
				$param['IdentityNo'] = '012345798';
				$param['Email'] = 'tester@lakupon.com';
				$param['PhoneNo'] = '123456789';
				$param['ArrayProduct'] = $param_product;
				$param['ArrayTicket'] = $param_voucher;
			}
		}
		debug($param);
		die;
		
		// -----------------------------------------------------------------------------------------
		
		// BACKEND HIT 
		$url = 'http://dev.pondokindahwaterpark.co.id/svc/addthirdpartyticket.php';
		$param = $ticket = $product = NULL;
		
		// $ticket = '';
		$ticket['Weekdays'] = array('A123','A456');
		$ticket['Weekend'] = array('BB546','B5776');
		
		if ( ! empty($ticket['Weekdays'])) $product[] = 'Weekdays';
		if ( ! empty($ticket['Weekend'])) $product[] = 'Weekend';
		
		$param['Ordercode'] = '110012';
		$param['FullName'] = 'Laku';
		$param['IdentityType'] = '987654321';
		$param['IdentityNo'] = '012345798';
		$param['Email'] = 'tester@lakupon.com';
		$param['PhoneNo'] = '123456789';
		$param['ArrayProduct'] = $product;
		$param['ArrayTicket'] = $ticket;
		// debug($param);
		// die;
		
		$curl = $this->curl->simple_post($url,$param);
		debug($curl);
		die;
		
	}
	
	function hit_wave()
	{
		// http://dev.pondokindahwaterpark.co.id/svc/Api/index.php/Login
		
		// $url = 'http://dev.pondokindahwaterpark.co.id/svc/Api/index.php/';
		$base = 'http://dev.pondokindahwaterpark.co.id/svc/Api/index.php/';
		
		// $url = $param = NULL;
		// $url = $base.'Login';
		// $param['username'] = 'PiwLakupon';
		// $param['password'] = 'Lakupon123';
		// $login = $this->curl->simple_post($url,$param);
		
		// debug($login);
		
		// Example return : 
		/*
		{
		"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUFQuIE9ubGluZSBQZXJ0YW1hIChsYWt1cG9uLmNvbSkiLCJpZCI6MSwidGltZSI6MTUwODgzOTMzNH0.RwBa7nQFtlKXsGtCQiPXMiV1_Clgh26BYch6i-lvZXg",
		"message": "success"
		}
		*/
		
		// Step 2 - GetAllProducts
		
		/* Return parameter
		[
			{
			"productId": "1",
			"name": "Weekdays",
			"price": "100000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "2",
			"name": "Weekend",
			"price": "150000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "3",
			"name": "After Six Weekdays",
			"price": "75000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "4",
			"name": "After SIx Weekends",
			"price": "100000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "5",
			"name": "Flowrider",
			"price": "55000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "6",
			"name": "Buy 1 Get 1 Weekdays",
			"price": "100000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "7",
			"name": "Triwulan Weekdays",
			"price": "800000",
			"ticketPerTransaction": "10"
			},
			{
			"productId": "8",
			"name": "Triwulan All Days",
			"price": "1200000",
			"ticketPerTransaction": "10"
			}
		]
		*/
		// --------------------------------------------
		// $url = $param = $list_product = $token = NULL;
		// $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUFQuIE9ubGluZSBQZXJ0YW1hIChsYWt1cG9uLmNvbSkiLCJpZCI6MSwidGltZSI6MTUwODgzOTMzNH0.RwBa7nQFtlKXsGtCQiPXMiV1_Clgh26BYch6i-lvZXg';
		// $url = $base.'GetAllProducts';
		// $param['token'] = $token;
		// $list_product = $this->curl->simple_post($url,$param);
		// debug($list_product);
		
		// --------------------------------------------
		// 3 
		$url = $param = $list_product = $product = NULL;
		$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUFQuIE9ubGluZSBQZXJ0YW1hIChsYWt1cG9uLmNvbSkiLCJpZCI6MSwidGltZSI6MTUwODgzOTMzNH0.RwBa7nQFtlKXsGtCQiPXMiV1_Clgh26BYch6i-lvZXg';
		$url = $base.'Booking'; // dokumentasinya ditulis booking.php SALAH NI
		$param['token'] = $token;
		
		$product[0]['productid'] = 1;
		$product[0]['totalTicket'] = 3;
		
		$product[1]['productid'] = 2;
		$product[1]['totalTicket'] = 5;
		
		$param['product'] = $product;
		
		debug($url);
		debug($param);
		$booking = $this->curl->simple_post($url,$param);
		debug($booking);
		
		// --------------------------------------------
		// 4 
		$url = $param = $list_product = $issued = NULL;
		
		$url = $base.'Issued'; // dokumentasinya ditulis booking.php SALAH NI
		$param['token'] = $token;
		
		$product[0]['productid'] = 1;
		$product[0]['totalTicket'] = 3;
		
		$product[1]['productid'] = 2;
		$product[1]['totalTicket'] = 5;
		
		$param['product'] = $product;
		
		debug($url);
		debug($param);
		$issued = $this->curl->simple_post($url,$param);
		debug($issued);
		
		die;
		
	}
	
	function hit_wavenew()
	{
		// http://dev.pondokindahwaterpark.co.id/svc/Api/index.php/Login
		
		// $url = 'http://dev.pondokindahwaterpark.co.id/svc/Api/index.php/';
		$base = 'http://dev.pondokindahwaterpark.co.id/svc/Api/index.php/';
		
		$url = $param = $token = NULL;
		$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUFQuIE9ubGluZSBQZXJ0YW1hIChsYWt1cG9uLmNvbSkiLCJpZCI6MSwidGltZSI6MTUxMDg4ODQ4NH0.JC5zYsLI_F1kiddPf_uHJsPxOI4zNhP26HphpWLxKkw';
		// $url = $base.'Login';
		// $param['username'] = 'PiwLakupon';
		// $param['password'] = 'Lakupon123';
		// $login = $this->curl->simple_post($url,$param);
		
		// debug($login);
		// die;
		// Step 1 - Get login token
		// Example return : 
		/*
		{
		"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUFQuIE9ubGluZSBQZXJ0YW1hIChsYWt1cG9uLmNvbSkiLCJpZCI6MSwidGltZSI6MTUxMDg4ODQ4NH0.JC5zYsLI_F1kiddPf_uHJsPxOI4zNhP26HphpWLxKkw",
		"message": "success"
		}
		*/
		
		// Step 2 - GetAllProducts
		
		/* Return parameter
		[
			{
			"productId": "1",
			"name": "Weekdays",
			"price": "100000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "2",
			"name": "Weekend",
			"price": "150000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "3",
			"name": "After Six Weekdays",
			"price": "75000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "4",
			"name": "After SIx Weekends",
			"price": "100000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "5",
			"name": "Flowrider",
			"price": "55000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "6",
			"name": "Buy 1 Get 1 Weekdays",
			"price": "100000",
			"ticketPerTransaction": "1"
			},
			{
			"productId": "7",
			"name": "Triwulan Weekdays",
			"price": "800000",
			"ticketPerTransaction": "10"
			},
			{
			"productId": "8",
			"name": "Triwulan All Days",
			"price": "1200000",
			"ticketPerTransaction": "10"
			}
		]
		*/
		// --------------------------------------------
		// $url = $param = $list_product = $token = NULL;
		// $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUFQuIE9ubGluZSBQZXJ0YW1hIChsYWt1cG9uLmNvbSkiLCJpZCI6MSwidGltZSI6MTUwODgzOTMzNH0.RwBa7nQFtlKXsGtCQiPXMiV1_Clgh26BYch6i-lvZXg';
		// $url = $base.'GetAllProducts';
		// $param['token'] = $token;
		// $list_product = $this->curl->simple_post($url,$param);
		// debug($list_product);
		
		// --------------------------------------------
		// 3 
		$url = $param = $list_product = $product = NULL;
		$url = $base.'Booking'; // dokumentasinya ditulis booking.php SALAH NI
		$param['token'] = $token;
		
		$product[0]['productid'] = 1;
		$product[0]['totalTicket'] = 3;
		
		$product[1]['productid'] = 2;
		$product[1]['totalTicket'] = 5;
		
		$param['product'] = $product;
		
		debug($url);
		debug($param);
		$booking = $this->curl->simple_post($url,$param);
		debug($booking);
		
		// --------------------------------------------
		// 4 
		$url = $param = $list_product = $issued = NULL;
		
		$url = $base.'Issued'; // dokumentasinya ditulis booking.php SALAH NI
		$param['token'] = $token;
		
		$product[0]['productid'] = 1;
		$product[0]['totalTicket'] = 3;
		
		$product[1]['productid'] = 2;
		$product[1]['totalTicket'] = 5;
		
		$param['product'] = $product;
		
		debug($url);
		debug($param);
		$issued = $this->curl->simple_post($url,$param);
		debug($issued);
		
		die;
		
	}
	
	public function cart()
	{
		$this->load->model('product_model');
		$this->load->model('order_model');
		
		$data['PAGE_HEADER'] = 'Cart';
		$data['CONTENT'] = $this->load->view('modular/cart',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function checkout()
	{
		$this->load->model('product_model');
		$this->load->model('order_model');
		
		$data['PAGE_HEADER'] = 'Checkout';
		$data['CONTENT'] = $this->load->view('modular/checkout',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function thankyou()
	{
		$this->load->model('product_model');
		$this->load->model('order_model');
		
		$data['PAGE_HEADER'] = 'Thankyou';
		$data['CONTENT'] = $this->load->view('modular/thankyou',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function smtp()
	{
		$this->load->library('email');
		$config['protocol']    = 'smtp';
        // $config['smtp_host']    = 'ssl://smtp.gmail.com';
        // $config['smtp_host']    = 'ssl://smtp.zoho.com';
        $config['smtp_host']    = 'smtp.zoho.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'rusdi@dapuramoy.com';
        $config['smtp_pass']    = 'rusdi2012';
        $config['charset']    = 'utf-8';
        $config['newline']    = '\r\n';
        $config['mailtype'] = 'text'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('rusdi@dapuramoy.com', 'myname');
        $this->email->to('rusdi.karsandi@gmail.com'); 

        $this->email->subject('Email Test Sending from SMTP');
        $this->email->message('Testing the email class.');  

        $send = $this->email->send();
		if (!$send) {
			echo $this->email->print_debugger();
		} else {
			echo "berhasil";
		}
		die;
		
		// smtp.zoho.com 465 587
		// rusdi@dapuramoy.com 
		// rusdi2012
	}
	
	// tested and work on 13mar 2021
	public function check_email()
	{
		$this->load->library('email');
		// $config['protocol']    = 'smtp';
        // $config['smtp_port']    = '465';
        // $config['smtp_timeout'] = '7';
        // $config['charset']    = 'utf-8';
        // $config['newline']    = '\r\n';
        $config['mailtype'] = 'html'; // or html
        // $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('rusdi@grevia.com', 'Iam Rusdi Grevia');
        $this->email->to('rusdi.karsandi@gmail.com'); 

        $this->email->subject('Email Test Sending from SMTP');
        $message = '
		<html>
		<head></head>
		<body>
			<p>Hello brader</p>
			
			<div>How are you doing</div>
			<br/><br/>
			
			This is from grevia tester
		</body>
		</html>
		';
		$this->email->message($message);  

        $send = $this->email->send();
		if (!$send) {
			echo $this->email->print_debugger();
		} else {
			echo "berhasil";
		}
		die;

	}
	
	public function daftar()
	{
		$this->load->model('daftar_model');
		$this->load->model('daftar_file_model');
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/daftar', $data, TRUE);
		$this->load->view('index', $data);
	}

	public function aan()
	{
		$CONTENT = $this->load->view('modular/v_aan', NULL, TRUE);
		
		$data['PAGE_TITLE'] = DEFAULT_PAGE_DESCRIPTION;
		$data['OG_TITLE'] = $data['PAGE_TITLE'];
		$data['OG_IMAGE'] = base_url().'asset/images/logo-grevia.jpg';
		$data['OG_DESCRIPTION'] = META_DESCRIPTION;
		
		$data['CONTENT'] = $CONTENT;
        $this->load->view('index_landing', $data);
	}

	public function terms_condition()
	{
		$CONTENT = $this->load->view('modular/v_terms_condition', NULL, TRUE);
		
		$data['PAGE_TITLE'] = DEFAULT_PAGE_DESCRIPTION;
		$data['OG_TITLE'] = $data['PAGE_TITLE'];
		$data['OG_IMAGE'] = base_url().'asset/images/logo-grevia.jpg';
		$data['OG_DESCRIPTION'] = META_DESCRIPTION;
		
		$data['CONTENT'] = $CONTENT;
        $this->load->view('index_landing', $data);
	}

	public function promo()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Promo');
		$data['PAGE_HEADER'] = 'Promo menarik Grevialabs';
        $data['CONTENT'] = $this->load->view('modular/v_promo',$data,TRUE);
        $this->load->view('index_landing', $data);
	}

	public function kamus_startup()
	{
		$data['BREADCRUMB'] = $this->common_model->breadcrumb(NULL, 'Promo');
		$data['PAGE_HEADER'] = 'Kamus Startup';
        $data['CONTENT'] = $this->load->view('modular/v_kamus_startup',$data,TRUE);
        $this->load->view('index_landing', $data);
	}
	
	public function pomodoro()
	{
		$data = NULL;
		$data['CONTENT'] = $this->load->view('modular/v_pomodoro', $data, TRUE);
		$this->load->view('index', $data);
	}

}