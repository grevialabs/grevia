<?php

class Booking extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('location_model');
		$this->load->model('studio_model');
		$this->load->model('studio_type_model');
		$this->load->model('studio_calendar_model');
		$this->load->model('seat_model');
		$this->load->model('movie_model');
		$this->load->model('movie_schedule_model');
		$this->load->model('booking_model');
		$this->load->model('booking_detail_model');
	}
	
	public function index()
	{
		$data['PAGE_HEADER'] = 'Booking Sytem - Grevia';
		$data['CONTENT'] = $this->load->view('booking/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function location()
	{
		$data['PAGE_HEADER'] = 'Booking Sytem - Grevia';
		$data['CONTENT'] = $this->load->view('booking/bok_location',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function schedule()
	{
		$data['PAGE_HEADER'] = 'Booking Sytem - Grevia';
		$data['CONTENT'] = $this->load->view('booking/bok_schedule',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function checkout()
	{
		$data['PAGE_HEADER'] = 'Booking Sytem - Grevia';
		$data['CONTENT'] = $this->load->view('booking/bok_checkout',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	public function thankyou()
	{
		$data['PAGE_HEADER'] = 'Booking Sytem - Grevia';
		$data['CONTENT'] = $this->load->view('booking/bok_thankyou',$data,TRUE);
		$this->load->view('index', $data);
	}

}