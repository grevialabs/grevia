<?php

class Video extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('video_model');
		$this->load->model('videolist_model');
		
		$data['PAGE_HEADER'] = 'My Youtube';
		$data['CONTENT'] = $this->load->view('video/index',$data,TRUE);
		$this->load->view('index', $data);
	}
	
	// public function roundcube_postfix()
	// {	
		// $data['PAGE'] = 'Tutorial Memperbesar Limit attachment di roundcube dan postfix - Grevia';
		// $data['CONTENT'] = $this->load->view('tutorial/roundcube_postfix', $data, TRUE);
        // $this->load->view('index', $data);
	// }

}