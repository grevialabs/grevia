<?php 
$base_url = base_url();
$asset_url = base_url().'asset/';

if (! is_internal()) {
	// $base_url = 'https://api1.muahunter.com/'; 
	// $asset_url = 'https://s1.muahunter.com/asset/';
}


$list_module = array(
	'acl_module',
	'acl_module_access',
	'address',
	'banner',
	'chat',
	'city',
	'client',
	'commision',
	'coupon',
	'email',
	'equip',
	'like_portfolio',
	'like_user',
	'order',
	'order_detail',
	'page',
	'payment_type',
	'portfolio',
	'profession',
	'request_payment',
	'role',
	'schedule_availability',
	'schedule',
	'search',
	'service_category',
	'service',
	'setting',
	'team',
	'tempcart',
	// 'transaction', DEPRECATED 
	// 'transaction_detail', DEPRECATED 
	'user',
	'user_bank',
	'user_city',
	'user_equip',
	'user_model',
	'user_review',
	'wallet'
	// '',
);
?>
<html>
<head>
	<title>Resep</title>
	<link rel="stylesheet" href="<?php echo $asset_url ?>css/bootstrap.min.css?v=201706221" >
	<link href="<?php echo $asset_url ?>css/style_default.min.css?v=201706221" rel="stylesheet">
	
	<script src="<?php echo $asset_url ?>js/jquery.1.12.4.min.js?v=201706221"></script>
	
	<link rel="stylesheet" href="<?php echo $base_url?>asset/css/chosen/chosen.min.css?v=201706221">
	<style type="text/css" media="all">

	</style>

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<br/>
				<div class="talLft fntHdr clrBlu">POSTER</div>
				<hr/>
			</div>
			<div class="col-sm-12">
				<!--
				<select class="input br chosen-select" name="f_module" id="f_module">
					<?php foreach ($list_module as $module) { ?>
					<option><?php echo $module?></option>
					<?php } ?>
				</select>
				-->
				<br/><br/>
				URL<br/>
				<input type="text" class="br input wdtFul" placeholder="url name" name="f_url" id="f_url" /><br/><br/>
				
				<!--
				<br/>
				<input type="text" class="input br" placeholder="method name" name="f_method" id="f_method" /><br/><br/>-->
				
				<input type="text" class="input br" placeholder="method name" name="f_method" id="f_method" /> *common method : 
				<b>get</b>, <b>get_list</b>, <b>save</b>, <b>update</b>, <b>delete</b>
				<br/>
				<select class="input br" name="f_request" id="f_request">
					<option value="get">GET</option>
					<option value="post">POST</option>
				</select>
				<br/>
				<button class="btn btn-info br add_param" >+ add param</button>
				<div id="div_param"></div>
				<br/>
				<button id="btn_submit" class="br btn btn-success" type="button">Submit</button>
				
				<div id="div_result"></div>
			</div>
		</div>
	</div>
	
	<script src="<?php echo $base_url?>asset/js/chosen.jquery.min.js?v=201706221" type="text/javascript"></script>
	<script type="text/javascript">
	var config = {
	  '.chosen-select'           : {allow_single_deselect:true},
	  '.chosen-select-deselect'  : {allow_single_deselect:true},
	  '.chosen-select-no-single' : {disable_search_threshold:10},
	  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	  '.chosen-select-width'     : {width:"95%"}
	}
	for (var selector in config) {
	  $(selector).chosen(config[selector]);
	}
	</script>

</body>
</html>
<script>

$(document).ready(function(){
	
	$('#btn_submit').click(function () {
		module = $('#f_module').val();
		method = $('#f_method').val();
		request = $('#f_request').val();
		
		if (method.length <= 0) {
			alert('method harus diisi');
			$('#f_method').focus();
			return false;
		}
		// http://10.8.8.107/arjuna/api_validator/login_merchant_account
		// url = '<?php echo $base_url?>api/v1/' + module + '/' + method + '';
		url = $('#f_url').val();
		
		// Get
		if (request == "get") {
			
			param_get = '?';
			$('.param_child').each(function(){
				param_name = param_val = '';
				param_name = $(this).find('.param_name').val();
				param_val = $(this).find('.param_val').val();
			
				if (param_name && param_val) {
					param_get += param_name + '=' + param_val + '&';
				}
			});
			
			// remove &
			param_get = param_get.slice(0,-1);
			
			url += param_get;
			$.get(url, function(data, status){
				$('#div_result').addClass('alert alert-info');
				if (status == 'success') status = '<span class="clrGrn b">Success</span>'; 
				$('#div_result').html("<div class='b talCnt'>RESULT</div><hr/>URI: " + url + "<br/><br/>data<br/>" + data + "<br/><br/>Status<br/><b>" + status + "</b>");
			});
		}
		
		// Post
		else if (request == "post") {
			
			param_post = '';
			$('.param_child').each(function(){
				param_name = param_val = '';
				param_name = $(this).find('.param_name').val();
				param_val = $(this).find('.param_val').val();
				
				if (param_name && param_val) {
					param_post += param_name + '=' + param_val + '&';
				}
			});
			
			// remove &
			param_post = param_post.slice(0,-1);
			
			url += '?secretkey=mu4hunt3r';
			$.post(url, param_post, function(data, status, callback){
				$('#div_result').addClass('alert alert-info');
				if (status == 'success') status = '<span class="clrGrn b">Success</span>'; 
				$('#div_result').html("<div class='b talCnt'>RESULT</div><hr/>URI: " + url + "<br/><br/>data" + data + "<br/><br/>Status <b>" + status + "</b>");
			});
			
			// bisa tapi ga pake dulu deh
			// $.ajax({
				// method: 'POST',
				// url: url,
				// data: param_post,
				// dataType: 'html',
			    // success: function (data, status) {
					// $('#div_result').html("<div class='b talCnt'>RESULT</div><hr/>URI: " + url + "<br/><br/>data<br/>" + data + "<br/><br/>Status<br/><b>" + status + "</b>");
				// }
			// });
		}
	});

	$('.add_param').click(function(){
		div_param = '<div class="param_child">';
		div_param += '<input type="text" class="input br param_name" placeholder="param name" name="param_name[]" value="" /> ';
		div_param += '<input type="text" class="input br param_val" placeholder="param value" name="param_val[]" value="" /> <a class="fa fa-cross remove_param btn btn-danger btn-xs">Hapus param</a> <br/></div>';
		$('#div_param').append(div_param);		
		
		// remove button
		$('.remove_param').on('click', function(){
			$(this).parent().html('');
		});
	});
	

	// set default input for development
	// $('.add_param:first').trigger("click");
	// $('.param_name:first').val('secretkey');
	// $('.param_val:first').val('mu4hunt3r');
	$('#f_method').val('get');
	
	// set request to POST if certain method 
	$('#f_method').focusout(function(){
		listMethod = ['save','update','delete'];
		
		// Check if contain an alement
		// if element not found then return -1
		if(jQuery.inArray($(this).val(), listMethod) !== -1) {
			$('#f_request').val('post');
		}
	})
	
	$('.add_param:first').trigger("click");
})
</script>