<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4U_wSzpo-pN1UTVpvdTYVG1RjuiVRHss&signed_in=true&region=ID&libraries=places">
</script>

<script type="text/javascript">
var address_source, address_destination, autocomplete_address_source, autocomplete_address_destination;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var jakarta = new google.maps.LatLng(-6.209844902932365,106.84993743896484);
var place_start, place_end;
var map;
var placeId1, placeId2;
var marker;

function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
var beaches = [
	['Rusdi house', -6.166937,106.84509400000002, 2],
	['Hesti home', -6.1336611,106.84327789999998, 1],
	
  ];
// -----------------------------------------------------------------------------
function setMarkers(map) {
    // Adds markers to the map.

	// Marker sizes are expressed as a Size of X,Y where the origin of the image
	// (0,0) is located in the top left of the image.

	// Origins, anchor positions and coordinates of the marker increase in the X
	// direction to the right and in the Y direction down.
	var image = {
	  url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
	  // This marker is 20 pixels wide by 32 pixels high.
	  size: new google.maps.Size(20, 32),
	  // The origin for this image is (0, 0).
	  origin: new google.maps.Point(0, 0),
	  // The anchor for this image is the base of the flagpole at (0, 32).
	  anchor: new google.maps.Point(0, 32)
	};
	// Shapes define the clickable region of the icon. The type defines an HTML
	// <area> element 'poly' which traces out a polygon as a series of X,Y points.
	// The final coordinate closes the poly by connecting to the first coordinate.
	var shape = {
	  coords: [1, 1, 1, 20, 18, 20, 18, 1],
	  type: 'poly'
	};
	for (var i = 0; i < beaches.length; i++) {
	  var beach = beaches[i];
	  var marker = new google.maps.Marker({
		position: {lat: beach[1], lng: beach[2]},
		map: map,
		icon: image,
		shape: shape,
		title: beach[0],
		zIndex: beach[3]
	  });
	}
}
// -----------------------------------------------------------------------------

google.maps.event.addDomListener(window, 'load', function () {
	// new google.maps.places.SearchBox(document.getElementById('txtSource'));
	// new google.maps.places.SearchBox(document.getElementById('txtDestination'));
	
	var mapOptions = {
		zoom: 12,
		center: jakarta
	};
	map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
	
	directionsDisplay = new google.maps.DirectionsRenderer({ 
		/* 'draggable': true */
	});
	
	directionsDisplay.setMap(map);
	
	// ADD TRAFFIC LAYER
	var trafficLayer = new google.maps.TrafficLayer();
	trafficLayer.setMap(map);
	
	// ADD MARKER
	setMarkers(map);
	
	var input_source = document.getElementById("txtSource");
	var input_destination = document.getElementById("txtDestination");
	
	address_source = document.getElementById("txtSource").value;
	address_destination = document.getElementById("txtDestination").value;
	
	autocomplete_address_source = new google.maps.places.Autocomplete(input_source, {});
	autocomplete_address_source.bindTo('bounds', map);
	autocomplete_address_source.addListener('place_changed', function() {
	  var place = autocomplete_address_source.getPlace();
	  place_start = place.place_id;
	});
	// autocomplete_address_source
	
	autocomplete_address_destination = new google.maps.places.Autocomplete(input_destination, {});
	autocomplete_address_destination.bindTo('bounds', map);
	autocomplete_address_destination.addListener('place_changed', function() {
		var place = autocomplete_address_destination.getPlace();
		place_end = place.place_id;
		
		reset_latlong();
	});
	
	var runningMap = function(e) {
		var key = e.which || e.keyCode;
		if (key === 13) { 
			
			address_source = document.getElementById("txtSource").value;
			address_destination = document.getElementById("txtDestination").value;
			
			// if (address_source == '') {
				// alert('Source harus diisi');
			// } else if (address_destination == '') {
				// alert('Destination harus diisi');
			// } else {
				// var place1 = autocomplete_address_source.getPlace();
				// place_start = place1.place_id;
				// console.log(' place_start' + place_start);
				
				// var place2 = autocomplete_address_destination.getPlace();
				// place_end = place2.place_id;
				// console.log(' place_end' + place_end);
			if (!address_source) {
				document.getElementById("txtSource").focus();
			} else if (!address_destination) {
				document.getElementById("txtDestination").focus();
			} else {
				GetRoute();
			}
			// }
		}
	};
	
	document.getElementById('txtSource').addEventListener('keypress', runningMap);
	document.getElementById('txtDestination').addEventListener('keypress', runningMap);
});

function GetRoute() {
	address_source = document.getElementById("txtSource").value;
	address_destination = document.getElementById("txtDestination").value;
			
	if (!place_start) {
		// alert('place_start ' + address_source);
		if (address_source) {
			placeId1 = address_source;
		} else {
			alert("Please select origin");
			return;
		}
	} else {
		placeId1 = {
			placeId: place_start
		};
		
	}
	
	if (!place_end) {
		
		var f_lat = $('#f_lat').val();
		var f_lng =	$('#f_lng').val();
		// GET FROM LAT LONG IF EXIST
		if (f_lat && f_lng) {
			placeId2 = new google.maps.LatLng(parseFloat(f_lat),parseFloat(f_lng));
		}else if (address_destination) {
			// GET FROM SEARCHBOX
			placeId2 = address_destination;
		} else {
			alert("Please select destination");
			return;
		}
	} else {
		// GET FROM AUTOCOMPLETE
		placeId2 = {
			placeId: place_end
		};	
	}
  
	focus_tomap();
	
	// directionsDisplay.setPanel(document.getElementById('dvPanel'));

	//*********DIRECTIONS AND ROUTE**********************//
	is_avoidtoll = false;
	if (document.getElementById("is_avoidtoll").checked) {
		is_avoidtoll = true;
	}		
	
	var request = {
		origin: placeId1,
		destination: placeId2,
		// destination: {lat:-6.1775313, lng:106.7919257},
		// destination: {lat: f_lat, lng: f_lng},
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.METRIC,
		avoidTolls: is_avoidtoll,
		avoidHighways: false
	};
	directionsService.route(request, function (response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});

	//*********DISTANCE AND DURATION**********************//
	var service = new google.maps.DistanceMatrixService();
	service.getDistanceMatrix({
		origins: [placeId1],
		destinations: [placeId2],
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.METRIC,
		avoidTolls: is_avoidtoll,
		avoidHighways: false
	}, function (response, status) {
		if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
			var distance = response.rows[0].elements[0].distance.text;
			var duration = response.rows[0].elements[0].duration.text;
			var dvDistance = document.getElementById("dvDistance");
			dvDistance.innerHTML = "";
			dvDistance.innerHTML += "Distance: " + distance + "<br />";
			dvDistance.innerHTML += "Duration:" + duration;
			// window.alert('nih '+[destination]);

		} else {
			window.alert("Unable to find your location.");
		}
	});
}
$(document).ready(function(){
	$('#txtDestination').focusout(function(){
		if (!document.getElementById("txtSource").value) {
			document.getElementById("txtSource").focus();
		} else if (!document.getElementById("txtDestination").value) {
			document.getElementById("txtDestination").focus();
		} else {
			GetRoute();
		}
	});	
	
	$('.triggerLocation').click(function(){
		tujuan = $(this).attr('value');
		latlong = $(this).attr('latlong');
		
		latlong = latlong.split(",");
		lat = latlong[0];
		lng = latlong[1];
		
		// FILL LAT LNG
		$('#f_lat').val(lat);
		$('#f_lng').val(lng);
		
		$('#txtDestination').val(tujuan);
		if (!document.getElementById("txtSource").value) {
			document.getElementById("txtSource").focus();
		} else if (!document.getElementById("txtDestination").value) {
			document.getElementById("txtDestination").focus();
		} else {
			GetRoute();
		}
	});
});

function detect_location()
{
	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			
			var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + '%2C' + position.coords.longitude + '&language=en';

			var address = '';
			$.getJSON(GEOCODING).done(function(location) {
				country = location.results[0].address_components[5].long_name;
				state = location.results[0].address_components[4].long_name;
				city = location.results[0].address_components[2].long_name;
				address = location.results[0].formatted_address;
				$('#txtSource').val(address);
				// $('#latitude').html(position.coords.latitude);
				// $('#longitude').html(position.coords.longitude);
			})
			// alert(GEOCODING);
			console.log(address);

		}, function() {
			// handleLocationError(true, infoWindow, map.getCenter());
			// alert('berhasil');
		});
	} else {
		// Browser doesn't support Geolocation
		// handleLocationError(false, infoWindow, map.getCenter());
		alert('not support geolocation');
	}
}
function resetbox()
{
	$('#txtSource').val('');
	$('#txtDestination').val('');
}
function reset_latlong()
{
	$('#f_lat').val('');
	$('#f_lng').val('');
}
function reset_destination()
{
	$('#txtDestination').val('');
	reset_latlong();
}	
function reset_source()
{
	$('#txtSource').val('');
}

function focus_tomap()
{
	$('html, body').animate({
        scrollTop: $("#dvMap").offset().top
    }, 1200);
}
	
</script>
<style>
#reticule {
    position:absolute;
    width:13px;
    height:13px;
    left:50%;
    top:50%;
    margin-top:-8px;
    margin-left:-8px;
    pointer-events: none;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h3 class="f18 b">Auto Address</h3><br/>
			<!-- START ADDRESS -->
			<div class="col-sm-2 b">
				Jakarta Barat<br/>
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Mall Ciputra, Jl. Arteri S. Parman, Grogol, Jakarta Barat, Daerah Khusus Ibukota Jakarta" latlong="-6.168413899999999,106.78697">Citraland</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" onclick="triggerLocation()" value="Mal Taman Anggrek, South Tanjung Duren, West Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1775313,106.7919257">Taman Anggrek</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Central Park Mall, West Jakarta City, Special Capital Region of Jakarta" latlong="-6.1776163,106.7890764" >Central Park</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Lippo Mall Puri, South Kembangan, West Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.190026,106.7363445" >Lippo limal puri</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Mall Puri Indah, South Kembangan, West Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1879651,106.7319927">Mall puri indah</a>
				
				<!-- SAMPLE
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="">Place</a>
				-->
			</div>
			
			<div class="col-sm-2 b">
				Jakarta Utara<br/>
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Ancol, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.12027945483452,106.82714939117432">Ancol</a>
				
				<a class="btn btn-xs btn-info br triggerLocation " href="javascript:void(0)" value="Artha Gading Mall, West Kelapa Gading, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.145754599999999,106.89207579999993">Arthagading</a>
			
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Emporium Pluit Mall, Jalan Pluit Selatan Raya, Penjaringan, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1273778,106.79102549999993">Emporium Pluit</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Mall Kelapa Gading, East Kelapa Gading, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.157542499999999,106.90839949999997">MKG</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Mall Of Indonesia, West Kelapa Gading, Kecamatan Kelapa Gading, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1508029,106.89211620000003">MOI gading</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Pluit Village Mall, Jalan Pluit Indah Raya, Pluit, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1162275,106.79127590000007">Pluit Village</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Pluit Junction, Jalan Pluit Raya, Penjaringan, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.126344400000001,106.79134929999998">Pluit Junction</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Bay Walk Mall, Jalan Pluit Karang Ayu I, Pluit, North Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1078198,106.77910759999997">Baywalk mall</a>
				
				
				<!-- SAMPLE
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="">Place</a>
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="">Place</a>
				-->

			</div>
			
			<div class="col-sm-2 b">
				Jakarta Selatan<br/>
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Gandaria City Mall, North Kebayoran Lama, South Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.244429,106.78335300000003">Gandaria City</a>
			
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Kota Kasablanka, Menteng Dalam, South Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.224171,106.842987">Kokas</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Lippo Mall Kemang, Special Capital Region of Jakarta, Indonesia" latlong="-6.2612085,106.8126532">Lippo mal kemang</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" onclick="triggerLocation()" value="PIM 2, Pondok Pinang, South Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.264378699999999,106.78494849999993">PIM</a>
				
				<!-- SAMPLE
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="">Place</a>
				-->
			</div>
			
			<div class="col-sm-2 b">
				Jakarta Pusat<br/>
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Grand Indonesia, Jl. Mohammad Husni Thamrin, Menteng, Central Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1954274,106.82189429999994">Grand Indonesia</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Golden Truly Supermarket, Jalan Gunung Sahari, South Gunung Sahari, Central Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1615316,106.83844420000003" >Golden Truly</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Plaza Indonesia, Gondangdia, Central Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.1934784,106.82219699999996">Plaza Indonesia</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Senayan City, Gelora, Central Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.2274387,106.79682030000004">Senayan City</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Plaza Senayan Mall, Jalan Asia Afrika, Gelora, Central Jakarta City, Special Capital Region of Jakarta, Indonesia" latlong="-6.225580899999999,106.7999552">Plaza Senayan</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Pacific Place, Special Capital Region of Jakarta, Indonesia" latlong="-6.2256158,106.81013480000001">Pacific Place</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Plaza Blok M, Special Capital Region of Jakarta, Indonesia" latlong="-6.244215899999999,106.79744879999998">Plaza Blok M</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Plaza Semanggi, Indonesia" latlong="-6.2197318,106.81447270000001">Plaza Semanggi</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Cilandak Town Square, Special Capital Region of Jakarta, Indonesia" latlong="-6.291561199999999,106.79967590000001">Cilandak citos</a>
				<!-- SAMPLE
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="">Place</a>
				-->
			</div>
			<div class="col-sm-2 b">
				Luar Jakarta<br/>
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="AEON MALL BSD CITY, Pagedangan, Tangerang, Banten, Indonesia" latlong="-6.3042148,106.64317529999994">Aeon mall</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Mall @ Alam Sutera, Banten, Indonesia" latlong="-6.222299800000001,106.65363200000002">Alam sutera</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Bintaro Jaya Xchange Mall, Pondok Jaya, South Tangerang City, Banten, Indonesia" latlong="-6.2854085,106.7280763">Bintaro Xchange</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="ikea near Alam Sutera, Jalan Jalur Sutera, Kunciran, Tangerang City, Banten, Indonesia" latlong="-6.2196963,106.66324080000004">IKEA</a>	
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Summarecon Mal Serpong, West Pakulonan, Tangerang, Banten, Indonesia" latlong="-6.241028699999999,106.62817010000003">Summarecon Serpong</a>
				
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="Summarecon Mal Bekasi, Marga Mulya, Bekasi City, West Java, Indonesia" latlong="-6.226629399999998,107.00157850000005" >Summarecon Bekasi</a>
				
				
				
				
				<!-- SAMPLE
				<a class="btn btn-xs btn-info br triggerLocation" href="javascript:void(0)" value="">Place</a>
				-->
			</div>
			<div class="col-sm-2 b">
				Help<br/>
				<a class="btn btn-sm btn-warning br" href="javascript:void(0)" onclick="resetbox()">Reset all textbox</a>
			</div>
			
			
			<div class="col-sm-12">
				<hr/>
				<div class="col-sm-7">
					Start: <a class="btn btn-xs btn-success br" href="javascript:void(0)" onclick="detect_location()" alt="Click for autodetect location" title="click for autodetect location">Detect Location</a> | 
					<a class="btn btn-xs btn-warning br" href="javascript:void(0)" onclick="reset_source()">reset box</a>
					<br/>
					<input type="text" id="txtSource" class="input br wdtFul" value="" placeholder="Dari" /><br/>
					Destination: <a class="btn btn-xs btn-warning br" href="javascript:void(0)" onclick="reset_destination()">reset box</a> <br/>
					<input type="text" id="txtDestination" class="input br wdtFul" value="" placeholder="Tujuan" />
					<input type="hidden" id="f_lat" />
					<input type="hidden" id="f_lng" />
				</div>
				<div class="col-sm-5">
					<br/>
					<input type="checkbox" class="input" id="is_avoidtoll" checked /> <label for="is_avoidtoll">avoid toll</label><br/><br/>
					<input type="button" value="Get Route" onclick="GetRoute()" class="btn btn-success" />
					<br/>
					<br/>
				</div>
				
				<div class="col-sm-12">
					
				</div>
				<br/>
			</div>
	
		</div>
	</div>
</div>

<div class="">
	<div class="">
		<div class="row">
			<div class="col-sm-12">
				<div id="dvDistance">

				</div>

				<div id="dvMap" style="width: 100%; height: 600px;overflow-y:auto; padding:0">

				</div>
				<div id="reticule"><img src="<?php echo base_url()?>asset/images/target_point.png" /></div>
			</div>
		</div>
	</div>
</div>