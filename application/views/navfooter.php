<?php
$asset_url = base_url().'asset/images/';
$year_now = date('Y',time());
?>
<!--
<div class="jumbotron bgBluLight" style="margin-bottom:0;padding:35px 10px">
	<div class="container">
		<div class="col-md-3 lnkLogo">
		<a class="fntNml"><i class="fa fa-comments fa-2x"></i> <span class="clrWht">Navigation</span></a><br/>

			<a href="<?php echo base_url()?>about">About <?php echo show_active_menu('about')?></a><br/>
			<a href="<?php echo base_url()?>contact">Contact <?php echo show_active_menu('contact')?></a>
		</div>
		
		<div class="col-md-3 lnkLogo">
		<a class="fntNml"><i class="fa fa-comments fa-2x"></i> <span class="clrWht">Navigation</span></a><br/>
		<a class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i>&nbsp;</a>
		<a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i>&nbsp;</a>
		</div>
		<div class="col-md-6 talCnt lnkWht"></div>
		<div class="clearfix"></div>
	</div>
</div>
-->

<!--
<?php if ($this->uri->segment(2) != 'admin') { ?>
<div class="jumbotron" style="margin-top:0;margin-bottom:0;padding:20px 0 10px 0">	
	<div class="container clrBlk">
		<div class="row">
			<div class="col-sm-3 talCnt">Follow Grevia :
				<a href="https://www.facebook.com/greviacom" title="Facebook Grevia" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a>&nbsp; &nbsp;
				<a href="https://twitter.com/greviacom" title="Twitter Grevia" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>&nbsp; &nbsp;
				<a href="https://www.instagram.com/grevianetworks" title="Instagram Grevia" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>&nbsp; &nbsp;
			</div>
			<div class="col-sm-9 talCnt">
				<div class="" id="divSubscribe">
					<div class="col-sm-4 br"><i class="fa fa-email-round"></i> Daftar Email anda ke newsletter.</div>
					<div class="col-sm-6 br"><input class="input wdtFul" type="text" name="t_email_subscribe" id="t_email_subscribe" placeholder="Email anda..."/>
					</div>
					<div class="col-sm-2 br"><button class="btn btn-info btn-sm subscribe wdtFul">Submit</button></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="jumbotron bgTopFooter" style="margin-bottom:0; padding-bottom:0; padding: 25px 0 1px 0">
	<div class="container clrLgtGry" style="padding: 10px">
		<div class="col-sm-2 talLft lnkBlk">
			<img class="img-responsive" style="max-height:120px" src="<?php echo base_url()?>asset/images/logo-grevia.png"/>
		</div>
		<div class="col-sm-3 talLft ">
	Grevia adalah situs yang menyediakan artikel dan berbagai tips seputar dunia startup, digital kreatif dan teknopreneur untuk menciptakan lebih banyak kreatifpreneur di Indonesia. <br/><br/>
		</div>
		
		<div class="col-sm-4 f12 lnkLgtGry talCnt" style="padding-left:25px">
		<a href="https://www.facebook.com/greviacom" title="Facebook Grevia" target="_blank"><i class="fa fa-facebook-official fa-3x"></i></a>&nbsp; &nbsp;
		<a href="https://twitter.com/greviacom" title="Twitter Grevia" target="_blank"><i class="fa fa-twitter fa-3x"></i></a>&nbsp; &nbsp;
		<i class="fa fa-linkedin fa-3x"></i>
		</div>
		<div class="col-sm-1 talLft clrWht"></div>
		<div class="col-sm-3 talLft clrWht">
			<div class="well well-sm clrBlk" id="divSubscribe">
				<div class="br">Daftar Email anda ke newsletter</div>
				<input class="input br wdtFul" type="text" name="t_email_subscribe" id="t_email_subscribe" placeholder="Email anda..."/><br/>
				<button class="btn btn-info wdtFul subscribe">Submit</button>
			</div>
			<br/>
			<a href="<?php echo current_url().'?lang=en'?>"/>English</a>
			<a href="<?php echo current_url().'?lang=id'?>"/>Indonesian</a>
			
		</div>
	</div>
</div>
-->
<?php } ?>
<section class="" style="margin-bottom:0; padding: 10px 0 35px 0;background:#343a3d">
	<div class="container lnkLogo fntMed">
		<div class="col-sm-12 talCnt">	
			<div class="col-sm-4 clrWht talJst">
				<h1 class="f18 b padNon">Jasa pembuatan website terpercaya</h1>
				Grevia adalah situs pembuatan jasa webdesign & media online terpercaya semenjak tahun 2011. Melayani pembuatan custom website dan company profile dengan kualitas profesional.

				<hr style="margin:10px 0 10px 0;"/>
				

				<span class="b">Grevia Networks</span><br/>
				<i class="fa fa-map-marker"></i> Jl. Kepu dalam 6 no 134 B<br/>
				Kemayoran - Jakpus 10620<br/>
				<i class="fa fa-whatsapp"></i> 0838.9199.8825<br/><br/>
			</div>
			<div class="col-sm-2 clrWht talLft line-height-180">
				<h2 class="padNon f18 b text-uppercase soft-orange">HOT</h2>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title="Pilih Paket web murah dari Grevia"><?php echo "Paket web murah"?></a><br/>
				<a href="<?php echo base_url()?>promo?ref=ftr" title="Menu Home"><?php echo 'Promo'; ?></a><br/>
				<a href="https://api.whatsapp.com/send?phone=6283891998825&text=Halo Admin Saya mau order website di Grevia" title="Konsultasi Gratis di Whatsapp"><?php echo 'Konsultasi Gratis'; ?></a><br/>
				<a href="<?php echo base_url()?>kamus-startup?ref=ftr" title="Kamus Startup"><?php echo 'Kamus Startup'; ?></a><br/>
			</div>

			<div class="col-sm-2 clrWht talLft line-height-180" >
				<h2 class="padNon f18 b text-uppercase soft-orange">PRODUCT</h2>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Company Profile"?></a><br/>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Wedding"?></a><br/>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Landing Page"?></a><br/>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Manage Webservice"?></a><br/>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Pembuatan API"?></a><br/>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Custom Web"?></a><br/>
				<a href="<?php echo base_url().'webdesign'?>?ref=ftr" title=""><?php echo "Konsultasi IT"?></a><br/>
				
				<br/>
			</div>

			<div class="col-sm-2 clrWht talLft line-height-180">
				<h2 class="padNon f18 b text-uppercase soft-orange">MENU</h2>
				<!--
					<a href="<?php echo base_url()?>?ref=ftr" title="Menu Home"><?php echo MENU_HOME?></a><br/>
				<a href="<?php echo base_url().'career'?>?ref=ftr" title="Halaman Karir dengan Grevia"><?php echo MENU_CAREER?></a><br/>
				-->
				<a href="<?php echo base_url().'about'?>?ref=ftr" title="Halaman Tentang Grevia"><?php echo MENU_ABOUT?></a><br/>
				<a href="<?php echo base_url().'portofolio'?>?ref=ftr" title="Portofolio web dari klien kami."><?php echo MENU_PORTOFOLIO?></a><br/>

				<a class="clrBlu" href="<?php echo base_url().'help'?>?ref=ftr" title="Halaman Knowledge Base Grevia">Bantuan</a><br/>
				<a class="clrBlu" href="<?php echo base_url().'tutorial'?>?ref=ftr" title="Halaman Tutorial Grevia">Tutorial</a><br/>
				<a class="clrBlu" href="<?php echo base_url().'tools'?>?ref=ftr" title="Halaman Tools Grevia">Tools</a><br/>
				
			</div>
			<!--
			<div class="col-sm-1 clrWht">
				<a href="<?php echo base_url().'#terms'?>"><?php echo MENU_TERMS?></a>
			</div>
			<div class="col-sm-1 clrWht">
				<a href="<?php echo base_url().'#faq'?>"><?php echo MENU_FAQ?></a>
			</div>
			<div class="col-sm-1 clrWht">
				<a href="<?php echo base_url().'#privacy'?>"><?php echo MENU_PRIVACY?></a>
			</div>
			-->
			
			
			
			<div class="col-sm-2 clrWht talLft line-height-180">
				<h2 class="padNon f18 b text-uppercase soft-orange">INFO</h2>
				<a href="<?php echo base_url().'contact'?>?ref=ftr" title="Halaman Kontak Grevia"><?php echo MENU_CONTACT?></a><br/>				
				<a href="<?php echo base_url().'cara_order'?>?ref=ftr" title="Cara Order web di Grevia"><?php echo "Cara Order"?></a><br/>			
				<a href="<?php echo base_url().'faq'?>?ref=ftr" title="FAQ / Pertanyaan yang sering ditanyakan"><?php echo MENU_FAQ?></a><br/>
				<a href="<?php echo base_url().'aturan_pengguna'?>?ref=ftr" title="Halaman tentang aturan pengguna"><?php echo "Aturan pengguna"?></a><br/>
				<a class="clrBlu" href="<?php echo base_url().'terms_condition'?>?ref=ftr" title="Syarat Ketentuan Grevia">Syarat Ketentuan</a><br/>
				<a class="clrBlu" href="<?php echo base_url().'career'?>?ref=ftr" title="Karir Grevia">Karir</a><br/>
			</div>
			
		</div>
		
			<!-- <br/>Page rendered in <strong>{elapsed_time}</strong> seconds {memory_usage}-->
		
		<!--
		<div class="col-sm-12 clrWht talCnt">
			
			<a href="<?php echo base_url()?>about">About<?php echo show_active_menu('about')?></a>
			&nbsp;<a>|</a>&nbsp;
			<a href="<?php echo base_url()?>contact">Contact<?php echo show_active_menu('contact')?></a>
			<br/>
			
		</div>
		-->
	</div>
</section>
<div class="jumbotron" style="margin-bottom:0;padding:20px 0 20px 0; background-color:#3e4447">	
	<div class="container">	
		<div class="col-sm-12 clrWht talRgt" >
			<div class="nohover"><span class="fntNml">Copyright &copy; 2011 - <?php echo $year_now?>. Grevia Networks</span></div>
		</div>
	</div>
</div>

<script>

function show_balloon(str,sec = 5, css = 'info')
{
	// Set modal info
	$('#balloon_content').html(str);
	$('#balloon').addClass('alert alert-' + css);
	$('#balloon').fadeIn()
	.css({right:-400, top:'50%'})
	.animate({right:15, top:'50%'}, 1000, function(){
		// callback
		
	});
	
	window.setTimeout(function() {
		$('#balloon').fadeIn()
		.animate({right:-400}, 1000, function(){
			
		});
	}, sec*1000);
}

function show_balloon_bottom(str,sec = 5, css = 'info')
{
	// Set modal info
	$('#balloon_content').html(str);
	$('#balloon').addClass('alert alert-' + css);
	$('#balloon').fadeIn()
	.css({left:-400, top:'90%'})
	.animate({left:15, top:'90%'}, 1000, function(){
		// callback
		
	});
	
	window.setTimeout(function() {
		$('#balloon').fadeIn()
		.animate({left:-400}, 1000, function(){
			
		});
	}, sec*1000);
}

// $(document).ready(function(){
	// show_balloon('ayam',12);
// });

</script>

<div id="balloon" class="" style="width:300px; position:fixed" >
	<span class="close" data-dismiss="alert">&times;</span>
	<span id="balloon_content"></span>
</div>

<script>
function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
		return false;
	}else{
		return true;
	}
  }
  
function togglebox(){
	if ($('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',true);
	}
	if (!$('.togglebox').prop('checked')) {
		$('.chkbox').prop('checked',false);
	}
}

function confirmDelete() {
	return confirm('<?php DELETE.' '. DATA.'. '.SELECTED ?> ?');	
}

function doConfirm() {
	return confirm('<?php echo CONFIRM.' '.ACTION?> ?');	
}

$(".subscribe").click(function(){
	var the_email = $('#t_email_subscribe').val();
	if (IsEmail(the_email)) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>ajax",
			data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=subscribe&email=" + the_email,
			cache: false,
			success: function(resp) {
				if (resp == "<?php echo INFO_SAVE_SUCCESS?>") {
					$('#divSubscribe').html('<div class=""><i class="fa fa-info"></i> &nbsp;<?php echo INFO_EMAIL_SUBSCRIBE_SUCCESS?></div>');
				} else {
					if (resp == "<?php echo INFO_EMAIL_SUBSCRIBE_EXIST?>") {
						$('#t_email_subscribe').focus();
					} 
					alert(resp);
				}
			}
		});
	} else {
		$('#t_email_subscribe').focus();
		alert('Email tidak valid.');
	}
});

$(document).ready(function(){ 
  // TRACK URL
	$('.adsTrack').each(function(){
		var target_url = $(this).attr("href");
		//target_url = target_url.replace("<?php echo base_url()?>","");
		target_url = encodeURIComponent(target_url);
		var source = $(this).closest('div').attr('source');
		var segment = $(this).closest('div').attr('segment');
		$(this).bind("mousedown", function(event){
			var name = $(this).attr("tracker_name");
			$(this).attr("href","<?php echo base_url()?>track?name="+name+"&source="+source+"&segment="+segment+"&url=" + target_url);
			
			
		});
		
		// $(this).bind("mousedown", function(event){
			// var name = $(this).attr("tracker_name");
			// $(this).attr("href","<?php echo base_url()?>track?name="+name+"&source="+source+"&segment="+segment+"&url=" + target_url);
			
			
		// });
		
		// document.oncontextmenu = function() {return false;};
		// $(document).mousedown(function(e){ 
		// if( e.button == 2 ) { 
			// alert('Right mouse button!'); 
			// return false; 
		// } 
			// return true; 
		// }); 
	});
	
});

</script>