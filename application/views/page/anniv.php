<?php 
date_default_timezone_set('Asia/Tokyo');  // you are required to set a timezone
$jd = '2015-7-27';
$now_day = date('d',time());

if ($now_day == date('d', strtotime($jd))) echo "<hr>congratz";

$date1 = new DateTime(date('Y-m-d',time()));
$date2 = new DateTime($jd);

$diff = $date1->diff($date2);

$happy_anniv = 'Happy anniv '.(($diff->format('%y') * 12) + $diff->format('%m')) . " months + ".$diff->format('%d')." per ".date('t',time())." days";
?>

<script type="text/javascript" src="<?php echo base_url()?>asset/plugin/countdownTimer/jquery-2.0.3.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>asset/plugin/countdownTimer/jquery.countdownTimer.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>asset/plugin/countdownTimer/jquery.countdownTimer.css" />

<?php echo $happy_anniv ?>

<hr/>

Countdown Married<br/><br/>
<div id="countdowntimer"><span id="future_date"><span></div>
 
<script type="text/javascript">
	$(function(){
		$('#future_date').countdowntimer({
			dateAndTime : "2016/12/18 19:00:00",
			size : "md",
			borderColor : "#FA0909",
			//backgroundColor:,
			regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
      		regexpReplaceWith: "$1<sup>days</sup> / $2<sup>hours</sup> / $3<sup>minutes</sup> / $4<sup>seconds</sup>"
		});
	});
</script>