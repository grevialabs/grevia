<?php 
if ($_POST)
{
	$post = $_POST;
	if (isset($post['command']) && $post['command'] != '')
	{
		$command = strtolower(trim($post['command']));
		
		$arr_answer[] = array(
			'command' => array('hi','hai','wassap','hei'),
			'response' => array('hi too','wassap bro','hi lovely','hai, ni hao','hi there','cool man, hi')
		);
		
		$arr_answer[] = array(
			'command' => array('bye','amigos','sayonara'),
			'response' => array('Bye, have a great day.','Bye :)','Amigos sombreros :)','See you soon.','cheers mate :D')
		);
		
		$message = $prev_random = NULL;
		// save random no before
		if (isset($post['prev_random'])) $prev_random = $post['prev_random'];
			
		foreach ($arr_answer as $ans)
		{
			// match
			if (in_array($command,$ans['command']) !== FALSE)
			{
				$random_order = NULL;
				$random_order = rand(0,count($ans['response'])-1);
				
				// if random number AND then create
				if ($random_order == $prev_random)
				{
					// 5 != 6 
					// echo $random_order .' != '.count($ans['response'])-1;
					// die;
					if ($random_order != count($ans['response'])-1) 
					{
						if ($random_order == 0) 
							$random_order = rand(1,count($ans['response']) - 1);
						else 
							// 1,2,3,4,5 => 
							$random_order = rand(1,$random_order - 1);
					}
					else
					{
						$random_order = rand(0,$random_order - 2);
					}
				}
				$message = $ans['response'][$random_order];
				break;
			}
		}
		
		if ( ! isset($message)) $message = 'Sorry master, robot don"t know your command';
	}
	else 
	{
		$message = 'Please say your command, master.';
	}
}
?>
<form method="post">
	<input type="text" name="command" placeholder="Ketikan command kamu..." />
	<input type="submit" />
	<div>
		<br/>
		coba ketikan : <b>hi</b> atau <b>bye</b> untuk lihat respon robot.
		<input type="hidden" name="prev_random" value="<?php if (isset($prev_random)) echo $prev_random ?>" />
	</div>
	<hr/>
	<div>
		<?php if (isset($message)) echo $message ?>
	</div>
</form>