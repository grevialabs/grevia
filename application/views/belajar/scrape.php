<?php 
// formula = format string to scrape
function ambilkata($source, $formula,$buka = '<td>',$tutup = '</td>')
{
	$tmp = $tmp2 = $ret = NULL;
	
	$tmp = $source;
	$tmp = explode($buka.$formula.$tutup,$tmp);
	if (isset($tmp[1])) {
		$tmp2 = explode($tutup,$tmp[1]);
		
		$ret = $tmp2;
		if (isset($tmp2[0])) {
			$ret = $tmp2[0];
			// remove prefix depan
			$ret = str_replace($buka,'',$ret);
		}
	}
	// debug($ret);
	// die;
	$ret = trim($ret);
	return $ret;
}

function ambil($url,$arr_formula)
{
	$ret = $tmp = $tmp2 = NULL;
	
	if (!isset($url)) return 'param url required';
	// if (!isset($buka)) return 'param buka required';
	// if (!isset($tutup)) return 'param tutup required';
	
	// content_1
	$content = hit($url);
	
	// $content = trim($content);
	$content = preg_replace('/\s{2,}+/', '',$content);
	
	$arr_replace = array(' width="15"',' width="450"',' width="10"',' width="135"',' width="500"','-');
	$arr_replace[] = '<td>:</td>';
	$content = str_replace($arr_replace,'',$content);
	
	if (!empty($arr_formula))
	{
		foreach ($arr_formula as $formula) 
		{
			// -----------------------------------------------
			// START HERE
			$buka = '<td>';
			$tutup = '</td>';
			$bukatutup = $buka.$formula.$tutup;
			
			$tmp = explode($bukatutup,$content);
			// debug($tmp);
			// die;
			if (isset($tmp[1])) {
				$tmp2 = explode($tutup,$tmp[1]);
				
				$ret = $tmp2;
				// debug($ret);
				// die;
				if (isset($tmp2[0])) {
					$ret = $tmp2[0];
					$ret = str_replace($buka,'',$ret);
					
					$formula = strtolower(str_replace(' ','_',$formula));
					// $result[] = array($formula => 'ayam');
					// $result[] = array($formula => ambilkata($ret,$formula));
					$result[] = array($formula => $ret);
					// $result[] = $ret;
				}
			}
			
			
			// checkpoint
			// get key by next tr
			// Jumlah Kondisi Pustu Baik

			// foreach ($arr_formula as $key => $sf) 
			// {
				// $result[] = array( $sf => ambilkata($ret,$sf));
			// }
			// $result = json_encode();
			// END HERE
			// -----------------------------------------------
		}
		// debug($result);
		// die;
	}	
	return $result;
}

function hit($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
	//curl_setopt($ch, CURLOPT_GET, true);	
	//curl_setopt($ch, CURLOPT_POST, true);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, "username=XXXXX&password=XXXXX");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$answer = curl_exec($ch);
	if (curl_error($ch)) {
		echo curl_error($ch);
	}
	return $answer;
}

$arr_string_formula = array(
	'Nama Puskesmas',
	'Kode Puskesmas',
	'Alamat',
	'Jenis Puskesmas',
	'Telp / Fax',
	'Status Poned',
	
	'Kondisi Bangunan Puskesmas',
	'Kondisi Bangunan Rumah Medis',
	'Jumlah Kondisi Pustu Baik',
	'Jumlah Kondisi Pustu Rusak Ringan',
	'Jumlah Kondisi Pustu Rusak Sedang',
	'Jumlah Kondisi Pustu Rusak Berat',
	
	'Jumlah Dokter Umum',
	'Jumlah Dokter Spesialis',
	'Jumlah Dokter Gigi',
	'Jumlah Perawat',
	'Jumlah Perawat Gigi',
	'Jumlah Bidan',
	'Jumlah Farmasi',
	'Jumlah Asisten Farmasi',
	'Jumlah Tenaga Gizi',
	'Jumlah Tenaga Kesehatan Lingkungan',
	'Jumlah Tenaga Kesehatan Masyarakat',
	'Jumlah Tenaga Keterapian Fisik',
	'Jumlah Tenaga Keteknisian Medis',
	'Jumlah Tenaga Analis Kesehatan',
	'Jumlah Tenaga Non Kesehatan',
	
	'Kondisi Ambulan Baik',
	'Kondisi Ambulan Rusak Ringan',
	'Kondisi Ambulan Rusak Sedang',
	'Kondisi Ambulan Rusak Berat',
	'Kondisi Sepeda Motor Baik',
	'Kondisi Sepeda Motor Rusak Ringan',
	'Kondisi Sepeda Motor Rusak Sedang',
	'Kondisi Sepeda Motor Rusak Berat',
	'Kondisi Kendaraan Lain Baik',
	'Kondisi Kendaraan Lain Rusak Ringan',
	'Kondisi Kendaraan Lain Rusak Sedang',
	'Kondisi Kendaraan Lain Rusak Berat',
	'Kondisi Pusling Roda 4 Baik',
	'Kondisi Pusling Roda 4 Rusak Ringan',
	'Kondisi Pusling Roda 4 Rusak Sedang',
	'Kondisi Pusling Roda 4 Rusak Berat',
	'Kondisi Pusling Air Baik',
	'Kondisi Pusling Air Rusak Ringan',
	'Kondisi Pusling Air Rusak Sedang',
	'Kondisi Pusling Air Rusak Berat',
	
	'Listrik',
	'Air',
	
	'Posyandu Mandiri',
	'Posyandu Purnama',
	'Posyandu Madya',
	'Posyandu Pratama',
	'Kelompok Peduli Kesehatan',
	'Dana Sehat',
	'Saka Bakti Husada',
	'Desa Siaga',
	'Pos UKK',
	'Pos Obat Desa',
	'Posyandu Lansia',
	'Poskestren',
	'Polindes',
	// '',
	// '',
	// '',
	// '',
	// '',
);
$url = 'http://siknasonline.depkes.go.id/gis/puskesmas_detail.php?id=P1101051101';
// $content = ambil($url,$arr_string_formula);
$content = ambil($url,$arr_string_formula);
debug($content);
die;
?>