<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-animation.min.css?v=20160708"/>

<!-- Include required JS files -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/shCore.js?v=20180110"></script>

<script type="text/javascript" src="<?php echo base_url()?>asset/js/shAutoloader.js?v=20180110"></script>
<script type="text/javascript" src="<?php echo base_url()?>asset/js/shBrushPhp.js?v=20180110"></script>
 
<!-- Include *at least* the core style and default theme -->
<link href="<?php echo base_url()?>asset/css/shCore.css?v=20180110" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>asset/css/shThemeDefault.css?v=20180110" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css?v=20180110' />

<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=20160708"/>
<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->

<link href="<?php echo base_url()?>asset/css/font-awesome-4.6.3.min.css?=20160708" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css?v=20180110"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css?v=20180110"/>

<div class="panel">
	<a href="<?php echo base_url().'belajar/scrapetwt';?>">Scrape twitter profile</a> | 
	<a href="<?php echo base_url().'belajar/scrapetwtpost';?>">Scrape Twitter post</a> |
	<a href="<?php echo base_url().'belajar/scrapeimage';?>">Scrape Image</a>
</div>

<?php 
$uri = NULL;
if (isset($_GET['uri'])) $uri = $_GET['uri'];
	?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12" style="margin-top:250px">
				<h3>Scrape post twitter</h3>
				<form method="get" action>
					Masukan uri twitter: contoh: https://twitter.com/alamudin_dr/status/1037341943770963970<br/>
					<input type="text" name="uri" value="<?php if ( isset($uri)) echo $uri ?>" class="input br wdtFul" />
					<input type="submit" class="btn btn-info btn-md"/>
				</form>
			</div>
		</div>
	</div>
	<?php


if (! isset($uri) || $uri == '') return '<hr/>Url harus diisi';
	
// https://twitter.com/ARLJ4

// formula = format string to scrape
function ambilkata($source, $formula,$formula2 = NULL)
{
	$tmp = $tmp2 = $ret = NULL;
	
	$tmp = $source;
	// $tmp = explode($buka.$formula.$tutup,$tmp);
	$tmp = explode($formula,$tmp);
	// debug($tmp,1);
	if (isset($tmp[1])) {
		$tmp2 = explode($formula2,$tmp[1]);
		
		$ret = $tmp2;
		if (isset($tmp2[0])) {
			$ret = $tmp2[0];
			// remove prefix depan
			// $ret = str_replace($buka,'',$ret);
		}
	}
	// debug($ret);
	// die;
	$ret = trim($ret);
	return $ret;
}

function hit($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
	// curl_setopt($ch, CURLOPTGET, true);	
	//curl_setopt($ch, CURLOPT_POST, true);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, "username=XXXXX&password=XXXXX");
	
	// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);     // return web page
	curl_setopt($ch, CURLOPT_HEADER         , false);    // don't return headers
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);     // follow redirects
	curl_setopt($ch, CURLOPT_ENCODING       , "");       // handle all encodings
	curl_setopt($ch, CURLOPT_USERAGENT      , "spider"); // who am i
	curl_setopt($ch, CURLOPT_AUTOREFERER    , true);     // set referer on redirect
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 120);      // timeout on connect
	curl_setopt($ch, CURLOPT_TIMEOUT        , 120);      // timeout on response
	curl_setopt($ch, CURLOPT_MAXREDIRS      , 10);       // stop after 10 redirects
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false);     // Disabled SSL Cert checks
	
	$resp = curl_exec($ch);
	if (curl_error($ch)) {
		echo curl_error($ch);
	}
	return $resp;
}

/*
// URL
// https://twitter.com/ARLJ4

// tweet 
<span class="u-hiddenVisually">Tweet, halaman saat ini.</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// following
<span class="u-hiddenVisually">Following</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>
<span class="u-hiddenVisually">Mengikuti</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// follower
<span class="u-hiddenVisually">Follower</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>
<span class="u-hiddenVisually">Pengikut</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// Like
<span class="u-hiddenVisually">Like</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>
<span class="u-hiddenVisually">Suka</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// location
<span class="ProfileHeaderCard-locationText u-dir" dir="ltr">
              <a href="/search?q=place%3Ace7988d3a8b6f49f" data-place-id="ce7988d3a8b6f49f">Indonesia</a>

        </span>

// image 
<img class="ProfileAvatar-image " src="https://pbs.twimg.com/profile_images/680265953028866049/QB1xrTbm_400x400.jpg" alt="ARLJ">

ambilkata
*/

$uri = $uri.'?lang=id';
$content = hit($uri);

// $content = $source;
$content = preg_replace('/\s{2,}+/', '',$content);
$tutup = ' data-is-compact';

// Get tweetpost from url tweet post
$tutup = '</p>';
$tweet = '<p class="TweetTextSize TweetTextSize--jumbo js-tweet-text tweet-text" lang="in" data-aria-label-part="0">';
$out['tweet'] = ambilkata($content,$tweet,$tutup);

// debug($content);
debug('<hr/>');
debug($out);
die;
?>