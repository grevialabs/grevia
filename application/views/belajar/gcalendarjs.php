<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css?v=1">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js?v=1"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js?v=1"></script>

<!-- <script src="<?php echo base_url().'asset/js/'; ?>gcalendar.js?v=1"></script>
-->

<!-- START HERE -->
<script>
(function ( $ ) {
 
    $.fn.gcalendar = function( options ) {
 
        // Default options
        var settings = $.extend({
            // setId: 'gcalendar', // div id of calendar
            setShowWeek: true, // show week on left table
            setYear: '2018', // year 
            setFirstDay: 'monday', // monday / sunday
        }, options );
 
        // Apply options
        // return this.append('Hello ' + settings.name + '!');
        // alert(settings.setFirstDay);

        var str = '';
        //var year = 2018;
        var showWeek = settings.setShowWeek;
        var year = settings.setYear;
		var week = 1;

        var date = new Date();
        var months = ["January","February","March","April","May","June","July","August","September","October","November","Desember"];
        var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

        str += 'Tahun ' + year + '<br/>';
        //str += '<br/><table class="table table-bordered">';

        for (month = 1; month<=12; month++) {
			
			//var lastmonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var dayperweek = 7;

            // set firstday calendar start from 0 = Sunday, 1 Monday
            var firstDay = 0;
            //var tgl;
            if (settings.setFirstDay == 'monday') {
                firstDay = 1;
            }
            if (settings.setFirstDay == 'sunday') firstDay = 0; 

            // str += "<div class='col-sm-12'>";
			str += '<div>';
            str += "Bulan " + months[month-1] + "<br/>";
            str += '<table class="table table-bordered">';

            str += '<tr>';

            if (showWeek) {
				str += '<td class="b" width="1" style="text-align:center">W</td>';
			}
            for (tgl=1;tgl<=7;tgl++) {
                tglkey = tgl;

                // firstDay not SUNDAY (which is monday)
                if (firstDay != 0) {
                    if (tgl == 7) {
                        tglkey = 0;
                    }

                } else {
                    tglkey = tgl-1;
                    //console.log('tglkey: ' + tglkey);
                }

                str += '<td class="b gch-' + days[tglkey].substr(0,3).toLowerCase() + '" style="text-align:center">' + days[tglkey] + '</td>';
                
            }

            //var lastday = new Date(year, month, 0);
            var lastday = new Date(year, month, 0).getDate();
            // lastday
            var isNewrow = true;
            var cols = 1;
            var isStart = false;
			
            str += '<tr>';

			for (day=1;day<=lastday;day++) 
			{
				var showWeekFlag = false;
                
                // Return 0 = Sunday, 1 = Monday, etc
                var dayName = new Date(month + "/" + day + "/" + year).getDay();
                var dayshortName = days[dayName].substr(0,3).toLowerCase();
				
                if (day == 1 && isNewrow == true) {
                    str += '<tr>';
					
					if (showWeek) {
						str += '<td class="" style="text-align:center">' + week + '</td>';
						showWeekFlag = true;
					}
                }

                var bgcolor = ' ';
                
                // check if 
                if (day == 1) {
                    //console.log('dayname ' + dayName + ' firstDay ' + firstDay);
                    // check dayname start not MONDAY
                    if (dayName != firstDay) {
                        
                        // add empty cols to table
                        for (tgl=1;tgl<=7;tgl++) {

                            tglkey = tgl;
                            // firstDay is not SUNDAY
                            if (firstDay != 0) {
                                if (tgl == 7) tglkey = 0;

                            } else {
                                // firstDay is sunday
                                tglkey = tgl-1;
                                //if (tgl == 7) tglkey = 0;
                                
                            }
							
							// Print day one only
                            if (tglkey == dayName) {

                                str += "<td class='" + bgcolor + " gcd-" + dayshortName + "' style='text-align:center'><span class='" + dayshortName + "' id='" + day + "-" + month + "-" + year + "'>" + day + "</span></td>";
                                
                                if (cols == dayperweek) {
                                    cols = 0;
									week++;
                                    str += '</tr>';
                                }
                                break;
                            } else {			

                                // Print empty data
								var cs = 'gcd-' + days[cols].substr(0,3).toLowerCase();
								if (cols == "sat" || cols == "sun") cs += ' weekday'; 
								else cs += ' weekend';
								str += "<td class='" + bgcolor + " " + cs + "'>&nbsp;</td>";
                                
                                if (cols == dayperweek) {

                                    cols = 0;
									week++;
                                    str += '</tr>';
                                }
                            }
                            cols++;
                            
                        }
                        
                        isStart = false;
                        isNewrow = false;
                        day = 1;
						
                    } else {
                        isStart = true;
                    }

                } else {
                    isStart = true;
                }
                
                // Print dates greater than 1 only
				if (isStart) {

					if (week > 52) week = 1;
					
					if (cols == 1 && showWeek) {
						if (!showWeekFlag) {
							str += '<td class="" style="text-align:center">' + week + '</td>';
							// week++;
							showWeekFlag = true;
						}
					}
					
                    str += "<td class='" + bgcolor + " gcd-" + dayshortName + "' style='text-align:center'><span id='" + day + "-" + month + "-" + year + "'>" + day + "<span><br/><span class='gcc" + dayshortName + "'></span></td>";
					
					// Print empty space with 
					if (day == lastday) {
						// str += '<td>' + cols + '</td>';
						// if (cols != 7 && ) {
							var sisaday = 7 - cols;
							for(sisa=1;sisa<=sisaday;sisa++) {
								str += '<td class="bgGry clrWht">' + sisa + '</td>';
							}
						// }
					}
                    
                    if (cols == dayperweek) {
                        cols = 0;
						week++;
                        str += '</tr>';
                    }
                }
                
                cols++;
            }
            str += "</table>";
            str += "</div>";
        }

        //str += "$('table tr').find('td:first').addClass('bg-danger');";
        //str += "$(document).ready(function(){";
        //str += "    $('table tr').find('td:first').addClass('bg-danger');";
        //str += "    $('table tr').find('td:nth-child(7)').addClass('bg-warning');";
        //str += "});";
        //$('#calendar').html(str);
        return this.append(str);

 
    };
 
}( jQuery ));
</script>
<!-- START HERE -->
<script>


$(document).ready(function(){
    //alert('bisanih');

    $('#gcalendar').gcalendar({
        // setYear: '2017',
        // setFirstDay: 'monday',
    });

    // if firstday sunday
    //$('table tr').find('td:first').addClass('bg-danger');
    //$('table tr').find('td:nth-child(7)').addClass('bg-warning');

    // if firstday monday
    // $('table tr').find('td:nth-child(7)').addClass('bg-danger');
    // $('table tr').find('td:nth-child(7)').addClass('bg-danger');
	
    $('.gch-sat, .gcd-sat').addClass('bg-danger');
    $('.gch-sun, .gcd-sun').addClass('bg-warning');
    $('.gc-today').addClass('bg-warning');
});

</script>
<!—- END HERE —->
  </head>
  <body>
    <div class="container">
        <div class="rows">
            <div id="gcalendar"></div>
        </div>
    </div> 
  </body>
<style>
.b{font-weight:bold}
.clrWht{color:white}
.clrGry{color:grey}
.bgGry{background:grey}
</style>
</html>