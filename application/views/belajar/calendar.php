<?php 
if ($_POST) 
{
	$post = $_POST;
	if (isset($post['do']) && $post['do'] == "save")
	{
		$save = $param = NULL;
		// $param = array(
			// 'input_date' => date('Y-m-d'),
			// 'notes' => 'tes pertama',
			// 'creator_id' => '1',
			// 'creator_date' => date('Y-m-d H:i:s'),
		// );
		$param = array(
			'input_date' => $post['input_date'],
			'notes' => $post['notes'],
			'creator_id' => '1',
			'creator_ip' => '1',
			'creator_date' => date('Y-m-d H:i:s'),
		);
		// var_dump($param);
		// die;
		$save = $this->db->insert('grv_calendar',$param);
		
		if ($save) 
			echo "success";
		else
			echo "failed";
		die;
	}
}

$query = $data = NULL;
$query = "SELECT * FROM grv_calendar WHERE creator_id = 1";
$data = $this->db->query($query)->result_array();
// var_dump($data);

/*
http://www.bootply.com/rzNQTlDlFX#
*/
?>
<html>
<head>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" media="all">
<link href="//code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" type="text/css" rel="stylesheet" media="all">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" >
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" >
<!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="shortcut icon" href="/bootply_ico.jpg">
<link rel="apple-touch-icon" href="/bootply_ico.jpg">

<script data-cfasync="false" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script data-cfasync="false" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script data-cfasync="false" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script data-cfasync="false" src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
		
<style>
.fc {
	direction: ltr;
	text-align: left;
	}
	
.fc table {
	border-collapse: collapse;
	border-spacing: 0;
	}
	
html .fc,
.fc table {
	font-size: 1em;
	}
	
.fc td,
.fc th {
	padding: 0;
	vertical-align: top;
	}



/* Header
------------------------------------------------------------------------*/

.fc-header td {
	white-space: nowrap;
	}

.fc-header-left {
	width: 25%;
	text-align: left;
	}
	
.fc-header-center {
	text-align: center;
	}
	
.fc-header-right {
	width: 25%;
	text-align: right;
	}
	
.fc-header-title {
	display: inline-block;
	vertical-align: top;
	}
	
.fc-header-title h2 {
	margin-top: 0;
	white-space: nowrap;
	}
	
.fc .fc-header-space {
	padding-left: 10px;
	}
	
.fc-header .fc-button {
	margin-bottom: 1em;
	vertical-align: top;
	}
	
/* buttons edges butting together */

.fc-header .fc-button {
	margin-right: -1px;
	}
	
.fc-header .fc-corner-right,  /* non-theme */
.fc-header .ui-corner-right { /* theme */
	margin-right: 0; /* back to normal */
	}
	
/* button layering (for border precedence) */
	
.fc-header .fc-state-hover,
.fc-header .ui-state-hover {
	z-index: 2;
	}
	
.fc-header .fc-state-down {
	z-index: 3;
	}

.fc-header .fc-state-active,
.fc-header .ui-state-active {
	z-index: 4;
	}
	
	
	
/* Content
------------------------------------------------------------------------*/
	
.fc-content {
	clear: both;
	zoom: 1; /* for IE7, gives accurate coordinates for [un]freezeContentHeight */
	}
	
.fc-view {
	width: 100%;
	overflow: hidden;
	}
	
	

/* Cell Styles
------------------------------------------------------------------------*/

.fc-widget-header,    /* <th>, usually */
.fc-widget-content {  /* <td>, usually */
	border: 1px solid #ddd;
	}
	
.fc-state-highlight { /* <td> today cell */ /* TODO: add .fc-today to <th> */
	background: #fcfcfc;
	}
	
.fc-cell-overlay { /* semi-transparent rectangle while dragging */
	background: #bcccbc;
	opacity: .3;
	filter: alpha(opacity=30); /* for IE */
	}
	


/* Buttons
------------------------------------------------------------------------*/

.fc-button {
	position: relative;
	display: inline-block;
	padding: 0 .6em;
	overflow: hidden;
	height: 1.9em;
	line-height: 1.9em;
	white-space: nowrap;
	cursor: pointer;
}


.fc-text-arrow {
	margin: 0 .1em;
	font-size: 2em;
	font-family: "Courier New", Courier, monospace;
	vertical-align: baseline; /* for IE7 */
	}


	
/*
  button states
  borrowed from twitter bootstrap (http://twitter.github.com/bootstrap/)
*/

.fc-state-default {
	background-color: #f5f5f5;
	}

.fc-state-hover,
.fc-state-down,
.fc-state-active,
.fc-state-disabled {
	color: #333333;
	background-color: #e6e6e6;
	}

.fc-state-hover {
	color: #333333;
	text-decoration: none;
	background-position: 0 -15px;
	-webkit-transition: background-position 0.1s linear;
	   -moz-transition: background-position 0.1s linear;
	     -o-transition: background-position 0.1s linear;
	        transition: background-position 0.1s linear;
	}

.fc-state-down,
.fc-state-active {
	background-color: #cccccc;
	background-image: none;
	outline: 0;
	box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
	}

.fc-state-disabled {
	cursor: default;
	background-image: none;
	opacity: 0.55;
	filter: alpha(opacity=65);
	box-shadow: none;
	}

	

/* Global Event Styles
------------------------------------------------------------------------*/

.fc-event-container > * {
	z-index: 8;
	}

.fc-event-container > .ui-draggable-dragging,
.fc-event-container > .ui-resizable-resizing {
	z-index: 9;
	}
	 
.fc-event {
	border: 1px solid #3a87ad; /* default BORDER color */
	background-color: #3a87ad; /* default BACKGROUND color */
	color: #fff;               /* default TEXT color */
	font-size: .85em;
	cursor: default;
	}

a.fc-event {
	text-decoration: none;
	}
	
a.fc-event,
.fc-event-draggable {
	cursor: pointer;
	}
	
.fc-rtl .fc-event {
	text-align: right;
	}

.fc-event-inner {
	width: 100%;
	height: 100%;
	overflow: hidden;
	}
	
.fc-event-time,
.fc-event-title {
	padding: 0 1px;
	}
	
.fc .ui-resizable-handle {
	display: block;
	position: absolute;
	z-index: 99999;
	overflow: hidden; /* hacky spaces (IE6/7) */
	font-size: 300%;  /* */
	line-height: 50%; /* */
	}
	
	
	
/* Horizontal Events
------------------------------------------------------------------------*/

.fc-event-hori {
	border-width: 1px 0;
	margin-bottom: 1px;
	}

.fc-ltr .fc-event-hori.fc-event-start,
.fc-rtl .fc-event-hori.fc-event-end {
	border-left-width: 1px;
	}

.fc-ltr .fc-event-hori.fc-event-end,
.fc-rtl .fc-event-hori.fc-event-start {
	border-right-width: 1px;
	}
	
/* resizable */
	
.fc-event-hori .ui-resizable-e {
	top: 0           !important; /* importants override pre jquery ui 1.7 styles */
	right: -3px      !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: e-resize;
	}
	
.fc-event-hori .ui-resizable-w {
	top: 0           !important;
	left: -3px       !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: w-resize;
	}
	
.fc-event-hori .ui-resizable-handle {
	_padding-bottom: 14px; /* IE6 had 0 height */
	}
	

table.fc-border-separate {
	border-collapse: separate;
	}
	
.fc-border-separate th,
.fc-border-separate td {
	border-width: 1px 0 0 1px;
	}
	
.fc-border-separate th.fc-last,
.fc-border-separate td.fc-last {
	border-right-width: 1px;
	}
	
.fc-border-separate tr.fc-last th,
.fc-border-separate tr.fc-last td {
	border-bottom-width: 1px;
	}
	
.fc-border-separate tbody tr.fc-first td,
.fc-border-separate tbody tr.fc-first th {
	border-top-width: 0;
	}
	
	

/* Month View, Basic Week View, Basic Day View
------------------------------------------------------------------------*/

.fc-grid th {
	text-align: center;
	}

.fc .fc-week-number {
	width: 22px;
	text-align: center;
	}

.fc .fc-week-number div {
	padding: 0 2px;
	}
	
.fc-grid .fc-day-number {
	float: right;
	padding: 0 2px;
	}
	
.fc-grid .fc-other-month .fc-day-number {
	opacity: 0.3;
	filter: alpha(opacity=30); /* for IE */
	/* opacity with small font can sometimes look too faded
	   might want to set the 'color' property instead
	   making day-numbers bold also fixes the problem */
	}
	
.fc-grid .fc-day-content {
	clear: both;
	padding: 2px 2px 1px; /* distance between events and day edges */
	}
	
/* event styles */
	
.fc-grid .fc-event-time {
	font-weight: bold;
	}
	
/* right-to-left */
	
.fc-rtl .fc-grid .fc-day-number {
	float: left;
	}
	
.fc-rtl .fc-grid .fc-event-time {
	float: right;
	}
	
	

/* Agenda Week View, Agenda Day View
------------------------------------------------------------------------*/

.fc-agenda table {
	border-collapse: separate;
	}
	
.fc-agenda-days th {
	text-align: center;
	}
	
.fc-agenda .fc-agenda-axis {
	width: 50px;
	padding: 0 4px;
	vertical-align: middle;
	text-align: right;
	white-space: nowrap;
	font-weight: normal;
	}

.fc-agenda .fc-week-number {
	font-weight: bold;
	}
	
.fc-agenda .fc-day-content {
	padding: 2px 2px 1px;
	}
	
/* make axis border take precedence */
	
.fc-agenda-days .fc-agenda-axis {
	border-right-width: 1px;
	}
	
.fc-agenda-days .fc-col0 {
	border-left-width: 0;
	}
	
/* all-day area */
	
.fc-agenda-allday th {
	border-width: 0 1px;
	}
	
.fc-agenda-allday .fc-day-content {
	min-height: 34px; /* TODO: doesnt work well in quirksmode */
	_height: 34px;
	}
	
/* divider (between all-day and slots) */
	
.fc-agenda-divider-inner {
	height: 2px;
	overflow: hidden;
	}
	
.fc-widget-header .fc-agenda-divider-inner {
	background: #eee;
	}
	
/* slot rows */
	
.fc-agenda-slots th {
	border-width: 1px 1px 0;
	}
	
.fc-agenda-slots td {
	border-width: 1px 0 0;
	background: none;
	}
	
.fc-agenda-slots td div {
	height: 20px;
	}
	
.fc-agenda-slots tr.fc-slot0 th,
.fc-agenda-slots tr.fc-slot0 td {
	border-top-width: 0;
	}

.fc-agenda-slots tr.fc-minor th,
.fc-agenda-slots tr.fc-minor td {
	border-top-style: dotted;
	}
	
.fc-agenda-slots tr.fc-minor th.ui-widget-header {
	*border-top-style: solid; /* doesn't work with background in IE6/7 */
	}
	


/* Vertical Events
------------------------------------------------------------------------*/

.fc-event-vert {
	border-width: 0 1px;
	}

.fc-event-vert.fc-event-start {
	border-top-width: 1px;
	}

.fc-event-vert.fc-event-end {
	border-bottom-width: 1px;
	}
	
.fc-event-vert .fc-event-time {
	white-space: nowrap;
	font-size: 10px;
	}

.fc-event-vert .fc-event-inner {
	position: relative;
	z-index: 2;
	}
	
.fc-event-vert .fc-event-bg { /* makes the event lighter w/ a semi-transparent overlay  */
	position: absolute;
	z-index: 1;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fff;
	opacity: .25;
	filter: alpha(opacity=25);
	}
	
.fc .ui-draggable-dragging .fc-event-bg, /* TODO: something nicer like .fc-opacity */
.fc-select-helper .fc-event-bg {
	display: none\9; /* for IE6/7/8. nested opacity filters while dragging don't work */
	}
	
/* resizable */
	
.fc-event-vert .ui-resizable-s {
	bottom: 0        !important; /* importants override pre jquery ui 1.7 styles */
	width: 100%      !important;
	height: 8px      !important;
	overflow: hidden !important;
	line-height: 8px !important;
	font-size: 11px  !important;
	font-family: monospace;
	text-align: center;
	cursor: s-resize;
	}
	
</style>

<script>
$.getScript('http://arshaw.com/js/fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js',function(){
  
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    events: [
	  {
        title: 'All Day Event',
        start: new Date(y, m, 1)
      },
      /*
	  {
        title: 'Berak',
        start: new Date(2016, 7, 28, 13, 20),
        end: new Date(2016, 7, 28, 18, 50),
		allDay: false
      },
      {
        title: 'Long Event',
        start: new Date(y, m, d-5),
        end: new Date(y, m, d-2)
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: new Date(y, m, d-3, 16, 0),
        allDay: false
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: new Date(y, m, d+4, 16, 0),
        allDay: false
      },
      {
        title: 'Meeting',
        start: new Date(y, m, d, 10, 30),
        allDay: false
      },
      {
        title: 'Lunch',
        start: new Date(y, m, d, 12, 0),
        end: new Date(y, m, d, 14, 0),
        allDay: false
      },
      {
        title: 'Birthday Party',
        start: new Date(y, m, d+1, 19, 0),
        end: new Date(y, m, d+1, 22, 30),
        allDay: false
      },
      {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        url: 'http://google.com/'
      }
	  */
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        url: 'http://google.com/'
      },
	  {
        title: 'Click for Google',
        start: new Date(y, m, 28),
        
        url: 'http://google.com/'
      },
	  <?php 
	  $str = NULL;
	  if (!empty($data))
	  {
		foreach ($data as $key => $rs)
		{
		  $y = $m = $d = NULL;
		  $y = date('Y',strtotime($rs['input_date']));
		  $m = date('m',strtotime($rs['input_date']))-1;
		  $d = date('d',strtotime($rs['input_date']));
		  
		  if (($key) != 0) $str.= "\t \t";
		  if (($key) != 0) $str.= "\r\n";
		  $str.= "{\r\n";
		  $str.= "\t\t title: '".$rs['notes']."',\r\n";
		  $str.= "\t\t start: new Date(".$y.", ".$m.", ".$d."),\r\n";
		  // $str.= 'end: new Date(".$rs[''].')',
		  $str.= "\t\t url: '".$rs['notes']."'\r\n";
		  $str.= "\t \t }";
		  if (($key + 1) != count($data)) $str.= ',';
		}
		
		echo $str;
	  }
	  ?>
    ]
  });
})
</script>
<script data-cfasync="false" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script data-cfasync="false" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>


</head>
<body>
	<!--
	<div class="container">
	  <ul class="nav nav-justified">
		<li><a class="text-center" href="#"><i class="fa fa-tag fa-5x"></i> <br>Tags</a></li>
		<li><a class="text-center" href="#"><i class="fa fa-bookmark fa-5x"></i> <br>Tasks</a></li>
		<li><a class="text-center" href="#"><i class="fa fa-camera fa-5x"></i> <br>Photos</a></li>
		<li><a class="text-center" href="#"><i class="fa fa-map-marker fa-5x"></i> <br>Tour</a></li>
		<li><a class="text-center" href="#"><i class="fa fa-music fa-5x"></i> <br>Tunes</a></li>
		<li><a class="text-center" href="#"><i class="fa fa-book fa-5x"></i> <br>Books</a></li>
		<li><a class="text-center" href="#"><i class="fa fa-film fa-5x"></i> <br>Videos</a></li>
	  </ul>
	</div>
	-->

	<div class="container">
		<hr>
		<div id="loading-response"></div>
		<div id="calendar"></div>
	</div>
</body>
<script>
$(document).ready(function(){
	console.log('init jq');
	// $('#calendar .fc-day').click(function(){
		// // var da = $(this).html();
		// var da = $(this).html();
		// alert(da);
		// // prompt();
		// console.log('jalan nih ' + da);
	// });
	
	// $('*[data-date="2016-08-31"]').click(function(){
		// // var da = $(this).html();
		// var da = $(this).html();
		// alert(da);
		// // prompt();
		// console.log('jalan nih ' + da);
	// });
	
	// $('.fc-day-number').click(function(){
		// var da = $(this).html();
		// kalendar = prompt('Masukan jadwal kamu',da);
		// // prompt();
	// });
	
	
	setTimeout(function(){ 
		var input_date, notes, content;
		
		$('.fc-day').click(function(){
			input_date = $(this).attr('data-date');
			notes = prompt('Masukan jadwal kamu');
			if (notes) {
				// $('#submit_merchant_inquiry').attr('disabled',true);
				// $('#submit_merchant_inquiry').html('<div class="talCnt"><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i></div>');
				$.ajax({
					type: "POST",
					url: "<?php echo base_url()?>belajar/calendar",
					data: "apitoken=<?php echo rand(100000000,9999999999);?>&do=save&input_date="+ input_date +"&notes=" + notes +"&creator_id=1",
					cache: false,
					success: function(resp) {

						$('#loading-response').html('<div class="alert alert-info talCnt"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>');
						// $('#merchant_inquiry_form').modal('show');
						setTimeout(function(){
							if (resp == "success") {
								$('#loading-response').html('<div class="alert alert-info talCnt"><br/><br/>Terimakasih atas pendaftaran anda sebagai merchant di Lakupon<br/> Kami akan segera menghubungi anda dalam 2 x 24 jam</div>');
								
								// var redir = "<?php echo base_url().'thankyou_register?email='?>" + encodeURIComponent(the_email);
								// content = $("#calendar").load( "<?php echo base_url()?>belajar/calendar #calendar" );
								// $('#loading-response').html(content);
								setTimeout(function(){
									window.location.replace('<?php echo current_full_url()?>');
								}, 2000)
								
							} else if (resp == "failed") {
								alert('respon gagal');
							}
							
						}, 2000);
					}
				});
			} else {
				
				// alert('gagal');
			}
		});
		
	}, 800);
	console.log('ending jq');
})
</script>
</html>	