<!DOCTYPE html>
<html lang="en">
<head>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.amd.min.js"></script>

</head>
<body>
	
	<span id="content-placeholder" class="text_js"></span>

	<script id="template_1" type="text/x-handlebars-template">
	 <div style="border: 1px solid black; width: 350px">
	 <i>{{hello_world}}</i><br>
	 {{test_var}}
	 </div>
	</script>

	<script>

	 var source   = $("#template_1").html();
	 var template = Handlebars.compile(source);
	 var context = {hello_world: "Hello world!", test_var: "Content generated using Handlebars.js template."};
	 var html    = template(context);
	 $("#content-placeholder").html(html);

	</script>
</body>
</html>
