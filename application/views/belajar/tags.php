	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>

	<link rel='stylesheet' href='<?php echo base_url()?>asset/css/ui-lightness/jquery-ui-1.10.4.min.css?v=20160708' />


	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery.1.11.2.min.js?v=20160708" ></script>
	<script type="text/javascript" src='<?php echo base_url()?>asset/js/jquery-ui-1.10.4.min.js?v=20160708' ></script>
	
	<script type="text/javascript" src='<?php echo base_url()?>asset/js/ui/jquery.ui.core.min.js?v=20160708' ></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/bootstrap.min.js?v=20160708" ></script>
	
	<link rel="stylesheet" href="<?php echo base_url()?>plugin/bootstrap-tagsinput/src/bootstrap-tagsinput.css?v=20160708"/>
	
	<script type="text/javascript" src="<?php echo base_url()?>plugin/bootstrap-tagsinput/src/typeahead.js?v=20160708" ></script>
	<script type="text/javascript" src="<?php echo base_url()?>plugin/bootstrap-tagsinput/src/bootstrap-tagsinput.js?v=20160708" ></script>
	

<?php 
/*
<div id="bloodhound">
  <input class="typeahead" type="text" placeholder="States of USA">
</div>

<script>
var state = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

// constructs the suggestion engine
var states = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // `states` is an array of state names defined in "The Basics"
  local: state
});

$('#bloodhound .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  source: states
});
</script>
*/
?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<br/><br/>
			<input type="text" class="input" value="Amsterdam,Washington" data-role="tagsinput" />
		</div>
	</div>
</div>
<script>
var citynames = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    url: '<?php echo base_url()?>plugin/bootstrap-tagsinput/examples/assets/citynames.json',
    filter: function(list) {
      return $.map(list, function(cityname) {
        return { name: cityname }; });
    }
  }
});
citynames.initialize();

$('input').tagsinput({
  typeaheadjs: {
    name: 'citynames',
    displayKey: 'name',
    valueKey: 'name',
    source: citynames.ttAdapter()
  }
});
</script>