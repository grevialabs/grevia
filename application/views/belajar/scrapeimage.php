<?php 
// takes URL of image and Path for the image as parameter
function download_image1($image_url, $image_file){
    $fp = fopen ($image_file, 'w+');              // open file handle

    $ch = curl_init($image_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
    curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
    // curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
    curl_exec($ch);

    curl_close($ch);                              // closing curl handle
    fclose($fp);                                  // closing file handle
}

?>

<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-animation.min.css?v=20160708"/>

<!-- Include required JS files -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/shCore.js?v=20180110"></script>

<script type="text/javascript" src="<?php echo base_url()?>asset/js/shAutoloader.js?v=20180110"></script>
<script type="text/javascript" src="<?php echo base_url()?>asset/js/shBrushPhp.js?v=20180110"></script>
 
<!-- Include *at least* the core style and default theme -->
<link href="<?php echo base_url()?>asset/css/shCore.css?v=20180110" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>asset/css/shThemeDefault.css?v=20180110" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css?v=20180110' />

<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=20160708"/>
<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->

<link href="<?php echo base_url()?>asset/css/font-awesome-4.6.3.min.css?=20160708" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css?v=20180110"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css?v=20180110"/>

<div class="panel">
	<a href="<?php echo base_url().'belajar/scrapetwt';?>">Scrape twitter profile</a> | 
	<a href="<?php echo base_url().'belajar/scrapetwtpost';?>">Scrape Twitter post</a> |
	<a href="<?php echo base_url().'belajar/scrapeimage';?>">Scrape Image</a>
</div>

<?php 
$username = NULL;
if (isset($_GET['uri'])) $username = $_GET['uri'];
	?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12" style="margin-top:250px">
				<h3>Scrape image</h3>
				<form method="get" action>
					Masukan url image:<br/>
					<input type="text" name="uri" value="<?php if ( isset($username)) echo $username ?>" class="input br wdtFul" />
					<input type="submit" class="btn btn-info btn-md"/>
				</form>
			</div>
		</div>
	</div>
	<?php


if (! isset($username) || $username == '') return '<hr/>Username harus diisi';
	
function hit($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
	// curl_setopt($ch, CURLOPTGET, true);	
	//curl_setopt($ch, CURLOPT_POST, true);
	
	// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);     // return web page
	curl_setopt($ch, CURLOPT_HEADER         , false);    // don't return headers
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);     // follow redirects
	curl_setopt($ch, CURLOPT_ENCODING       , "");       // handle all encodings
	curl_setopt($ch, CURLOPT_USERAGENT      , "spider"); // who am i
	curl_setopt($ch, CURLOPT_AUTOREFERER    , true);     // set referer on redirect
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 120);      // timeout on connect
	curl_setopt($ch, CURLOPT_TIMEOUT        , 120);      // timeout on response
	curl_setopt($ch, CURLOPT_MAXREDIRS      , 10);       // stop after 10 redirects
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false);     // Disabled SSL Cert checks
	
	$resp = curl_exec($ch);
	if (curl_error($ch)) {
		echo curl_error($ch);
	}
	return $resp;
}


$content = $furl = $fpath = $name = NULL;
$name = mt_rand(10000000,99999999);
$furl = $username;

$fpath = 'D:\wamp64\www\grv\application\views\belajar\disini.jpg';
if (! is_internal()) 
	$fpath = '/home/grevia/public_html/asset/images/scrape/'.$name.'.jpg';

download_image1($furl,$fpath);

$out = 'Gagal simpan image';
if (file_exists($fpath)) $out = 'Berhasil simpan image "'.$name.'" di '.$fpath.' <br/><img src="'.$fpath.'"/>';

debug('<hr/>');
debug($out);
die;
?>
?>