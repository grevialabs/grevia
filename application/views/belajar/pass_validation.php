<!DOCTYPE html>
<html>
<head>
<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
<meta charset=utf-8 />
<title>JS Bin</title>

<script>
$(document).ready(function(){
	
	$("input[type=text]").keyup(function () {
		var validated =  false;
		
		if(!/\d/.test(this.value)) {
			message = 'min. 1 numeric';
		} else if(!/[a-z]/.test(this.value)) {
			message = 'min. 1 lowercase';
		} else if(!/[A-Z]/.test(this.value)) {
			message = 'min. 1 uppercase';
		} else if(this.value.length < 8) {
			message = 'min. 8 char';
		} else {
			$validated = true;
			message = 'berhasil';
		}

		$('#msg').html(message);
	});
})
</script>
<body>
<input type="text" />
<div id="msg"></div>
</body>
</html>