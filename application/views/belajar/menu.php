<html>
<head>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery.1.11.2.min.js"></script>
	<script type="text/javascript" src='<?php echo base_url()?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/bootstrap.min.js?v=20160708"></script>

	<style>
	/* The side navigation menu */
	.sidenav {
		height: 100%; /* 100% Full-height */
		width: 0; /* 0 width - change this with JavaScript */
		position: fixed; /* Stay in place */
		z-index: 1; /* Stay on top */
		top: 0;
		left: 0;
		background-color: #111; /* Black*/
		overflow-x: hidden; /* Disable horizontal scroll */
		padding-top: 60px; /* Place content 60px from the top */
		transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
	}

	/* The navigation menu links */
	.sidenav a {
		padding: 8px 8px 8px 32px;
		text-decoration: none;
		font-size: 25px;
		color: #818181;
		display: block;
		transition: 0.3s
	}

	/* When you mouse over the navigation links, change their color */
	.sidenav a:hover, .offcanvas a:focus{
		color: #f1f1f1;
	}

	/* Position and style the close button (top right corner) */
	.sidenav .closebtn {
		position: absolute;
		top: 0;
		right: 25px;
		font-size: 36px;
		margin-left: 50px;
	}

	/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
	#main {
		transition: margin-left .5s;
		padding: 20px;
	}

	/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
	@media screen and (max-height: 450px) {
		.sidenav {padding-top: 15px;}
		.sidenav a {font-size: 18px;}
	}
	</style>
<script>
$(document).ready(function(){
	$('#main').click(function(){
		closeNav();
	})
});

// /* Set the width of the side navigation to 250px */
// function openNav() {
	// document.getElementById("mySidenav").style.width = "250px";
	// document.getElementById("main").style.marginLeft = "250px";
// }

// /* Set the width of the side navigation to 0 */
// function closeNav() {
	// document.getElementById("mySidenav").style.width = "0";
	// document.getElementById("main").style.marginLeft = "0";
// }

// /* Open the sidenav */
// function openNav() {
    // document.getElementById("mySidenav").style.width = "100%";
// }

// /* Close/hide the sidenav */
// function closeNav() {
    // document.getElementById("mySidenav").style.width = "0";
// }

// OPACITY NIH
/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}
</script>
</head>
<body>
	<div id="mySidenav" class="sidenav">
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	  <a href="#">About</a>
	  <a href="#">Services</a>
	  <a href="#">Clients</a>
	  <a href="#">Contact</a>
	</div>

	<!-- Use any element to open the sidenav -->
	<button onclick="openNav()" class="btn btn-success">open</button>

	<!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
	<div id="main">
	Menu untuk slideshow referensi <br/>
	<br/>
	Lorem ipsum dolor sit amet<br/>
	

	<p><br /><strong>Memberikan Nilai kepada Pelanggan Melalui Produk Anda</strong></p>
	<p>Ketika memulai bisnis, pastilah kita ingin mendapat keuntungan yang sebanyak-banyaknya, namun terkadang kita lupa bahwa apa yang kita ciptakan harus bisa memberikan nilai&nbsp;bagi orang lain. Jadilah pelaku bisnis di bidang digital bisnis yang dapat menunjukkan perbedaan dan memberikan core values pada produk anda.</p>
	<p>Seperti <strong>Tokopedia.com</strong>&nbsp;yang membantu&nbsp;bisnis lokal&nbsp;untuk meningkatkan pendapatan&nbsp;para penjualnya, ataupun aplikasi chatting <strong>Skype</strong> yang membantu para pengguna untuk berkomunikasi dengan gratis via VoIP. Jika kita berfokus pada berbagi nilai, produk yang kita buatpun akan semakin sukses.</p>
	<p><br /><strong>Terpenting Adalah Bisnis Anda, Bukan Banyaknya Pengikut</strong><br /><br />Banyak pelaku bisnis online yang berlomba-lomba untuk menarik hati pengunjung menjadi pengikut website atau toko onlinenya. Memang ketika Anda berkunjung ke sebuah toko online dengan pengikut yang banyak, dapat meyakinkan Anda bahwa toko online tersebut dapat dipercaya. <br /><br />Namun jika ternyata pengikut tersebut tidak tertarik dengan produk yang ada juga sama saja dengan pembohongan. Lebih baik memiliki sedikit pengikut atau bahkan sama sekali tidak memiliki pengikut. Namun banyak pelanggan yang tertarik bekerja sama dengan bisnis Anda.</p>
	<p><br /><strong>Berani&nbsp;Kompetisi &amp; Fokus pada pelanggan</strong><br /><br />Dalam bisnis bidang digital memang perlu startup yang matang. Memberikan pelayanan yang terbaik untuk pelanggan Anda sangat penting. Bahkan meyakinkan pelanggan bahwa bisnis Anda lebih baik dari pesaing juga bukan masalah, asalkan tetap dalam batas singgungan yang wajar. <br /><br />Kompetisi sendiri adalah pertanda bahwa bisnis yang sedang anda bangun memiliki potensi profit yang besar, jangan menghindarinya dan layanilah pelanggan dengan servis terbaik untuk meningkatkan kepuasan pelanggan yang menggunakan&nbsp;produk anda.</p>
	<p><br />Ketiga poin penting bagi Anda yang sudah siap untuk ikut terjun dalam dunia bisnis online. Dunia bisnis yang tidak dapat dikatakan main-main. Semua bekal harus mampu dijaga agar bisnis online berjalan baik termasuk digital bisnis serta kematangan startup.</p>
	<p>&nbsp;</p>

	</div>

</body>
</html>