<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<style>
.container, .inp {
    color: #DDD;
    background: #222;
    padding: 4px 6px;
    border: 1px #222 solid;
    font-family: Consolas, sans-serif;
    font-size: 13px;
}
.container p {
    margin: 0;
    padding: 3px 6px;
}
.container p:nth-child(odd) {
    background: #333;
}
.container {
    max-height: 300px;
    overflow-x: hidden;
    overflow-y : scroll;
    padding: 0;
}
.inp {
    width: 100%;
    box-sizing: border-box;
}
</style>
<div >
	<div class="container">
		<p class="row">Command 1</p>
		<p class="row">Command 2</p>
		<p class="row">Command 3</p>
		<p class="row">Command 4</p>
		<p class="row">Command 5</p>
		<p class="row">Command 6</p>
		<p class="row">Command 7</p>
		<p class="row">Command 8</p>
		<p class="row">Command 9</p>
		<p class="row">Command 10</p>
		<p class="row">Command 11</p>
		<p class="row">Command 12</p>
		<p class="row">Command 13</p>
		<p class="row">Command 14</p>
		<p class="row">Command 15</p>
		<p class="row">Command 16</p>
		<p class="row">Command 17</p>
		<p class="row">Command 18</p>
		<p class="row">Command 19</p>
		<p class="row">Command 20</p>
		<p class="row">Command 21</p>
		<p class="row">Command 22</p>
		<p class="row">Command 23</p>
		<p class="row">Command 24</p>
		<p class="row">Command 25</p>
		<p class="row">Command 26</p>
		<p class="row">Command 27</p>
		<p class="row">Command 28</p>
		<p class="row">Command 29</p>
		<p class="row">Command 30</p>
		<p class="row">Command 31</p>
		<p class="row">Command 32</p>
		<p class="row">Command 33</p>
		<p class="row">Command 34</p>
		<p class="row">Command 35</p>
		<p class="row">Command 36</p>
		<p class="row">Command 37</p>
		<p class="row">Command 38</p>
		<p class="row">Command 39</p>
		<p class="row">Command 40</p>
		<p class="row">Command 41</p>
		<p class="row">Command 42</p>
		<p class="row">Command 43</p>
		<p class="row">Command 44</p>
		<p class="row">Command 45</p>
		<p class="row">Command 46</p>
		<p class="row">Command 47</p>
		<p class="row">Command 48</p>
		<p class="row">Command 49</p>
		<p class="row">Command 50</p>
	</div>
</div>

<input type="text" class="inp">

<script>
var $cont = $('.container');
$cont[0].scrollTop = $cont[0].scrollHeight;

$('.inp').keyup(function(e) {
    if (e.keyCode == 13) {
        $cont.append('<p>' + $(this).val() + '</p>');
        $cont[0].scrollTop = $cont[0].scrollHeight;
        $(this).val('');
    }
})
.focus();
</script>