<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-animation.min.css?v=20160708"/>

<!-- Include required JS files -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/shCore.js?v=20180110"></script>

<script type="text/javascript" src="<?php echo base_url()?>asset/js/shAutoloader.js?v=20180110"></script>
<script type="text/javascript" src="<?php echo base_url()?>asset/js/shBrushPhp.js?v=20180110"></script>
 
<!-- Include *at least* the core style and default theme -->
<link href="<?php echo base_url()?>asset/css/shCore.css?v=20180110" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>asset/css/shThemeDefault.css?v=20180110" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css?v=20180110' />

<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=20160708"/>
<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->

<link href="<?php echo base_url()?>asset/css/font-awesome-4.6.3.min.css?=20160708" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css?v=20180110"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.min.css?v=20160708"/>
<link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css?v=20180110"/>

<div class="panel">
	<a href="<?php echo base_url().'belajar/scrapetwt';?>">Scrape twitter profile</a> | 
	<a href="<?php echo base_url().'belajar/scrapetwtpost';?>">Scrape Twitter post</a> |
	<a href="<?php echo base_url().'belajar/scrapeimage';?>">Scrape Image</a>
</div>

<?php 
$username = NULL;
if (isset($_GET['uri'])) $username = $_GET['uri'];
	?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12" style="margin-top:250px">
				<h3>Scrape post twitter</h3>
				<form method="get" action>
					Masukan username twitter:<br/>
					<input type="text" name="uri" value="<?php if ( isset($username)) echo $username ?>" class="input br wdtFul" />
					<input type="submit" class="btn btn-info btn-md"/>
				</form>
			</div>
		</div>
	</div>
	<?php


if (! isset($username) || $username == '') return '<hr/>Username harus diisi';
	

$source = '
<div class="ProfileNav" role="navigation" data-user-id="4505619192">
    <ul class="ProfileNav-list">
<li class="ProfileNav-item ProfileNav-item--tweets is-active">
          <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-nav" title="227 Tweet" data-nav="tweets"
              tabindex=0
>
            <span class="ProfileNav-label" aria-hidden="true">Tweet</span>
              <span class="u-hiddenVisually">Tweet, halaman saat ini.</span>
            <span class="ProfileNav-value"  data-count=227 data-is-compact="false">227
            </span>
          </a>
        </li><li class="ProfileNav-item ProfileNav-item--following">
        <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" title="366 Mengikuti" data-nav="following"
            href="/ARLJ4/following"
>
          <span class="ProfileNav-label" aria-hidden="true">Mengikuti</span>
            <span class="u-hiddenVisually">Mengikuti</span>
          <span class="ProfileNav-value" data-count=366 data-is-compact="false">366</span>
        </a>
      </li><li class="ProfileNav-item ProfileNav-item--followers">
        <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" title="27 Pengikut" data-nav="followers"
            href="/ARLJ4/followers"
>
          <span class="ProfileNav-label" aria-hidden="true">Pengikut</span>
            <span class="u-hiddenVisually">Pengikut</span>
          <span class="ProfileNav-value" data-count=27 data-is-compact="false">27</span>
        </a>
      </li><li class="ProfileNav-item ProfileNav-item--favorites" data-more-item=".ProfileNav-dropdownItem--favorites">
        <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" title="691 Menyukai" data-nav="favorites"
            href="/ARLJ4/likes"
>
          <span class="ProfileNav-label" aria-hidden="true">Suka</span>
            <span class="u-hiddenVisually">Suka</span>
          <span class="ProfileNav-value" data-count=691 data-is-compact="false">691</span>
        </a>
      </li><li class="ProfileNav-item ProfileNav-item--more dropdown is-hidden">        <a class="ProfileNav-stat ProfileNav-stat--link ProfileNav-stat--moreLink js-openSignupDialog js-nonNavigable" role="button" href="#more">
          <span class="ProfileNav-label">&nbsp;</span>
          <span class="ProfileNav-value">Selengkapnya <span class="ProfileNav-dropdownCaret Icon Icon--medium Icon--caretDown"></span></span>
        </a>
        <div class="dropdown-menu">
          
          <ul><li>
              <a href="/ARLJ4/likes" class="ProfileNav-dropdownItem ProfileNav-dropdownItem--favorites is-hidden u-bgUserColorHover u-bgUserColorFocus u-linkClean js-nav">Suka</a></li></ul>
        </div>
</div>
';

// https://twitter.com/ARLJ4

// formula = format string to scrape
function ambilkata($source, $formula,$formula2 = NULL)
{
	$tmp = $tmp2 = $ret = NULL;
	
	$tmp = $source;
	// $tmp = explode($buka.$formula.$tutup,$tmp);
	$tmp = explode($formula,$tmp);
	// debug($tmp,1);
	if (isset($tmp[1])) {
		$tmp2 = explode($formula2,$tmp[1]);
		
		$ret = $tmp2;
		if (isset($tmp2[0])) {
			$ret = $tmp2[0];
			// remove prefix depan
			// $ret = str_replace($buka,'',$ret);
		}
	}
	// debug($ret);
	// die;
	$ret = trim($ret);
	return $ret;
}

function hit($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
	// curl_setopt($ch, CURLOPTGET, true);	
	//curl_setopt($ch, CURLOPT_POST, true);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, "username=XXXXX&password=XXXXX");
	
	// Using proxy
	$arr_proxy = array(
		// '187.44.1.167:8080', // BZ
		// '123.205.131.69:21776', // taiwan
		// '139.59.207.66:8080', // SG
		// '163.47.11.113:3128', // SG
		
		// '36.67.85.3:8080', // all local indo proxy
		// '150.107.251.26:8080',
		// '182.30.225.36:8080',
		// '182.253.130.178:8080',
		
		// elite from https://premproxy.com/proxy-by-country/Indonesia-01.htm
		'139.255.112.129:53281',
		'114.7.5.214:53281',
		'182.16.171.1:53281',
	);
	curl_setopt($ch, CURLOPT_PROXY , $arr_proxy[mt_rand(0,count($arr_proxy)-1)]);     // return web page
	
	// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);     // return web page
	curl_setopt($ch, CURLOPT_HEADER         , false);    // don't return headers
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);     // follow redirects
	curl_setopt($ch, CURLOPT_ENCODING       , "");       // handle all encodings
	curl_setopt($ch, CURLOPT_USERAGENT      , "spider"); // who am i
	curl_setopt($ch, CURLOPT_AUTOREFERER    , true);     // set referer on redirect
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 120);      // timeout on connect
	curl_setopt($ch, CURLOPT_TIMEOUT        , 120);      // timeout on response
	curl_setopt($ch, CURLOPT_MAXREDIRS      , 10);       // stop after 10 redirects
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false);     // Disabled SSL Cert checks
	
	$resp = curl_exec($ch);
	if (curl_error($ch)) {
		echo curl_error($ch);
	}
	return $resp;
}

/*
// URL
// https://twitter.com/ARLJ4

// tweet 
<span class="u-hiddenVisually">Tweet, halaman saat ini.</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// following
<span class="u-hiddenVisually">Following</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>
<span class="u-hiddenVisually">Mengikuti</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// follower
<span class="u-hiddenVisually">Follower</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>
<span class="u-hiddenVisually">Pengikut</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// Like
<span class="u-hiddenVisually">Like</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>
<span class="u-hiddenVisually">Suka</span><span class="ProfileNav-value" data-count="227" data-is-compact="false">227</span>

// location
<span class="ProfileHeaderCard-locationText u-dir" dir="ltr">
              <a href="/search?q=place%3Ace7988d3a8b6f49f" data-place-id="ce7988d3a8b6f49f">Indonesia</a>

        </span>

// image 
<img class="ProfileAvatar-image " src="https://pbs.twimg.com/profile_images/680265953028866049/QB1xrTbm_400x400.jpg" alt="ARLJ">

ambilkata
*/

// $url = 'https://twitter.com/ARLJ4';
$url = 'https://twitter.com/'.$username.'?lang=id';
$content = hit($url);

// $content = $source;
$content = preg_replace('/\s{2,}+/', '',$content);
$tutup = ' data-is-compact';

// Ngaco
$tweet = '<span class="u-hiddenVisually">Tweet, halaman saat ini.</span><span class="ProfileNav-value"data-count=';
$out['tweet'] = ambilkata($content,$tweet,$tutup);

// Test
$following = '<span class="u-hiddenVisually">Mengikuti</span><span class="ProfileNav-value" data-count=';
$out['following'] = ambilkata($content,$following,$tutup);

$follower = '<span class="u-hiddenVisually">Pengikut</span><span class="ProfileNav-value" data-count=';
$out['follower'] = ambilkata($content,$follower,$tutup);

$like = '<span class="u-hiddenVisually">Suka</span><span class="ProfileNav-value" data-count=';
$out['like'] = ambilkata($content,$like,$tutup);

// $tweet = '<span class="u-hiddenVisually">Following</span><span class="ProfileNav-value" data-count=';
// $out['tweet'] = ambilkata($content,NULL,$tweet,$tutup);

$location = '<span class="ProfileHeaderCard-locationText u-dir" dir="ltr">';
$tutup = '</span>';
$out['location'] = ambilkata($content,$location,$tutup);

// <img class="ProfileAvatar-image " src="https://pbs.twimg.com/profile_images/680265953028866049/QB1xrTbm_400x400.jpg" alt="ARLJ">
// image
$image = '<img class="ProfileAvatar-image " src="';
$tutup = '"';
$out['image'] = ambilkata($content,$image,$tutup);
$out['imageview'] = '<img src="'.$out['image'].'" />';

// debug($content);
debug('<hr/>');
debug($out);
die;
?>