<?php
public function validate_csv()
{
	session_start();	
	if($_POST)
	{	
		if(isset($_POST['sc_id']))
		{
			foreach($_POST['sc_id'] as $tiba)
			{
				if(isset($_POST['issue']))
				{
					parse_str($tiba, $out);
					
					$ret = $this->payment_model->combo_maksimal(array('shopping_cart_id' => $out['issue'],'return'=>1));
					
					$paid['id'] = $out['issue'];
					$paid['paid'] = $out['paid'];
					$this->payment_model->update_cart_status($paid);
					
					
					// $data['status'] = 4;
					// $data['id'] = $out['issue'];
					// $det=$this->payment_model->curl('deal_manager/cart_detail', 'get',array('shopping_cart_id' => $out['issue']));
					// $coup = false;
					
					// foreach((array) json_decode($det['string']) as $item_row)
					// {	
						// $row = (array) $item_row;
						// if($row['contract_id']==3)
						// {
							// $coup = true;
						// }
					// }
					
					// $det=$this->payment_model->curl('general_manager/issue_callback', 'post',array('cart_id' => $out['issue']));
					
					//OPEN GA------------------------------------------------------------------
					/*
					$this->load->model('ga_model');
					
					$transaction_id = $out['transaction_id'];
					$ga_data = $out['ga_data'];
					$ga_data = explode(',',$ga_data);
					$total_revenue = $ga_data[0] ;
					$total_shipping = (isset($ga_data[1])?$ga_data[1]:0) ; 
					
					$head = $this->ga_model->ga_transaction( array(
						// TRANSACTION
						't' => 'transaction',
						'ti' => $transaction_id,
						'ta' => 'Lakupon',
						'tr' => $total_revenue, // not including tax or shipping
						'ts' => $total_shipping,
						'tt' => '0'
					));
					
					$list_items = $this->payment_model->get_cart_detail($transaction_id);
					$list_items = (array) json_decode($list_items['string']);
					foreach ($list_items as $row) {
						$row = (array) $row;
						$item_name = str_replace('- (reserved, empty)','',$row['deal_name']);
						$item_code = $row['size_id'];
						$detail = $this->ga_model->ga_items_transaction( array(
							// ITEM
							't' => 'item',
							'ti' => $transaction_id,
							'ic' => $item_code,
							'in' => $item_name,
							'ip' => $row['price'], 
							'iq' => $row['quantity']
						));
					}
					*/
					
					//CLOSE GA ------------------------------------------------------------------
					
					// $ret = $this->payment_model->update_cart_status($data);
					// if(($coup))
					// {
						// $data['cart_id'] = $out['issue'];
						// $coupons = $this->payment_model->issue_coupon_by_cart_id($data);
					// }
					// $this->load->model('mail_model');
					// $this->mail_model->payment_code_mail_pgw($out);
					
					$ret = (array) json_decode($ret['string']);
					
					if($_SESSION['ceespi']['payment_type'] == 1 || $_SESSION['ceespi']['payment_type'] == 35)
					{
						$_SESSION['ceespi'][$out['rel']][5] = 'done';
						$_SESSION['ceespi'][$out['rel']][6] = $ret['transaction_id'];
						$_SESSION['ceespi'][$out['rel']][7] = $ret['name'];
						$_SESSION['ceespi'][$out['rel']][8] = $ret['created_at'];
					}
					if($_SESSION['ceespi']['payment_type'] == 2)
					{
						$_SESSION['ceespi'][$out['rel']][9] = 'done';
						$_SESSION['ceespi'][$out['rel']][10] = $ret['transaction_id'];
						$_SESSION['ceespi'][$out['rel']][11] = $ret['name'];
						$_SESSION['ceespi'][$out['rel']][12] = $ret['created_at'];
					}
					$masukin['shopping_cart_id'][] = $out['issue'];
				}
			}
			$det = $this->payment_model->curl('transaction_manager/putting_ga', 'get', $masukin);
			$content['data'] = $det['string'];
			$this->load->view('ga_transition_page',$content);
			$this->general_model->redirect_with_parameter(current_url());
		}
		else if(isset($_POST['exit']))
		{
			unset($_SESSION['ceespi']);
			unset($_SESSION['ceespi']['name']);
			redirect(current_url());
		}
		else if(isset($_POST['export']))
		{
			// $str = '';
			// var_dump($_SESSION['ceespi']); exit;
			foreach($_SESSION['ceespi'] as $k => $row) 
			{
				if(is_numeric($k))
				{
					$str .= '"'.implode('","', $row ).'"'.PHP_EOL;
				}
			}
			$this->export_csv($str, 'edited_'.$_SESSION['ceespi']['name']);
		}
		else
		{
			$file = fopen($_FILES['f_file']['tmp_name'],"r");
			
			// var_dump($file);
			
			while(! feof($file))
			{	
				$data = fgetcsv($file);
				
				// Payment transfer web & BBM Manual transfer same treatment
				if($_POST['pete'] == 1 || $_POST['pete'] == 35)
				{
					if(!isset($data[5]))
					{
						$data[5] = '';
					}
				}
				if($_POST['pete'] == 2)
				{
					if(!isset($data[9]))
					{
						$data[9] = '';
					}
				}
				
				$all[] = $data; 
			}
			fclose($file);
			
			$_SESSION['ceespi'] = $all;
			$_SESSION['ceespi']['name'] = $_FILES['f_file']['name'];
			
			if(isset($_POST['pete']))
			{
				$_SESSION['ceespi']['payment_type'] = $_POST['pete'];
			}
			redirect(current_url());
		}
	}
	$content['content']=$this->load->view('payment/export_csv_form',$null,true);
	$this->load->view('index',$content);
}

?>