<?php 

$set = $this->general_model->payment_gw_config($this->config->item('CIMBClicks')); 
$this->session->set_userdata('pgw_setting',$set);
$trans = (isset($transaction_id))?$transaction_id:$this->session->userdata('transactioning');
//$trans = $_SESSION['transactioning'];
$_POST['transaction_id']=(isset($transaction_id))?$transaction_id:$this->input->post('transaction_id');

$cart_solo = $this->general_model->curl('transaction_manager/cart_only','get',array('transaction_id'=>$_POST['transaction_id']));
$cart_solo = (array) json_decode($cart_solo['string']);

$ses = $this->session->all_userdata();
if(!isset($ses['lakupon_user'])){
	
	$dbuser=$this->general_model->curl('membership_manager/membership','get',array('id'=>$cart_solo['member_id']));
	$dbuser = (array) json_decode($dbuser['string']);
	$ses['lakupon_user']=$dbuser;
}

$ses = $ses['lakupon_user'];
$user_detail=null;
$user_id = $set['user_id'];
$pass = $set['password'];
$transtime = strtr($cart_solo['created_at'],array('-'=>'',' '=>'',':'=>''));


$signature = md5($user_id .$cart_solo['total_cost'] .$_POST['transaction_id']. $pass);
$xml_str = array( 
   'user_id'=>$user_id,
   'amount'=>$cart_solo['total_cost'],
   'transaction_id'=>$_POST['transaction_id'],
   'date_time'=>$transtime,
   'bank_id'=>1,
   'signature'=>$signature,
   'prod_desc'=>'Belanja di Lakupon.com',
   'username'=>$ses['first_name'].' '.$ses['last_name'],
   'useremail'=>$ses['email'],
   'usercontact'=>$ses['phone'],
   'remark'=>'Pembayaran Tunai'
   );
//$this->session->set_userdata('trans', $xml_str);

$dataxml = '<inputtrx_request>';
	
	foreach($xml_str as $key=>$val)
	{
		$dataxml .='<'.$key.'>'.$val.'</'.$key.'>';
	}
	$dataxml .= '</inputtrx_request>';
	// var_dump($xml);
        //setting the curl parameters.
		$url = $set['queue'];
        $c = curl_init($url);
        // curl_setopt($c, CURLOPT_URL, $url);
		// Following line is compulsary to add as it is:
		
		// curl_setopt($ch, CURLOPT_MUTE, 1);
		// curl_setopt($c, CURLOPT_HEADER,true);
		curl_setopt($c, CURLOPT_POST, true);
		curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
		curl_setopt($c, CURLOPT_POSTFIELDS, $dataxml);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		echo $datac = curl_exec($c);
		// var_dump(curl_getinfo ( $c));
		// var_dump($set);

        //convert the XML result into array
        $inquiry_return = json_decode(json_encode(simplexml_load_string($datac)), true);
		
		curl_close($c);
		
		$this->session->unset_userdata('pgw_setting');

		$input['status'] = 1;
		$input['shopping_cart_id'] = $cart_solo['id'];
		$return = $this->general_model->curl('transaction_manager/cart_update_status','put',$input);	
		
		// header('Location: '. $set['target'].'?user_id='.$inquiry_return['user_id'].'&billreferenceno='.$inquiry_return['billreferenceno']);
		// $this->session->unset_userdata('trans');
		if(isset($inquiry_return['billreferenceno'])){
			
			$cart_solo = $this->general_model->curl('transaction_manager/cart_only','get',array('transaction_id'=>$_POST['transaction_id']));
			$cart_solo = (array) json_decode($cart_solo['string']);
			
			$cart['id'] = $cart_solo['id'];
			$cart['reference_number'] = $inquiry_return['billreferenceno'];
			
			$change_cart = $this->general_model->curl('transaction_manager/cart_update','put',$cart);
		
			header('Location: '. $set['target'].'?user_id='.$inquiry_return['user_id'].'&billreferenceno='.$inquiry_return['billreferenceno']);
		}else{
			echo 'Transaction : '.$_POST['transaction_id'].' gagal dilakukan';
		}

	
?>
