<?php
$bcawallet_config = $this->general_model->payment_gw_config($this->config->item('bca_wallet')); 

$cart_only = null;
$cart_solo = $this->general_model->curl('transaction_manager/cart_only','get',array('transaction_id' => $transaction_id));
$cart_solo = (array) json_decode($cart_solo['string']);

// UPDATE status to CHECKOUT
$cart['shopping_cart_id'] = $cart_solo['id'];
$cart['status'] = 1;
$change_status = $this->general_model->curl('transaction_manager/cart_update_status','put',$cart);

// // DUMMY JSON RESPONSE GET URL
// if ($_POST) 
// {
	// //payment acknowledgement from sprint
	// if (isset($_POST['Signature']) && isset($_POST['TransactionID']))
	// {
		// // give response back
		
		// $reason['Indonesian'] = 'Transaksi '.$_POST['TransactionID'].'telah berhasil';
		// $reason['English'] = 'Transaction '.$_POST['TransactionID'].' success';
		// $response = array(
			// 'MerchantID' => $bcawallet_config['MERCHANT_ID'],
			// 'TransactionID' => $_POST['TransactionID'],
			// 'FlagStatus' => 00,
			// 'ReasonStatus' => $reason
		// );
		
		// header('Content-Type: application/json');
		// $send = json_encode($response);
		// echo $send;
		// die;
	// }
	// else
	// {
		// // SEMENTARA pake nanti apus ya
		// // header('Content-Type: application/json');
		// // //echo '<pre>';
		// // print_r(json_encode($_POST));
		// // //echo '</pre>';
		// // die;
	// }
// }

/*
 * Requirement php library: mcrypt
 * - http://php.net/manual/en/book.mcrypt.php
*/

function encrypt($data, $secret) {
    //Generate a key from a hash
    $key = $secret;

    $key .= substr($key, 0, 8);
    $blockSize = mcrypt_get_block_size(MCRYPT_3DES, MCRYPT_MODE_ECB);
    $len = strlen($data);
    $pad = $blockSize - ($len % $blockSize);
    $data .= str_repeat(chr($pad), $pad);

    $encData = mcrypt_encrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_ECB);

    return base64_encode($encData);
}

// Curl here
/* ------------------------------------------------------------------------------------------------------------------------------
1. CURL TO SPRINT
FIRST STEP PRE AUTH 
*/
// Curl here
$signature = $transaction_id_18 = NULL;
$signature = encrypt($bcawallet_config['CLIENT_ID'],$bcawallet_config['SECRET_KEY']);

// GET 18 transaction_id
// ex: 8absM.7518007119136732234 result -> 7518007119136732234
$arr_trans_id = explode('.',$transaction_id);
$arr_trans_id[1] = substr($arr_trans_id[1],-18);

$param['MerchantID'] = $bcawallet_config['MERCHANT_ID'];
$param['TransactionID'] = $transaction_id_18 = $arr_trans_id[1];
$param['RequestDate'] = date('Y-m-d H:i:s',time()); // isi createdate
$param['Amount'] = $cart_solo['total_cost'].".00"; // isi amount
$param['Tax'] = "0.00" ;
$param['CurrencyCode'] = 'IDR'; // gajelas
$param['Signature'] = $signature; // gajelas
$param['Description'] = NULL;
$param['CallbackURL'] = base_url().'purchase/bca_wallet/'.$transaction_id_18;//$bcawallet_config['PAYMENT_FLAG_URL'];
$param['UserSession'] = NULL;
$param['TransactionNote'] = NULL;

$data_post = NULL;
foreach($param as $key => $value) { $data_post .= $key.'='.$value.'&'; }
$data_post = rtrim($data_post, '&');

// submit url sprint server
$url = $bcawallet_config['PRE_AUTH_URL'];

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($curl, CURLOPT_TIMEOUT, 40);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_POST, COUNT($param));
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($param));
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

$result = curl_exec($curl);

curl_close($curl);

$responses = $result;
$responses = (array) json_decode($result);
$is_valid = FALSE;

if (!empty($responses))
{
	// Here is the respond from CURL
	if (isset($responses['Reason']) && isset($responses['AuthStatus']))
	{
		if ($responses['AuthStatus'] == '00')
		{
			// UPDATE STATUS TO CHECKOUT AND 
			
			$is_valid = TRUE;
			$cart['shopping_cart_id'] = $cart_solo['id'];
			$cart['reference_number'] = 'AccToken:'.$responses['AccToken'].',PaymentID:'.$responses['PaymentID'];
			$change_status = $this->general_model->curl('transaction_manager/cart_update_status','put',$cart);
			
			// success response
			// redirect users to bca wallet login page
			
			// $bcwallet_login_url = "https://10.20.200.82:7003/login?PaymentID=".$responses['PaymentID'];
			// redirect($bcwallet_login_url)
			redirect($responses['LandingPageURL']);
		}
		else if ($responses['AuthStatus'] == '01')
		{
			// SPRINT RETURN ERROR
			// UPDATE STATUS TO CANCEL
			$is_valid = FALSE;
		}
	}
	else
	{
		// SPRINT RETURN ERROR
		// UPDATE STATUS TO CANCEL
		$is_valid = FALSE;
	}

}
else 
{
	// SPRINT RETURN ERROR
	// UPDATE STATUS TO CANCEL
	$is_valid = FALSE;
}

if (!$is_valid)
{
	// SPRINT RETURN ERROR
	
	// INCREASE STOCK
	$temp['transaction_id'] = $cart_solo['transaction_id'];
	$update = $this->general_model->curl('transaction_manager/increase_stock_new','post',$temp);
	$update = $this->general_model->curl('transaction_manager/increase_redeem_stock','post',$temp);
	
	// UPDATE STATUS TO CANCEL
	$param = array();
	$param = array( 
		'id' => $cart_solo['id'],
		'status' => 9
	);
	$update_cart = $this->general_model->curl('transaction_manager/cart_update','put',$param);
	
	redirect(base_url().'purchase/bca_wallet/'.$transaction_id_18);
}
?>