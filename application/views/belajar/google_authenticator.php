<?php
$secret = NULL;
if (isset($_GET['secret'])) $secret = $_GET['secret'];

// google_authenticator.php
$ga = new PHPGangsta_GoogleAuthenticator();
if ( ! isset($secret)) {
	$secret = $ga->createSecret();
	redirect(current_full_url().'?secret='.$secret);
	die;
}
// echo "Secret is: ".$secret."\n\n";

$qrCodeUrl = $ga->getQRCodeGoogleUrl('Grevia', $secret);

$post = NULL;
$otp = $ga->getCode($secret);
if ($_POST) 
{
	$post = $_POST;
	
	// debug($post);
	// die;
	if ($post['input'])
	{
		$otp_input = $post['input'];
		
		// 2 = 2*30sec clock tolerance
		$checkResult = $ga->verifyCode($secret, $otp_input, 2);    
		if ($checkResult) 
		{
			$message = 'Success';
		} 
		else
		{
			$message = 'Failed token, please try again.';
		}
	}
}

?>
<html>
<head>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
	<!-- Include *at least* the core style and default theme -->
	<link href="<?php echo base_url()?>asset/css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>asset/css/shThemeDefault.css" rel="stylesheet" type="text/css" />
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css?v=20160708"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=20160708"/>
	<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->
	
	<link href="<?php echo base_url()?>asset/css/font-awesome-4.6.3.min.css?=20160708" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.min.css?v=20160708"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css"/>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.1.11.2.min.js"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.min.js?v=20160708"></script>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<input type="text" class="input wdtFul" value="<?php echo $secret?>" readonly/> <hr/>

			<form method="post">
				<img src="<?php echo $qrCodeUrl?>"/><br/>
				<input type="text" class="input wdtFul" name="f_input" value="" placeholder="Input code" /><br/>
				<input type="submit" class="btn btn-success" name="btn_submit" value="submit" />
				
			</form>
			<?php
			
			echo "Checking Code '$otp' and Secret '$secret':\n";
			?>
			<?php if (isset($message)) echo '<div class="b alert alert-info">'.$message.'</div>'; ?>
		</div>
	</div>
</div>
</body>
</html>