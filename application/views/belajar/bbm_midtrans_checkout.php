<?php
// Redirect user to home if no cookies bbm_id
if(!isset($_COOKIE['bbm_profile']))
{
	$_SESSION['temp_error'] = array(array('message'=>'BBM ID not Connected'));
	redirect(base_url());
}
$bbm_profile = (array) json_decode($_COOKIE['bbm_profile']);

$CONFIG = $this->general_model->payment_gw_config($this->config->item('bbm_checkout')); 

// get member data 
$bbm_profile = (array) json_decode($_COOKIE['bbm_profile']);
$data_member = $this->member_model->curl('membership_manager/membership','get',array('bbm_id' => $bbm_profile['bbmId']));
$data_member = (array) json_decode($data_member['string']);


// $transaction_id = '535cL.97119035182809088';

// got transaction id from passing controller payment -> checkout
$param_shoppingcart = NULL;
$param_shoppingcart['transaction_id'] = $transaction_id;

// get cart here
$shoppingcart = null;
$shoppingcart = $this->general_model->curl('transaction_manager/cart_only','get',$param_shoppingcart);
$shoppingcart = (array) json_decode($shoppingcart['string']);

$shoppingcart_detail = $this->general_model->curl('transaction_manager/cart_details','get',$param_shoppingcart);
$shoppingcart_detail = (array) json_decode($shoppingcart_detail['string']);

$tmp_items = NULL;
$tmp_items = array(
	array(
		'id' => '9999',
		'name' => 'Total belanja',
		'price' => (int) $shoppingcart['total_cost'],
		'quantity' => (int) 1
	)
);
// if ( ! empty($shoppingcart_detail))
// {
	// foreach ($shoppingcart_detail as $key => $rsd)
	// {
		// $rsd = (array) $rsd;
		// // var_dump($rsd);die;
		// $tmp_items[] = array(
			// 'id' => $rsd['deal_package_id'],
			// 'name' => $rsd['deal_package'],
			// 'price' => (int) $rsd['package_price'],
			// 'quantity' => (int) $rsd['purchase_quantity']
		// );
		
	// }
// }

$post_data = array();
$post_data['payment_types'] = array('CREDIT_CARD','MANUAL_TRANSFER');
$post_data['va_custom_expiry'] = array(
	"order_time" => date("Y-m-d H:i:s")." +0700",
    "duration" => 1,
    "unit" => "minutes"
);

$tmp_manual_transfer = array();
$tmp_manual_transfer['manual_transfer_accounts'] = $this->config->item('payment_manual_bbm');;

// $tmp_manual_transfer['manual_transfer_accounts'] = array(
	// array(
		// 'number' => '5320322555',
		// 'holder' => 'PT Online Pertama',
		// 'branch' => 'Jakarta',
		// 'bank' => 'BCA'
	// )
// );

$tmp_manual_transfer['webhook_url'] = $CONFIG['LAKUPON_WEBHOOK_BASE_URL'].'purchase/bbmcheckout_manual'; // hit ke arjuna
// $tmp_manual_transfer['webhook_url'] = 'http://localhost/arjuna/purchase/bbmcheckout_manual';
// $tmp_manual_transfer['webhook_url'] = 'http://staging.lakupon.com/purchase/bbmcheckout_manual';
// $tmp_manual_transfer['webhook_url'] = 'http://www.grevia.com/lakupon_notif_manual';
$tmp_manual_transfer['unique_payment'] = array(
	'substract' => FALSE, // if false then include unique_payment in total
	'value' => (int) $shoppingcart['payment_code']
);
$tmp_manual_transfer['custom_expiry'] = 24; // expire in hour
$tmp_manual_transfer['custom_instruction'] = 'Mohon lakukan pembayaran sebelum 2 x 24 jam';

$post_data['manual_transfer'] = $tmp_manual_transfer;

// ========================================================================

$firstname = $lastname = $phone = ' ';
if (isset($data_member['first_name'])) { $firstname = $data_member['first_name']; }
if (isset($data_member['last_name'])) { $last_name = $data_member['last_name']; }
if (isset($data_member['phone'])) { $phone = '+62'.$data_member['phone']; }

$post_data['customer'] = array(
	// 'shipping_address' => $tmp_shipping_address,
	'email' => $CONFIG['LAKUPON_CHECKOUT_EMAIL'],
	'firstname' => $firstname,
	'lastname' => $lastname,
	'phone' => $phone,
);

$post_data['items'] = $tmp_items;

// $post_data['environment'] = 'DEVELOPMENT'; // DEVELOPMENT or PRODUCTION
$post_data['environment'] = $CONFIG['MODE'];

// $post_data['redirect_url'] = base_url().'thankyou/'.$shoppingcart['transaction_id']; // DEPRECATED 22 mei 2017 
$post_data['redirect_url'] = base_url().'bbmc_voucher'; // Show message from latest transaction ACTIVE 23 MEI 2017
$post_data['is_for'] = 'bbm';

// print_r($post_data,1);
// print_r(json_encode($post_data));
// die;

$ch = curl_init();
$bbm_staging_url = $CONFIG['BBM_INQUIRY_URL']."/cart?access_token=".$CONFIG['BBM_ACCESS_TOKEN'];

curl_setopt($ch, CURLOPT_URL, $bbm_staging_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
curl_setopt($ch, CURLOPT_POST, 1);

$headers = array();
$headers[] = "Content-Type: application/json";
$headers[] = "Accept: application/json";
$headers[] = "X-Passport-ID: ".$CONFIG['BBM_PASSPORT_ID'];
$headers[] = "X-Passport-Email: ".$CONFIG['BBM_PASSPORT_EMAIL'];
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$callback_result = curl_exec($ch);

if (curl_errno($ch)) 
{
	// error happened no response from midtrans
	// try again 2 times and #3 times redirect to home
	// $callback_result = curl_exec($ch);
	
	$try = $current_full_url = NULL;
	$current_full_url = current_full_url();
	// die;
	if ( ! isset($_GET['try']))
	{
		// run script 
		$callback_result = curl_exec($ch);
		
		// redirect
		redirect($current_full_url.'?try=1');
		die;
	}
	else
	{
		if ($_GET['try'] <= 2) 
		{
			// run script 
			$callback_result = curl_exec($ch);
			
			// redirect 
			$current_full_url = str_replace('try='.$_GET['try'],'try='.($_GET['try']+1),$current_full_url);
			redirect($current_full_url);
			die;	
		}
		else 
		{
			// run script 
			$callback_result = curl_exec($ch);
			
			// redirect to home
			$this->session->set_flashdata('error_header','Mohon maaf terjadi kesalahan. Silakan coba lagi.');
			redirect(base_url());
			die;
		}
		
		
	}
	// echo $current_full_url;
	// die;
	
	
	// echo 'mohon maaf terjadi Error:' . curl_error($ch);
	$this->session->set_flashdata('error_header','Mohon maaf terjadi kesalahan. Silakan coba lagi.');
	$checkout_url = base_url();
	redirect($checkout_url);
	die;
}
else 
{
	// $callback_result = '{"status":"success","data":{"redirect_url":"https://stg-checkout.veritrans.co.id/cart/b4a335c2-8bdf-4171-84ea-08bf9a4a5825?src=bbm","order_id":"Lakupon-1492396848432"}}';

	// get response callback
	$response = (array) json_decode($callback_result);
	$response['data'] = (array) $response['data'];
	
	// get transaction_id & redirect
	$redirect_url = $response['data']['redirect_url'];
	
	// UPDATE CART status to CHECKOUT & save midtrans order_id to reference _number
	$param_shopping_cart = array();
	$param_shopping_cart['id'] = $shoppingcart['id'];
	$param_shopping_cart['reference_number'] = $response['data']['order_id'];
	$update_cart = $this->general_model->curl('transaction_manager/cart_update','put',$param_shopping_cart);
	
	// var_dump($post_data);
	// var_dump($callback_result);
	// var_dump($update_cart);
	// die;
	redirect($redirect_url);
	die;
	
}
curl_close ($ch);
// die;
?>