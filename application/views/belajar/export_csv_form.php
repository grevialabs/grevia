<?php
if(isset($_SESSION['ceespi']))
{
	// var_dump($_SESSION['ceespi']); exit;
	$i = 0;
	foreach($_SESSION['ceespi'] as $key => $data)
	{
		// Manual transfer web / BBM
		if($_SESSION['ceespi']['payment_type'] == 1 || $_SESSION['ceespi']['payment_type'] == 35)
		{
			$price = str_replace(array(',','.00 CR'),'',$data[3]);
			if(is_numeric($price) && $data[5]!='done')
			{
				$in[$i]['prc'] = $price;
				$in[$i]['thou'] = floor($price/1000)*1000;
				$in[$i]['txt'] = preg_replace(array("/[^0-9a-zA-Z -._+@]/","/[*]/"),"", filter_var(strip_tags($data[1]), FILTER_SANITIZE_STRING));
				$in[$i]['rel'] = $key;
				$i++;
			}
		}
		
		if($_SESSION['ceespi']['payment_type'] == 2)
		{
			$price = str_replace(array(',','.00'),'',$data[8]);
			if(is_numeric($price) && $data[9]!='done' && $price != 0)
			{
				$in[$i]['prc'] = $price;
				$in[$i]['thou'] = floor($price/1000)*1000;
				$in[$i]['txt'] = preg_replace(array("/[^0-9a-zA-Z -._+@]/","/[*]/"),"", filter_var(strip_tags($data[4]), FILTER_SANITIZE_STRING));
				$in[$i]['rel'] = $key;
				$i++;
			}
		}
	}
	
	
	$in['payment_type'] = $_SESSION['ceespi']['payment_type'];
	
	// var_dump($in);
	
	$res = $this->payment_model->curl('payment_manager/sc_price_matching', 'post', $in);
	
	// print_r($res['string']);
	
	$hasil = json_decode($res['string']);
		
	unset($in['payment_type']);
?>
<form method="POST" class=nocalm>
	<button class="btn btn-success" name="exit" value="1">Close File</button>
	<button class="btn btn-success" name="export" value="1">Save Sesion as CSV</button>
</form>
<h3>
<?php echo $_SESSION['ceespi']['name']; ?>
</h3>
<form method="POST">
	<table class="table table-striped table-bordered table-hover">
	<thead style="font-weight:bold">
		<tr class="info" style="height:40px">
			<td>Payment Code &nbsp;</td>
			<td style="margin-left:10px;">Txt &nbsp;</td>
			<td style="margin-left:10px;">Matches Transaction &nbsp;</td>
		</tr>
	</thead>
<?php
	foreach($in as $kuda)
	{
?>
		<tr class="mahmud">
		<td style="text-transform:capitalize;">
		<?php echo number_format($kuda['prc']); ?>
		</td>
		<td style="text-transform:capitalize;">
		<?php echo $kuda['txt']; ?>
		</td>
		<td style="text-transform:capitalize;" class="silit">
		
		<?php
		if(!empty($hasil->$kuda['prc']))
		{
			foreach($hasil->$kuda['prc'] as $aak)
			{
				if($aak->payment_code_match == 'payment_code_match')
				{
					echo '<div class="alert alert-success" role="alert">';
				}
				else
				{
					echo '<div class="alert alert-warning tempek hide" role="alert" >';
				}
				$ga_data = ($aak->total_cost-$aak->shipping_cost)+$aak->payment_code.','.$aak->shipping_cost;
				echo '<input type="checkbox" class="wowo" name="sc_id[]" value="'.http_build_query(array('issue'=>$aak->id,'email'=>$aak->email,'transaction_id'=>$aak->transaction_id,'ga_data'=>$ga_data,'rel'=>$kuda['rel'],'paid'=>$kuda['prc'])).'">';
				echo ' ';
				echo $aak->first_name;
				echo ' ';
				echo $aak->last_name;
				echo '<br>';
				echo ' Rp. '. number_format($aak->total_cost + $aak->payment_code);
				echo '<br>';
				echo $aak->transaction_id;
				echo '<br>';
				echo '<strong>'.$aak->confirmation_name.'</strong>';
				echo '<br>';
				echo $aak->created_at;
				echo '</div>';
			}
		}
		else
		{
			echo '<div class="alert alert-danger" role="alert">No Transaction Matches</div>';
		}
		?>
		</td>
		</tr>
<?php
	}
?>
		<tfoot>
			<tr>
				<td colspan=100% style="padding:10px 10px">
					<button type="submit" class="btn btn-success" name="issue" value="1">Issue</button>
				</td>
			<tr>
		</tfoot>
	</table>
</form>
<?php
}
else
{
?>
	<form method="post" enctype="multipart/form-data">
		<label style="margin-top:5px" class="col-sm-2 control-label">CSV Files</label>
			<div class="col-sm-10" style="margin-bottom:20px">
				<input type=file class="form-control" name=f_file accept=".csv" required/>
			</div>
		<label style="margin-top:5px" class="col-sm-2 control-label">Payment</label>
			<div class="col-sm-10" style="margin-bottom:20px">
				<select name="pete">
					<option value='1'>BCA</option>
					<option value='2'>Mandiri</option>
					<option value='35'>BBM Manual transfer</option>
				</select>
			</div>
		<button class="btn btn-success" name="upload" value="add">Open File</button>
	</form>
<?php
}
?>

<script>
	$('.silit').each(function(){
		var tai = $(this).find('.tempek');
		if(tai.length > 0)
		{
			$(this).prepend('<button type="button" class="momok">Show/Hide '+tai.length+' matches</button>');
		}
	})
	
	$(document).on('click', '.momok', function(){
		var tdsilit = $(this).parents('.silit').find('.tempek').removeClass('hide');
		$(this).removeClass('momok');
		$(this).addClass('nonok');
	})
	
	$(document).on('click', '.nonok', function(){
		var tdsilit = $(this).parents('.silit').find('.tempek').addClass('hide');
		$(this).removeClass('nonok');
		$(this).addClass('momok');
	})
</script>