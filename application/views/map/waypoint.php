<?php 
$full_name = NULL;
$list_journey = $paramj = $string_map = $first_lat = $first_lng = $first_location = $first_creator_date = $last_lat = $last_lng = $last_location = $last_creator_date = NULL;

$paramj['creator_id'] = 1;
$paramj['order'] = "creator_date DESC";
$paramj['creator_date'] = date('Y-m-d');
$paramj['paging'] = TRUE;
$list_journey = $this->journey_model->get_list($paramj);
$list_journey = $list_journey['data'];


if ( ! empty($list_journey)) 
{
	// debug($list_journey);die;
	$string_map.= "[";
	foreach($list_journey as $key => $lj)
	{
		if ($key == 0) {
			$full_name = $lj['full_name'];
			$last_lat = $lj['lat'];
			$last_lng = $lj['lng'];
			$last_location = $lj['location_name'];
			$last_creator_date = date("d-m-Y H:i:s", strtotime($lj['creator_date']));
		}
		
		$string_map.= "{";
		$string_map.= "lat: ".$lj['lat'].", lng:".$lj['lng'];
		$string_map.= "}";
		
		if ($key != count($list_journey)-1) {
			$string_map.= ", ";
		} else {
			// 
			$full_name = $lj['full_name'];
			$first_lat = $lj['lat'];
			$first_lng = $lj['lng'];
			$first_location = $lj['location_name'];
			$first_creator_date = date("d-m-Y H:i:s", strtotime($lj['creator_date']));
		}
	}
	$string_map.= "]";
}
else 
{
	$first_lat = -6.2112255;
	$first_lng = 106.7822477;
	
	$last_lat = "";
	$last_lng = "";
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Polylines</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
	  #last_online{
		background: #fff;
		min-width: 300px;
		max-width: 100%;
		border: 1px solid grey;
		padding: 15px;
		position: fixed;
		left:0;
		top: 0;
		z-index:9;
		overflow: auto;
	  }
	  #historyLocationModal{
		  z-index:99;
	  }
    </style>
  </head>
  <body>
  
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-4.6.3.min.css?=20160708" >
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
	
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery.1.11.2.min.js?v=20160708" ></script>
	<script type="text/javascript" src='<?php echo base_url()?>asset/js/jquery-ui-1.10.4.min.js?v=20160708' ></script>
	<script type="text/javascript" src='<?php echo base_url()?>asset/js/jquery.validate.min.js' ></script>
	<script type="text/javascript" src='<?php echo base_url()?>asset/js/ui/jquery.ui.core.min.js?v=20160708' ></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/bootstrap.min.js?v=20160708" ></script>


    <div id="map"></div>
	<div id="last_online">
		<button class="btn btn-warning btn-block" onClick="window.location.reload()">REFRESH DATA</button><br/>
		User <b><?php echo $full_name ?></b><br/>
		Last Location:
		<?php echo $last_location ?>
		<br/> Last Online: <?php echo $last_creator_date; ?>
		<!--Motoko Kusanagi (Scarlett Johansson) adalah seorang agen cyborg yang hidup di era tahun 2029 dimana robot-robot dengan kecerdasan buatan hidup berdampingan dengan manusia. <br/>

		Kusanagi dan sejumlah anggota tim keamanan, Public Security Section 9 memiliki kewajiban untuk menghentikan kejahatan yang dilakukan oleh hacker terkenal, the Puppet Master dan mengantisipasi pergerakan dari penjahat misterius yang ingin menghancurkan perkembangan teknologi dari Hanka Robotic.<br/>
		-->
		<br/><br/>
		<button class="btn btn-info btn-block" data-toggle="modal" data-target="#historyLocationModal">Show <?php echo count($list_journey)?> last data</button>
	</div>

	<!-- Modal -->
	<div id="historyLocationModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">History Location</h4>
		  </div>
		  <div class="modal-body">
			<?php 
			if ( ! empty($list_journey))
			{
				foreach ($list_journey as $key => $lj)
				{
					?>
					<?php echo $key+1; ?>. 
					Last Location:
					<?php echo $lj['location_name'] ?>
					<br/> Date: <?php echo date("d-m-Y H:i:s", strtotime($lj['creator_date'])); ?><hr/>
					<?php
					
					// if ($key == 9 ) break;
				}
			}
			?>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>

    <script>
	  // https://developers.google.com/maps/documentation/javascript/examples/polyline-simple
	  
      // This example creates a 2-pixel-wide red polyline showing the path of William
      // Kingsford Smith's first trans-Pacific flight between Oakland, CA, and
      // Brisbane, Australia.
		

      function initMap() {
		  
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: {lat: <?php echo $first_lat?>, lng: <?php echo $first_lng?>},
          mapTypeId: 'terrain'
        });

        // var flightPlanCoordinates = [
          // {lat: 37.772, lng: -122.214},
          // {lat: 21.291, lng: -157.821},
          // {lat: -18.142, lng: 178.431},
          // {lat: -27.467, lng: 153.027}
        // ];
		
		var flightPlanCoordinates = <?php echo $string_map ?>;
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 4
        });

        flightPath.setMap(map);
		
		var start_marker = new google.maps.Marker({
          position: {lat:<?php echo $first_lat?>, lng:<?php echo $first_lng?>},
          map: map,
          title: 'Start journey'
        });
		
		var last_marker = new google.maps.Marker({
          position: {lat:<?php echo $last_lat?>, lng:<?php echo $last_lng?>},
          map: map,
          title: 'Last online location',
		  icon : 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
        });
		
		
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1tbIAqN0XqcgTR1-FxYoVTVq6Is6lD98&callback=initMap">
    </script>
  </body>
</html>