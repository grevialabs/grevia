<?php
$param = array(
'paging' => TRUE,
'order' => 'ArticleID DESC',
'offset' => 4,
);
$list_articles = $this->article_model->get_list($param);
$list_articles = $list_articles['data'];
// <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>

?>
	<style>
      #map-canvas {
        height: 700px;
		width: 700px;
        margin: 0px;
        padding: 0px
      }
    </style>

  <script src="https://maps.googleapis.com/maps/api/js?key=&v=3.exp&signed_in=true&libraries=places"></script>
    <script>
var map;
var infowindow;

function initialize() 
{
  // var pyrmont = new google.maps.LatLng(-33.8665433, 151.1956316);

  var myLatIng = new google.maps.latLng(-6.1893496,106.8358383);
  //var pyrmont = new google.maps.latLng(-6.1893496,106.8358383);
  map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: myLatIng,
    zoom: 14
  });
  
   // Try HTML5 geolocation
  // var pos;
  // if(navigator.geolocation) {
    // alert('jalan ni gelo');
	// navigator.geolocation.getCurrentPosition(function(position) {
      // pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      // var infowindow = new google.maps.InfoWindow({
        // map: map,
        // position: pos,
        // content: 'Your location is here.'
      // });

      // map.setCenter(pos);
    // }, function() {
      // handleNoGeolocation(true);
    // });
  // } else {
    // // Browser doesn't support Geolocation
    // handleNoGeolocation(false);
  // }

  var request = {
    location: myLatIng,
    radius: 1000,
    types: 'store'
  };
  //alert(pos);
  
  // var marker = new google.maps.Marker({
      // position: myLatlng,
      // map: map,
      // title: 'Hello World!'
  // });
  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch(request, callback);
}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
    }
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }
  //alert(content);

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

<div class="container">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div id="map-canvas"></div>
		</div>
		<div class="col-md-2"></div>
	</div>
	<div class="clearfix"></div>
</div>