<?php

?>

{
   "html_attributions" : [ "Listings by \u003ca href=\"http://www.openrice.com/\"\u003eOpenRice\u003c/a\u003e" ],
   "next_page_token" : "CoQC-AAAAHNLgR9i4s7ZJT4CdsvYl92dNgUqtkzNJRz6nhHHaHDK05pZXr-t6OVRSH0V1IEvy2nbQzRTA6z_neVhiFqNAjvMoUQAw7lUkkhE431I6U2_xt3BAKRWevUE-AlZrNpEGyTCwtDzqYRHjQAgrdJKaHEEmCQLPp1mUeby1pwFLtU6bowVoXWkvvbGz-Jb_a15d2j4Y5t4xVATT37mR159CwBVdiQLrf_oyCBpaxlVlr0teNb3t7ShZsQEXLa5R-LnZ-9w9cmKCk9TsU4B5HhLWpJLvquhhwcNcpyzhr_T9NkiNO7oEWmtEV1MskjoieyvOoiP7bUe6VrkH5lU5ectSNsSEK0YaLwMELyIdaPDd9_3oo4aFKsmKFaudLVpHIYQQ07OlrQz3ptn",
   "results" : [
      {
         "geometry" : {
            "location" : {
               "lat" : -6.192843,
               "lng" : 106.841073
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "166f24669ae8f315114dfda460b1353dfdbadcbf",
         "name" : "Oasis Restaurant",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 331,
               "html_attributions" : [],
               "photo_reference" : "CnRuAAAAD_GdMhIiS9I8Z6fcjKY0j7GMYXUaT14_c5DdRuQ1h0Z0GzDabJkTPdCxEGzV8DqvgMvnaOg5iIT04q1vtsXyMnuzgI-vd58RH0y2bnbQQSmb6sVhTlpINfqWOFIbyulnRNgejaVznZHTmLRbJSTkhxIQC_bgEOl_E2R3eZZdT0ZX6hoUF9-lDMnely80GNzdknZCqMhISP0",
               "width" : 332
            }
         ],
         "place_id" : "ChIJpW0uYT_0aS4R3sG0n_cahTQ",
         "rating" : 4.1,
         "reference" : "CnRjAAAAyaqdrJYxUPdDdXqa-X74Km4TIuylWcG0dCeSjQv1-hb98J6OaogWe96XtEQ-NzBlsLtmmDVkFuoRxnbVa2WgcbH5Xv3lCmTTBC_PTh4rtPuwtrwhyezVHD_IRQIdI4iW5aRGAkRix2WXbE1fNwgY7hIQwbgtFHRQ0ieqxvJ4c8gRlhoUb9Pfv_1szH7c0PEVJkEzqRKJhHs",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "JL Raden Saleh Raya No 47, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.186699,
               "lng" : 106.835775
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "79a29356d1fb35be9bc4fccdf079669352d4878b",
         "name" : "Blue Elephant Restaurant and Cooking School",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 748,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAACKR_vZ9vVyHnw2GbyW5L3CnXyeJRScn4heepnpyot5VHFcke-_-bi41Jfo-DwtVsGobdL6m8Et_UxxAeNsSLC6GsTDhSR2PK0IoEN1BkOfNKJoNDSpL15l6_sC-UOos5HcHj0aTlPBGhRNF0Ms8kwxIQ1_lXoqncE_Tm8TT-yt0dUxoUcyBTlc2ekTZHVk7g747k06cU3Q0",
               "width" : 1024
            }
         ],
         "place_id" : "ChIJ86MccDr0aS4RhXdtGLIPaK8",
         "reference" : "CoQBfwAAAPQ6GwNkiw1tvtdKQThtJ4cav9F9MSJQ5zGtFMmfNyN-CQ2XKSZ8xQ9s1j5xBQlJEjv8Yu5_dXvCE_PY2LLO_ssh8XekvFVD2M_5bakyBNAquTxCgTf5eWrBy1KR_pJFudU8P9C5PFduN1lzu7Ur20aPCtteTQohwCXNE3u5WcISEhAioVtKPJXC4GWFOtTasjVhGhRuYjMtp_rGj4ND9wO8n45JcCikkw",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Taman Cut Mutiah No.2, Jakarta"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.192936,
               "lng" : 106.840106
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "209474f0e1dcf4bdef2e3c7c5de614f3c243ab64",
         "name" : "Aljazeerah Restaurant & Cafe",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 760,
               "html_attributions" : [ "From a Google User" ],
               "photo_reference" : "CnRoAAAAeJHU06ZZkHErXJPnzwciWDqUb1dhRyX4k7VxgzZXhBK3UYMN5JoYxUn-DK0vuRnW_50cmxYvAeLiu2yFQWfnZ7kpZShDeEbL-83XjejW9uGFIP83BuZfrB95PH4g0IewCkteIVhpD2dhzFPXEIIp-xIQ1jpz6UcqoN2KC5_w5R9TeRoUgAyK775OJTNnywtWgeMrUNg0gwo",
               "width" : 1344
            }
         ],
         "place_id" : "ChIJaVt7tUD0aS4RQoyPm2Ksa34",
         "rating" : 4,
         "reference" : "CnRvAAAA4pnCCH7sGDt0y0RDRHW1J0dZYN89h0cpHyr04FzLjBgFoo7Xy6eREtRtIn_ozQUUCWY3U56wlPT4nmVGN3n0T9erCbLb5WknQACNV9kxuPhKQamX5vQg0rwNCYy3gxXPgwn2PiM3zLSVTt1z7J1eehIQdHUo8KVcDJM-wRRi1nfV-BoUWsihH7UqgzhSEtoHzlZadgFT6UU",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Raden Saleh Cikini no 58, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.187554,
               "lng" : 106.834247
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "fba1443d752fdaee882eeaf7bdd5c1d04975dcd4",
         "name" : "Sofyan Hotel Betawi",
         "opening_hours" : {
            "open_now" : true,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 701,
               "html_attributions" : [],
               "photo_reference" : "CnRqAAAAAJgbbziwcWa_ZraEQ1PMj_woCiuN11_h0cLeBWZeeqZ_3uGxhmSw8BSce7_-dg9w-a8_BfeiN8v2VBE5WhvOCYk7q6wFhTYVajpZe7jq-lJFS0bk55Wl6CFnx0Q9gu8uBCoZtgpq7GMnqxfSPEfHuBIQtwXvHfFPGHiV3RrX9Ep_OxoUwRP57qeA1dSqGv_aIs59c2x6k_I",
               "width" : 701
            }
         ],
         "place_id" : "ChIJDQeEFTr0aS4R__ua3Ii48g0",
         "rating" : 3.9,
         "reference" : "CnRmAAAAtROYKf5trLCJrj0mwi42xLDs4MbCi_JBobaAaimkyd87To5rymqdvhtgR556LGw-6NU11v1JGNRtB2CTHk1nQgpS6E1By-zIzKdULaEZql4mgBwFGizTBL2LclhFw56PyV3Rv0Ap4ViFo7NFJdmtpBIQwZ64-G6puDZKwtBpVksLyRoULYVjWrGFpOS7SDGd86yto0aa8nw",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "lodging", "food", "establishment" ],
         "vicinity" : "Jl. Cut Mutia No.9, Menteng, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.194069,
               "lng" : 106.836327
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "7923001208e59cc8e70f48e8f12a1096b3a227db",
         "name" : "Lara Djonggrang",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 1200,
               "html_attributions" : [],
               "photo_reference" : "CpQBggAAAGwT4Gl0afxmvZnqsuWkXVVfO_MOPwLWbxZWMyfcoY9U6-I0ErGoqu5u2iRMfAEMBRJxh8ZOiRkCCcDXEem22fHl8beBhgP7PbslIcSjA-ONqm9UAHaLpjeeKjJPJ5CQ1E8zcnWWlwdxI9tERp6SZQgCmKokaBYwOiPJIDmR6rB7yWXbBETy46tF0jduCAZBTBIQlCV7mpb4nUiIX1Zu62nhWhoUagLX76X66ZRseJI5_8wRBtPOf9I",
               "width" : 1203
            }
         ],
         "place_id" : "ChIJZ3XYYj70aS4RyDFzTQx4BFY",
         "rating" : 4.2,
         "reference" : "CnRiAAAAh6rCnuT74ykNENnWfVeUH384CR2Z2FYRKksLj33BVXk72lpa41KXG5EJmtsIFTQdurZG8qswt3tXwYdRI1tSSOJ2vtcUw0wtfclxFG-792HPezENwhfqrye0GnD6IuNG_2_e-wE-K9hGwLz0zaGs_hIQcdLxP76-eTUaQDAjbhf9rRoUJmJBMBf8CPfcqa8GqW5Du-PelAg",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Teuku Cik Ditiro No.4, Menteng"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.187066,
               "lng" : 106.832219
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "f0483f38fd3ad9e3e8c661906727fec11dfaa941",
         "name" : "Bakmi Gang Kelinci - Johar",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "place_id" : "ChIJVx9XkTr0aS4RCVpShh3_6Js",
         "reference" : "CnRuAAAALJ55UHmWQaGgfMaEsAPv8SsGineGSotZGjtiagGFWz2WFYvdn7vcwMuxx8FeRrvoYR7C3q8ZCwDrUHC-253FaUa3RVU6Ze7nN5tluzhXnRtqXuRzNDuhfB3GqcDETAtqX9sG55DzYTVcTwk09WYdEBIQO9p5yehRH31n9s2e7p2-uBoUE-U8NSCe8ZuGJHH03MYlF325Tp4",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Johar No.6E, Menteng, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.192522,
               "lng" : 106.838767
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "5af1553594091e389b0b0c799be3265f8b5f13ba",
         "name" : "Gerai Es Teler 77 Hotel Ibis Budget Cikini",
         "place_id" : "ChIJTWkBsiL0aS4RzYiq5G56c2w",
         "reference" : "CoQBfQAAAN7a_kRgsLLOQDe7iX6FWoPGsy3tvCQWQM1Qn_nhxwc-VM3CCJ-HRBv1KqqsmBcCIaENKMQzjLlFAUFqmnRItD_VGegOMPX_jePP231elMDdLbmJIBuolisMhZIQuXRj2ADcHmInbpD-s4Lphsnbv-SKzMTkNHJgI2cFZYCouIWpEhBoYeUVYVLPmjRwOUGQpp1DGhTl6cz1k25POZulJ0fRi7PvwnUgsQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Hotel Ibis Budget Cikini Lt. GF 07, Jl. Cikini Raya No. 75, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.193473,
               "lng" : 106.838836
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "4fdac99db39a4870413107700f11116dd5189cef",
         "name" : "Texas Chicken",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 800,
               "html_attributions" : [ "From a Google User" ],
               "photo_reference" : "CnRoAAAAAc2Y64Uu7onDludt1vQUPoBodA7RyMTeL5WK13YHxQ5JGF0Bv913MJpbexscyPqU3iBdDI4Um0k2avEHede7l80nr7qykIYjHKQtUJJNIxntv4I5zYY5F7-vRsDEHrv0zmH3NSLQtiKtAgcuxUKQAhIQ-uJmvHDPBqLpCQ7KgMYZXhoUoLSlwXP8e5cdAq6Fen7t1krPZDA",
               "width" : 800
            }
         ],
         "place_id" : "ChIJRy0T3Dj0aS4REPNCP2XwpPs",
         "reference" : "CnRhAAAA3k-tw3So9xbFTNodriHcKu0G04JMJlTIgYeNFWXMO2zqk8x8F_b4nBJqn5Hj6KtjT8aoi-iCoI4xxjqV0mnp-ygYHIEtCTj81pUksBYH4GMbVrD6uiS8ESNhb4lJ3d4lA24s4w2e5QzmPIDnT8gexBIQOJYKJo0Y_sVld9HZOOQ1vhoUB6UYSicO_6nqlTuFZUisyVYlNrQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Cikini Raya No. 60 A, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.189213,
               "lng" : 106.839628
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "7a81518b18fb8132a7a1c3f3a36cb39b70436d42",
         "name" : "Mc Donalds Dharmala Cikini",
         "opening_hours" : {
            "open_now" : true,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 1224,
               "html_attributions" : [ "From a Google User" ],
               "photo_reference" : "CnRoAAAATVJ6zOEnAeajWEVE39InG5cWQ-zuUtINebOsgDUxt-A4KwdCe9qctIBXf01SKlPw-Fscj0Gnpz0LdRo_twTK0tcfsZh4hSwukclXPJE9Gnb5C8B8EIanSI0KSr1CmrPGx1Vddm5imyeSIbmfpqCqMxIQpCyISoQcupctLz3kvrst9hoU65p7yfqpQzgcjGMRLiaOSq-OBvY",
               "width" : 1632
            }
         ],
         "place_id" : "ChIJmf-dURX0aS4Ree4gX9ZbVW0",
         "price_level" : 1,
         "reference" : "CnRtAAAAGeGrPUZzkr5wjk09tLZ7bxqMAIZm4n_Okpw4cbAFNFguDVMvTPbPB8syaUPrOXAgD0D-51nuezVV--e-m5tGcJ8PmoRAHe971fIVV-cqj0JNekhqi2H38JoqpQhfJGsIfq5AQbufUJPdQQ2dcGQLBRIQQfr5aE-nuCzpvIWz6vpFiBoUCmC1ANS9FC4e8bQ9x9EuFYyoEyg",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Pegangsaan Timur No. 1 A, Ground Floor, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.192893,
               "lng" : 106.83853
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "8819e27528adb974aa8f1b351f716e628dd87671",
         "name" : "Gado-Gado Bon-Bin",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "place_id" : "ChIJE4jF6z70aS4RDFkz90tcPZE",
         "rating" : 3.9,
         "reference" : "CnRlAAAAd4tFsQNO_91MGWaq355Iu7ab2_Po0dMVelHArgdGQfR-0F9Tz6gqQ2VpQONA8M1-LnB8tsTseQKWl6fhLqE9pUsYY-F_-nQH7OYH7HUtGB9i_KEI9KgMeFxk4gzWxvHlJG51qULQHYPm43NtU3vXgxIQ3FRw7hR4FSrTpp3LC7CN4BoUivcMPuOEgn0QmjFkhLg_TYja6y8",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Cikini IV No. 5, Cikini, Menteng, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.192155,
               "lng" : 106.836217
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "a047b8ddbd829180dbc377a33f4fba7ac6bb303a",
         "name" : "Restoran Trio",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "place_id" : "ChIJg__3Wjn0aS4RXvVQG4enmyk",
         "reference" : "CmRgAAAARWEnIp1kfb5yK-kaji8OQ-gPcoNnWV8vt1jwyFbXmgrM2XN688stYn9b_oY8QoEspgGASIWhbK8-UstqJdbbf5l1bk33BBxtAtLD9ObxCzqMh_EcfjTSNDSkeCmxfwjcEhD-MhLUKIW4NH_F6dxt9w8wGhQ8t3YtQBJrtIZca2O3i2cAag_6-A",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Rp. Soeroso No. 29A, Cikini, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.187817,
               "lng" : 106.836897
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "f497c6406cae2f3a74671db0cae392669d2e375c",
         "name" : "Vietopia",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 495,
               "html_attributions" : [],
               "photo_reference" : "CpQBhgAAAJR9F-iLx1QMdtYj-5kb-aDu82Fg7YGmZzMGwdICZVBjbTfSnXyIu8QWX2d9c-dOAps6okTKjEOV24b1b_07d6sdVqgr1_NEvUt8IX_Psuq1cKmZ4jV0e0z9QpIu_KxMuLcTNHiJKplZo7gYPgbB2rJBTZ24IvnOlKtpQDQDYYYJD04l8elp6cq65EKl6aaV-hIQh7zRjAdUrsWwgD6pGXq-dhoUsmw6DZOAwYfD1ZI-QAf-AGiJJp8",
               "width" : 496
            }
         ],
         "place_id" : "ChIJv4zbIjn0aS4Rq3AEeKSPj2o",
         "reference" : "CmRbAAAAruFZyEYkP-fUPmd50MiofidhCYl60aKIYNlmDnLac4Bi0b56bS7WU1WK86K0APLtJqdox5JaWzglUHEUNXTQ-myuD0BiOJq6gcqbHh-SpKbrRfCJKyl-eAhIIRrGd7C2EhB8CX2bREaIbkeS_hlVpYPIGhT8x6JKDfR2Q1NnS8yKQXgtHrZydQ",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Cikini Raya No. 33, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.188219,
               "lng" : 106.836801
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "c982bd6f5e8cc4f98681fa9c2c88bfa8df053faa",
         "name" : "HEMA RESTO",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 1536,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAAE9jxtuHKSE0uLC_wo8NG4W7GP-rDnXG7MmyASnavidy1YTIfdr5XkbJrFf2xJ6apB6gn9hkbZVHjhDyXlHAkAJgOWxPf_5GhKzPsZ50wEJAHr2FL2Lzl43D7zmOcuz1UrYdUvm_K2Jrw6NCsicrhbBIQrgDHVXm5wxU600gO8lZT_RoUKEnzH0Rb2IRZafIsReCcVCnlpa0",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJZ_0OJj_0aS4RmaXzF0MbF9o",
         "reference" : "CmReAAAAGOe-bCuS0U8IL8o3ORnoIo2yfgGWiANrzOKJkQGn9gyFsYwEz0OxY94vbBeajAyPlHM4Lrziv1beBdrl58Q6x0A2n8RJ0KCtUDpVl82aRFyKEPi10SJ-3Go7mO4DpzydEhBNAgKm-K5P8I-CkQpOSUr7GhSjBSKOBCNfOjiUnKrXPRz0fZL53g",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Menteng Huis Jln. Cikini Raya No.2-4, Menteng, Jakarta"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.1856,
               "lng" : 106.836
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "2cc3f008800ee87cae7bdb65d95ccc6510e7d8a4",
         "name" : "Istana Restoran",
         "place_id" : "ChIJqfk4Wzf0aS4RzsA9z7WUjJo",
         "reference" : "CnRjAAAACHvsyvVVBi1uzW-haH9ReG-hgRc2M2FkRatZxcU6qZLyWQCvZPN8x_ycET8N9xOxwWdgBdezO24jmmrBYvoZ6MuF4XoD7rr0AGGe5HaWSPB7QH3xcLarT3btkPRCWin-x23-Bet34W7-v9-6oEKMWxIQAX1eVKmdOmxYqz9VrQ4eZRoUANIXtt5Rbr0FKxJJEIEtwNmNCpI",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl Menteng Raya No 29, Gedung Tedja Buana, Menteng, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.189239,
               "lng" : 106.837847
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "2fdbaa7228ee0be8554e4be2287aa8c254f1ef95",
         "name" : "Bombay Blue",
         "place_id" : "ChIJr2m91j70aS4R2Tp8yHJXgDI",
         "reference" : "CmReAAAAfRV1UdaWjMsTaT5R_MGrOo6ZjT1YpKsZQ9CNdiDp0wscC-Sl5hDJNH5PATO8VPciOMS9nkYeV-btky_WSvv9osSzgykXqOol8CXMiw0WfTHWb3Pmevd7gmyOICUZvNj7EhCR2GzMGzXn4J70L7kECLuSGhTerrfL2FsdEpePow0ckFX9L2pMEg",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Cikini Raya No. 40, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.192203,
               "lng" : 106.838812
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "15f541531d6a07f3e75b27baec0e833ab5b9460d",
         "name" : "Tjikini 17",
         "place_id" : "ChIJsSyuJj_0aS4RcemBZXiAhYc",
         "reference" : "CmReAAAAMcEf8Q6cyJjkr_1Ei_mEetkfkYPSRXmAg6GjLBA36vJH6YwFGG-Yr9iqWeEEsfwb-TMcLLuW2UvasbaIJ7baR7Fb4BTBkr1plOppKdGd6EuT_l3yugEdTt5rC19ILMKgEhA-vS3AvmNuJLE8_XzcITAIGhT5TylKa5fPflXAk-9r49Te7fbq1g",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Raya Cikini No. 17 Cikini Menteng Jakarta Pusat DKI Jakarta"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.189216,
               "lng" : 106.837685
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "a9cb6c889f31ff3f5ecb96b2d9318eef34cb3f86",
         "name" : "Waroeng JJ",
         "opening_hours" : {
            "open_now" : true,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 165,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAA1t9gnutzOTZeB_tL8QGFKJYaIsRm6wZg15Osfd-Tgruq9b7XNIMO2ChKxep6B0N2vngDyLZLV2KSJkTATwCnKw3J21MxT8YkOKkTfVU21bgWTY4lmgKX4nQY77gsauxi5PwFjbyr_qJL7deQcuzV2xIQO580LzbjhmW8xTeo4UFSzRoU33r-qW6S63XlxRAS2dla1x87_zU",
               "width" : 287
            }
         ],
         "place_id" : "ChIJy0hxDzn0aS4RLU-YZLpoKjc",
         "reference" : "CmRdAAAAcUXsStUwygTS0b74upHOjhBMjTcbsx3UyOsr0wgSVEJR2xAddzlQDtttYZs2KFt7uOvWPkEKTv9q6RqLmPWZSowgtBdP7L8SOUbdRGxrnXWQDe9-hfXuUFqBLX3ilZ6FEhCaWiZlOB4fricRPQ3tEs0nGhSriA6JPE6-q8sfW1WlhghxCRYExA",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Cikini Raya No.71, Belakang Bank Mandiri Syariah, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.188925,
               "lng" : 106.833493
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "a7eb5fce7a60083cd09aeaca5b64004139185a27",
         "name" : "Tugu Kunstkring Paleis",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 439,
               "html_attributions" : [],
               "photo_reference" : "CpQBggAAAJpQdIN4el1ideStOLnqIjPkWQqkkn9sizXP3FGU-3Q3aYlTI-k1WIXv9ennQ3MIffveTQlGdhJQLGnqavF7Iyl41WPchQU_e9Pj754xqoEAT0OrtT8H_U0fTHFI3SX_pz3q1YYfcQJNf6oGAnGMqMrTb3KCEXQ2U5AfFVneP6lv6jL5FAD1BXW8nLP8wX2v8hIQM7w6XrAJCoUpTxEM02VTxhoUxthAHP68JwemTqNWmgxSJjvGezE",
               "width" : 439
            }
         ],
         "place_id" : "ChIJ7eBosjv0aS4RG9Aqt4j0sgM",
         "reference" : "CnRpAAAAI_obNSGurxPS5ODtbzdILScqlNu_S3xZv2fuNkeJujOWoCWVHQpDhd3-6DguqoBr1p8U_WR4BTeLPrBeA5m3W6fhJ5TafphbwyXn4t2Gtr1Ihj2VrtV119_1KrkOj7kWzIPaz4Sy6Vrq0W-0AO-zHRIQh0Q2AUjSg2-XWtxv_tVM7xoUP9RirCf26c5li1ZCpc8H8FEnV0o",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Teuku Umar No.1, Menteng, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.187882,
               "lng" : 106.83699
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "c4b1f8527920549c48adad1c2fa3670cadf0366c",
         "name" : "Dua Nyonya",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 848,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAAbs-b5wsIQw-69T-meuMdCpflIDIlNYBjT2FZzABs0PV6w2YzGgLjImk7SNxI3_ryha92xNxzaesr3TzT9PwJ_QSfUg_tDyZaNoW0vfekuousMnsFqDX1uiA3Sun0pErfvtxNb_uRY6lKGJ32Z6ObEhIQV9Y5Wg_LnZke01_fz3kDUhoURJ4JAUGq9KxDgkLfTKayjYp_QGE",
               "width" : 1280
            }
         ],
         "place_id" : "ChIJLSH8Jz_0aS4RyizVJgakXk4",
         "reference" : "CmRdAAAAyq_WXsN73EO-qA83844aqu-dNNC6aG4hKYNniEAvxQ9ooasIVwL0sdM8UXwzyCZlY4j70W5ZkvYf9gZg09EO3WVKFaO8da3SYZ0IP7GqpiD1hXaquAPE2kJMD0twU2_oEhDtUS-gW92IAewMkXU_Bq0PGhSLtMTD3ym1JqAoJrogfeK2gvWJ7Q",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jl. Cikini Raya No. 27, Cikini, Menteng, Jakarta Pusat"
      },
      {
         "geometry" : {
            "location" : {
               "lat" : -6.187062,
               "lng" : 106.832579
            }
         },
         "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
         "id" : "6370cd75e93c5a6f526be9fbe5c188839f7a39e3",
         "name" : "Restoran Rendezvous",
         "opening_hours" : {
            "open_now" : false,
            "weekday_text" : []
         },
         "photos" : [
            {
               "height" : 1536,
               "html_attributions" : [],
               "photo_reference" : "CnRnAAAAQKsh3G37THaseAuGqlZFNHDT2kP0XkeIfuz0AdmBuhvzcFYNMCOudtvxAclsqSorwu4CeczPxHEr6xV4mn5lPhagwbZCAEuwXSvneJChF83cdL6-t8T1GqX23T5V2G22j9UCgEw65djurDTDj93KHBIQnilaoN2Et6vjJJxfoOOaPxoUFcQFj8jMSHi8daF_pyJ8wOmHFrU",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJoaUdiDr0aS4RjSt1RIFHDVc",
         "reference" : "CnRmAAAA2zw0Gp7N1uVkLO3tYz-7PgMWildezDXZOzyREjErB5KTyQm6dBdnO5XOiE84eve5hH5LKf5XAshqWrpSBAM8L-gm6WiLh_ZB8OJqK2ZQ0fXJq4hui7sTFLyQ7YVI3tb0KI3fd5k2Hv9PDWSqBz-VDBIQ-nxgMsWZbhIELMxqAPs9sRoUMWSA9cC1V0JqIcCjweq_fJm4rCs",
         "scope" : "GOOGLE",
         "types" : [ "restaurant", "food", "establishment" ],
         "vicinity" : "Jalan Johar No. 2B-C, Jakarta Pusat"
      }
   ],
   "status" : "OK"
}