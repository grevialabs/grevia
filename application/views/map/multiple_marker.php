<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Polylines</title>
    <style>
	html, body {
        height: 80%;
        margin: 0;
        padding: 0;
      }
    #map_wrapper {
		
		height: 100%;
	}

	#map_canvas {
		width: 100%;
		height: 100%;
	}
    </style>
  </head>
  <body>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	
    <div id="map_wrapper">
		<div id="map_canvas" class="mapping"></div>
	</div>
	
    <script>
	  // https://developers.google.com/maps/documentation/javascript/examples/polyline-simple
	  
      // This example creates a 2-pixel-wide red polyline showing the path of William
      // Kingsford Smith's first trans-Pacific flight between Oakland, CA, and
      // Brisbane, Australia.
		<?php 
		$list_journey = $paramj = $string_map = $string_window = $first_lat = $first_lng = $end_lat = $end_lng = NULL;
		
		$paramj['creator_id'] = 1;
		$paramj['order'] = "creator_date DESC";
		$paramj['paging'] = TRUE;
		$list_journey = $this->journey_model->get_list($paramj);
		$list_journey = $list_journey['data'];
		
		if ( ! empty($list_journey)) 
		{
			// debug($list_journey);die;
			$string_map.= "[";
			$string_window.= "[";
			foreach($list_journey as $key => $lj)
			{
				if ($key == 0) {
					$first_lat = $lj['lat'];
					$first_lng = $lj['lng'];
				}
				
				$string_map.= "[";
				$string_map.= $key.", ".$lj['lat'].", ".$lj['lng'];
				$string_map.= "]";
				
				$string_window.= "[ 'jendela info: ".$key."']";
				
				if ($key != count($list_journey)-1) {
					$string_map.= ", ";
					$string_window.= ", ";
				} else {
					// 
					$end_lat = $lj['lat'];
					$end_lng = $lj['lng'];
				}
			}
			$string_map.= "]";
			$string_window.= "]";
		}
		else 
		{
			$first_lat = -6.2112255;
			$first_lng = 106.7822477;
			
			$end_lat = "";
			$end_lng = "";
		}
		?>

      jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyB1tbIAqN0XqcgTR1-FxYoVTVq6Is6lD98&sensor=false&callback=initialize";
    document.body.appendChild(script);
	});

	function initialize() {
		var map;
		var bounds = new google.maps.LatLngBounds();
		var mapOptions = {
			mapTypeId: 'roadmap'
		};
						
		// Display a map on the page
		map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		map.setTilt(45);
			
		// // Multiple Markers
		// var markers = [
			// ['London Eye, London', 51.503454,-0.119562],
			// ['Palace of Westminster, London', 51.499633,-0.124755]
		// ];
							
		// // Info Window Content
		// var infoWindowContent = [
			// ['<div class="info_content">' +
			// '<h3>London Eye</h3>' +
			// '<p>The London Eye is a giant Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres (394 ft).</p>' +        '</div>'],
			// ['<div class="info_content">' +
			// '<h3>Palace of Westminster</h3>' +
			// '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
			// '</div>']
		// ];
		
		// Multiple Markers
		var markers = <?php echo $string_map?>;
							
		// Info Window Content
		var infoWindowContent = <?php echo $string_window?>;
			
		// Display multiple markers on a map
		var infoWindow = new google.maps.InfoWindow(), marker, i;
		
		// Loop through our array of markers & place each one on the map  
		for( i = 0; i < markers.length; i++ ) {
			var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
			bounds.extend(position);
			marker = new google.maps.Marker({
				position: position,
				map: map,
				title: markers[i][0]
			});
			
			// Allow each marker to have an info window    
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infoWindow.setContent(infoWindowContent[i][0]);
					infoWindow.open(map, marker);
				}
			})(marker, i));

			// Automatically center the map fitting all markers on the screen
			map.fitBounds(bounds);
		}

		// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
		var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
			this.setZoom(14);
			google.maps.event.removeListener(boundsListener);
		});
		
	}
    </script>
  </body>
</html>