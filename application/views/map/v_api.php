<html>
    <head>

<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB1tbIAqN0XqcgTR1-FxYoVTVq6Is6lD98&sensor=false">
</script>
<script type="text/javascript">

var locations = [
  ['Lara Jonggrang', -6.194945, 106.836267, 'Restoran Lara Jonggrang<br/>Jl blabla'],
  ['Gado-Gado Bonbin', -6.192598, 106.837576, 'Gado Gado bonbin'],
  ];

  function initialize() {

    var myOptions = {
      center: new google.maps.LatLng(-6.1900492,106.8368033),
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP

    };
    var map = new google.maps.Map(document.getElementById("default"), myOptions);
    setMarkers(map,locations)
  }

function setMarkers(map,locations){

    var marker, i

	for (i = 0; i < locations.length; i++)
	{  

	var merchant = locations[i][0]
	var lat = locations[i][1]
	var long = locations[i][2]
	var add =  locations[i][3]

	latlngset = new google.maps.LatLng(lat, long);

	var marker = new google.maps.Marker({  
	  map: map, title: merchant , position: latlngset  
	});
	
	map.setCenter(marker.getPosition())
	var content = "<div style='float:left;width:45px;margin:7px 12px 0 0'><img src='http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png' width='45px'/></div><div style='float:right;'><h3 style='margin:5px 0'>" + merchant +  '</h3>' + "Alamat: " + add + "</div><div style='clear:both'></div>";     
	var infowindow = new google.maps.InfoWindow()

	google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
		return function() {
		   infowindow.setContent(content);
		   infowindow.open(map,marker);
		};
	})(marker,content,infowindow)); 

	}
}

  </script>
 </head>
 <body onload="initialize()">
  <div id="default" style="width:100%; height:100%"></div>
 </body>
  </html>