<?php 
// http://jsfiddle.net/kcjg5jxL/

?><html>
<head></head>
<body>
<style>
html, body, #map {
    height: 500px;
    width: 500px;
    margin: 0px;
    padding: 0px
}
</style>

<div id="map" style="border: 2px solid #3872ac;"></div>

<script>
var MapPoints = '[{"address":{"address":"plac Grzybowski, Warszawa, Polska","lat":"52.2360592","lng":"21.002903599999968"},"title":"Warszawa"},{"address":{"address":"Jana Paw\u0142a II, Warszawa, Polska","lat":"52.2179967","lng":"21.222655600000053"},"title":"Wroc\u0142aw"},{"address":{"address":"Wawelska, Warszawa, Polska","lat":"52.2166692","lng":"20.993677599999955"},"title":"O\u015bwi\u0119cim"}]';

//var MapPoints = '[{"JourneyModel":{"journey_id":"5","lat":null,"lng":null,"location_name":"home","creator_id":"1","creator_ip":null,"creator_date":"2016-07-14 07:51:13","editor_id":"1","editor_ip":"139.0.26.90","editor_date":"2016-07-14 08:09:16"},{"journey_id":"6","lat":null,"lng":null,"location_name":"lakupon","creator_id":"1","creator_ip":null,"creator_date":"2016-07-14 17:31:02","editor_id":"1","editor_ip":"112.215.63.19","editor_date":"2016-07-14 17:49:31"},{"journey_id":"7","lat":null,"lng":null,"location_name":"Sarinah","creator_id":"1","creator_ip":null,"creator_date":"2016-07-14 18:01:51","editor_id":"1","editor_ip":"112.215.63.12","editor_date":"2016-07-14 18:30:08"},{"journey_id":"8","lat":null,"lng":null,"location_name":"Kwetiaw aciap mangga besar","creator_id":"1","creator_ip":null,"creator_date":"2016-07-14 19:15:43","editor_id":"1","editor_ip":"112.215.63.17","editor_date":"2016-07-14 19:43:34"},{"journey_id":"9","lat":null,"lng":null,"location_name":"Hesti home","creator_id":"1","creator_ip":null,"creator_date":"2016-07-14 19:43:54","editor_id":"1","editor_ip":"112.215.63.13","editor_date":"2016-07-14 20:02:54"}}';

var MY_MAPTYPE_ID = 'custom_style';

function initialize() {

    if (jQuery('#map').length > 0) {

        var locations = jQuery.parseJSON(MapPoints);

        window.map = new google.maps.Map(document.getElementById('map'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        });

        var infowindow = new google.maps.InfoWindow();
        var flightPlanCoordinates = [];
        var bounds = new google.maps.LatLngBounds();

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].address.lat, locations[i].address.lng),
                map: map
            });
            flightPlanCoordinates.push(marker.getPosition());
            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i]['title']);
                    infowindow.open(map, marker);
                }
            })(marker, i));
            }

            map.fitBounds(bounds);

            var flightPath = new google.maps.Polyline({
                map: map,
                path: flightPlanCoordinates,
                strokeColor: "#FF0000",
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
</body>
</html>