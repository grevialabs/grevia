<?php 
$base_url = base_url();
$uri1 = $this->uri->segment(1);
$uri2 = $this->uri->segment(2);

$get_location_id = $get_movie_id = NULL;
if (isset($_GET['location_id'])) $get_location_id = $_GET['location_id'];
if (isset($_GET['movie_id'])) $get_movie_id = $_GET['movie_id'];

$obj_list_movie_schedule = $paramloc = NULL;
$paramloc['group_by'] = 'location_name';
$paramloc['status'] = 1;
$obj_list_movie_schedule = $this->movie_schedule_model->get_list_group($paramloc);
$obj_list_movie_schedule = $obj_list_movie_schedule['data'];
?>

<h2>Schedule</h2>
<?php 
if (isset($get_location_id))
{
	// Search movies by location_id
	// Default date must be for today
	$param = NULL;
	$param['location_id'] = $get_location_id;
	$param['group_by'] = 'location_name, studio_type_name, studio_code';
	$param['order'] = 'price_weekday ASC';
	$obj_list_movie_schedule = $this->movie_schedule_model->get_list_group($param);
	// debug($obj_list_movie_schedule);
	$obj_list_movie_schedule = $obj_list_movie_schedule['data'];
	if ( ! empty($obj_list_movie_schedule))
	{
		foreach ($obj_list_movie_schedule as $ms)
		{
			// each movies
			$list_start_time = $list_movie_schedule_id = NULL;
			$list_start_time = explode(',',$ms['list_start_time']);
			$list_movie_schedule_id = explode(',',$ms['list_movie_schedule_id']);
			// debug($list_start_time);
			// debug($ms);
			
			// Get Price active by weekday, jumat, or holiday
			$obj_price = $param_price = NULL;
			$param_price['input_date'] = date('Y-m-d');
			$obj_price = $this->studio_calendar_model->get_price($param_price);
			// debug($obj_price);
			
			// Default price weekday
			$price = $ms['price_weekday'];
			$price_category = 'weekday';
			if ( ! empty($obj_price)) 
			{
				if ($obj_price['is_price_jumat']) {
					$price = $obj_price['price_jumat'];
					$price_category = 'jumat';
				} else if ($obj_price['is_price_holiday']) {
					$price = $obj_price['price_holiday'];
					$price_category = 'holiday';
				}
			}
			
			// Set start time with nice format 
			$str_start_time = NULL;
			foreach($list_start_time as $key => $st)
			{
				$str_start_time.= left($st,5);
				if ($key != count($list_start_time)-1) $str_start_time.= ", ";
			}
			?>
			<h3><?php echo $ms['movie_title']?></h3>
			<h4><?php echo $ms['location_name']?></h4>
			<?php echo $ms['start_date']?><br/><br/>
			
			<div class="pull-left">
				<img src="<?php echo $base_url.'asset/images/booking/bioskop.jpg'?>" style="max-height:200px" /><br/>
			</div>
			<div class="pull-left" style="padding-left:15px">
				
				<span class="b">Price:</span> <?php echo format_money($price).' (Kategori '.$price_category.')'; ?><br/><br/>
				
				<span class="b">Studio Type:</span> <?php echo $ms['studio_type_name']?><br/><br/>
				
				<span class="b">Jam tayang:</span><br/>
				- <?php echo $str_start_time?>
				
				<hr/>
				BOOKING<br/>
				<form method="post" action="<?php echo base_url().'booking/';?>checkout">
				<select name="f_movie_schedule_id" class="input">
				<?php 
				foreach ($list_start_time as $key => $start) {
					?>
					<option value="<?php echo $list_movie_schedule_id[$key];?>"><?php echo left($start,5); ?></option>
					<?php
				}
				?>
				</select>
				<button type="submit" class="btn btn-sm btn-success">BOOK</button><br/><br/>
				</form>
			</div>
			<div class="clearfix"></div><hr/>
			<?php
		}
	}
	
	// Show list movie
	?>
	<div></div>
	<?php
}
else 
{
	if ( ! empty($obj_list_movie_schedule))
	{
		?>
		<ul>
		<?php
		foreach ($obj_list_movie_schedule as $rs)
		{
			?>
			<li><a href="<?php echo $base_url.$uri1.'/'.$uri2.'?location_id='.$rs['location_id']?>"><?php echo $rs['location_name']?></a></li>
			<?php
		}
		?>
		</ul>
		<?php
	}
}
?>