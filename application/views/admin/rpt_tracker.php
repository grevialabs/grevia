<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;

$PAGE = 'Admin Tracker';
$PAGE_HEADER = $PAGE.'<hr>';
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $param,$message,$get_tracker_id;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(2)) $module = $this->uri->segment(2);
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $get_tracker_id = $this->uri->segment(4);

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;

if (isset($_GET['order']))
{
	// $param['order'] = $_GET['order']; // creator_date DESC, total_view DESC, name, url, source, segment
	
	if ($_GET['order'] == 'creator_date_asc') $param['order'] = 'creator_date ASC';
	else if($_GET['order'] == 'creator_date_desc') $param['order'] = 'creator_date DESC';
	else if($_GET['order'] == 'total_view_asc') $param['order'] = 'total_view ASC';
	else if($_GET['order'] == 'total_view_desc') $param['order'] = 'total_view DESC';
}
$data = $this->tracker_model->get_list($param);
// debug($data);
// die;

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-12">Laporan klik halaman landing page HOME</div>
	<div class="col-xs-6">
		&nbsp;
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			
			<select name="order" class="input">
				<option value="creator_date_asc" <?php if (isset($_GET['order']) && $_GET['order'] == 'creator_date_asc') echo 'selected'; ?>>CreateDate Asc</option>
				<option value="creator_date_desc" <?php if (isset($_GET['order']) && $_GET['order'] == 'creator_date_desc') echo 'selected'; ?> >CreateDate Desc</option>
				<option value="total_view_asc" <?php if (isset($_GET['order']) && $_GET['order'] == 'total_view_asc') echo 'selected'; ?>>TotalView Asc</option>
				<option value="total_view_desc" <?php if (isset($_GET['order']) && $_GET['order'] == 'total_view_desc') echo 'selected'; ?>>TotalView Desc</option>
			</select>
			
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_tracker_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Name</td>
				<td>Segment</td>
				<td>Source</td>
				<td>Url</td>
				<td>Date</td>
				<td>TotalViewed</td>
				<td class="talRgt">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['tracker_id'];
				$i += 1;
				?>
				<tr>
				<td class="parentcheckbox"><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['name']; ?></td>
				<td><?php echo $rs['segment']; ?></td>
				<td><?php echo $rs['source']; ?></td>
				<td><?php echo $rs['url']; ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td><?php echo $rs['total_view']; ?></td>
				<td class="talRgt"><a href="<?php echo $this->uri->segment(2).'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				</td>
				</tr>
				<?php 
			}
			?>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		// $obj = array();
		// if ($do != 'insert')
		// {
			// $tmp['SubscribeID'] = $gettracker_id;
			// $obj = $this->search_model->get($tmp);
		// }
	}
	?>
</div>
<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>