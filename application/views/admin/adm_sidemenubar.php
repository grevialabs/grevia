	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'bg-primary';
		}
		return $str;
	}
	?>
	<div class="bg-info talRgt lnkRed navbar-collapse collapse">
	  
	<!--
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"  aria-expanded="false">
		Dropdown <span class="caret"></span>
		</button>
		<ul class="nav navbar-nav">
		<li><a class=" <?php echo active('dashboard').active('index')?>" href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home fa-fw"></i>&nbsp; Admin Dashboard</a></li>
		<li><a class=" <?php echo active('article')?>" href="<?php echo base_url()?>admin/article"><i class="fa fa-book fa-fw"></i>&nbsp; Article</a></li>
		<li><a class=" <?php echo active('articlecategory')?>" href="<?php echo base_url()?>admin/articlecategory"><i class="fa fa-pencil fa-fw"></i>&nbsp; Article Category</a></li>
		<li><a class=" <?php echo active('member')?>" href="<?php echo base_url()?>admin/member"><i class="fa fa-cog fa-fw"></i>&nbsp; Member</a></li>
		<li><a class=" <?php echo active('forum')?>" href="<?php echo base_url()?>admin/forum"><i class="fa fa-lock fa-fw"></i>&nbsp; Forum</a></li>
		<li><a class=" <?php echo active('subscribe')?>" href="<?php echo base_url()?>admin/subscribe"><i class="fa fa-lock fa-fw"></i>&nbsp; Subscribe</a></li>
		<li><a class=" <?php echo active('subscribe')?>" href="<?php echo base_url()?>admin/report_search"><i class="fa fa-lock fa-fw"></i>&nbsp; Report Search</a></li>
		<li class="dropdown">
			<ul class="dropdown-menu" role="menu" >
				<li role="presentation"><a role="menuitem" tabindex="1" href="#">Action</a></li>
				<li role="presentation"><a role="menuitem" tabindex="1" href="#">Another action</a></li>
				<li role="presentation"><a role="menuitem" tabindex="1" href="#">Something else here</a></li>
				<li role="presentation"><a role="menuitem" tabindex="1" href="#">Separated link</a></li>
			</ul>
		</li>
		</ul>
	</div>
	-->
	

     
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav lnkBlk">
			<li><a class=" <?php echo active('dashboard').active('index')?>" href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home fa-fw"></i>&nbsp; Admin Dashboard</a></li>
			<li><a class=" <?php echo active('article')?>" href="<?php echo base_url()?>admin/article"><i class="fa fa-book fa-fw"></i>&nbsp; Article</a></li>
			<li><a class=" <?php echo active('articlecategory')?>" href="<?php echo base_url()?>admin/articlecategory"><i class="fa fa-pencil fa-fw"></i>&nbsp; Article Category</a></li>
			<li><a class=" <?php echo active('member')?>" href="<?php echo base_url()?>admin/member"><i class="fa fa-users fa-fw"></i>&nbsp; Member</a></li>
			<li><a class=" <?php echo active('subscribe')?>" href="<?php echo base_url()?>admin/subscribe"><i class="fa fa-user fa-fw"></i>&nbsp; Subscribe</a></li>
			<li class="dropdown ">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <i class="fa fa-bar-chart fa-fw"></i>&nbsp; Report<span class="caret"></span></a>
			  <ul class="dropdown-menu " role="menu">
				<li><a class=" <?php echo active('report_search')?>" href="<?php echo base_url()?>admin/report_search">Report Search</a></li>
				<li><a class=" <?php echo active('report_search')?>" href="<?php echo base_url()?>admin/report_tracker">Report Tracker</a></li>
			  </ul>
			</li>
			<li>
				
			</li>
			
		  </ul>
		</div><!--/.nav-collapse -->

	
	</div><br/>
	
	
