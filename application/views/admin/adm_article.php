<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;

$PAGE = 'Admin Artikel';
$PAGE_HEADER = $PAGE.'<hr>';
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $param,$message,$get_article_category_id;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $get_article_category_id = $this->uri->segment(4);

/*
  | SAVE 
*/
if (post('f_insert')) {
	
	if (post('f_insert')) {
		
		$article_category_id = post("f_article_category_id");
		if (isset($article_category_id)) $article_category_id = implode(',', post("f_article_category_id"));
		$title = filter( post("f_title") );
		$slug = filter( post("f_slug") );
		$content = post("f_content");
		$short_description = filter( post("f_short_description") );
		$quote = post("f_quote");
		$tag = filter( post("f_tag") );
		//$image = filter( post("f_image") );
		$image_description = filter( post("f_image_description") );
		$is_publish = 0;
		if (post('f_is_publish') == 1 ) $is_publish = 1 ;
		$short_url = filter( post("f_short_url") );
		
		if (is_filled($title)) {
			if (post('f_is_publish') == 1 ) $is_publish = 1 ;else $is_publish = 0;
			
			// IMAGE
			if (isset($_FILES["f_image"]["name"]) && is_filled($_FILES["f_image"]["name"])) 
			{
				$list_allowed_ext = array("gif", "jpeg", "jpg", "png");
				$temp = explode(".", $_FILES["f_image"]["name"]);
				$file_image_extension = end($temp);
				$file_image_type = $_FILES["f_image"]["type"];
				$file_image_size = $_FILES["f_image"]["size"];
				$file_image_name = $_FILES["f_image"]["name"];
				
				$str = "";
				$is_image_valid = FALSE;
				
				if (($file_image_type == "image/gif") || ($file_image_type == "image/jpeg") || ($file_image_type == "image/jpg")	|| ($file_image_type == "image/pjpeg") || ($file_image_type == "image/x-png")	|| ($file_image_type == "image/png"))
				{
					$is_image_valid = TRUE;
				}
				else
				{
					$str.= "- Tipe bukan gambar.<br/>";
				}
				
				if ($file_image_size < 1000000)
				{
					$is_image_valid = TRUE;
				}
				else
				{
					$str.= "- Size tidak cukup.<br/>";
				}
				
				if (in_array($extension, $list_allowed_ext))
				{
					$is_image_valid = TRUE;
				}
				else
				{
					$str.= "- Extension salah.<br/>";
				}
				
				if ($is_image_valid) 
				{
					$image = $file_image_name;
					if ($_FILES["f_image"]["error"] > 0) 
					{
						$message['message'].= "Image failed upload because error.";
					} 
					else 
					{
						if (is_internal())$upload_directory = "F:/xampp/htdocs/staging/asset/images/article/";
						else $upload_directory = "/home/grevia.com/public_html/asset/images/article/";
						
						if (file_exists($upload_directory . $file_image_name)) 
						{
							echo $file_image_name . " already exists. ";
						} 
						else 
						{
							$is_move_success = move_uploaded_file($_FILES["f_image"]["tmp_name"],$upload_directory . $file_image_name);
							if ($is_move_success) 
							{
								$image = $file_image_name;
							} 
							else 
							{
								$message['message'].= "Image failed upload.";
							}
						}
					}
				}
			}
			else
			{
				$image = "";
			}
			$param = array(
				'article_category_id' => $article_category_id,
				'title' => $title,
				'slug' => $slug,
				'content' => $content,
				'short_description' => $short_description,
				'quote' => $quote,
				'tag' => $tag,
				'view' => 0,
				'image' => $image,
				'image_description' => $image_description,
				'is_publish' => $is_publish,
				'short_url' => $short_url,
			);
			
			$save = $this->article_model->save($param);
			($save)?$message['message'] = MESSAGE::SAVE:$message['message'] = MESSAGE::ERROR;
			
			// UPDATE TO SHORT URL API
			if ($save) 
			{
				// // This is the URL you want to shorten
				// //  http://goo.gl/CNuwCO
				// $longUrl = 'http://www.grevia.com/article/41/3-langkah-awal-untuk-mengeksekusi-ide-anda-membangun-startup';

				// // Get API key from : http://code.google.com/apis/console/
				// $apiKey = 'AIzaSyBcvHZffHXEK4dCxXC_HGCk4s6huhP6E4Q';

				// $postData = array('longUrl' => $longUrl);
				// $jsonData = json_encode($postData);

				// $curlObj = curl_init();

				// curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.$apiKey);
				// curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				// curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				// curl_setopt($curlObj, CURLOPT_HEADER, 0);
				// curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
				// curl_setopt($curlObj, CURLOPT_POST, 1);
				// curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

				// $response = curl_exec($curlObj);

				// // Change the response json string to object
				// $json = json_decode($response);

				// curl_close($curlObj);

				// $shortLink = get_object_vars($json);
				// echo "Shortened URL is: ".$shortLink['id'];
				// var_dump($json);
			}
			
			$message['message'] = getMessage($message['message']);
			
			//sleep(1);
			$last = $this->article_model->get(array('last' => TRUE));
			$new_article = base_url().'admin/'.$this->uri->segment(2).'/'.$last['article_id'];
			echo $new_article;
			$this->common_model->js_sleep_redirect($new_article);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | UPDATE 
*/
if (post('f_update')) {
	$category_name = filter( post("f_category_name") );
	$description = filter( post("f_description") );
	$is_publish = 0;
	if (post('f_is_publish') == 1 ) $is_publish = 1 ;
	if (post('f_update') && is_numeric($get_article_category_id)) {
		$param['article_category_id'] = $get_article_category_id;
		$obj_article_category = $this->article_model->get($param);
		if (!empty($obj_article_category)) {
			$param = array(
				'category_name' => $category_name,
				'description' => $description
			);

			$update = $this->article_model->update($get_article_category_id, $param);
			
			($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($get_article_category_id)) {
		$delete = $this->article_model->delete($get_article_category_id);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	
	$group_action = $_POST['lst_group_action'];
	if ($group_action == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			foreach (post('chkbox') as $key => $val) {
				$delete = $this->article_model->delete($val);
			}
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
	
	$allowed_group_action = array('publish','unpublish','hot','unhot');
	if (in_array($group_action, $allowed_group_action)) {
	
		//if ($group_action == "publish" || $group_action == "unpublish" || $group_action == "hot" || $group_action == "unhot") {
		if (!empty($_POST['chkbox'])) { 
			$update = false;
			
			switch($group_action) {
				case "publish":
					$tmp['is_publish'] = '1';
					break;
				case "unpublish":
					$tmp['is_publish'] = '0';
					break;
				case "hot":
					$tmp['is_hot'] = '1';
					break;
				case "unhot":
					$tmp['is_hot'] = '0';
					break;
			}
			
			foreach (post('chkbox') as $key => $val) {
				$update = $this->article_model->update($val,$tmp);
			}
			
			if ($update) {
				($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
}

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['is_publish'] = 'all';
$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->article_model->get_list($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url().'admin/'.$MODULE.'/insert';?>" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; New <?php echo $MODULE?></a></div><br/>
		<?php } ?>
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_article_category_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Article</td>
				<td>Short Description</td>
				<td>Hot</td>
				<td>IsPublish</td>
				<td>View</td>
				<td>Date</td>
				<td class="talRgt">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['article_id'];
				$i += 1;
				
				$is_hot = '';
				if ($rs['is_hot']) $is_hot = '<i class="fa fa-rocket clrRed" alt="Hot Article" title="Hot Article"></i>'; 
				
				$is_publish = '<div class="btn-sm"><i class="fa fa-minus-circle clrRed" alt="Unpublish" title="Unpublish"></i></div>';
				if ($rs['is_publish']) $is_publish = '<div class="btn-sm"><i class="fa fa-check-circle clrBlu" alt="Publish" title="Publish"></i> </div>';
				?>
				<tr>
				<td class="parentcheckbox"><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>"/></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['category_name'].br().$rs['title']; ?></td>
				<td><?php echo $rs['short_description']; ?></td>
				<td><?php echo $is_hot; ?></td>
				<td><?php echo $is_publish; ?></td>
				<td><?php echo $rs['view']; ?></td>
				<td><?php echo date(DATE_FORMAT,strtotime($rs['creator_date'])); ?></td>
				<td class="talRgt"><a href="<?php echo $this->uri->segment(2).'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-lg" onclick="return doConfirm()"></i></a> 
				<a href="<?php echo $this->uri->segment(2).'/delete/'.$id; ?>" onclick=""><i class="clrRed fa fa-times fa-lg" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action">
					<option class="" value="hot">Hot</option>
					<option class="" value="unhot">Unhot</option>
					<option class="" value="publish">Publish</option>
					<option class="" value="unpublish">Unpublish</option>
					<option class="" value="delete">Delete</option>
					</select>
					<button class="btn btn-default btn-sm" name="btn_group_action" value="1">Action</button></div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		$obj = array();
		if ($do != 'insert')
		{
			$tmp['article_category_id'] = $get_id;
			$obj = $this->article_model->get($tmp);
		}
	?>
	<?php if ($do == "edit") echo "<div class='fntLg'>Edit ".$MODULE."</div><br>"; ?>
	<form class='form-horizontal' role='form' method='post'>
		
		<div class='form-group form-group-sm form-group form-group-sm-sm'>
			<label for='f_article_category_id' class='col-sm-2'>Category</label>
			<div class='col-sm-10'>
			<?php 
			$data_category = $this->articlecategory_model->get_list();
			$obj_list_article_category = $data_category['data'];
			
			if (!empty($obj)) $arrArticleCategory = explode(',', $obj['article_category_id']);
			?>
			<select class='form-control multiple-select' name='f_article_category_id[]' multiple="multiple" size="<?php echo count($obj_list_article_category)?>" style="height:100%"><?php
			
			foreach ($obj_list_article_category as $category) {
				$selected = '';
				if (in_array($category['article_category_id'], $arrArticleCategory, true)) $selected = ' selected';;
				echo '<option value="'.$category['article_category_id'].'" '.$selected.'>'.$category['category_name'].'</option>';
			}
			?></select>
			

			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_title' class='col-sm-2'>Title*</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_title' id='f_title' placeholder='Enter Title' value='<?php if (!empty($obj)) echo $obj['title']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_slug' class='col-sm-2'>Slug</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_slug' id='f_slug' placeholder='Enter Slug' value='<?php if (!empty($obj)) echo $obj['slug']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'>Content<br/><textarea style='height:500px' class='form-control mceEditor' name='f_content' id='f_content' cols='40' rows='45'><?php if (!empty($obj)) echo $obj['content']?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_short_description' class='col-sm-2'>ShortDescription</label>
			<div class='col-sm-10'><textarea class='form-control' name='f_short_description' id='f_short_description' placeholder='Enter Short Description' style='height:80px'><?php if (!empty($obj)) echo $obj['short_description']?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_quote' class='col-sm-2'>Quote</label>
			<div class='col-sm-10'><textarea class='form-control' name='f_quote' id='f_quote' placeholder='Enter Quote' style='height:80px' ><?php if (!empty($obj)) echo $obj['Quote']?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_tag' class='col-sm-2'>Tag</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_tag' id='f_tag' placeholder='Enter Tag' value='<?php if (!empty($obj)) echo $obj['tag']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_image' class='col-sm-2'>Image</label>
			<div class='col-sm-10'>
				<div class='input-group'>
				<span class="input-group-addon">
					<i class="glyphicon glyphicon-picture"></i>
				</span>
				<input type='file' class='form-control' id='f_image' name='f_image' onchange="$('#previewImage')[0].src = window.URL.createObjectURL(this.files[0]);$('#previewImage').show();" /></div>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><?php if (!empty($obj)) echo '<img src="'.base_url().'asset/images/article/'.$obj['image'].'" class="" style="max-width:400px"/>'.BR; ?><img class='previewImage' id='previewImage' /></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_image_description' class='col-sm-2'>Image Description</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_image_description' id='f_image_description' placeholder='Enter Image Description' value='<?php if (!empty($obj)) echo $obj['image_description']?>'/></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_is_publish' class='col-sm-2'>Is Publish</label>
			<div class='col-sm-10'><input type='checkbox' name='f_is_publish' id='f_is_publish' value='1' <?php if (!empty($obj) && $obj['is_publish'] == '1') echo 'checked';?> ></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_short_url' class='col-sm-2'>Short URL</label>
			<div class='col-sm-10'><input class='form-control' name='f_short_url' id='f_short_url' placeholder='Enter ShortUrl' value='<?php if (!empty($obj)) echo $obj['short_url']?>'></div>
		</div>
		
		<div class='form-group form-group-sm col-sm-12'>
		<?php if($do == 'insert') { ?>
		<button class='btn btn-success' name='f_insert' value='1'><?php echo SAVE?></button>
		<?php } else if ($do == 'edit'){ ?>
		<button class='btn btn-success' name='f_update' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
		<div class="clearfix"></div>
	</form>
	
	<?php
	}
	?>
</div>

<!-- TINYMCE -->
<script src="<?php echo base_url()?>asset/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
        mode : "specific_textareas",
        editor_selector : "mceEditor",
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste textcolor youtube"
        ],

        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify | styleselect fontselect fontsizeselect | link unlink image | forecolor backcolor | youtube | bullist numlist | outdent indent | table | hr | subscript superscript | charmap emoticons | ",
        toolbar2: " ",

        menubar: false,
        toolbar_items_size: 'medium'
});</script>
<!-- TINYMCE -->
<script>
$("#f_title").keyup(function () {
    var textValue = $(this).val();
    textValue = textValue.toLowerCase();
	textValue = textValue.replace(/[^a-zA-Z0-9]+/g,"-");
    $('#f_slug').val(textValue);
});
$(document).ready(function(){
	//$(".multiple-select").height("auto");
});
</script>

<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });
	

});
</script>