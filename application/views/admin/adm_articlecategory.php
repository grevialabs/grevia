<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;

$PAGE = 'Admin Artikel Kategori';
$PAGE_HEADER = $PAGE.'<hr>';
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $param,$message,$get_article_category_id;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $get_article_category_id = $this->uri->segment(4);

/*
  | SAVE 
*/
if (post('f_insert')) {
	
	$category_name = filter( post("f_category_name") );
	$description = filter( post("f_description") );
	$isPublish = 0;
	if (post('f_IsPublish') == 1 ) $isPublish = 1 ;
	
	if (is_filled($category_name)) {
		if (post('f_IsPublish') == 1 ) $isPublish = 1 ;else $isPublish = 0;
		
		$param = array(
			'category_name' => $category_name,
			'description' => $description,
			//'IsPublish' => $isPublish
		);
		
		$save = $this->articlecategory_model->save($param);
		
		($save)?$message['message'] = MESSAGE::SAVE:$message['message'] = MESSAGE::ERROR;
		$message['message'] = getMessage($message['message']);
	} else {
		$message['message'] = getMessage(MESSAGE::NOT_FOUND);
	}
}

/*
  | UPDATE 
*/
if (post('update')) {
	$category_name = filter( post("category_name") );
	$description = filter( post("description") );
	$isPublish = 0;
	if (post('IsPublish') == 1 ) $isPublish = 1 ;
	if (post('update') && is_numeric($get_article_category_id)) {
		$param['article_category_id'] = $get_article_category_id;
		$obj_article_category = $this->articlecategory_model->get($param);
		if (!empty($obj_article_category)) {
			$param = array(
				'category_name' => $category_name,
				'description' => $description
			);

			$update = $this->articlecategory_model->update($get_article_category_id, $param);
			
			($update)?$message['message'] = MESSAGE::UPDATE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | DELETE 
*/
if ($do == "delete") {
	
	if (is_numeric($get_article_category_id)) {
		$delete = $this->articlecategory_model->delete($get_article_category_id);
		if ($delete) {
			($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
			$message['message'] = getMessage($message['message']);
		} else {
			$message['message'] = getMessage(MESSAGE::NOT_FOUND);
		}
	}
}

/*
  | GROUP ACTION 
  | CHECKED BOX
*/
if (isset($_POST['btn_group_action'])) {
	if ($_POST['lst_group_action'] == "delete") {
		if (!empty($_POST['chkbox'])) { 
			$delete = false;
			foreach (post('chkbox') as $key => $val) {
				$delete = $this->articlecategory_model->delete($val);
			}
			
			if ($delete) {
				($delete)?$message['message'] = MESSAGE::DELETE:$message['message'] = MESSAGE::ERROR;
				$message['message'] = getMessage($message['message']);
			} else {
				$message['message'] = getMessage(MESSAGE::NOT_FOUND);
			}
			
		}
	}
	//pre($_POST,TRUE);
}

// SEARCH
if (get('keyword')) {
	$param['category_name'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->articlecategory_model->get_list($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<?php 

?>
<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="talLft"><a href="<?php echo base_url()?>admin/articlecategory/insert" class="btn btn-success br"><i class="fa fa-plus"></i>&nbsp; Insert New Category</a></div><br/>
		<?php } ?>
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<?php
	if ((!is_filled($do) && !is_filled($get_article_category_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Article Category Name</td>
				<td class="talRgt">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['article_category_id'];
				$i += 1;
				?>
				<tr>
				<td class="parentcheckbox"><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['category_name']; ?></td>
				<td class="talRgt"><a href="<?php echo $this->uri->segment(2).'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> 
				<a href="<?php echo $this->uri->segment(2).'/delete/'.$id; ?>" onclick="return confirm('Yakin menghapus data ini ?')"><i class="clrRed fa fa-times fa-2x" title="Delete data" alt="Delete data"></i></a></td>
				</tr>
				<?php 
			}
			?>
			<tr>
				<td colspan="100%">
					<div id="group_action">With checked do <select class="input" name="lst_group_action"><option class="" value="delete">Delete</option></select>
					<button class="btn btn-default btn-sm" name="btn_group_action" id="btn_group_action"value="1">Action</button>
					</div>
				</td>
			</tr>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		$obj = array();
		if ($do != 'insert')
		{
			$tmp['article_category_id'] = $get_id;
			$obj = $this->articlecategory_model->get($tmp);
		}
	?>
	<?php if ($do == "edit") echo "<div class='fntLg'>Edit ".$MODULE."</div><br>"; ?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_category_name' class='col-sm-2'>CategoryName</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_category_name' id='f_category_name' placeholder='Enter Category Name' value='<?php if (!empty($obj)) echo $obj['category_name']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Slug' class='col-sm-2'>Description</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_description' id='f_description' placeholder='Enter Description' value='<?php if (!empty($obj)) echo $obj['description']?>'></div>
		</div>
		
		<div class='form-group form-group-sm col-sm-12'>
		<?php if ($do == 'insert') { ?>
		<button class='btn btn-success' name='f_insert' value='1'><?php echo SAVE?></button>
		<?php } else if ($do == 'edit'){ ?>
		<button class='btn btn-success' name='f_update' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
		<div class="clearfix"></div>
	</form>
	<?php
	}
	?>
</div>
<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>