<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER;

$PAGE = 'Admin Search';
$PAGE_HEADER = $PAGE.'<hr>';
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;

global $param,$message,$get_search_id;

$do = $get_id = '';
$offset = OFFSET;
//$offset = 1;
$page = 1;
if ($this->input->get('page') && $this->input->get('page') > 1) $page = $this->input->get('page');
if ($this->uri->segment(2)) $module = $this->uri->segment(2);
if ($this->uri->segment(3)) $do = $this->uri->segment(3);
if ($this->uri->segment(4)) $get_id = $get_search_id = $this->uri->segment(4);

// SEARCH
if (get('keyword')) {
	$param['keyword'] = get('keyword');
}

$param['paging'] = TRUE;
$param['offset'] = $offset;
$data = $this->search_model->get_list($param);

$total_rows = $data['total_rows'];
$list_data = $data['data'];
?>

<div class="col-xs-12">
<?php echo $SIDEMENUBAR ?>
</div>

<div class="col-xs-12">
	<div class="col-xs-12">Laporan keyword yang paling banyak dicari di halaman SEARCH</div>
	<div class="col-xs-6">
		&nbsp;
	</div>
	<div class="col-xs-6">
		<?php if (!is_filled($do)) { ?>
		<div class="br talRgt">
			<form method="get">
			<input class="input input-sm" type="text" name="keyword" value="<?php if (get('keyword')) echo get('keyword')?>" placeholder="Keyword" />
			<button class="btn btn-info btn-sm" > <i class="fa fa-search-minus"></i> Filter</button>
			</form>
		</div>
		<?php } ?>
	</div>
	<div class="clearfix"></div>
	
	<?php if (isset($message['message'])) echo message($message['message'])?>
		
	<table class="table table-hover table-striped table-responsive">
		<tr class="b">
			<td width=1>#</td>
			<td>Keyword</td>
			<td>Hit</td>
			<td>Last Hit</td>
		</tr>
	<?php
	// ---------------------------------------------------------------------------------
	$param = NULL;
	$param['offset'] = 40;
	$param['limit'] = 0;
	$param['paging'] = TRUE;
	$data_topsearch = $this->search_model->get_list_report($param);
	$data_topsearch = $data_topsearch['data'];
	$i = 1;
	foreach($data_topsearch as $key => $obj)
	{
		?>
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $obj['keyword'] ?></td>
			<td><?php echo $obj['hit'] ?></td>
			<td><?php echo $obj['creator_date'] ?></td>
		</tr>
		<?php 
		$i++;
	}
	
	?>
	</table>
	<?php
	// ---------------------------------------------------------------------------------
	if ((!is_filled($do) && !is_filled($get_search_id)) || $do == "delete")
	{
		if (!empty($data))
		{
			$str = "";
			$total_rows = $data['total_rows'];
			$list_data = $data['data'];
			?>
			
			<form method="post">
			<table class="table table-hover table-striped table-responsive">
			<tr class="b">
				<td width=1><input type="checkbox" class="chkbox togglebox" onclick="togglebox()" /></td>
				<td width=1>#</td>
				<td>Keyword</td>
				<td>Date</td>
				<td>IP</td>
				<td class="talRgt">Option</td>
			</tr>
			<?php
			$i = 0;
			if (is_numeric($page) && $page > 0) 
			{
				$i = ($page - 1) * $offset;
			}
			foreach($list_data as $key => $rs)
			{
				$rs = (array) $rs;
				$id = $rs['search_id'];
				$i += 1;
				?>
				<tr>
				<td><input type="checkbox" name="chkbox[]" id="chkbox[]" class="chkbox" value="<?php echo $id?>" /></td>
				<td><?php echo $i ; ?></td>
				<td><?php echo $rs['keyword']; ?></td>
				<td><?php echo $rs['creator_date']; ?></td>
				<td><?php echo $rs['creator_ip']; ?></td>
				<td class="talRgt">
				<!--<a href="<?php echo $this->uri->segment(2).'/edit/'.$id; ?>" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-2x"></i></a> -->
				</td>
				</tr>
				<?php 
			}
			?>
			</table>
			</form>
			<br/>
			
		<?php
			if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
		}
		else 
		{
			echo 'No data Exist';
		}
	}
	
	//if (isset($do) || isset($get_id))
	else
	{
		$obj = array();
		if ($do != 'insert')
		{
			$tmp['SubscribeID'] = $get_search_id;
			$obj = $this->search_model->get($tmp);
		}
	?>
	<?php if ($do == "edit") echo "<div class='fntLg'>".EDIT.' '.$MODULE."</div><br>"; ?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_Name' class='col-sm-2'><?php echo NAME?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Name' id='f_Name' placeholder='Enter Name' value='<?php if (!empty($obj)) echo $obj['Name']?>' required></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_Email' class='col-sm-2'><?php echo EMAIL ?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_Email' id='f_Email' placeholder='Enter Email' value='<?php if (!empty($obj)) echo $obj['Email']?>' required></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_IsSubscribe' class='col-sm-2'>Subscribe</label>
			<div class='col-sm-10' align="left"><input type='checkbox' class='form-control' name='f_IsSubscribe' id='f_IsSubscribe' placeholder='Enter Subscribe' value='1' <?php if (!empty($obj) && $obj['IsSubscribe'] == 1) echo 'checked'?>></div>
		</div>
		
		<div class='form-group form-group-sm col-sm-12'>
		<?php if ($do == 'insert') { ?>
		<button class='btn btn-success' name='f_insert' value='1'><?php echo SAVE?></button>
		<?php } else if ($do == 'edit'){ ?>
		<button class='btn btn-success' name='f_update' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
		<div class="clearfix"></div>
	</form>
	<?php
	}
	?>
</div>
<script>
$(document).ready( function() {
	
	function togglebox(){
		if ($('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',true);
		}
		if (!$('.togglebox').prop('checked')) {
			$('.chkbox').prop('checked',false);
		}
	}
	
	$('#group_action').hide();
	$('.chkbox').click(function(){
        var count = $("[type='checkbox']:checked").length;
		if (count >= 1) {
             $('#group_action').show();
        } else if(count <= 0){
            $('#group_action').hide();
        }
    });

});
</script>