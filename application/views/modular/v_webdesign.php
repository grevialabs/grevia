<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = 'Jasa pembuatan web dan situs Company Profile - Grevia Webdesign';
$PAGE_HEADER = NULL;
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, 'Webdesign');
?>
<!--
<div class="col-xs-6">
</div>
cecece
-->
<style>
.mrgBtm0{ margin-bottom:0 }
.hero-price{ font-size:30px }

.panel-hover:hover {
	background: lightblue !important;
	cursor: pointer;
}

</style>
<div class="container" style="margin-bottom:0"> 
	<div class="row">
		<div class="col-sm-12 bgWht">

			<img class="wdtFul" src="<?php echo base_url().'asset/images/banner/banner_webdesign.jpg'?>" /><br/><br/>
			<h1 class="text-uppercase clrBlu b" style="padding:0;margin:0">Jasa Webdesign</h1><hr/>

			Dunia digital, sekarang ini sudah mulai banyak diperhatikan oleh para pemilik bisnis. Hampir semua orang mencari kebutuhan mereka via internet atau mesin pencari Google. Produk / jasa Anda bisa dengan mudah ditemukan jika anda memiliki situs yang baik<br/> <br/>

			Grevia (Group Evangelist Internet Indonesia), memiliki <span class="b"><?php echo (date('Y')-2011); ?> tahun pengalaman</span> dalam membuat website yang terjamin dengan akses ke konten yang cepat, SEO friendly (mudah dicari di mesin pencari google), menarik dan mudah digunakan. <br/><br/>

			Sebagai sebuah startup, kami sangat mengutamakan kualitas yang baik untuk membantu operasional bisnis ataupun branding anda melalui aplikasi berbasis web ataupun situs company profile.<br/><br/>

			Mengapa memilih jasa webdesign Grevia ? <br/>
			<?php 
			$icon_yes = '<i class="fa fa-check btn btn-xs btn-success"></i>';
			$icon_no = '<i class="fa fa-remove clrRed"></i>';
			?>
			<table class="table table-bordered table-hover">
			<tr class="alert-info">
				<td class="">Poin</td>
				<td class="talCnt b text-info">Grevia</td>
				<td class="talCnt">Webdesign lain</td>
			</tr>
			<tr>
				<td class="">Responsive Mobile</td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_no; ?></td>
			</tr>
			<tr>
				<td class="">99% uptime server</td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
			</tr>
			<tr>
				<td class="">Security & 24 hour support</td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_no; ?></td>
			</tr>
			<tr>
				<td class="">Custom layout</td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_no; ?></td>
			</tr>
			<tr>
				<td class="">Modern Design</td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_no; ?></td>
			</tr>
			<tr>
				<td class="">SEO Optimize <i class="fa fa-info"></i></td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_no; ?></td>
			</tr>
			<tr>
				<td class="">Google Analytics</td>
				<td class="talCnt"><?php echo $icon_yes; ?></td>
				<td class="talCnt"><?php echo $icon_no; ?></td>
			</tr>
			</table>
			<br/><br/>

			<div class="col-sm-12">
				<h2 class="marNon br text-uppercase">Paket Company Profile</h2><br/>
			</div>

			<section class="">
				<?php $price = 3500000; ?>
				<div class="col-sm-4">
					<div class="panel panel-success panel-hover">
						<div class="panel-heading talCnt">
						Paket Simple
						</div>
						<div class="panel-body padNon">
							<table class="table table-bordered table-hover talCnt mrgBtm0">
								<tr><td><span class="hero-price"><?php echo format_money($price)?></span><br/>per tahun <br/>atau<br/> <b><?php echo format_money($price/12) ?> per bulan</b></td></tr>
								<tr><td>Gratis domain .COM / NET / ORG / INFO atau BIZ </td></tr>
								<tr><td>Akun Hosting Linux 750 MB  & Bandwidth Unlimited</td></tr>
								<tr><td>5 Email (Pop3, Forwarder, Autoresponder), Webmail</td></tr>
								<tr><td class="talLft">
								<div style="padding-left:15%">
								5 halaman<br/>
								<ul class="talLft" >
									<li>Home</li>
									<li>About Us</li>
									<li>Contact Us</li>
									<li>Profile</li>
									<li>Product</li>
								</ul>
								* boleh diganti menu lain
								</div>
								</td></tr>
								<tr><td>Fitur CMS (Content Management System):
								<ul class="talLft">
								<li>User login</li>
								<li>Kelola konten (tambah, ubah dan hapus)</li>
								<li>Wysiwyg</li>
								<li>Kelola menu (tambah atau hapus)</li>
								</ul>
								</td></tr>
								<tr><td class="b">*minimum kontrak per tahun</td></tr>
							</table>
						</div>
					</div>
				</div>
				<?php $price = 7500000; ?>
				<div class="col-sm-4">
					<div class="panel panel-info panel-hover">
						<div class="panel-heading talCnt">
						Paket Silver <span class="b btn btn-xs btn-danger"><i class="fa fa-thumbs-o-up"></i> best seller</span>
						</div>
						<div class="panel-body padNon">
							<table class="table table-bordered table-hover talCnt mrgBtm0">
								<tr><td><span class="hero-price"><?php echo format_money($price)?></span><br/>per tahun <br/>atau<br/> <b><?php echo format_money($price/12) ?> per bulan</b></td></tr>
								<tr><td>Gratis domain .COM / NET / ORG / INFO atau BIZ </td></tr>
								<tr><td>Akun Hosting Linux 1.5 GB  & Bandwidth Unlimited</td></tr>
								<tr><td>10 Email (Pop3, Forwarder, Autoresponder), Webmail</td></tr>
								<tr><td class="talLft">
								<div style="padding-left:15%">
								8 halaman<br/>
								<ul class="talLft">
									<li>Home</li>
									<li>About Us</li>
									<li>Contact Us</li>
									<li>Product 1</li>
									<li>Product 2</li>
									<li>Product 3</li>
									<li>Product 4</li>
									<li>Product 5</li>
								</ul>
								* boleh diganti menu lain
								</div>
								</td></tr>
								<tr><td>Fitur CMS (Content Management System):
								<ul class="talLft">
								<li>User login</li>
								<li>Kelola konten (tambah, ubah dan hapus)</li>
								<li>Wysiwyg</li>
								<li>Kelola menu (tambah atau hapus)</li>
								</ul>
								</td></tr>
								<tr><td class="b">*minimum kontrak per tahun</td></tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-danger panel-hover">
						<div class="panel-heading talCnt">
						Paket Gold
						</div>
						<div class="panel-body padNon">
							<table class="table table-bordered table-hover talCnt mrgBtm0">
								<tr><td><span class="hero-price">25,000,000 IDR</span><br/>Sekali bayar untuk selamanya<br/><br/></td></tr>
								<tr><td>Gratis domain .COM / NET / ORG / INFO atau BIZ </td></tr>
								<tr><td>Akun Hosting Linux 3 GB  & Bandwidth Unlimited</td></tr>
								<tr><td>20 Email (Pop3, Forwarder, Autoresponder), Webmail</td></tr>
								<tr><td class="talLft">
								<div style="padding-left:15%">
								10 halaman<br/>
								<ul class="talLft">
									<li>Home</li>
									<li>About Us</li>
									<li>Contact Us</li>
									<li>Portofolio</li>
									<li>Profile</li>
									<li>Product 1</li>
									<li>Product 2</li>
									<li>Product 3</li>
									<li>Product 4</li>
									<li>Product 5</li>
								</ul>
								* boleh diganti menu lain
								</div>
								</td></tr>
								<tr><td>Fitur CMS (Content Management System):
								<ul class="talLft">
								<li>User login</li>
								<li>Kelola konten (tambah, ubah dan hapus)</li>
								<li>Wysiwyg</li>
								<li>Kelola menu (tambah atau hapus)</li>
								</ul><br/>
								</td></tr>
								<tr><td class="b">*Hanya sekali bayar untuk selamanya</td></tr>
							</table>
						</div>
					</div>
				</div>
			</section>

			<div class="col-sm-12">
				<h2 class="marNon br text-uppercase">Paket Custom Web</h2><br/>
			</div>
			<?php $price = 8000000; ?>
			<div class="col-sm-4">
				<div class="panel panel-default panel-hover">
					<div class="panel-heading talCnt">
					Invoicing System
					</div>
					<div class="panel-body padNon">
						<table class="table table-bordered table-hover talCnt mrgBtm0">
							<tr><td><?php echo format_money($price)?> per tahun <br/>atau<br/> <b><?php echo format_money($price/12) ?> per bulan</b></td></tr>
							<tr><td>Gratis domain .com, .net</td></tr>
							<tr><td>Hosting 750 MB</td></tr>
							<tr><td>5 Email</td></tr>
							<tr><td>Fitur
							<ul class="talLft">
								<li>Hak akses user</li>
								<li>User</li>
								<li>Produk</li>
								<li>Invoice</li>
								<li>Suratjalan</li>
								<li>Faktur</li>
								<li>Reporting</li>
							</ul>
							</td></tr>
							<tr><td class="b">*minimum kontrak per tahun</td></tr>
						</table>
					</div>
				</div>
			</div>
			<?php $price = 3500000; ?>
			<div class="col-sm-4">
				<div class="panel panel-default panel-hover">
					<div class="panel-heading talCnt">
					Website Wedding
					</div>
					<div class="panel-body padNon">
						<table class="table table-bordered table-hover talCnt mrgBtm0">
							<tr><td><?php echo format_money($price)?> per tahun <br/></td></tr>
							<tr><td>Gratis domain .com, .net</td></tr>
							<tr><td>Hosting 750 MB</td></tr>
							<tr><td>5 Email</td></tr>
							<tr><td>Pilih Theme dari Template yang tersedia</td></tr>
							<tr><td class="b">*minimum kontrak per tahun</td></tr>
						</table>
					</div>
				</div>
			</div>
			<?php //$price = 3500000; ?>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading talCnt">
					Custom Web
					</div>
					<div class="panel-body padNon">
						<table class="table table-bordered table-hover talCnt mrgBtm0">
							<tr><td>Silakan hubungi kami<br/></td></tr>
							<tr><td>Gratis domain .com, .net</td></tr>
							<tr><td>Hosting 750 MB</td></tr>
							<tr><td>5 Email</td></tr>
							<tr><td>4 Halaman Custom Layout</td></tr>
							<tr><td class="b">*minimum kontrak per tahun</td></tr>
						</table>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<br/>
				<h3>Add on <b>Custom Services</b></h3>
				<?php
				$i = 0;
				?>
				<table class="table table-responsive table-striped table-bordered" style="cellpadding: 15px">
				<tr class="b">
					<td width="1">#</td>
					<td>Services</td>
					<td>Harga</td>
					<td width="25px">Option</td>
				</tr>
				<?php $i++; ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><span class='b text-uppercase'>Domain co.id</span><br/> Pendaftaran domain co.id per tahun</td>
					<td>500.000</td>
					<td class="talCnt"><a href="#" target="_blank" class="btn btn-sm btn-info talCnt">Order</a></td>
				</tr>
				<?php $i++; ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><span class='b text-uppercase'>Konsultasi IT</span><br/>Jasa konsultasi IT seputar pembuatan API, arsitektur aplikasi, instalasi server, migrasi server, maintenance web.</td>
					<td>150.000</td>
					<td class="talCnt"><a href="#" target="_blank" class="btn btn-sm btn-info talCnt">Order</a></td>
				</tr>
				<?php $i++; ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><span class='b text-uppercase'>Konsultasi Startup</span><br/>Jasa konsultasi projek Startup seputar pembuatan API, arsitektur aplikasi, instalasi server, migrasi server, maintenance web.</td>
					<td>150.000</td>
					<td class="talCnt"><a href="#" target="_blank" class="btn btn-sm btn-info talCnt">Order</a></td>
				</tr>
				<?php $i++; ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><span class='b text-uppercase'>Manage Product SaaS</span><br/>Jasa manage product website SaaS (Software As A Service) projek API, arsitektur aplikasi, instalasi server, monitoring.</td>
					<td>150.000</td>
					<td class="talCnt"><a href="#" target="_blank" class="btn btn-sm btn-info talCnt">Order</a></td>
				</tr>
				<?php $i++; ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><span class='b text-uppercase'>Premium Support</span>
					<br/>Jasa support seputar instalasi server, migrasi server, maintenance web, backup script dan data.</td>
					<td>600.000</td>
					<td class="talCnt"><a href="#" target="_blank" class="btn btn-sm btn-info talCnt">Order</a></td>
				</tr>
				<?php $i++; ?>
				
				<!--
				<tr>
					<td><?php echo $i; ?></td>
					<td><span class='b text-uppercase'></span><br/></td>
					<td></td>
					<td class="talCnt"><a href="#" target="_blank" class="btn btn-sm btn-info talCnt">Order</a></td>
				</tr>
				-->
				</table>
			</div>
			
			<!-- Tokopedia & Shopee
			-->

			
		</div>
	</div>
</div>

<section class="" style="padding:35px 0 65px 0;background:#bae9ff">
	<div class="container" >
		<div class="row">
			<div class="col-sm-12">
				<div class="talCnt" id="faq"><h1>F.A.Q</h1><br/></div>
				<div class="col-sm-6">
					<span class="b">Mengapa harga paket website di Grevia mahal ?</span><br/>
					<span class="clrSftGry">Silakan anda bandingkan harga kami dengan harga kompetitor lain, boleh dikatakan harga kami adalah harga yang sangat kompetitif.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik, namun saya takut, apakah Grevia bisa dipercaya ?</span><br/>
					<span class="clrSftGry">Kami bekerja secara profesional dan sudah bergerak di bidang jasa teknologi ini selama 4 tahun. Silakan lihat portofolio klien kami di <a href="portofolio">sini</a>.</span>
					<br/><br/>
					
					<span class="b">Apakah anda menerima jasa pembuatan web berbasis CMS opensource, misal Wordpress, atau Blogspot ?</span><br/>
					<span class="clrSftGry">Tidak. Hal ini dikarenakan security CMS opensource yang rawan dibobol, sehingga kami mengembangkan pembuatan website dengan framework kami sendiri yang lebih secure, cepat dan mudah untuk di-custom(flexibel) sesuai kebutuhan anda.</span>
					
				</div>
				
				<div class="col-sm-6">
					<span class="b">Apa yang dimaksud Maintenance ?</span><br/>
					<span class="clrSftGry">Jasa maintenance termasuk jasa memperbaiki bug/error, menjamin uptime server agar bisa selalu diakses selama 24 jam non-stop.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik tapi saya bingung mau membuat website seperti apa ?</span><br/>
					<span class="clrSftGry">Silakan hubungi form atau kontak kami, kami akan berikan jasa konsultasi kapan saja, gratis.</span>
					<br/><br/>
					
					<span class="b">Dimana lokasi server Grevia ? Bagaimana dengan kecepatan aksesnya ?</span><br/>
					<span class="clrSftGry">Server kami semua terletak di Singapura, dan kecepatan akses tidak kalah dengan server lokal atau IIX.</span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="" style="padding:35px 0 65px 0;background:#fff;min-height: 300px">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="b clrRed">PEMESANAN</h2>
				
				<br/><br/>
			</div>
			
			<div class="col-sm-2">

			</div>
			<div class="8">
				<div class="talCnt">
					Untuk pemesanan silakan hubungi Rusdi di 0838.9199.8825 (telepon / Whatsapp)<br/><br/>
					atau <br/><br/>
					<a class="btn btn-success btn-sm" href="<?php echo base_url().'contact?subject=jasa%20webdesign'?>">Isi form kontak kami disini</a>
				</div>
			</div>
			<div class="col-sm-2">
			</div>
			
		</div>
	</div>
</section>
<!--
<div class="col-sm-12">
	<h1 class="talCnt">Fitur</h1>
	<div class="col-sm-4 col-sm-offset-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-2">
	</div>
	<br/><br/>
</div>
-->