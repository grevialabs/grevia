<div class="col-xs-12">
<?php
if (!empty($data))
{
	$str = "";
	$total_rows = $data['total_rows'];
	$list_data = $data['data'];
	?>
	<table class="table table-hover">
	<tr>
		<td width=1>#</td>
		<td>Title</td>
		<td>Replied</td>
		<td>Viewed</td>
	</tr>
	<?php
	$i = 0;
	if (is_numeric($page) && $page > 0) 
	{
		$i = ($page - 1) * $offset;
	}
	foreach($list_data as $key => $rs)
	{
		$i += 1;
		$topic = $rs['Topic'];
		$slug = strtolower(str_replace(' ','-',$topic));
		?>
		<tr class="<?php if ($rs['IsHelpfulExist'] == 1) echo 'bg-warning'?>">
		<td><?php echo $i;?></td>
		<td><?php if ($rs['IsHelpfulExist']) { ?><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Answered<br/><?php } ?>
		<a href="<?php echo base_url().'forumdetail/'.$rs['ForumTopicID'].'/'.$slug ?>"><?php echo $topic?></a></td>
		<td><?php echo $rs['TotalReply']; ?></td>
		<td><?php echo $rs['View']; ?></td>
		</tr>
		<?php 
	}
	?>
	</table>
	<?php
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
}


?>
</div>
<!--
<div class="col-xs-3 " align="right">
<blockquote class='bg-primary'>
ini sidebar nih
<div style="background:#fff;color:#000">-- Anonym</div>
</blockquote>
</div>
-->
