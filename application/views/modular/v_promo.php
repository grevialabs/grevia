<?php 
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB, $META_DESCRIPTION;

$PAGE_TITLE = 'Promo menarik bulan ini - Grevia';
$META_DESCRIPTION = 'Promo menarik jasa pembuatan website di Grevia.';
// $PAGE_HEADER = NULL;
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, ABOUT_US);

?>
<div class="container">	
	<div class="row">
		<div class="col-sm-12">
			<h2 class="b clrBlu text-uppercase">Promo bulan ini</h2><hr/>
			<p>
				Dapatkan promo menarik <span>Gratis biaya instalasi Server di Grevia</span> selama bulan November - Desember di 2020.
			</p>
			<p>
				Syarat dan Ketentuan
				<ol>
					<li>Hanya berlaku bagi pelanggan baru yang belum pernah menggunakan jasa Grevia</li>
					<li>Jasa instalasi server sebesar 250 ribu akan digratiskan apabila pembayaran dilakukan secara penuh sebesar 100% dalam sekali bayar</li>
					<!-- <li></li> -->
					<!-- <li></li> -->
					
				</ol>

				<br/>
				<br/>
			</p>

			<p class='talCnt'>
				Tunggu apalagi ayo segera daftar paket kamu sekarang.<br/><br/>
				<a class='btn btn-success btn-md' href='<?php echo base_url().'order?ref=promo'?>'>Order sekarang</a><br/><br/>
			</p>
		</div>
	</div>
</div>

