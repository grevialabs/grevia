<?php 

/*|
  | LOGIN MASI BELUM PAKE INI
*/
if ($this->input->post('hdnLogin')) 
{
	$gInfo = '';
	$email = filter(post('l_Email'));
	$password = filter(post('l_Password'));
	if(isset($email) && isset($password))
	{
		$member = $this->member_model->get(array('Email' => $email));
		if(!empty($member))
		{
			if($member['Password'] == encrypt($password))
			{
				$encrypt = encrypt($member['MemberID']."#".$member['FullName']."#".$member['Email']."#".$member['IsAdmin']);
				// 24 minutes expired cookie on default
				$expiredCookie = time() + 24 * 60;
				if (post('l_RememberMe')) $expiredCookie = time() + 30 * 86400;
				
				setcookie('hash', $encrypt, $expiredCookie, base_url());
				redirect('member/profile');
			}
			else
			{
				$gInfo = 'Email / Password tidak sesuai';
			}
		}
		else
		{
			$gInfo = 'Email tidak ditemukan';
		}
	}
}
/*|
  | UPDATE
*/
if ($_POST) {
	$gInfoReg = NULL;
	if(post('hdnRegister') == 1) {
		
		if(!is_filled($_POST['FullName'])) {
			$gInfoReg.= 'Nama harus diisi';
		} else if(!is_filled($_POST['Email'])) {
			$gInfoReg.= 'Email harus diisi';
		} else if(!is_filled($_POST['Password'])) {
			$gInfoReg.= 'Password harus diisi';
		} else if(!is_filled($_POST['Gender'])) {
			$gInfoReg.= 'Gender harus diisi';
		} else if(!is_filled($_POST['DOB'])) {
			$gInfoReg.= 'Tgl lahir harus diisi';
		} else {
			$objMember = NULL;
			$objMember = $this->member_model->get(
				array('Email' => $_POST['Email'])
			);
			if (empty($objMember)) {
				$email = $_POST['Email'];
				$fullName = $_POST['FullName'];
				$activeCode = genRandomString(10);
					
				$param = array(
					'FullName' => $_POST['FullName'],
					'Name' => $_POST['FullName'],
					'Email' => $_POST['Email'],
					'Password' => encrypt($_POST['Password']),
					'Gender' => $_POST['Gender'],
					'DOB' => $_POST['DOB'],
					'ActiveCode' => $activeCode,
					'IsAdmin' => 0,
					'IsSubscribe' => 1,
					'IsActive' => 0,
					'IsDeleted' => 0,
					'CreatorDateTime' => getDateTime(),
				);
				$save = $this->member_model->save($param);
				
				if($save){			
					
					// benerin lagi nih
					$email_member.='<html><head></head><body><div style="padding:15px">Dear '.$fullName.',<br/><br/>Anda mendapatkan email ini karena anda telah mendaftar di Grevia.com.';
					$email_member.='Silakan klik <a href="'.base_url().'activation?do=submit&email='.$email.'&code='.$activeCode.'">Link ini</a> atau ketik url ini ke browser anda '.base_url().'activation?do=submit&email='.$email.'&code='.$activeCode.' untuk konfirmasi akun anda.<br/><br/>';
					$email_member.='<p>Salam.<br/>Tim Grevia</p></div></body></html>';
					if(!is_internal()){
						// SEND EMAIL HERE
						$to = $email;
						$subject = "Aktivasi Akun Member Grevia.com";
						$headers = "From: Grevia Team.<noreply@grevia.com> \r\n";
						$headers .= "Reply-To: \r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$send = mail($to,$subject,$email_member,$headers);
						$gInfoReg.= 'Registrasi berhasil. Silakan periksa akun email anda untuk link aktivasi akun Grevia anda';
					}else{
						$gInfoReg.= 'Registrasi berhasil.'.br().$email_member;
					}
				}else{
					$gInfoReg = 'Registrasi gagal, Silakan coba lagi atau hubungi administrator';
				}
			}else{
				$gInfoReg = 'Email sudah terdaftar. Silakan gunakan email lain';
			}
		}
	}
}
?>
<div class='col-md-6'>
	<h1 class="title-header">&nbsp;Login</h1><hr/>
	<a href="forgotpassword">Lupa Password</a> | <a href="activation">Kirim ulang email aktivasi</a><br/>
	<div class="errLog"></div>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='text' class='form-control' name='l_Email' id='l_Email' placeholder='Email...' value='<?php if ($this->input->post('l_Email')) echo filter(post('l_Email'))?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='password' class='form-control' name='l_Password' id='l_Password' placeholder='Password...' value=''></div>
		</div>
		<div class='form-group form-group-md'>
			<div class='col-sm-1 checkbox ' style='padding-left:30px'>
				<label><input type='checkbox' name='l_RememberMe' id='l_RememberMe' value='1' /></label>
			</div>
			<div class='col-sm-11' style='padding-top:8px'><label for='l_RememberMe'>Ingat saya</label></div>
		</div>
		<div class='form-group'>
			<div class='col-sm-12'><input type="hidden" name="hdnLogin" value="1"/><button type="submit" value="" class="btn btn-success"/>Login</button></div>
		</div>
	</form>
	<?php
	if (isset($gInfo)) echo print_message($gInfo);
	?>
</div>

<div class='col-md-6'>
	<h1 class="title-header">&nbsp;Registrasi</h1><hr/>
	<div class='col-sm-2'>
		<div id='fblogin'><fb:login-button scope="public_profile,email" size="large" onlogin="checkLoginState();"></fb:login-button></div>
	</div>
	
	<div class='col-sm-2'>
		<div id="gConnect">
			<button class="g-signin"
				data-scope="https://www.googleapis.com/auth/plus.profile.emails.read"
				data-requestvisibleactions="http://schemas.google.com/AddActivity"
				data-clientId="<?php echo GOOGLE_CLIENT_ID ?>"
				data-callback="onSignInCallback"
				data-theme="dark"
				data-cookiepolicy="single_host_origin">
			</button>
		</div>
	</div>
	<div class='clearfix'></div>
	
	<form method="post" id="frmReg" class="form-group">
		<div class="errReg"></div>
		<?php if (isset($gInfoReg))echo message($gInfoReg); ?>
		<div class="input-group margin-bottom-sm br">
			<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
			<input class="form-control" type="text" placeholder="Nama Lengkap" name="f_FullName" required/>
			<input type='hidden' name='f_FacebookID' id='f_FacebookID'/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
		  <input class="form-control" type="text" placeholder="Email" name="f_Email" required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
		  <input class="form-control" type="password" placeholder="Password" name="f_Password" minlength="4" required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-neuter fa-fw"></i></span>
		  <select name="f_Gender" id='f_Gender' placeholder="Gender" title="Pilih Gender" class="form-control" required>
			<option></option>
			<option value="1" <?php //if($gender == 1)echo 'selected'?>>Pria</option>
			<option value="2" <?php // if($gender == 2)echo 'selected'?>>Wanita</option>
		  </select>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
		  <input class="form-control datepicker" type="text" placeholder="Tgl lahir" name="f_DOB" required/>
		</div>
		<div class='form-group form-group-md'>
			<div class='col-sm-1 checkbox ' style='padding-left:30px'>
				<label><input type='checkbox' name='f_IsAgree' id='f_IsAgree' value='1' checked /></label>
			</div>
			<div class='col-sm-11' style='padding-top:8px'><label for='f_IsAgree'> Saya setuju dengan syarat dan ketentuan dari <strong>Grevia</strong></label></div>
		</div>
		<div class="form-group form-group-md br">
			<div class="col-sm-12"><input type='hidden' name='hdnRegister' value='1'/><button value="Register" class="btn btn-info"/>Register</button></div>
		</div>
		<div class='clearfix'></div><br/>
		
		<!--
		<div class="errReg"></div>
		<p><input type="" placeholder="Nama lengkap" name="f_FullName" id="f_FullName" maxlength="50" value="<?php //echo $fullName;?>" minlength="4" required /></p>
		<input type='hidden' name='f_FacebookID' id='f_FacebookID'/>
		<p><input type="email" placeholder="Email valid" name="f_Email" id="f_Email" maxlength="50" value="<?php //echo $email;?>" required /></p>
		<p><select name="f_Gender" id='f_Gender' placeholder="Gender" title="Pilih Gender" class="wdtFul" required>
		<option></option>
		<option value="1" <?php //if($gender == 1)echo 'selected'?>>Pria</option>
		<option value="2" <?php // if($gender == 2)echo 'selected'?>>Wanita</option></select></p>
		<p><input type="password" placeholder="Kata Sandi" name="f_Password" minlength="4" required/></p>
		<p><input type="text" name="f_BirthDate" placeholder="Tgl Lahir" class="datepicker" id='f_BirthDate' value="<?php //echo $birthDate;?>"/></p>
		<p><input type="checkbox" class="checkbox w10" name="f_IsAgree" value="1" checked required/> Saya setuju dengan syarat dan ketentuan dari <strong>Grevia</strong></p>
		<p><input type='hidden' name='hdnRegister' value='1'/><button value="Register" class="btn btn-info"/>Register</button></p>
		<?php if (isset($gInfoReg))echo print_message($gInfoReg); ?>
		-->
	</form>
</div>


<!-- START FACEBOOK -->

<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
	  //$('#fblogin').hide();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      //document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      // document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo FACEBOOK_APP_ID?>',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    //console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      //alert(JSON.stringify(response));
	  //console.log('Successful login for: ' + response.name);
      //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
	  <?php if (!is_filled(get('data'))) { ?>
	  //document.getElementById('status').innerHTML = JSON.stringify(response);
	  //window.location = '<?php echo base_url().'login?facebook=1'?>' + '&data=' + JSON.stringify(response);
	  <?php } ?>
	  document.getElementById('f_FacebookID').value = response.id;

	  document.getElementById('f_FullName').value = response.name;
	  document.getElementById('f_Email').value = response.email;
	  var userGender = response.gender;
	  if (response.gender == 'male') userGender = '1';
	  else if( response.gender == 'female') userGender = '2';
		var dd = document.getElementById('f_Gender');
		for (var i = 0; i < dd.options.length; i++) {
			if (dd.options[i].value == userGender) {
				dd.selectedIndex = i;
				break;
			}
		}
    });
  }
</script>
<!-- GOOGLE + LOGIN -->
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
<!-- GOOGLE + LOGIN -->


<script type="text/javascript">
var helper = (function() {
  var BASE_API_PATH = 'plus/v1/';

  return {
    /**
     * Hides the sign in button and starts the post-authorization operations.
     *
     * @param {Object} authResult An Object which contains the access token and
     *   other authentication information.
     */
    onSignInCallback: function(authResult) {
      gapi.client.load('plus','v1').then(function() {
        $('#authResult').html('Auth Result:<br/>');
        for (var field in authResult) {
          $('#authResult').append(' ' + field + ': ' +
              authResult[field] + '<br/>');
        }
        if (authResult['access_token']) {
          $('#authOps').show('slow');
          $('#gConnect').hide();
          helper.profile();
          helper.people();
        } else if (authResult['error']) {
          // There was an error, which means the user is not signed in.
          // As an example, you can handle by writing to the console:
          console.log('There was an error: ' + authResult['error']);
          $('#authResult').append('Logged out');
          $('#authOps').hide('slow');
          $('#gConnect').show();
        }
        console.log('authResult', authResult);
      });
    },

	/**
     * Calls the OAuth2 endpoint to disconnect the app for the user.
     */
    disconnect: function() {
      // Revoke the access token.
      $.ajax({
        type: 'GET',
        url: 'https://accounts.google.com/o/oauth2/revoke?token=' +
            gapi.auth.getToken().access_token,
        async: false,
        contentType: 'application/json',
        dataType: 'jsonp',
        success: function(result) {
          console.log('revoke response: ' + result);
          $('#authOps').hide();
          $('#profile').empty();
          $('#visiblePeople').empty();
          $('#authResult').empty();
          $('#gConnect').show();
        },
        error: function(e) {
          console.log(e);
        }
      });
    },

    /**
     * Gets and renders the list of people visible to this app.
     */
    people: function() {
      gapi.client.plus.people.list({
        'userId': 'me',
        'collection': 'visible'
      }).then(function(res) {
        var people = res.result;
        // $('#visiblePeople').empty();
        // $('#visiblePeople').append('Number of people visible to this app: ' +
            // people.totalItems + '<br/>');
        // for (var personIndex in people.items) {
          // person = people.items[personIndex];
          // $('#visiblePeople').append('<img src="' + person.image.url + '">');
        // }
      });
    },

    /**
     * Gets and renders the currently signed in user's profile data.
     */
    profile: function(){
      gapi.client.plus.people.get({
        'userId': 'me'
      }).then(function(res) {
        var profile = res.result;
		var email_google;
		alert(res.result.emails.value);
		<?php if (is_filled(get('data')) ) { ?>
		//window.location = '<?php echo base_url().'login?data=' ?>' + JSON.stringify(profile);
		<?php } ?>
        $('#f_Email').val(profile.email);
		$('#f_FullName').val(profile.displayName);
		email_google = JSON.parse(profile.emails);
		alert(email_google[0].value);
		$('#profile').empty();
		$('#profile').append(JSON.stringify(profile));
        // $('#profile').append(
            // $('<p><img src=\"' + profile.image.url + '\"></p>'));
        // $('#profile').append(
            // $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
            // profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
        // if (profile.cover && profile.coverPhoto) {
          // $('#profile').append(
              // $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
        // }
      }, function(err) {
        var error = err.result;
        $('#profile').empty();
        $('#profile').append(error.message);
      });
    }
  };
})();

/**
 * jQuery initialization
 */
$(document).ready(function() {
  $('#disconnect').click(helper.disconnect);
  // if ($('[data-clientId="<?php echo GOOGLE_CLIENT_ID?>"]').length > 0) {
    // alert('This sample requires your OAuth credentials (client ID) ' +
        // 'from the Google APIs console:\n' +
        // '    https://code.google.com/apis/console/#:access\n\n' +
        // 'Find and replace YOUR_CLIENT_ID with your client ID.'
    // );
  // }
});

/**
 * Calls the helper method that handles the authentication flow.
 *
 * @param {Object} authResult An Object which contains the access token and
 *   other authentication information.
 */
function onSignInCallback(authResult) {
  helper.onSignInCallback(authResult);
}
</script>
<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->


<!-- END FACEBOOK -->

	