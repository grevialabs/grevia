<?php
if ($_POST && is_filled($_POST['do'])) {
	
	$str = '';
	$do = $_POST['do'];
	
	// SUBSCRIBE
	if ($do == "subscribe" && is_filled($_POST['email'])) {
		$tmp['email'] = $_POST['email'];
		$tmp['creator_date'] = get_datetime();
		
		if (!$this->subscribe_model->get($tmp)) {
			$save = $this->subscribe_model->save($tmp);
			if ($save) $str = INFO_SAVE_SUCCESS;
			else $str = INFO_ERROR_OCCURED;
		} else {
			$str = INFO_EMAIL_SUBSCRIBE_EXIST;
		}
	} else {
		$str = INFO_DATA_INVALID;
	}
} else {
	header_status(404);
	header_status(200);
	die;
}
//$str = 'die';
echo $str;
die;
?>