<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;
global $SETTING;

$PAGE = 'Shopping Checkout';

$this->order_model->redirect_to_cart_if_cart_empty();

// $template = $this->order_model->email_template_order();
// $template = $this->order_model->email_order(cookie_order_id());
// echo $template;
// die;

// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;
$PAGE_HEADER = $PAGE;

// REDIRECT USER TO LAST PAGE HE/SHE AT AFTER LOGIN
$refer_url = explode(base_url(),current_url());
$refer_url = urlencode($refer_url[1]);
$login = base_url().'login?url='.$refer_url;

// DEPRECATED BECAUSE GUEST NOW CAN SHOPPING
// if (!is_member()) redirect($login);

$cookie_order_id = $order_id = $get_product_size_id = $get_quantity = NULL;

if (isset($_GET['product_size_id']) AND is_numeric($_GET['product_size_id'])) $get_product_size_id = $_GET['product_size_id'];
if (isset($_GET['quantity']) AND is_numeric($_GET['quantity'])) $get_quantity = $_GET['quantity'];

// $this->session->set_flashdata('yamete','ternyata bisa nih');
// $message = $this->session->flashdata('yamete');
// if (isset($message)) echo $message;
// $this->order_model->email_order_new(cookie_order_id());
// SUBMIT AND CHECKOUT CART
if ($_POST && isset($_POST['btn-finish'])) 
{
	$obj_order = $this->order_model->get(array(
		'order_id' => cookie_order_id()
		
	));
	
	/*
	 | EMAIL TO CUSTOMER HERE
	 | -------------------------------------------------------------------------------------------------------------
	*/
	$this->order_model->email_order_new(cookie_order_id());
	
	// UPDATE ORDER IF MEMBER
	$cookie_order_id = cookie_order_id();
	$update_order['member_id'] = NULL;
	if (member_cookies('member_id'))
	{
		$update_order['member_id'] = $update_order['creator_id'] = member_cookies('member_id');
		$this->order_model->update($cookie_order_id,$update_order);
	}
	setcookie('order_id',NULL,time()-3600,'/');
	
	
	// UPDATE HEADER AND DETAIL ORDER FINAL
	/*
	 | UPDATE STATUS TO 1,
	 | UPDATE ORDER DETAIL PRICE WITH PRODUCT SIZE PRICE; UPDATE ORDER HEADER ex: subtotal, shipping_weight, shipping_cost
	 | INSERT LOG STATUS,
	*/
	$this->order_model->cron_finalize_order($cookie_order_id);
	
	$insert_log = $this->log_model->save(array(
		'order_id' => $cookie_order_id,
		'actionlog' => 'Member Order Checkout',
		'log' => NULL,
		'member_id' => $update_order['member_id']
	));
	
	redirect(base_url().'shoppingcart/thankyou?order_id='.$cookie_order_id);
	die;
}

$param_order = NULL;
$obj_order = $obj_list_order_detail = NULL;

$param_order['order_id'] = cookie_order_id();
$obj_order = $this->order_model->get($param_order);
$obj_list_order_detail = $this->order_model->get_list_detail($param_order);
$obj_list_order_detail = $obj_list_order_detail['data'];
?>

<div class="col-sm-12">
	<div class="talCnt hidden-xs">
		<strike style="width:70px" class="clrBlk"><i class="fa fa-circle clrBlk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</strike>	
		<strike style="width:70px" class="clrBlk"><i class="fa fa-circle clrBlk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</strike>
		<strike style="width:70px" class="clrGry"><i class="fa fa-motorcycle clrBlk"></i>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i></strike>
		<br/> 
		<span class="b" style="padding-right:65px"> 
			<i class="fa fa-arrow-circle-right"></i> 1. Shoppingcart
		</span> 
		
		<span class="b" style="padding-right:85px"> 
			<i class="fa fa-arrow-circle-right"></i> 2. Shipping
		</span>
		
		<span class="b" style="padding-right:65px"> 
			<i class="fa fa-arrow-circle-right"></i> 3. Checkout
		</span>
		
		<span class="clrGry" style=""> 
			<i class="fa fa-arrow-circle-right"></i> 4. Thankyou 
		</span>
	</div><br/>
</div>
	
<div class="col-sm-12">
	
	<?php 
	if (!empty($obj_order))
	{
		?>
		<div class="col-sm-12">
		<table class="table table-bordered">
			<tr class="alert bg-info talCnt">
				<th width="5px" class="talCnt">#</th>
				<th>Produk</th>
				<th class="talCnt" width="15%">Qty</th>
				<th class="talCnt" width="">Harga</th>
				<th class="talCnt" width="15%">Subtotal</th>
			</tr>
		<?php
		$grandtotal = $subtotal = $tax = $total_weight = 0;
		foreach ($obj_list_order_detail as $key => $shopcart)
		{
			$key++;
			// list_shoppingcart
			// debug($shopcart);
			$subtotal = 0;
			$subtotal = $shopcart['product_size_price']*$shopcart['quantity'];
			$grandtotal += $subtotal; 
			
			$prime_image = explode(';',$shopcart['list_image']);
			$prime_image = $prime_image[0];
			
			$total_weight += $shopcart['weight'];
			
			?>
			<tr>
				<td class="talCnt"><?php echo $key?></td>
				<td><a href="<?php echo base_url().'product/'.$shopcart['product_size_id'].'/'.$shopcart['slug']?>"><?php echo $shopcart['product_size_name'].''; ?></a><br/>
				<img src="<?php echo asset_url().'images/product/100/'.$prime_image?>" class="img-shopcart"/>
				</td>
				<td class="talCnt">
				<?php echo $shopcart['quantity']?></td>
				<td class="talRgt"><?php echo format_money($shopcart['product_size_price'])?></td>
				<td class="talRgt"><?php echo format_money($subtotal)?></td>
			</tr>
			<?php
		}
		$total_weight = ceil($total_weight);
		$shipping_cost = $total_weight * $obj_order['cost_perkilo'];
		$shipping_cost = $obj_order['shipping_cost'];
		$payment_code = $obj_order['payment_code'];
		
		?>
		<tr>
			<td colspan="4" valign="top" class="b">Subtotal</td>
			<td colspan="2" valign="top" class="talRgt"><?php echo format_money($grandtotal)?></td>
		</tr>
		<?php $grandtotal += $shipping_cost; ?>
		<tr>
			<td colspan="4" valign="top" class="b">Shipping Cost</td>
			<td colspan="2" valign="top" class="talRgt"><?php echo format_money($shipping_cost).BR.' ('.$total_weight.' kg & '.$obj_order['shipping_distance'].' km)'?></td>
		</tr>
		<?php $grandtotal += $payment_code; ?>
		<tr>
			<td colspan="4" valign="top" class="b">Payment Code</td>
			<td colspan="2" valign="top" class="talRgt"><?php echo format_money($payment_code)?></td>
		</tr>
		<tr>
			<td colspan="4" valign="top" class="b">Grandtotal</td>
			<td colspan="2" valign="top" class="talRgt"><?php echo format_money($grandtotal)?></td>
		</tr>
		<tr>
			<td colspan="4" valign="top" class="b">Payment Type</td>
			<td colspan="2" valign="top" class="talCnt"><?php echo $obj_order['payment_type_name']?></td>
		</tr>
		</table>
		
		<table class="table table-bordered">
		<tr>
			<td width="20%" class="b">Nama Penerima</td>
			<td><?php if (isset($obj_order['shipping_name'])) echo $obj_order['shipping_name']; ?></td>
		</tr>
		<tr>
			<td class="b">Email Penerima</div>
			<td><?php if (isset($obj_order['shipping_email'])) echo $obj_order['shipping_email']; ?></td>
		</tr>
		<tr>
			<td class="b">Telp Penerima</div>
			<td><?php if (isset($obj_order['shipping_phone'])) echo $obj_order['shipping_phone']; ?></td>
		</tr>
		<tr>
			<td class="b">Alamat Penerima</div>
			<td><?php if (isset($obj_order['shipping_address'])) echo $obj_order['shipping_address']; ?></td>
		</tr>		
		<tr>
			<td class="b" valign="top">Catatan *opsional</td>
			<td valign="top"><div class="h200" style="min-height:150px"><?php if (isset($obj_order['shipping_notes'])) echo $obj_order['shipping_notes']; ?></div></td>
		</tr>
		</table>
		
		<div class="col-sm-12 talCnt">
			<form method="post">
			<a class="btn btn-md btn-default br-md min-width-180" href="<?php echo base_url().'shoppingcart/shipping';?>"><i class="fa fa-arrow-circle-left"></i> KEMBALI </a>
			<button class="btn btn-md btn-success btn-finish br-md min-width-180" name="btn-finish" value="1">SELESAIKAN BELANJA <i class="fa fa-arrow-circle-right"></i></button>
			<span class="btn-loading"><i class="fa fa-spinner fa-pulse fa-lg"></i></span>
			</form>
			<br/>
		</div>
		<br/><br/>
		</div>
	
	<?php 
	}
	?>
	
	<?php ?>
	
</div>

<script>
$(document).ready(function(){
	$('.btn-loading').hide();
	$('.btn-finish').click(function(){
		$(this).hide();
		$('.btn-loading').show();
	})
})
</script>