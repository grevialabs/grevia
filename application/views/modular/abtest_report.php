<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE =  'AB Testing - Grevia';
$PAGE_HEADER = 'AB Testing Report';
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE_HEADER);

$get_abtest_id = $get_do = NULL;
if (isset($_GET['abtest_id'])) $get_abtest_id = $_GET['abtest_id'];
if (isset($_GET['do'])) $get_do = $_GET['do'];

function get_status_test($stat)
{
	if (!isset($stat) || $stat != '1') $stat = 0;
	
	if ($stat == 1) return '<i class="fa fa-check text-success" aria-hidden="true" title="Selesai"></i>';
	else if ($stat == 0) return '<i class="fa fa-times text-danger" aria-hidden="true" title="Belum selesai"></i>';
}
?>

<div class="col-sm-12">
<?php
if ($get_do == 'detail' && isset($get_abtest_id)) 
{
	// -----------------------------------
	// Detail data start
	$obj_list_report = $param = NULL;
	$param['abtest_id'] = $get_abtest_id;
	$obj_list_report = $this->abtest_model->get_list_detail($param);
	$obj_list_report = $obj_list_report['data'];
	
	$url_abtest = base_url();
	
	if (! empty($obj_list_report)) 
	{
		$key_top = NULL;
		$key_bottom = NULL;
		
		// debug($obj_list_report);
		foreach ($obj_list_report as $key => $rep) {
			if ($key == 0) {
				$key_top = $key;
			} else {
				
				// key > 1
				// current greater than prev
				if ($obj_list_report[$key]['ratio_click_percent'] > $obj_list_report[$key - 1]['ratio_click_percent']) {
					// compare with current 
					if ($obj_list_report[$key]['ratio_click_percent'] > $obj_list_report[$key_top]['ratio_click_percent']) $key_top = $key;
				}
				
				if ($obj_list_report[$key]['ratio_click_percent'] < $obj_list_report[$key - 1]['ratio_click_percent']) {
					// compare with current
					$key_bottom = $key;
					if ($obj_list_report[$key]['ratio_click_percent'] < $obj_list_report[$key_bottom]['ratio_click_percent']) $key_bottom = $key; 
				}
			}
		}
		?>
		<table class="table table-bordered">
		<thead>
		<tr class="talCnt bgBlu clrWht">
			<td>#</td>
			<td>Judul</td>
			<td>URL abtest</td>
			<td>Sample</td>
			<td>Click</td>
			<td>Click Ratio</td>
			<td>Status Test</td>
			<td>Peringkat</td>
			<td>Option</td>
		</tr>
		</thead>
		<tbody>
			<?php
			// debug($key_top);
			$i = 1;
			$is_report_complete = 1;
			foreach ($obj_list_report as $key => $rep) 
			{
				$css_top = $css_bottom = $trophy = NULL;
				if ($key == $key_top) {
					$css_top = 'bg-success';
					$trophy = '<i style="color:gold;" class="fa fa-trophy" aria-hidden="true"></i>';
				}
				
				if ($key == $key_bottom) $css_bottom = 'bg-danger';
				
				$visitor_percent = NULL;
				$visitor_percent = (($rep['quota_each_detail_used'] / $rep['quota_each_detail'])*100);
				
				// status: done or not
				$status_test = 0;
				if ($visitor_percent == '100') {
					$visitor_percent = '(<b>'.$visitor_percent.'%</b>)';
					$status_test = 1;
				} else {
					$visitor_percent = '('.$visitor_percent.'%)';
					$is_report_complete = 0;
				}
				
				?>
				<tr class="talCnt <?php echo $css_top.' '.$css_bottom; ?>">
					<td><?php echo $i; ?></td>
					<td><?php echo $rep['title'] ?></td>
					<td><?php echo $url_abtest.$rep['fullmodulename'] ?></td>
					<td><?php echo $rep['quota_each_detail'] ?></td>
					<td><?php echo $rep['total_pageview_click'].' of '. $rep['quota_each_detail_used'].' '.$visitor_percent ?></td>
					<td><?php echo $rep['ratio_click_percent'] ?>%</td>
					<td><?php echo get_status_test($status_test) ?></td>
					<td><?php echo $trophy ?></td>
					<td><a href="?do=preview&abtest_id=<?php echo $rep['abtest_id']; ?>" class="btn btn-info btn-xs">preview</a></td>
				</tr>
				<?php
				$i++;
			}
			?>
			<?php
			// get status
				?>
				<tr>
					<td colspan="100%">Progress status : <?php echo ($is_report_complete) ? "<b class='text-success'>DONE</b>" : "<b class='text-warning'>ON PROGRESS</b>"; ?></td>
				</tr>
				<?php
			
			?>
		</tbody>
		</table>
		<?php
	}
	else 
	{
		?>
		Data tidak ditemukan
		<?php
	}
	
	// Detail data end
	// -----------------------------------
}
else 
{
	// -----------------------------------
	// List detail start
	$obj_list = $param = NULL;
	if (isset($get_abtest_id)) {
		$param['abtest_id'] = $get_abtest_id;
	}
	$obj_list = $this->abtest_model->get_list($param);
	$obj_list = $obj_list['data'];

	if (! empty($obj_list)) 
	{
		?>
		<table class="table table-hover table-bordered">
		<thead>
		<tr class="talCnt bgBlu clrWht">
			<td>#</td>
			<td>Judul</td>
			<td>URL abtest</td>
			<td>Tgl</td>
			<td>Status</td>
			<td>Option</td>
		</tr>
		</thead>
		<tbody>
		<?php
		// debug($obj_list);
		$url_abtest = base_url();
		$i = 1;
		foreach($obj_list as $obj)
		{
			?>
			<tr class="talCnt">
				<td><?php echo $i; ?></td>
				<td class="talLft"><?php echo $obj['title'] ?></td>
				<td class="talLft"><?php echo $url_abtest.$obj['modulename'] ?></td>
				<td><?php echo date('d-m-Y H:i',strtotime($obj['startdate'])).' - '.date('d-m-Y H:i',strtotime($obj['enddate'])) ?></td>
				<td><?php echo get_status($obj['status']); ?></td>
				<td><a href="?do=detail&abtest_id=<?php echo $obj['abtest_id']; ?>" class="btn btn-info btn-xs">Detail</a></td>
			</tr>
			<?php
			$i++;
		}
		?>
		</tbody>
		</table>
		<?php
	}
	// List detail end
	// -----------------------------------
}
?>
</div>