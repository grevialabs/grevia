<?php



$param = array(
'status' => '1'
);
$list_product = $this->product_model->get_list($param);
$list_product = $list_product['data'];

// debug($list_product);
// die;

if ( ! empty($list_product))
{
?>

	<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<td class="talCnt">#</td>
		<td>Name</td>
		<td>Desc</td>
		<td>Harga</td>
		<td>Option</td>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($list_product as $k => $lp) { ?>
	<tr>
		<td class="talCnt"><?php echo ($k + 1)?></td>
		<td><b><?php echo $lp['name']?></b><?php if (isset($lp['teaser'])) echo BR.'<span class="i">'.$lp['teaser'].'</div>';?></td>
		<td><?php echo nl2br($lp['description']); ?></td>
		<td class="talRgt"><?php echo format_money($lp['price']); ?></td>
		<td class="talCnt">
		<input type="hidden" name="hdn_product_id" value="<?php echo $lp['product_id']; ?>" />
		<a href="<?php echo base_url().'cart?do=add&product_id='.$lp['product_id']; ?>"><button class="btn btn-success btn-sm" type="submit">BELI</button></a></td>
	</tr>
	<?php } ?>
	</tbody>
	</table>
<?php 
}
?>
<!--
<div class="jumbotron" style="background:#EBAA00;margin-bottom:0;min-height:250px">
	<div class="container" style="">
		<div class="row clrWht talCnt wow fadeIn">
			<div class="col-sm-12">
				<h2>Grevia, jasa pembuatan website & company profile terbaik di jakarta.</h2><br/><br/>
			</div>
			
			<div class="col-sm-4 br wow fadeIn" >
				<i class="fa fa-thumbs-up fa-5x" aria-hidden="true"></i> <br/>Web terbaik
			</div>
			<div class="col-sm-4 br wow fadeIn" data-wow-delay="0.3s">
				<i class="fa fa-html5 fa-5x" aria-hidden="true"></i> <br/>Support HTML 5
			</div>
			<div class="col-sm-4 br wow fadeIn" data-wow-delay="0.6s">
				<i class="fa fa-cloud-upload fa-5x" aria-hidden="true"></i> <br/>Cloud Server
			</div>
			
			<div class="col-sm-12">
				<br/><br/>
			</div>
			
			<div class="col-sm-4 br wow fadeIn" data-wow-delay="0.9s">
				<i class="fa fa-money fa-5x" aria-hidden="true"></i> <br/>Harga terbaik
			</div>
			<div class="col-sm-4 br wow fadeIn" data-wow-delay="1.2s">
				<i class="fa fa-desktop fa-5x" aria-hidden="true"></i> <br/>Responsive Mobile
			</div>
			<div class="col-sm-4 br wow fadeIn" data-wow-delay="1.5s">
				<i class="fa fa-smile-o fa-5x" aria-hidden="true"></i> <br/>Melayani dengan senyum
			</div>
			
			<div class="col-sm-12">
				<div class="talCnt clrWht">
					<br/><br/>
					<a class="btn btn-success btn-lg" href="order" title="Order Web sekarang">Buat website kamu sekarang</a>
				</div>
			</div>
		</div>
	</div>
</div>
-->

<div class="jumbotron" style="background:#fff;margin-bottom:0;min-height:250px">
	<div class="container" style="">
		<div class="col-sm-12" style="">
			Grevia adalah perusahaan yang melayani pembuatan website, company profile dengan harga termurah dan terbaik di Jakarta. Kami telah melayani pelanggan sejak tahun 2011 dengan prinsip mengutamakan optimalisasi website untuk menunjang bisnis anda. Dengan pengalaman beberapa tahun, kami menjamin website yang kami buat menggunakan teknologi terbaik yaitu desain yang responsif, cloud server, uptime 99% dan SEO Google Friendly.<br/><br/>
			
			Sebagai bukti komitmen kepuasan pelanggan Grevia, kami berikan <b>*garansi cashback uang kembali</b>, jika dalam tempo 14 hari (setelah project web selesai), website yang anda pesan dari kami, tidak sesuai atau mengalami gangguan.<br/><br/>
			<div class="f12 b">*syarat & ketentuan berlaku.</div>
		</div>
	</div>
</div>