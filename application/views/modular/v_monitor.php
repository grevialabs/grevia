<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = 'Monitor Server - Grevia';
$PAGE_HEADER = 'Monitor Server Grevia';
$BREADCRUMB = $this->common_model->breadcrumb(NULL, ORDER);

$gInfo = '';

$host = 'som.grevia.com';

// function check_status($url, $port = '80', $timeout = 30)
// {	
	// if (! isset($url)) return 'URL required';

	// $url = str_replace(array('http://','https'),'',$url);

	// if ($socket =@ fsockopen($url, $port, $errno, $errstr, $timeout)) {
		// return '<span class="text-success b"><i class="fa fa-check" aria-hidden="true"></i>online!</span>';
		// fclose($socket);
	// } else {
		// return '<span class="text-danger b"><i class="fa fa-times" aria-hidden="true"></i>offline</span>';
	// }
// }

function check_online($url, $port = NULL) {
	
	$agent = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; pt-pt) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27";
	// $url = str_replace(array('http://','https'),'',$url);

	// if (isset($port) && $port == '443') 
		// $url = 'https://'.$url;
	// else
		// $url = 'http://'.$url;

	$curlInit = curl_init($url);
	// curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,5);
	
	// sets the content of the User-Agent header
    curl_setopt($curlInit,CURLOPT_USERAGENT, $agent);

	curl_setopt($curlInit,CURLOPT_HEADER,true);
	curl_setopt($curlInit,CURLOPT_VERBOSE,false);
	curl_setopt($curlInit,CURLOPT_NOBODY,true);
	curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curlInit,CURLOPT_SSL_VERIFYPEER,false);
	// curl_setopt($curlInit,CURLOPT_SSL_VERIFYHOST,1);
	
	// max number of seconds to allow cURL function to execute
    curl_setopt($curlInit,CURLOPT_TIMEOUT, 15);
 
	//get answer
	$response = curl_exec($curlInit);
	
	// get HTTP response code
    $httpcode = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
 
	curl_close($curlInit);

	// if ($response) return true;
	// return false;

	// if ($response) 
	if($httpcode>=200 && $httpcode<300)
		return '<span class="text-success b"><i class="fa fa-check" aria-hidden="true"></i>online!</span>';
	
	return '<span class="text-danger b"><i class="fa fa-times" aria-hidden="true"></i>offline</span>';
	
}

// AJAX REQUEST
// $get = $_POST;
$get = $_GET;
if (isset($get['ajax']) && isset($get['url'])) 
{
	echo check_online($get['url']);
	die;
}

$list_url_client[] = 'http://www.dapuramoy.com';
$list_url_client[] = 'http://www.hrprimesolution.com';
$list_url_client[] = 'http://www.uzinretailindonesia.com';
$list_url_client[] = 'http://www.metropackindonesia.com';
$list_url_client[] = 'http://www.solusilantnaiindonesia.com';


$list_url[] = 'http://www.dapuramoy.com';
$list_url[] = 'http://grevia.win';
$list_url[] = 'http://scm.grevia.com';
// $list_url[] = 'http://duitlo.grevia.com';
// $list_url[] = 'http://jobtalento.grevia.com';
// $list_url[] = 'http://hrp.grevia.com';
$list_url[] = 'http://www.grevia.com';



?>

<style>

</style>
<div class="row">
	<div class="col-sm-12">
		
		<!-- CLIENT -->
		CLIENT
		<table class="table table-bordered table-striped">
		<thead>
		<tr class="bgBlu talCnt text-uppercase b clrWht">
			<td>#</td>			
			<td class="talLft">Domain</td>
			<td>Port 80</td>
		</tr>
		<thead>
		<tbody>
		<?php foreach ($list_url_client as $key => $url) { ?>
		<tr class="talCnt">
			<td><?php echo $key + 1; ?></td>			
			<td class="talLft"><a href="<?php echo $url?>"><?php echo $url?></a></td>
			<td><div id="lbl_client_<?php echo $key + 1 ?>"></div></td>
		</tr>
		<?php } ?>
		</tbody>
		</table>
		
		
		<!-- INTERNAL -->
		INTERNAL SERVICE
		<table class="table table-bordered table-striped">
		<thead>
		<tr class="bgBlu talCnt text-uppercase b clrWht">
			<td>#</td>			
			<td class="talLft">Domain</td>
			<td>Port 80</td>
		</tr>
		<thead>
		<tbody>
		<?php foreach ($list_url as $key => $url) { ?>
		<tr class="talCnt">
			<td><?php echo $key + 1; ?></td>			
			<td class="talLft"><a href="<?php echo $url?>"><?php echo $url?></a></td>
			<td><div id="lbl_internal_<?php echo $key + 1 ?>"></div></td>
		</tr>
		<?php } ?>
		</tbody>
		</table>
	</div>
</div>

<script>
$(document).ready(function(){
	$('img').addClass('img-responsive');
	
	<?php foreach ($list_url_client as $key => $url) { ?>
		checkHttp("lbl_client_<?php echo $key + 1 ?>","<?php echo $url?>");
	<?php } ?>
	
	<?php foreach ($list_url as $key => $url) { ?>
		checkHttp("lbl_internal_<?php echo $key + 1 ?>","<?php echo $url?>");
	<?php } ?>
})

function checkHttp(divId, url){
	if (divId == '') return false;
	if (url == '') return false;
		
	$('#' + divId).html('<i class="fa fa-spinner fa-spin"></i>');
	$.ajax({
	  type: 'GET',
	  url: '<?php echo current_full_url() ?>',
	  data: {ajax:1, divId: divId, url: url}, 
	  
	  async: true,
	  success: function(resp){
	   $('#' + divId).html(resp);
	  },
	  error: function(){
		resp = 'Error. <a href="javascript:history.go(0)">Reload here</a>';
		$('#' + divId).html(resp);
	  }
	});
}
</script>