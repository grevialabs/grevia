<?php 

$arr_month = array(1=>'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember');

$year = date('Y');
// $year = 2017;
$str = NULL;
for($mth=1;$mth<=12; $mth++) {
	$str.= '<div class="col-md-12 bdrGry">Bulan '.$arr_month[$mth].' '.$year;
	
	$str.= '<table class="table table-bordered">';
	
	// $str.= '<tr>';
	// $start = 1;
	$dayarray = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
	$str.= '<tr>';
	// $str.= '<td>Week</td>';
	foreach ($dayarray as $key => $day) {
		$color = NULL;
		if (($key+1) == 6) $color = 'bg-warning';
		else if (($key+1) == 7) $color = 'bg-danger';
		$str.= '<td class="'.$color.'">'.$day.'</td>';
	}
	$str.= '</tr>';
	
	$startcol = 1; //row per calendar
	$availrow = 5; //row per calendar
	
	$maxcol = 7; //cols per calendar
	$end = date("t");
	$totalweek = ceil($end / $maxcol)+1;
	
	// loop per total week in month
	// for($start=1;$start<=$totalweek;$start++) {
		
	// ********************************************
	// check if firstdate 1/2/3/4/5/6/7 => 1 SENIN, 7 SUNDAY
	$firstdate = strtotime('1-'.$mth.'-'.$year);
	$firstdate = date('N',$firstdate);
	
	// custom position calendar, coz start not monday
	$tmpweek = NULL;
	for ($d30=1;$d30<=$end;$d30++) {
		
		$is_start = FALSE;
		
		$daykey = NULL;
		$daykey = strtotime($d30.'-'.$mth.'-'.$year);
		$daykey = date('N',$daykey);
		
		// firstday not monday & first loop
		if ($daykey > 1 && $d30 == 1) {
			for ($dk=1;$dk<$daykey;$dk++){
				$tmpweek[$dk][] = '';
			}				
			$d30 = 1;
			$is_start = TRUE;
		} else {
			$is_start = TRUE;
		}
		
		if ($is_start) {
			$tmpweek[$daykey][] = $d30;
		}
	}
	// ********************************************
	
	for($start=1;$start<=count($tmpweek);$start++) {
		// $week = $mth * $start;
		$str.= '<tr>';
		// $str.= '<td>'.$week.'</td>';
		
	
		// ------------------------
		for($sevenday=1;$sevenday<=7;$sevenday++) {
			// debug($tmpweek[$sevenday]);
			$color = NULL;
			if ($sevenday == 6) $color = 'bg-warning';
			else if ($sevenday == 7) $color = 'bg-danger';
			
			$str.= '<td class="'.$color.'">';

			if (isset($tmpweek[$sevenday][($start-1)])) {
				$str.= $tmpweek[$sevenday][($start-1)];
				// .' '.$dayarray[$daykey-1];
			} else {
				$str.= '&nbsp;';
			}
			
			$str.= '</td>';
		}
		// ------------------------

		$str.= '</tr>';
	}
	// $str.= '</tr>';
	
	// $str.= '<tr>';
	// $str.= '<>';
	// $str.= '<>';
	// $str.= '<>';
	// $str.= '<>';
	// $str.= '</tr>';
	$str.= '</table>';
	
	$str.= '</div>'.'<br/>';
}

?>
<div>
<?php 

echo $str;
?>	
</div>