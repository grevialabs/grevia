<?php
function check($amount = 0)
{
	if ( ! is_numeric($amount) || $amount <= 0) return 0;
	
	// Set variabel limit 50 jt 
	// Jika ingin dirubah tinggal diganti variabel sesuai kloter / tahapnya
	$rate1 = 0.05;
	$rate_amount1 = 50000000;
	
	$rate2 = 0.15;
	$rate_amount2 = 200000000;
		
	$rate3 = 0.25;
	$rate_amount3 = 250000000;
	
	$rate4 = 0.3;
	
	$tax = 0;
	for ($i=1;$i<=4;$i++) 
	{
		if ($i == 1)
		{
			// Limit dibawah 50 jt
			if ($amount < $rate_amount1) 
			{
				$tax += $rate1 * $amount;
				return $tax;
				break;
			}
			// Sisa uang dikurangi limit 50jt untuk penghitungan berikutnya
			$amount -= $rate_amount1;
			$tax += $rate1 * $rate_amount1;
		}
		else if ($i == 2) 
		{
			if ($amount < $rate_amount2) 
			{
				$tax += $rate2 * $amount;
				return $tax;
				break;
			}
			$amount -= $rate_amount2;
			$tax += $rate2 * $rate_amount2;
		}
		else if ($i == 3) 
		{
			if ($amount < $rate_amount3) 
			{
				$tax += $rate3 * $amount;
				return $tax;
				break;
			}
			$amount -= $rate_amount3;
			$tax += $rate3 * $rate_amount3;
		}
		else if ($i == 4) 
		{
			$tax += $rate4 * $amount;
		}		
	}
	return $tax;
}

$income = 75000000;
// $income = 750000000;
// $income = 1000000000;
// $income = 1467600000;
// $income = 201600000;
//aaa
//echo 'Pajak dari '.number_format($income).' adalah '.number_format(check($income));
//die;
?>

<?php

$this->load->helper('captcha');
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = CONTACT . ' - Grevia';
$PAGE_HEADER = 'Hubungi kami';
$BREADCRUMB = $this->common_model->breadcrumb(NULL, CONTACT);

$gInfo = '';
if (isset($_POST['hdnCapcayHash']))
{
	$capcay = decrypt($_POST['hdnCapcayHash']);
	$input_captcha = strtoupper($_POST['input_captcha']);
	if ($capcay == $input_captcha) 
	{
		$name = post('tName');
		$email = filter($this->input->post('tEmail'));
		$subject = filter($this->input->post('tSubject'));
		$message = htmlentities($this->input->post('tMessage'));
		if(!is_filled($name)){
			$gInfo .= "Name must be filled.";
		} elseif (!is_filled($email)){
			$gInfo .= "Email must be filled.";
		} elseif (!is_filled($subject)){
			$gInfo .= "Subject must be filled.";
		} elseif (!is_filled($message)){
			$gInfo .= "Message must be filled.";
		} else {
			// echo "oke berhasil bro";die;
			$save = array(
				'fullname' => $name,
				'email' => $email,
				'subject' => $subject,
				'message' => $message,
			);
			$save = $this->inbox_model->save($save);
			
			$message = '
			<html>
			<body>
			<table style="" cellpadding="15px" cellspacing="0" border="1">
			<tr>
				<td width="80px">Name</td>
				<td>'.$name.'</td>
			</tr>
			<tr>
				<td width="80px">Email</td>
				<td>'.$email.'</td>
			</tr>
			<tr>
				<td width="80px">Subject</td>
				<td>'.$subject.'</td>
			</tr>
			<tr>
				<td width="80px">Name</td>
				<td>'.$message.'</td>
			</tr>
			</table>
			</body>
			</html>
			';
			
			$to = 'rusdi.karsandi@gmail.com';
			$headers = "From: Admin Grevia<noreply@grevia.com> \r\n";
			$headers .= "Reply-To: \r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			if (!is_internal()) {
				$send = mail($to,$subject,$message,$headers);
			} else {
				$send = TRUE;
			}
			
			if ($send) 
				$gInfo = "Thank you for your email, we will respond in 2 x 24 hour.";
			else
				$gInfo = "Sorry, error connection. Please try again.";
		}
	} 
	else 
	{
		$gInfo = "Ooops, your captcha input was wrong.";
	}
}

$captcha = strtoupper(genRandomString(5));

$vals = array(
	'word'	=> $captcha,
	'img_path'	=> './asset/images/captcha/',
	'img_url'	=> base_url().'asset/images/captcha/',
	'font_path'	=> './asset/fonts/proximanova-regular-webfont.ttf',
	'img_width'	=> '150',
	'img_height' => 30,
	'expiration' => 7200
);

$cap = create_captcha($vals);

?>

<script>
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
  

function initMap() {
 
   var contentString = '<div><b>Grevia Networks</b><br/> Jl Kepu Dalam VI no 134 B<br/>kemayoran, Jakarta Pusat.</div>';

  var myLatLng = {lat: -6.1667824, lng: 106.8449875};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
  
  //-------------------------------------------
  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  // var marker = new google.maps.Marker({
    // position: uluru,
    // map: map,
    // title: 'Uluru (Ayers Rock)'
  // });
  // marker.addListener('click', function() {
    infowindow.open(map, marker);
  // });
}

</script>
<style>
 #map {
	height: 400px;
	width : 100%;
	padding: 15px ;
 }

</style>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6">
		Silakan mengisi form dibawah ini untuk menghubungi tim kami. <i class="fa fa-question-circle pointer" data-toggle="popover" data-trigger="hover" title="Panduan Mengisi" data-content="Silakan mengisi seluruh field dengan lengkap"></i>
		<br/><br/><?php if (is_filled($gInfo)) echo message($gInfo)."";?>
		<form method="post">
		<input type="text" name="tName" class="input wdtFul" placeholder="Name" required /><br/><br/>
		<input type="text" name="tEmail" class="input wdtFul" class="input round bdrGry" placeholder="Email" required /><br/><br/>
		<input type="text" name="tSubject" class="input wdtFul" placeholder="Subject" required /><br/><br/>
		<textarea placeholder="Message here..." name="tMessage" rows="8" class="input wdtFul" required></textarea><br/><br/>
		<input type="hidden" name="hdnSubmit" value="1"/>
		<input type="hidden" name="hdnCapcayHash" value="<?php echo encrypt($captcha)?>"/>

		<input type="text" class="input br text-uppercase" placeholder="CAPTCHA"  maxlength="5" name="f_input_captcha" required />
		<?php echo $cap['image'].BR; ?>
		<br/>
		<input type="submit" class="btn btn-success" value="Submit"/>
		</form><br/>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div id="map"></div><br/><br/>
	</div>
</div>

<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNBifdMEWimVBn9YmqoPzluMrccOuWLQA&signed_in=true&libraries=places&callback=initMap">
</script>

<script>
$(document).ready(function(){
	$('img').addClass('img-responsive');
})
</script>