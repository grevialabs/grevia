<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Keranjang Belanja';

// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.DEFAULT_PAGE_TITLE;
$PAGE_HEADER = $PAGE;

$cookie_order_id = $order_id = $get_product_size_id = $get_quantity = $do = NULL;

if (isset($_GET['do'])) $do = $_GET['do'];
if (isset($_GET['product_id']) AND is_numeric($_GET['product_id'])) $get_product_id = $_GET['product_id'];
if (isset($_GET['quantity']) AND is_numeric($_GET['quantity'])) $get_quantity = $_GET['quantity'];

/* ----------------------------------------------------------------------------------------------------- */
// CHECK IF COOKIE ORDER ID IS SECURE AND VALID
if (isset($_COOKIE['order_id']) && is_numeric($_COOKIE['order_id']))
{
	// COOKIE EXIST BRO
	$cookie_order_id = $_COOKIE['order_id'];
	// echo "cookie exist ".$cookie_order_id;
	// debug($_COOKIE['order_id']);
}
else
{
	// CREATE NEW COOKIE & NEW ORDER ID FOR CUSTOMER
	$cookie_name = "order_id";
	$cookie_order_id = $order_id = $this->order_model->get_new_order_code();
	setcookie($cookie_name, $cookie_order_id, time() + (86400 * 30), '/'); // 86400 = 1 day
}
/* ----------------------------------------------------------------------------------------------------- */

// SAVE ORDER HERE
if ($do == 'add' && isset($_GET['product_id']))
{
	// debug($_GET);
	// debug($cookie_order_id);
	// die;

	$obj_order = $this->order_model->get( array('order_id' => $cookie_order_id) );
	if (empty($obj_order))
	{
		// CHECK IF DATA ORDER NOT EXIST THEN INSERT NEW
		$param_order = NULL;
		$param_order['order_id'] = $cookie_order_id;
		$param_order['payment_code'] = rand(100,999);
		$save_order = $this->order_model->save($param_order);
	}
	// else
	// {
		// ELSE UPDATE
	// }
	// if ($SETTING['is_allow_stock_minus'] && $obj_product['stock'] > 0)

	$param_order = $obj_product_size = NULL;

	$obj_product = $this->product_model->get(array('product_id' => $get_product_id));

	$param_order['order_id'] = $cookie_order_id;
	$param_order['product_id'] = $_GET['product_id'];
	$param_order['price'] = $obj_product['sell_price'];
	// $param_order['quantity'] = 1;
	$save_order_detail = $this->order_model->save($param_order);

	// REDIRECT TO CART
	redirect('cart');
}

/* ----------------------------------------------------------------------------------------------------- */
// INSERT TO TABLE ORDER
if (isset($get_product_id))
{
	// GET PRODUCT DETAIL
	$obj_product = $this->product_model->get(array('product_id' => $get_product_id));

	if (isset($obj_product['product_id']))
	{
		// INSERT TO TABLE ORDER
		$param_order = NULL;
		$param_order['order_id'] = $cookie_order_id;
		$param_order['product_id'] = $get_product_id;
		$param_order['price'] = $obj_product['sell_price'];
		// $param_order['quantity'] = 1;
		
		// $param_order['member_id'] = $get_quantity;
		// $param_order['creator_id'] = $get_quantity;

		$save_order = $this->order_model->save($param_order);

		// REDIRECT TO LAST PRODUCT ID
		redirect(base_url().'product/'.$obj_product['product_id'].'/'.$obj_product['slug']);
	}
	else
	{
		// DATA PRODUCT NOT FOUND THROW ERROR
	}
}
/* ----------------------------------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------------------------------- */
// DELETE DETAIL ITEM
if ($do == "delete" && isset($get_product_id))
{
	$param_cart_delete = NULL;
	$param_cart_delete['order_id'] = $_COOKIE['order_id'];
	$param_cart_delete['product_id'] = $get_product_id;
	$obj_cart_delete = $this->order_model->get_order_detail($param_cart_delete);
	// debug($obj_cart_delete);die;
	if (!empty($obj_cart_delete))
	{
		$delete_detail = $this->order_model->delete_detail($obj_cart_delete['order_detail_id']);
		if ($delete_detail)
			$this->session->set_flashdata('message', MESSAGE::DELETE);
		else
			$this->session->set_flashdata('message', MESSAGE::ERROR);
		redirect(current_url());
	}
}
/* ----------------------------------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------------------------------- */
// POST BACK AND REDIRECT NEXT STEP SHOPPING TO CHECKOUT
if ($_POST)
{
	$post = $_POST;

	// debug($post);
	// die;
	// SUBMIT POST
	// update_cart
	// if (isset($post['btn_update_cart']))
	if (isset($post['product_id']) && isset($post['qty']))
	{
		// unset($post['update_cart']);
		// debug($post);die;
		$list_order_notes = $list_product_id = $list_qty = NULL;

		if (isset($post['order_notes'])) $list_order_notes = $post['order_notes'];
		if (isset($post['product_id'])) $list_product_id = $post['product_id'];
		if (isset($post['qty'])) $list_qty = $post['qty'];

		for ($i=0;$i<count($list_order_notes);$i++)
		{
			if (isset($list_product_id[$i]) && $list_qty[$i] > 0)
			{
				$param_update = NULL;
				$param_update['order_id'] = cookie_order_id();
				$param_update['product_id'] = $list_product_id[$i];
				if (isset($list_order_notes[$i])) $param_update['notes'] = $list_order_notes[$i];
				$param_update['quantity'] = $list_qty[$i];
				$update_order_detail = $this->order_model->save($param_update);
				// debug($param_update);
				// die;
			}
		}
		// die;
		if ($update_order_detail)
			$this->session->set_flashdata('message', MESSAGE::UPDATE);
		else
			$this->session->set_flashdata('message', MESSAGE::ERROR);

		redirect(current_url());
		die;

	}
	else if (isset($post['btn_login']))
	{
		// debug($_POST);die;
		// $expedition_id = $_POST['expedition_id'];
		// $payment_type_id = $_POST['payment_type_id'];
		// $param_update = NULL;

		// if (isset($_POST['shipping_name'])) $param_update['shipping_name'] = $_POST['shipping_name'];
		// if (isset($_POST['shipping_email'])) $param_update['shipping_email'] = $_POST['shipping_email'];
		// if (isset($_POST['shipping_phone'])) $param_update['shipping_phone'] = $_POST['shipping_phone'];
		// if (isset($_POST['shipping_city'])) $param_update['shipping_city'] = $_POST['shipping_city'];
		// if (isset($_POST['shipping_address'])) $param_update['shipping_address'] = $_POST['shipping_address'];
		// if (isset($_POST['shipping_notes'])) $param_update['shipping_notes'] = $_POST['shipping_notes'];

		// IF COOKIE ORDER EXIST
		if (cookie_order_id())
		{

			// $param_update['expedition_id'] = $expedition_id;
			// $param_update['payment_type_id'] = $payment_type_id;
			// $update_order = $this->order_model->update(cookie_order_id(), $param_update);

			// if ($update_order) redirect(base_url().'shoppingcart/checkout');
			// if ($update_order) redirect(base_url().'shoppingcart/shipping');

			$member = $this->member_model->get(array(
				'email' 	=> $post['email'],
				'password' 	=> encrypt($post['password'])
			));

			if (isset($member['member_id']))
			{
				$encrypt = encrypt($member['member_id']."#".$member['first_name']." ".$member['last_name']."#".$member['email']."#".$member['is_admin']);

				// DEFAULT one month login
				$expiredCookie = time() + 30 * 86400;

				// 24 hour expired cookie on default
				//$expiredCookie = time() + 24 * 3600;
				//if (post('l_RememberMe')) $expiredCookie = time() + 30 * 86400;

				setcookie('hash', $encrypt, $expiredCookie, base_url());
				redirect(base_url().'shoppingcart/shipping');
			}
			else
			{
				$gInfo = "Data Login member tidak ditemukan.";
			}
		}
	}
}
// die;
/* ----------------------------------------------------------------------------------------------------- */

// debug($order_id);
/* --------------------------------- CREATE COOKIE IF ADD PRODUCT */

// $cookie = array(
    // 'name'   => 'order_id',
    // 'value'  => $order_id,
    // 'expire' => '86500',
    // 'domain' => base_url(),
    // 'path'   => '/',
    // 'prefix' => 'myprefix_',
    // 'secure' => TRUE
// );

// $this->input->set_cookie($cookie);
// die;
/* --------------------------------- */

$param_order = NULL;
$obj_order = $obj_list_order_detail = NULL;

if (cookie_order_id())
{
	$param_order['order_id'] = $_COOKIE['order_id'];
	$obj_order = $this->order_model->get_detail($param_order);
}
?>
<div class="talLft visible-xs">
	<div style="background-color:#cecece;float:left;width:100%" class="padMed">1. <i class="fa fa-shopping-cart"></i> Pilih item</div>
	<div style="background-color:#e6eef4;float:left;width:100%" class="padMed">2. <i class="fa fa-circle"></i> Alamat pengiriman</div>
	<div style="background-color:#e6eef4;float:left;width:100%" class="padMed">3. <i class="fa fa-flag "></i> Selesaikan belanja</div>
	<br/>
</div>
<div class="clearfix"></div>
<div class="col-sm-12">

	<div class="talCnt hidden-xs">
		<strike style="width:70px" class="clrGry"><i class="fa fa-shopping-cart clrBlk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</strike>
		<strike style="width:70px" class="clrGry"><i class="fa fa-circle clrGry"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</strike>
		<strike style="width:70px" class="clrGry"><i class="fa fa-circle clrGry"></i>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i></strike>
		<br/>
		<span class="b" style="padding-right:65px">
			<i class="fa fa-arrow-circle-right"></i> 1. Shoppingcart
		</span>

		<span class="clrGry" style="padding-right:85px">
			<i class="fa fa-arrow-circle-right"></i> 2. Shipping
		</span>

		<span class="clrGry" style="padding-right:65px">
			<i class="fa fa-arrow-circle-right"></i> 3. Checkout
		</span>

		<span class="clrGry" style="">
			<i class="fa fa-arrow-circle-right"></i> 4. Thankyou
		</span>
	</div><br/>
</div>

<div class="col-sm-12" style="padding:0">

	<?php
	// PRINT MESSAGE
	if ($this->session->flashdata('message')) echo message(getMessage($this->session->flashdata('message')));
	// echo message();
	// echo "<hr/>".getMessage(MESSAGE::UPDATE)."<hr/>";
	// debug($obj_order);
	?>
	<?php
	if (!empty($obj_order))
	{
		?>
		<form method="post">
			<div class="">
			<div class="">
				<table class="table table-bordered">
					<tr class="alert bg-info talCnt">
						<th width="2%" class="talCnt hidden-xs">#</th>
						<th width="">Produk</th>
						<th class="talCnt hidden-xs" width="15%">Harga</th>
						<th class="talCnt hidden-xs" width="15%">Subtotal</th>
						<th class="talCnt hidden-xs" width="18%">Option</th>
					</tr>
				<?php
				$grandtotal = $subtotal = $tax = 0;
				if (isset($obj_order))
				{
					// list_shoppingcart
					// debug($obj_order);
					$subtotal = 0;
					$subtotal = $obj_order['final_price']*$obj_order['quantity'];
					$grandtotal += $subtotal;

					?>
					<tr>
						<td class="hidden-xs talCnt">1</td>
						<td>
							<div class="">
								<a class="br" href="<?php echo base_url().'product/'.$obj_order['product_id'] ?>">
								<?php echo $obj_order['product_name'].''; ?></a>
								<textarea name="f_order_notes[]" class="input wdtFul f_order_notes" rows="5" placeholder="Catatan pembelian." ><?php if (isset($obj_order['notes'])) echo $obj_order['notes']; ?></textarea>
								Quantity :
								<input type="text" id="" class="f_qty numeric input" name="f_qty[]" value="<?php echo $obj_order['quantity']?>" maxlength="2" size="3" />
								
								<div class="visible-xs">
									X <?php echo format_money($obj_order['final_price'])?>
									<br/>
									<div class="b" style="padding:15px 0"><?php echo format_money($subtotal)?><?php if ($obj_order['promo_price'] > 0) echo BR.'<div class="b clrRed"><s>'.$obj_order['price'].'</s></div>'; ?></div>
								</div>
								<div class="visible-xs">
									<div class=""><a href="?do=delete&product_id=<?php echo $obj_order['product_id']?>" class="btn btn-sm btn-danger btn-block"><i class="fa fa-trash fa-2x"></i></a></div>
								</div>

								<input type="hidden" name="f_product_id[]" value="<?php echo $obj_order['product_id']?>" />
							</div>
						</td>
						<!--
						<td class="hidden-xs talCnt">

						<input type="text" class="f_qty input" name="f_qty[]" value="<?php echo $obj_order['quantity']?>" maxlength="2" size="3" class="numeric" /></td>
						-->
						<td class="hidden-xs talRgt"><?php echo $obj_order['quantity'].' x '.format_money($obj_order['final_price'])?>
						<?php if ($obj_order['promo_price'] > 0) echo BR.'<div class="b clrRed"><s>'.format_money($obj_order['price']).'</s></div>'; ?></td>
						<td class="hidden-xs talRgt"><?php echo format_money($subtotal)?></td>
						<td class="talCnt hidden-xs"><a href="?do=delete&product_id=<?php echo $obj_order['product_id']?>" class="btn btn-md btn-danger btn-block"><i class="fa fa-trash"></i></a></td>
					</tr>

					<?php
				}
				?>
				<tr>
					<td colspan="100%">
					Promo Code <input type="text" class="input br-md min-width-180" id="f_promo_code" name="f_promo_code" value="" placeholder="promo code" />
					</td>
				</tr>
				<tr class="visible-xs">
					<td>
					Subgrandtotal<br/><div class="b"><?php echo format_money($grandtotal)?></div></td>
				</tr>
				<tr class="hidden-xs">
					<td colspan="4">Subgrandtotal</td>
					<td colspan="2"><?php echo format_money($grandtotal)?></td>
				</tr>
				</table>
				<input type="submit" id="btn_update_cart" name="update_cart" class="btn btn-success wdtFul hide" value="UPDATE" />
				<br/>
			</div>
			</div>
		</form>

		<?php if (!is_member()) { ?>
		<!-- START CUSTOMER DATA -->
		<div class="">
			<div class="col-sm-12 bdrGry">
				<div class="col-sm-12">
					<br/>
				</div>
				<div class="col-sm-6" style="border-right:1px solid #cecece">
					<h4 class="text-uppercase b">CHECKOUT AS MEMBER</h4>
					<form method="post">
						<input type="email" class="input wdtFul br-md" placeholder="Email" name="f_email" required />
						<input type="password" class="input wdtFul br-md" placeholder="password" name="f_password" required />
						<button class="btn btn-info br-md" name="btn_login" value="1">LOGIN</button>
					</form>
				</div>
				<div class="col-sm-6">
					<h4 class="text-uppercase b">CHECKOUT INSTAN SEBAGAI TAMU</h4>
					<div class="talCnt">
						Anda tidak perlu menjadi anggota untuk berbelanja.<br/> Silakan klik tombol <b>"Lanjut"</b> dibawah ini untuk menyelesaikan pesanan sebagai tamu secara instan.<br/><br/><br/>
						<a class="btn btn-success text-uppercase" href="<?php echo base_url()?>checkout">Lanjutkan <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
						<!--<button class="btn btn-success">Lanjutkan</button>-->
					</div>
				</div>

				<div class="col-sm-12">
					<br/>
				</div>
			</div>
		</div>
		<!-- END CUSTOMER DATA -->
		<?php } else { ?>
		<div class="row">
			<div class="col-sm-12 talCnt">
				<a class="btn btn-md btn-default br-md btn-block-xs min-width-180 btn-navigation" href="<?php echo base_url().'product';?>"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> KEMBALI </a>
				<a class="btn btn-info br-md min-width-180 btn-navigation" href="<?php echo base_url()?>checkout">LANJUTKAN <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
			</div>
		</div>
		<?php } ?>

		<div class="row">
			<div class="col-sm-12">
				&nbsp;
			</div>
		</div>

		<?php
	}
	else
	{
		?>
		Oops...Tidak ada barang di keranjang belanja anda. :(
		<?php
	}
	?>
</div>
<script>
$(document).ready(function(){
	$('.f_qty').change(function(){
		var val= this.value;
		// alert(val);
		if (val == 0)
		{
			$(this).val(1);
		}
		else
		{
			$('#btn_update_cart').trigger('click');
		}
	});

	// $('#f_promo_code').change(function(){
		// $('#btn_update_cart').click();
	// });

	$('.f_order_notes').change(function(){
		$('#btn_update_cart').click();
	});
})
</script>
