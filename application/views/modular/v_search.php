<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = SEARCH .' '. ARTICLE ;
$PAGE_HEADER = SEARCH .' '. ARTICLE ;
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE_HEADER);

/* start paging */
$page = 1;
if (isset($_GET['page'])) $page = $_GET['page'];
if (OFFSET) $offset = OFFSET;
//$offset = 30;
$param['offset'] = $offset;

/* end paging */

$q = NULL;
if (isset($_GET['q']))
{
	$q = $_GET['q'];
	$PAGE_TITLE.= ' '.$q;
	$PAGE_HEADER.= ' '.$q;
}
$str = "";

if (isset($q))
{
	$param = array('keyword' => $q);
	$data = $this->article_model->get_list($param);
	$obj_list_article = $data['data'];
	if (!empty($obj_list_article))
	{
		/* AUTO REDIRECT TO ARTICLE IF ONLY 1 RESULT */
		// if( count($obj_list_article) == 1 ){
			// foreach($obj_list_article as $obj){
				// redirect( friendly_url("/showarticle.php?aid=".$obj['article_id()."&title=".$obj['title()) );
			// }
		// }
		$i = 1;

		$str.= '<div class="bg-info padLrg"><i class="fa fa-info"></i>&nbsp; Ditemukan '.$data['total_rows'].' '.$q.' artikel terkait keyword <span class="b">'. $q .' </span> </div>';
		foreach ($obj_list_article as $key => $obj)
		{
			$key+= 1;
			if ($obj['is_publish'])
			{
				/* COLORING BORDER ON TOP */
				$bgcolor = "";
				if($i > 1)$bgcolor = " bdrTopGry";
				$friendly_url = base_url()."article/".$obj['article_id']."/".$obj['slug'];
				
				if (is_internal()) {
					//OFFLINE
					$image_url = base_url()."asset/plugin/autothumbnail/index.php?src=grevia.com/asset/images/article/".$obj['image']."&width=340";
				} else {
					// ONLINE
					$image_url = base_url()."asset/plugin/autothumbnail/index.php?src=asset/images/article/".$obj['image']."&width=340";
					//$image_url = base_url()."asset/images/article/".$obj['image']."&width=340";
					//http://www.grevia.com/asset/plugin/autothumbnail/index.php?src=asset/images/article/2013.jpg&width=500
				}
				//$image_url = base_url()."asset/images/article/".$obj['Image'];
				$str.= "
				<div class='".$bgcolor."'><br/>
					<div class='col-md-5' source='search' segment='article'>
					<a tracker_name='image-". $key ."' class='adsTrack' href='".$friendly_url."'>
					<!--<img data-original='".$image_url."' class='lazy img-responsive wdtFul' style='max-height:180px'/>-->
					<img data-original='".$image_url."' src='".$image_url."' class='lazy img-responsive wdtFul' style='max-height:180px'/>
					</a><br/>
					</div>
					<div class='col-md-7' source='search' segment='article'>
					<a tracker_name='article-". $key ."' href='".$friendly_url."' class='adsTrack b clrBlk f18'>".$obj['title']."</a><br/><br/>
					<span class='fntBld fntMed'>".$obj['short_description']."</span>
					</div></div>
					<div class='clearfix'>
				</div><hr>
				";
				$i++;
			}
		}
	} else {
		$str.= "Artikel dengan kata kunci <span class='b'>".$q."</span> tidak ditemukan.<br/>";
		
		/*---------------------------------------------------------------------*/
		// GET DISTINCT TOP KEYWORD SEARCH
		$list_search = $this->search_model->get_list_report( array('limit' => '0', 'offset' => '150', 'hit' => 3) );
		$list_search = $list_search['data'];

		// array of words to check against
		$list_keywords = NULL;
		foreach($list_search as $val) 
		{
			$list_keywords[] = $val['keyword'];
		}

		// no shortest distance found, yet
		$shortest = -1;

		// loop through words to find the closest
		foreach ($list_keywords as $word) {

			// calculate the distance between the input word,
			// and the current word
			$lev = levenshtein($q, $word);

			// check for an exact match
			if ($lev == 0) {

				// closest word is this one (exact match)
				$closest = $word;
				$shortest = 0;

				// break out of the loop; we've found an exact match
				break;
			}

			// if this distance is less than the next found shortest
			// distance, OR if a next shortest word has not yet been found
			if ($lev <= $shortest || $shortest < 0) {
				// set the closest match, and shortest distance
				$closest  = $word;
				$shortest = $lev;
			}
		}

		if ($shortest == 0) {
			//$str.= "Exact match found: $closest\n";
		} else {
			$str.= "<div class='padLrg bg-warning'><i class='fa fa-search'></i>&nbsp; Did you mean: <span class='i'><a href='search?q=".$closest."'>".$closest."</a></i>?</div><br/>";
		}

		/*---------------------------------------------------------------------*/
	}
} else {
	$str.= "Silakan masukan kata pencarian.";
}
?>
<div class="col-sm-9">
<?php
echo $str;
?>
</div>
<div class="col-sm-3">
	<?php echo $SIDEBAR_RIGHT; ?>
</div>