<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = CAREER . ' - Grevia';
$PAGE_HEADER = NULL;
$BREADCRUMB = $this->common_model->breadcrumb(NULL, CAREER);
?>
<!--
<div class="col-xs-6">
</div>
-->
<!--	
<div class="col-sm-12">
<h1 style="padding:0;margin:0">LATAR BELAKANG</h1><hr/>
Grevia adalah singkatan dari <span class="b i">Group Evangelist Internet Indonesia</span>, sebuah situs yang menyediakan artikel seputar entrepreneurship, startup dan dunia digital. Seluruh artikel kami berdasarkan dari pengalaman pribadi dan pengalaman para pengusaha digital dari Asia dan Sillicon Valley.<br/><br/>

Grevia adalah wadah untuk membantu para individu / teknopreneur yang ingin memulai ide bisnis mereka, dengan memberikan informasi seputar startup, cerita sukses dari Founder-Founder dan analisa mendalam membangun bisnis.<br/><br/>

<span class="b">Misi</span> : Membantu para individu yang ingin memulai bisnis di dunia digital di Indonesia<br/><br/>
<span class="b">Visi</span> : Menciptakan lebih banyak para pekerja kreatif & teknopreneur di dunia digital Indonesia<br/><br/>
-->
<div class="row">
	
	<!--
	Bagi kalian yang memiliki passion di dunia startup, Grevia menawarkan peluang berkarir :<br/><br/>

	<div class="col-sm-6">
		<div class="b alert alert-info">Content Writer</div>
		memiliki kriteria berikut :<br/>
		<ul>
			<li class="b">Part time & bekerja secara remote.</li>
			<li>Memiliki passion di dunia startup</li>
			<li>Bisa menulis artikel sesuai dengan topik tertentu</li>
			<li>Kreatif & mengerti dasar SEO.</li>
		</ul>
		Deskripsi pekerjaan : 
		<ul>
			<li>Menyiapkan artikel untuk Grevia seputar teknologi, startup, teknopreneurship,dll</li>
		</ul>
	</div>
	<div class="col-sm-6">
		<div class="b alert alert-sm alert-info">Social Media Specialist</div>
		memiliki kriteria berikut :<br/>
		<ul>
			<li class="b">Part time & Bekerja secara remote.</li>
			<li>Memiliki passion di dunia startup</li>
			<li>Kreatif & memiliki komitmen</li>
			<li>Mengerti penggunaan sosial media Facebook, Twitter & Instagram</li>
		</ul>
		Deskripsi pekerjaan : 
		<ul>
			<li>Memposting artikel, tweet ke sosial media Grevia (Facebook, Twitter & Instagram)</li>
		</ul>
	</div>
	
	<div class="col-sm-6">
		<div class="b alert alert-sm alert-info">Sales</div>
		memiliki kriteria berikut :<br/>
		<ul>
			<li class="b">Part time & Bekerja secara remote.</li>
			<li>Memiliki pengalaman sebagai sales.</li>
			<li>Energik, supel & memiliki komitmen.</li>
			<li>Memiliki jaringan / koneksi yang luas.</li>
		</ul>
		Deskripsi pekerjaan : 
		<ul>
			<li>Mencari klien yang membutuhkan jasa aplikasi berbasis web.</li>
		</ul>
	</div>
	<div class="col-sm-6">
		<div class="b alert alert-sm alert-info">Magang</div>
		memiliki kriteria berikut :<br/>
		<ul>
			<li>Mampu beradaptasi dengan lingkungan kerja.</li>
			<li>Mau belajar segala hal.</li>
		</ul>
		Deskripsi pekerjaan : 
		<ul>
			<li>Membantu keseharian operasional.</li>
		</ul>
	</div>
	
	<div class="col-sm-12">
		<hr/>
		Silakan kirim lamaran anda dengan posisi serta gaji yang diharapkan ke email di rusdi[at]grevia.com<br/>
		<b>*hanya kandidat yang memenuhi kualifikasi yang akan kami hubungi.</b><br/><br/>

	</div>
	-->

	<div class="col-sm-12">
		<h1 style="padding:0;margin:0">KARIR</h1><hr/>
		<div class="talCnt">
		Saat ini tidak ada lowongan yang tersedia.<br/><br/>
		<a href="<?php echo base_url()?>" class="btn btn-info">Kembali ke halaman depan.</a>
		</div>
	</div>
	
</div>