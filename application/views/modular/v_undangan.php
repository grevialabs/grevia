<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Undangan';
$BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - Undangan';
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron" >
	<div class="container">
		<div class="row min-height-250">
			<div class="col-xs-2">
			&nbsp;
			</div>
			<div class="col-xs-8 talCnt">
				“Kebahagiaan duniawi terbesar yang dapat dialami manusia adalah perpaduan dari pernikahan yang mengikat dua hati yang saling mencintai menjadi satu."<br/>
				(Sutta Pitaka - Digha Nikaya).
				
				<h3 class="b">Rusdi Karsandi, S.Kom</h3>
				Putra bungsu dari Tn. Lim Sau Phen & Ny. Bong Nyuk Cin<br/>
				dengan<br/>
				<h3 class="b">Hesti Suryani, S.T</h3>
				Putri bungsu dari Tn. He Thiam Khun & Ny. Tjhu Siat Tju<br/>
				<br/>
				<br/>
			</div>
			<div class="col-xs-2">
			&nbsp;
			</div>
			
			<div class="col-xs-12">
			
			</div>
			
			<div class="col-xs-2">
				&nbsp;
			</div>
			<div class="col-xs-4 talCnt">
				Pemberkatan Pernikahan:<br/><br/>

				Minggu, 18 Desember 2016<br/>
				Pukul 12.30 WIB<br/>
				Vihara Avalokitesvara<br/>
				Jl Mangga Besar Raya no. 58<br/>
				Taman Sari<br/>
				Jakarta Barat
			</div>
			<div class="col-xs-4 talCnt">
				Resepsi Pernikahan:<br/><br/>

				Minggu, 18 Desember 2016<br/>
				Pukul 19.00 – 21.00 WIB<br/>
				The Royal Jade Restaurant<br/>
				Glow Tower Lt. 5<br/>
				Komp. Ruko Seasons City Blok F No. 1<br/>
				Jl. Prof. Dr. Latumenten Raya No. 33<br/>
				Jakarta Barat
			</div>
			<div class="col-xs-2">
				&nbsp;
			</div>
			
			<div class="col-xs-12">
			
			</div>

			<div class="col-xs-2">
			&nbsp;
			</div>
			<div class="col-xs-8 talCnt">
				<br/><br/>
				Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila Bapak/Ibu/Saudara/I berkenan <br/>
				hadir untuk memberikan doa restu kepada kedua mempelai.<br/><br/>
				Kami yang berbahagia,<br/>
			</div>
			<div class="col-xs-2">
			&nbsp;
			</div>
			
			<div class="col-xs-12">
			
			</div>

			<div class="col-xs-6 talCnt">
			Kel. Lim Sau Phen.
			</div>
			<div class="col-xs-6 talCnt">
			Kel. He Thiam Khun.
			</div>
			
			<div class="col-xs-12 talCnt">
			<h4 class="b i">Rusdi & Hesti</h4>
			</div>

			
			</div>
		</div>
	</div>
</div>