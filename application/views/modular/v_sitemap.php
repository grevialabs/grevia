<?php 
$data = NULL;
$data = $this->article_model->get_list();
$data = $data['data'];

header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" ?>';

$root = "http://www.grevia.com/";
$root = base_url();

function makeUrlString ($urlString) {
    return htmlentities($urlString, ENT_QUOTES, 'UTF-8');
}
?>
<urlset xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

	<url>
		<loc><?php echo $root?></loc>
	</url>
	<url>
		<loc><?php echo $root?>about</loc>
	</url>
	<url>
		<loc><?php echo $root?>contact</loc>
	</url>
	<url>
		<loc><?php echo $root?>login</loc>
	</url>
	<?php 
	foreach ($data as $rs) {
		echo "<url>\n";
		echo "		<loc>".makeUrlString(base_url()."article/".$rs['article_id']."/".$rs['slug'])."</loc>";
		echo "\n	</url>\n";
	}
	?>
</urlset>