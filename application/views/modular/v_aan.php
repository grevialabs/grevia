<style>

.grouplanding {
    width: 1200px;
}

/* landing needed to position the button. Adjust the width as needed */
.landing {
  position: relative;
  width: 20%;
  display: inline-block;
  margin-left: 50px;
}

/* Make the image responsive */
.landing img {
  width: 100%;
  /* height: 100 px; */
  height: auto;
}

/* Style the button and place it in the middle of the landing/image */
.landing .btntop {
  position: absolute;
  top: 20%;
  left: 80%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  background-color: orange;
  color: white;
  font-size: 12px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;
}

.landing .btnbtm {
  position: absolute;
  top: 80%;
  left: 20%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  background-color: red;
  color: white;
  font-size: 12px;
  padding: 12px 24px;
  border: none;
  cursor: pointer;
  border-radius: 5px;
}

.landing .btn:hover {
  background-color: black;
}
</style>

<div class="grouplanding">
    <div class="landing">
        <img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/12/8/21692124/21692124_564917e8-9ef4-4156-94c0-872e26a81c3c_400_400.jpg" />
        <div class="btntop">mantap bgt</div>
        <div class="btnbtm">Harga bro</div>
    </div>
    <div class="landing">
        <img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/12/8/21692124/21692124_564917e8-9ef4-4156-94c0-872e26a81c3c_400_400.jpg" />
        <div class="btntop">mantap bgt</div>
        <div class="btnbtm">Harga bro</div>
    </div>
    <div class="landing">
        <img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/12/8/21692124/21692124_564917e8-9ef4-4156-94c0-872e26a81c3c_400_400.jpg" />
        <div class="btntop">mantap bgt</div>
        <div class="btnbtm">Harga bro</div>
    </div>
    <div class="landing">
        <img src="https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/12/8/21692124/21692124_564917e8-9ef4-4156-94c0-872e26a81c3c_400_400.jpg" />
        <div class="btntop">mantap bgt</div>
        <div class="btnbtm">Harga bro</div>
    </div>
</div>