<?php

if ($_FILES)
{
	// IMAGE
	// debug($_FILES);
	// die;
	if (isset($_FILES["f_image"]["name"])) 
	{
		$list_allowed_ext = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["f_image"]["name"]);
		$file_image_extension = end($temp);
		$file_image_type = $_FILES["f_image"]["type"];
		$file_image_size = $_FILES["f_image"]["size"];
		$file_image_name = $_FILES["f_image"]["name"];
		// echo $file_image_name;
		// die;
		
		$str = "";
		$is_image_valid = FALSE;
		
		if (($file_image_type == "image/gif") || ($file_image_type == "image/jpeg") || ($file_image_type == "image/jpg")	|| ($file_image_type == "image/pjpeg") || ($file_image_type == "image/x-png")	|| ($file_image_type == "image/png"))
		{
			$is_image_valid = TRUE;
		}
		else
		{
			$str.= "- Tipe bukan gambar.<br/>";
		}
		
		if ($file_image_size < 1000000)
		{
			$is_image_valid = TRUE;
		}
		else
		{
			$str.= "- Size tidak cukup.<br/>";
		}
		
		if (in_array($file_image_extension, $list_allowed_ext))
		{
			$is_image_valid = TRUE;
		}
		else
		{
			$message['message'].= "- Extension salah.<br/>";
		}
		
		if ($is_image_valid) 
		{
			$image = $file_image_name;
			if ($_FILES["f_image"]["error"] > 0) 
			{
				$message['message'].= "Image failed upload because error.";
			} 
			else 
			{
				if (is_internal())$upload_directory = "E:/wamp/www/grevia.com/asset/images/article/";
				else $upload_directory = "/mnt/volume-sgp1-01/image/";
				
				// if (is_internal())$upload_directory = "D:/xampp/htdocs/grevia.com/asset/images/article/";
				// else $upload_directory = "/home/grevia/public_html/asset/images/article/";
				
				if (file_exists($upload_directory . $file_image_name)) 
				{
					echo $file_image_name . " already exists. ";
				} 
				else 
				{
					$is_move_success = move_uploaded_file($_FILES["f_image"]["tmp_name"],$upload_directory . $file_image_name);
					if ($is_move_success) 
					{
						$image = $file_image_name;
					} 
					else 
					{
						$message['message'].= "Image failed upload.";
					}
				}
			}
		}
	}
	else
	{
		$image = NULL;
	}
}

if (isset($message['message'])) echo $message['message'].'<hr/>';
if (isset($image)) echo 'Sukses: '.$image.'<hr/>';
?>
<form method="post" enctype="multipart/form-data">
	<div class='form-group form-group-sm'>
		<label for='f_image' class='col-sm-2'>Image</label>
		<div class='col-sm-10'>
			<div class='input-group'>
			<span class="input-group-addon">
				<i class="glyphicon glyphicon-picture"></i>
			</span>
			<input type='file' class='form-control' id='f_image' name='f_image' onchange="$('#previewImage')[0].src = window.URL.createObjectURL(this.files[0]);$('#previewImage').show();" /></div>
		</div>
	</div>
	<input type="submit" value="Submit"/>
</form>