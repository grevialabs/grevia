<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = 'Pomodoro';
$PAGE_HEADER = 'Pomodoro';
$PAGE_HEADER_ID = 'Pomodoro';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, 'Webdesign');
?>

<style>
.mrgBtm0{ margin-bottom:0 }
</style>
<div class="container" style="margin-bottom:0"> 
	<div class="row">
		<div class="col-sm-12 bgWht f35">
			<!-- box here -->
			<span id="min"></span>
			<span id="">:</span>
			<span id="sec"></span>
		</div>
		
		<div class="col-sm-12 bgWht">
			<button id="btn_long_pom" type="button" value="Long start" >Long start</button>
			<button id="btn_short_pom" type="button" value="Short start">Short start</button>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	console.log('semprul');
	/* dotimer */
	$('#btn_long_pom').bind("click", function(){ dotimer('long')});
$('#btn_short_pom').bind("click", function(){ dotimer('short')});
	
	/*  param accept long / short */
	function dotimer(param) {
		
		
		var vmin = 0;
		var vsec = 0;
		var vnow;
		
		var long_pom = 25*60*1000;
		var short_pom = 10*60*1000;
		var myparam;
		
		if (param == '') { console.log('hastalavista baby'); }
		else if (param == 'long'){myparam = long_pom; }
		else if (param == 'short'){myparam = short_pom; }
		
		/* do timer until 0; */
		console.log('running myparam');
		console.log(myparam);
		countdown(myparam);
		console.log('running myparam done');
		
	}

	function countdown(param_timer) {
		
		var x = setInterval(function() {
			var tnow;
			var tmin;
			var tsec;

			console.log('countdown init');
			console.log('param_timer' + param_timer);
			tnow = param_timer - 1000;
			param_timer = tnow;
			console.log('tnow ' + tnow);
				
			
		
			/* print date  */
			tmin = Math.floor(tnow / (60*1000));
			tsec = Math.floor((tnow % (60*1000)) / 1000);
			
			$('#min').html(tmin);
			$('#sec').html(tsec);
			
			
			/* 
			clearInterval(x);
			countdown(tnow);
			trigger music to browser;
			timer stop
			*/
			
			if (tnow < 0) {
				reset_timer_zero();
				clearInterval(x);
				return false;
			}
		},1000);
		
	}

	function reset_timer_zero()
	{
		vmin = '00';
		vsec = '00';
		
		$('#min').html(vmin);
		$('#sec').html(vsec);
	}
});

</script>