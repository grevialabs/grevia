<?php
$param = array(
'paging' => TRUE,
'order' => 'article_id DESC',
'offset' => 11,
'offset' => 14,
'order' => 'publish_date DESC'
);
$list_articles = $this->article_model->get_list($param);
$list_articles = $list_articles['data'];
?>
<!-- Button trigger modal -->
<!--
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>
-->

<!-- Modal -->
<div class="modal fade sr-only" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="jumbotron bgSftLight" style="padding:20px 10px">
	<div class="container ">
		<div class="row" style="min-height:350px">
			<div class="col-md-9 visible-lg">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				  <?php 
				  $list_slides = array($list_articles[0],$list_articles[1],$list_articles[2]);
				  $i = 1;
				  foreach($list_slides as $key => $rs) { 
				  ?>
					<div class="item talCnt <?php if ($i == 1) echo 'active';?>">
					  <img src="<?php echo base_url().'asset/images/article/'.$rs['image']?>" class="lazy img-responsive wdtFul" style="height:500px;" alt="Artikel <?php echo $rs['title']?>" title="Artikel <?php echo $rs['title']?>">
					  <div class="carousel-caption bgBlk lnkWht wdtFul transparent-70" style="left:0;font-size:18px">
						<a class="adsTrack" href="<?php echo base_url().'article/'.$rs['article_id'].'/'.$rs['slug']?>" alt="Artikel <?php echo $rs['title']?>" title="Artikel <?php echo $rs['title']?>"><?php echo $rs['title']?></a>
					  </div>
					</div>
				  <?php
					$i++;
				  }
				  ?>
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="list-group">
				<a class="list-group-item bgRed clrWht b" >HOT ARTICLES</a>
			<?php 
			$param = array(
			'is_hot' => 1,
			'paging' => TRUE,
			'order' => 'a.article_id DESC',
			'limit' => 0,
			'offset' => 7,
			);
			$list_siderights = $this->article_model->get_list($param);
			$list_siderights = $list_siderights['data'];
			foreach ($list_siderights as $key => $rs) {
				?>
					<a class="list-group-item" href="<?php echo base_url().'article/'.$rs['article_id'].'/'.$rs['slug']?>" alt="Artikel <?php echo $rs['title']?>" title="Artikel <?php echo $rs['title']?>">
					<i class="fa fa-rocket"></i>&nbsp;
					<?php echo $rs['title']?></a>
				<?php
			}
			?>
				</div>
			</div>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<!-- 
TEMPORARY COMMENT

<div class="col-md-3" class="padMed">
<?php //if (isset($SIDEBAR)) echo $SIDEBAR; ?>
<blockquote class='reverse-block bg-warning'>
Quotes
<div style="background:#fff;color:#000">-- Anonym</div>
</blockquote>
<br/>

<div class="bg-primary padLrg">Subscribe<br/><input class="btn-xs clrBlk wdtFul" type="text" name="f_email_subscribe"/><br style="margin-bottom:10px"/>
<input class="btn btn-info wdtFul" type="submit" value="Submit"/></div>

</div>
<div class="clearfix"></div>
-->

<!-- NEW LINE -->
<div class="container">
	<div class="row sm-hidden">
		<?php 
		$list_body_articles = array($list_articles[6],$list_articles[7],$list_articles[8],$list_articles[9],$list_articles[10],$list_articles[11]);
		$i = 1;
		foreach ($list_body_articles as $rs) {
		?>
			
			<div class="col-sm-2 br">
				<div class="b bgBlu padLrg clrWht lnkWht tdcNon f16"><a href="<?php echo base_url().'articlecategory/'.$rs['category_slug']?>"/><?php echo $rs['category_name']?></a></div>
				<a class="adsTrack" href="<?php echo base_url().'article/'.$rs['article_id'].'/'.$rs['slug'] ?>" alt="Artikel <?php echo $rs['title']?>" title="Artikel <?php echo $rs['title']?>" ><img class="lazy 
				img-responsive wdtFul" src="<?php echo base_url()?>asset/images/article/<?php echo $rs['image']?>" style="min-height:120px;max-height:220px;max-width:100%"/></a>
				<a class="adsTrack" href="<?php echo base_url().'article/'.$rs['article_id'].'/'.$rs['slug'] ?>" alt="Artikel <?php echo $rs['title']?>" title="Artikel <?php echo $rs['title']?>" ><?php echo $rs['title']?></a>
			</div>
		<?php if ($i%6 == 0) { ?><div class="clearfix"></div><?php } ?>
		<?php 
			$i++;
		}
		?>
		
	</div>
</div>
<div class="clearfix"></div><br/>