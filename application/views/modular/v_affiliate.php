<?php 
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB, $META_DESCRIPTION;

$PAGE_TITLE = 'Affiliasi - Grevia';
$META_DESCRIPTION = 'Promo menarik jasa pembuatan website di Grevia.';
// $PAGE_HEADER = NULL;
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, ABOUT_US);

?>
<div class="container">	
	<div class="row">
		<div class="col-sm-12">
			<h2 class="b clrBlu text-uppercase">Program Afiliasi / Komisi Referal</h2><hr/>
			<p>
				Buat kalian yang mempromosikan situs Grevia dan berhasil meyakinkan calon klien baru, kami punya hadiah menarik buat kamu, yaitu komisi sebesar 10% dari nilai projek yang masuk. Untuk syaratnya ada dibawah ini ya.
			</p>

			Syarat dan Ketentuan
			<ol>
				<li>Hanya berlaku bagi pelanggan baru yang belum pernah menggunakan jasa Grevia</li>
				
				<li><h4 class="b padNon">Siapkan materi atau konten</h4>
				Kami akan membantu menata konten di website anda.
				<br/><br/></li>
				
				<li><h4 class="b padNon">Pilih Template.</h4>
				Pilihlah template yang telah kami sediakan.<br/><br/></li>
				
				<li><h4 class="b padNon">Pembayaran.</h4>Setelah paket dan materi siap, anda tinggal melakukan pembayaran dan kami akan segera proses mengerjakan website anda dalam 4-7 hari kerja<br/><br/></li>
				
				<li><h4 class="b padNon">Website selesai & live.</h4>Situs website yang anda inginkan telah live atau bisa diakses secara online.<br/><br/></li>
			</ol>
		</div>
	</div>
</div>

