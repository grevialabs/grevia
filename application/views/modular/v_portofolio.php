<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = 'Jasa pembuatan web dan situs Company Profile - Grevia Webdesign';
$PAGE_HEADER = NULL;
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, 'Webdesign');
?>
<div class="container" style="padding-bottom:55px;">
	<div class="row">
		<div class="col-sm-12 bgWht">
			<h2 class="clrBlu b text-uppercase">PORTOFOLIO</h2><hr/>

			Portofolio website kami meliputi :<br/>
			<span class="b">(Konten akan kami update secara berkala.)</span>
			<br/><br/>

			<?php 
			$img_uri1 = base_url().'asset/images/portofolio/hrp.jpg';
			$uri1 = "http://www.hrprimesolution.com";
			
			$img_uri2 = base_url().'asset/images/portofolio/dui.jpg';
			$uri2 = "http://www.duitlo.com?ref=grv";
			
			$img_uri3 = base_url().'asset/images/portofolio/scm.jpg';
			$uri3 = "http://scm.grevia.com";
			
			$img_uri4 = base_url().'asset/images/portofolio/job.jpg';
			$uri4 = "http://www.jobtalento.com";
			
			$img_uri5 = base_url().'asset/images/portofolio/dapuramoy.jpg';
			$uri5 = "http://www.dapuramoy.com";
			
			?>
			<div class="col-sm-7">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri1?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri1?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri1?></a>
			</div>
			<div class="col-sm-5">
				Situs company profile untuk HRPrimeSolution yang berisi tentang deskripsi dan bisnis perusahaan berlatar belakang rekrutmen dengan :
				<ol>
					<li>Tampilan responsive</li>
					<li>Animasi & Desain modern</li>
					<li>Google Map penunjuk alamat perusahaan</li>
					<li>Form kontak dengan notifikasi email</li>
					<li>WYIWSG / HTML editor untuk konten</li>
				</ol>
			</div>
			
			<div class="col-sm-12"><hr/></div>
			
			<div class="col-sm-7">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri2?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri2?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri2?></a>
			</div>
			<div class="col-sm-5">
				Duitlo adalah sebuah situs yang membantu user untuk mengelola keuangan personal dengan menyediakan perencanaan keuangan dan pengambilan keputusan di dalam aplikasi yang mudah digunakan. Situs ini dibuat dari konsep dengan bantuan teman yang bergerak di bidang financial advisor namun ingin dibuat .<br/>
				Status: <b class="text-warning">Inactive</b>
				
			</div>
			
			<div class="col-sm-12"><hr/></div>
			
			<div class="col-sm-7">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri3?>" class="img-responsive imageresource" /><br/>
					<a href="<?php echo $uri3?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri3?></a>
				</a>
			</div>
			<div class="col-sm-5">
				
				SCM Grevia adalah aplikasi Penjualan barang / Semi Supply Chain Management meliputi <br/>
				<ol>
					<li>Input stok barang</li>
					<li>pencatatan penjualan</li>
					<li>Surat jalan, Retur barang, Delivery order</li>
					<li>Pembuatan invoice</li>
					<li>Akses level halaman untuk setiap user</li>
					<li>Notifikasi invoice jatuh tempo</li>
					<li>Reporting bulanan</li>
					<li>Komisi karyawan sales per invoice</li>
				</ol>
				Status: <b class="text-success">Active</b>

				
			</div>
			
			<div class="col-sm-12"><hr/></div>
			
			<div class="col-sm-7">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri4?>" class="img-responsive imageresource" /><br/>
				</a><br/>
				<a href="<?php echo $uri4?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri4?></a>
				
			</div>
			<div class="col-sm-5">
				Jobtalento adalah sistem e-recruitment untuk perusahaan melakukan tahap seleksi secara online dengan menyediakan psikotes, tes logic, dll.

				Status: <b class="text-warning">Inactive</b>

			</div>
			
			<div class="col-sm-12"><hr/></div>
			
			<div class="col-sm-7">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri5?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri5?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri5?></a>
			</div>
			<div class="col-sm-5">
				<div style="padding-top:15px"></div>
				Dapuramoy adalah situs e-commerce custom made dengan CMS dari tim Grevia. Fiturnya meliputi input stok, penjualan barang, konfirmasi pembayaran, reporting dan tracking pengiriman berdasarkan KM dengan API google map untuk menentukan ongkos kirim sesuai jarak.<br/><br/>

				System ini cocok untuk e-commerce yang melayani pembelian barang di daerah jakarta dan tidak menggunakan ekspedisi.<br/><br/>

				Status: <b class="text-success">Active</b>
			</div>

			<div class="col-sm-12"><hr/></div>
			
			<div class="col-sm-12">
				<h2 class="clrBlu b text-uppercase">Theme</h2><hr/>
				Template website yang bisa dipilih :<br/><br/>
			</div>

			<?php 
			$img_uri1 = base_url().'asset/images/template/company_one.JPG';
			$uri1 = base_url()."theme/company_one";
			
			$img_uri2 = base_url().'asset/images/template/company_two.JPG';
			$uri2 = base_url()."theme/company_two";
			
			$img_uri3 = base_url().'asset/images/template/company_three.JPG';
			$uri3 = base_url()."theme/company_three";
			
			$img_uri4 = base_url().'asset/images/template/company_four.JPG';
			$uri4 = base_url()."theme/company_four";
			
			?>
			<div class="col-sm-6">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri1?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri1?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri1?></a>
			</div>
			<div class="col-sm-6">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri2?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri2?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri2?></a>
			</div>
			
			<div class="col-sm-12"><hr/></div>
			
			<div class="col-sm-6">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri3?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri3?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri3?></a>
			</div>
			<div class="col-sm-6">
				<a href="javascript:void(0);" class="pop">
					<img src="<?php echo $img_uri4?>" class="img-responsive imageresource" /><br/>
				</a>
				<a href="<?php echo $uri4?>" class="text-primary btn btn-info btn-block" target="_blank"><i class="fa fa-desktop"></i> <?php echo $uri4?></a>
			</div>
			
			
		</div>
	</div>
</div>

<!--
<section class="" style="padding-bottom:55px;background:#fff">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="talCnt"><h1>F.A.Q</h1><br/></div>
				<div class="col-sm-6">
					<span class="b">Mengapa harga paket website di Grevia mahal ?</span><br/>
					<span class="clrSftGry">Silakan anda bandingkan harga kami dengan harga kompetitor lain, boleh dikatakan harga kami adalah harga yang sangat kompetitif.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik, namun saya takut, apakah Grevia bisa dipercaya ?</span><br/>
					<span class="clrSftGry">Kami bekerja secara profesional dan sudah bergerak di bidang jasa teknologi ini selama 4 tahun. Silakan lihat portofolio klien kami.</span>
					<br/><br/>
					
					<span class="b">Apakah anda menerima jasa pembuatan web berbasis CMS opensource, misal Wordpress, atau Blogspot ?</span><br/>
					<span class="clrSftGry">Tidak. Hal ini dikarenakan security CMS opensource yang rawan dibobol, sehingga kami mengembangkan pembuatan website dengan framework kami sendiri yang lebih secure, cepat dan mudah untuk di-custom(flexibel) sesuai kebutuhan anda.</span>
					
				</div>
				
				<div class="col-sm-6">
					<span class="b">Apa yang dimaksud Maintenance ?</span><br/>
					<span class="clrSftGry">Jasa maintenance termasuk jasa memperbaiki bug/error, menjamin uptime server agar bisa selalu diakses selama 24 jam non-stop.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik tapi saya bingung mau membuat website seperti apa ?</span><br/>
					<span class="clrSftGry">Silakan hubungi form atau kontak kami, kami akan berikan jasa konsultasi kapan saja, gratis.</span>
					<br/><br/>
					
					<span class="b">Dimana lokasi server Grevia ? Bagaimana dengan kecepatan aksesnya ?</span><br/>
					<span class="clrSftGry">Server kami semua terletak di Singapura, dan kecepatan akses tidak kalah dengan server lokal atau IIX.</span>
				</div>
				<br/><br/>
			</div>
		</div>
	</div>
</section>
-->
<!--
<div class="col-sm-12">
	<h1 class="talCnt">Fitur</h1>
	<div class="col-sm-4 col-sm-offset-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-2">
	</div>
	<br/><br/>
</div>
-->
<div class="col-sm-12">
	<br/><br/>
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body" style="width:100%">
        <img src="" id="imagepreview" class="img-responsive" >
      </div>
    </div>
  </div>
</div>

<script>
$(".pop").on("click", function() {
   $('#imagepreview').attr('src', $(this).find('img').attr('src'));
   // here asign the image to the modal when the user click the enlarge link
   
   $('#imagemodal').modal('show');
   // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
</script>