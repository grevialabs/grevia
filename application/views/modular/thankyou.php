<?php 
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Thankyou - Finish Shopping';

// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;

$cookie_order_id = $order_id = $get_product_size_id = $get_quantity = NULL;


if (isset($_GET['order_id']) && is_numeric($_GET['order_id'])) $order_id = $_GET['order_id'];

$param_order = NULL;
$obj_order = $obj_list_order_detail = NULL;

if (isset($order_id))
{
	$param_order['order_id'] = $order_id;
	$obj_order = $this->order_model->get($param_order);
	if (isset($obj_order['order_id'])) 
	{
		$obj_list_order_detail = $this->order_model->get_list_detail($param_order);
		$obj_list_order_detail = $obj_list_order_detail['data'];
	}
	else 
	{
		redirect(base_url().'shoppingcart/cart');
	}
}
?>
<div class="talLft visible-xs">
	<div style="background-color:#e6eef4;float:left;width:100%" class="padMed">1. <i class="fa fa-shopping-cart"></i> Pilih item</div>
	<div style="background-color:#e6eef4;float:left;width:100%" class="padMed">2. <i class="fa fa-motorcycle"></i> Alamat pengiriman</div>
	<div style="background-color:#cecece;float:left;width:100%" class="padMed">3. <i class="fa fa-flag "></i> Selesaikan belanja</div>
	<br/>
</div>
<div class="clearfix"></div>
<div class="col-sm-12">
	<div class="talCnt hidden-xs">
		<strike style="width:70px" class="clrBlk"><i class="fa fa-circle clrBlk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</strike>	
		<strike style="width:70px" class="clrBlk"><i class="fa fa-circle clrBlk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</strike>
		<strike style="width:70px" class="clrBlk"><i class="fa fa-circle clrBlk"></i>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-motorcycle"></i></strike>
		<br/> 
		<span class="b" style="padding-right:65px"> 
			<i class="fa fa-arrow-circle-right"></i> 1. Shoppingcart
		</span> 
		
		<span class="b" style="padding-right:85px"> 
			<i class="fa fa-arrow-circle-right"></i> 2. Shipping
		</span>
		
		<span class="b" style="padding-right:65px"> 
			<i class="fa fa-arrow-circle-right"></i> 3. Checkout
		</span>
		
		<span class="b" style=""> 
			<i class="fa fa-arrow-circle-right"></i> 4. Thankyou 
		</span>
	</div><br/>
</div>

<div class="col-sm-12">
	<?php 
	if (!empty($obj_order))
	{
		?>
		<div class="row">
			<div class="col-sm-12">
			Terimakasih telah berbelanja di <?php echo SITE_NAME ?>
			<form method="post">
			<table class="table table-bordered">
				<tr class="alert bg-info talCnt">
					<th width="50px" class="talCnt">#</th>
					<th>Produk</th>
					<th class="talCnt" width="120px">Qty</th>
					<th class="talCnt" width="160px">Harga</th>
					<th class="talCnt" width="200px">Subtotal</th>
				</tr>
			<?php
			$grandtotal = $subtotal = $tax = $total_weight = 0;
			foreach ($obj_list_order_detail as $key => $shopcart)
			{
				$key++;
				// list_shoppingcart
				// debug($shopcart);
				$subtotal = 0;
				$subtotal = $shopcart['product_size_price']*$shopcart['quantity'];
				$grandtotal += $subtotal; 
				
				$prime_image = explode(';',$shopcart['list_image']);
				$prime_image = $prime_image[0];
				
				$total_weight += $shopcart['weight'];
				
				?>
				<tr>
					<td class="talCnt"><?php echo $key?></td>
					<td><a href="<?php echo base_url().'product/'.$shopcart['product_size_id'].'/'.$shopcart['slug']?>"><?php echo $shopcart['product_size_name'].''; ?></a><br/>
					<img src="<?php echo asset_url().'images/product/100/'.$prime_image?>" />
					</td>
					<td class="talCnt"><?php echo $shopcart['quantity']?></td>
					<td class="talRgt"><?php echo format_money($shopcart['product_size_price'])?></td>
					<td class="talRgt"><?php echo format_money($subtotal)?></td>
				</tr>
				<?php
			}
			$total_weight = ceil($total_weight);
			// $shipping_cost = $total_weight * $obj_order['cost_perkilo'];
			$shipping_cost = $obj_order['shipping_cost'];
			$payment_code = $obj_order['payment_code'];
			
			?>
			<tr>
				<td colspan="4" valign="top" class="b">Subtotal</td>
				<td colspan="2" valign="top" class="talRgt"><?php echo format_money($grandtotal)?></td>
			</tr>
			<?php 
			$grandtotal += $shipping_cost;
			?>
			<tr>
				<td colspan="4" valign="top" class="b">Payment Code</td>
				<td colspan="2" valign="top" class="talRgt"><?php echo format_money($payment_code)?></td>
			</tr>
			<?php 
			$grandtotal += $payment_code;
			?>
			<tr>
				<td colspan="4" valign="top" class="b">Shipping Cost</td>
				<td colspan="2" valign="top" class="talRgt"><?php echo format_money($shipping_cost).'<br/> ('.$total_weight.' kg & '.$obj_order['shipping_distance'].' km)'?></td>
			</tr>
			<tr>
				<td colspan="4" valign="top" class="b">Grandtotal</td>
				<td colspan="2" valign="top" class="talRgt"><?php echo format_money($grandtotal)?></td>
			</tr>
			<tr>
				<td colspan="6" valign="top" class=""><b>Metode Pembayaran</b><br/><br/>
				<b><?php echo $obj_order['payment_type_name']?></b><br/>
				Bank <?php echo $obj_order['payment_type_account_bank']?><br/>
				<?php echo $obj_order['payment_type_account_no']?> a/n <?php echo $obj_order['payment_type_account_name']?>
				</td>
			</tr>
			</table>
			
			<table class="table table-bordered">
			<tr>
				<td width="20%" class="b">Nama Penerima</td>
				<td><?php if (isset($obj_order['shipping_name'])) echo $obj_order['shipping_name']; ?></td>
			</tr>
			<tr>
				<td class="b">Email Penerima</td>
				<td><?php if (isset($obj_order['shipping_email'])) echo $obj_order['shipping_email']; ?></td>
			</tr>
			<tr>
				<td class="b">Telp Penerima</td>
				<td><?php if (isset($obj_order['shipping_phone'])) echo $obj_order['shipping_phone']; ?></td>
			</tr>
			<tr>
				<td class="b">Alamat Penerima</td>
				<td><?php if (isset($obj_order['shipping_address'])) echo $obj_order['shipping_address']; ?></td>
			</tr>		
			<tr>
				<td class="b">Catatan *opsional</td>
				<td><?php if (isset($obj_order['shipping_notes'])) echo $obj_order['shipping_notes']; ?></td>
			</tr>
			</table>
			
			Kami telah mengirimkan detail transaksi anda ke email <b><?php if (isset($obj_order['shipping_email'])) echo $obj_order['shipping_email'];?></b>. Mohon segera lakukan pembayaran sebesar <b><?php echo format_money($obj_order['grandtotal']+$obj_order['payment_code']) ?></b> ke <b><?php echo 'Bank '.$obj_order['payment_type_account_bank'].' '.$obj_order['payment_type_account_no'].' a/n '.$obj_order['payment_type_account_name']?></b> cantumkan ID Pesanan Anda saat transfer agar transaksi ini dapat diproses dengan cepat.<br/><br/>
			
			Jika anda sudah melakukan pembayaran, mohon melakukan konfirmasi pembayaran <a href="<?php echo base_url().'payment_confirmation?order_id='.$order_id?>" alt="Konfirmasi pembayaran disini" title="Konfirmasi pembayaran disini">disini</a> <br/><br/>
			
			<div class="talCnt">
				<a class="btn btn-md btn-info br-md" href="<?php echo base_url();?>"> KEMBALI KE HALAMAN DEPAN </a>
			</div>
			
			<br/><br/>
			</div>
		</div>
	
	<?php 
	}
	else 
	{
		echo "No Data Available.";
	}
	?>
	
	<?php ?>
	
</div>