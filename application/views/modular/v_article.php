<?php 
global $PAGE, $PAGE_HEADER, $BREADCRUMB, $META_KEYWORDS, $META_DESCRIPTION;

//$BREADCRUMB = $this->common_model->breadcrumb(NULL, 'About');
//$PAGE_HEADER = 'Tentang Grevia';
//$PAGE_TITLE = NULL;
// $PAGE_TITLE = ;

if ( ! empty($data))
{
	$int_average_rating = left($data['average_rating'],1);
	
	if( !is_internal() ) {
?>
<!-- FB API FOR COMMENTS -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=359223105905";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- FB API FOR COMMENTS -->
<?php 
	}
} 
?>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Product",
        "aggregateRating": {
            "@type": "AggregateRating",
            "worstRating": "1",
            "bestRating": "5",
            "ratingValue": "<?php echo $data['average_rating']?>",
            "reviewCount": "<?php echo $data['total_rating']?>"
        },
        "description": "<?php echo $data['title']; ?>",
        "name": "Jasa web development Terbaik Grevia",
        "image": "<?php echo base_url()?>asset/images/article/<?php echo $data['image']?>",
        "offers": {
            "@type": "Offer",
            "availability": "http://schema.org/InStock",
            "price": "300000",
            "priceCurrency": "IDR"
        }
	}
</script>

<div class="col-sm-9 col-md-9">
<?php
if (!empty($data))
{
	$PAGE = $data['title'];
	//$PAGE_TITLE = $data['title'] .' - Grevia';
	//$OG_TITLE = $data['title'] .' - Grevia';
	$META_DESCRIPTION = $data['short_description'];
	$META_KEYWORDS = $data['tag']; 

	$friendlyUrl = current_url();
	
	$article_category_id = array();
	//var_dump($articleCategory);
	if (strpos($data['article_category_id'],',') !== FALSE) {
		$article_category_id = explode(',', $data['article_category_id']);
		foreach ($article_category_id as $key => $val) 
		{
			$obj = NULL;
			$obj = $this->articlecategory_model->get(array('article_category_id' => $val));
			$array_article_category[] = array( 'category_name' => $obj['category_name'], 'slug' => $obj['slug'] );
		}
	} else {
		$obj = NULL;
		$obj = $this->articlecategory_model->get(array($data['article_category_id']));
		$array_article_category[] = array( 'category_name' => $obj['category_name'], 'slug' => $obj['slug'] );
	}
	
	?>
	<h1><?php echo $data['title']?></h1>
	
	<div>
	<?php 
	
	foreach ($array_article_category as $key => $val) {
		echo ' <a class="btn btn-xs btn-info" href="'.base_url().'articlecategory/'.$val['slug'].'?ref=top_art" alt="Artikel Kategori '.$val['category_name'].'" title="Artikel Kategori '.$val['category_name'].'"><span class="clrWht">'.$val['category_name'].'</span></a> ';
	}
	?>
	</div><br/>
	
	<div>
	<?php 
	
	?>
	Rating : <span style="width:65px; height:13px; display: inline-block; background-size: 67px 15px;" class="star-<?php echo $int_average_rating ?>" title="Rating <?php echo $data['average_rating']; ?> dari <?php echo $data['total_rating']?> orang" ></span> (<?php echo $data['total_rating'] ?>)
	
	<?php if (isset($data['short_description'])) echo '<div class="i clrSftGry" style="">'.$data['short_description'].'</div>'.BR; ?>
	<img class="img-responsive wdtFul" style="" src="<?php echo base_url()?>asset/images/article/<?php echo $data['image']?>"/><br/>
	<?php if (isset($data['image_description'])) { ?>
	<div class="clrGry talRgt"><?php echo $data['image_description']?></div><br/>
	<?php } ?>
	<?php 
	// SOCIAL
	$pagecontent = '';
	$short_url = current_url();
	if (isset($data['short_url'])) 
	{
		$pagecontent.= "<label for='divShortUrl' title='Copy short url'>
		<i class='fa fa-external-link'></i>
		ShortUrl</label>
		<input id='divShortUrl' title='Copy short url' style='font-size:12px;padding: 2px 8px' onClick='this.select();' class='fntBld' readonly='readonly' value='".$data['short_url']."'><br/><br/>";
		$short_url = $data['short_url'];
	}
	$pagecontent.= "<table cellpadding='0' cellspacing='15' style='padding: 10px'>";

	$pagecontent.= "<tr><td style='padding-right:10px' valign='top'>
	<!-- start fb share button-->
	<iframe src='http://www.facebook.com/plugins/like.php?href=". current_url() ."&width&layout=button_count&action=like&show_faces=false&share=true&height=21&appId=".FACEBOOK_APP_ID ."' width='145px' height='25px' style='float:left; border:none'></iframe>
	<!-- // fb share button-->
	</td>
	<td style='padding-right:10px' valign='top'>
	<!-- start twitter share-->
	<a class='twitter-share-button' href='https://twitter.com/intent/tweet?data-counturl=".current_url()."&url=".$short_url."&via=greviacom&text=".urlencode($data['title'])."'>Tweet</a>
	
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='//platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','twitter-wjs');</script>
	<!-- // twitter share-->
	</td>";
	// DONT USE G+
	/*
	<td>
		<!-- start google+ share -->
		<div class='g-plus hidden-sm' style='width:100px;height:22px' data-action='share' data-annotation='bubble' data-height='24' data-href='".current_url()."'></div>

		<!-- Place this tag after the last share tag. -->
		<script type='text/javascript'>
		  (function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/platform.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
		<!-- end google+ share -->
	</td>
	*/
	$pagecontent.= "</tr></table>";
	echo $pagecontent;
	
	//QUOTE HERE
	if (isset($data['quote'])) {
		echo "<div class='divQuote'>".$data['quote']."</div>";
	}
	?>
	<p><?php echo html_entity_decode(htmlentities($data['content']))?></p>
	</div>
	
	<?php if (!is_internal()) { ?>
	<div class="clrBlk fntHdr fntBld">Comments</div>
	<!-- COMMENTS BY FACEBOOK -->
	<div class="fb-comments wdtFul" data-href="<?php echo $friendlyUrl?>" data-width="100%"></div><br/>
	<span class="clrWht fntBld"> <fb:comments-count href=<?php echo $friendlyUrl?>/></fb:comments-count> </span>
	<!-- COMMENTS BY FACEBOOK -->
	<?php 
	}
	?>
	<style>
	.authorpic {
		width: 100px;
		height: 100px;
		border-radius: 150px;
		-webkit-border-radius: 150px;
		-moz-border-radius: 150px;
		background: url(<?php echo base_url().'asset/images/member/'.$data['author_image']?>) no-repeat;
	}
	p {
		font-size: 16px
	}
	</style>
	<?php
	$pagecontent = "<br/><div class=' round'>
		<div class='b clrRed fntBld f18 '>AUTHOR</div>
		<table cellpadding='5'>
		<tr>
			<td class=' talCnt'><div class='authorpic'></div></td>
			<td class=' valTop' valign='top'><div class='padMed'>".$data['full_name'];
	if (isset($data['twitter'])) 
	{
		$pagecontent.= " <i class='fa fa-twitter'></i> <a href='https://twitter.com/".$data['twitter']."' target='_blank'>@".$data['twitter']."</a>";
	}
	
	if (isset($data['facebook'])) 
	{
		$pagecontent.= " <i class='fa fa-facebook'></i> <a href='https://www.facebook.com/".$data['facebook']."' target='_blank'>".$data['facebook']."</a>";
	}
	
	if (isset($data['linkedin'])) 
	{
		$pagecontent.= " <i class='fa fa-linkedin'></i> <a href='".$data['linkedin']."' target='_blank'> linkedin</a>";
	}
	if (isset($data['Website'])) 
	{
		//$pagecontent.= " | <i class='fa fa-television'></i> <a href='".$data['Website']."' target='_blank'>".$data['Website']."</a>";
	}
	$pagecontent.=" 
			<br/>
			<br/>".$data['about_me']."<br/></div></td>
		</tr>
		</table>
		</div><br/>";
	
	/* TAG HERE */
	$list_tag = explode(',',$data['tag']);
	$pagecontent.= "<div class='b clrLgtBlu fntBld f18'>TAG</div>";
	$pagecontent.= "<div>";
	$tags = "";
	foreach ($list_tag as $val) {
		$tags.= "<a style='margin-top:10px' class='btn btn-warning' href='".base_url()."search?q=".urlencode($val)."'>#".$val."</a> ";
	}  
	$pagecontent.= $tags."</div><br/>";
		
	/* RELATED ARTICLE HERE */
	$tag_keyword = str_replace(",","|",$data['tag']);
	
	$param = array(
		'tag' => $tag_keyword,
		'is_publish' => '1',
		'not_article_id' => $article_id,
		'order' => 'a.creator_date DESC',
		'paging' => TRUE,
		'limit' => 0,
		'offset' => 5
	);
	
	$tmpdata = $this->article_model->get_list($param);
	$list_related_article = $tmpdata['data'];

	if (!empty($list_related_article)) 
	{
		$pagecontent.= "<div class='b clrOrg f18'>ARTIKEL TERKAIT</div>";
		$pagecontent.= "<div id='related-article' class='lnkBlk' source='showarticle' segment='related-bottom' ><ul>";
		$i=1;
		foreach($list_related_article as $key => $tmp) {
			$pagecontent.= "<li><a tracker_name='url-".$i."' class='adsTrack' href='".base_url()."article/".$tmp['article_id']."/".$tmp['slug']."?ref=btm_art_rel' alt='Artikel ".$tmp['title']."' title='Artikel ".$tmp['title']."'>".$tmp['title']."</a></li>";
			$i++;
		}
		$pagecontent.= "</ul></div><br/>";
	}
	echo $pagecontent;
}
?>
</div>

<div class="col-sm-3 col-md-3">
<?php echo $SIDEBAR?>
</div>