<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = 'Terms & Condition';
$PAGE_HEADER = 'Terms & Condition';
$PAGE_HEADER_ID = 'Syarat & Ketentuan';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, 'Webdesign');
?>
<!--
<div class="col-xs-6">
</div>
-->
<style>
.mrgBtm0{ margin-bottom:0 }
</style>
<div class="container" style="margin-bottom:0"> 
	<div class="row">
		<div class="col-sm-12 bgWht">
			Dengan menggunakan situs atau pelanggan jasa Grevia Networks, anda sebagai pihak Klien / Pengguna setuju dengan hal berikut:
			
			<h3>Kebijakan Privasi</h3>
			<ul>
				<li>Grevia berhak mengumpulkan data-data pengguna yang disimpan baik di dalam Cookies / data yang telah diinput ke dalam sistem, untuk tujuan pengembangan situs Grevia.</li>
				<li>Grevia berhak menggunakan data pengguna untuk keperluan Analytics Website.</li>
			</ul>
			<br/>
			
			<h3>Pengerjaan Project & Kerjasama Klien</h3>
			<ul>
				<li>Grevia tidak bertanggung jawab atas materi atau konten yang tercantum di situs pelanggan, jika terjadi adanya pelanggaran hak cipta atau konflik atas materi yang disepakati untuk muncul dari pihak eksternal.</li>
				<li>Pihak Klien wajib menyediakan materi konten untuk dipasang di situs klien oleh tim manajemen Grevia.</li>
				<li>Pihak Klien wajib mengikuti SOP dan template yang telah disediakan oleh pihak manajemen Grevia</li>
				<li>tidak ada kewajiban pihak Grevia Networks untuk mengisi / menyediakan konten semacam teks, gambar, atau ikon,dll di situs web milik klien, kecuali ada kesepakatan di awal sesuai notes di Inquiry</li>
				<li>Deadline atau tenggat waktu disepakati bersama di awal project. Perihal terjadinya gangguan / error / force majeur di saat pengerjaan project, tim Manajemen Grevia berhak merubah deadline project secara sepihak sesuai waktu yang dibutuhkan tim kami.</li>
				<li>Grevia hanya menyediakan jasa pembuatan aplikasi berbasis web sesuai kesepakatan dengan pengguna, sebagaimana adanya dan berhak menonaktifkan produk yang ada jika terjadi ketidaksepakatan dengan pengguna.</li>
				<li>Grevia berhak menolak permintaan pihak Klien jika dirasa konten atau effort pengerjaan project tidak dapat dipenuhi pihak Grevia.</li>
				<!--<li>Dalam kurun waktu 14 hari setelah website mulai dibuat, .</li>-->
			</ul>
			<br/>
			
			<h3>Harga dan Pembayaran</h3>
			<ul>
				<li>Grevia berhak merubah harga paket yang tertera di situs ataupun quotation yang sedang berjalan sewaktu-waktu tanpa pemberitahuan ke pengguna.</li>
				<li>Konfirmasi pembayaran dilakukan maksimal 2 x 24 jam.</li>
				<li>Dana yang telah masuk ke dalam rekening milik Grevia Networks, dalam kondisi apapun baik <i>Force Majeur</i> ataupun <i>Disaster Recovery Plan</i> tidak dapat dikembalikan.</li>
			</ul>
			<br/>
			
			<h3>Syarat Ketentuan Garansi Uang Kembali</h3>
			Pihak Klien bisa mengajukan klaim untuk garansi uang kembali apabila:
			<ul>
				<li>Pihak Klien telah melunasi penuh seluruh termin pembayaran atas Inquiry yang ada.</li>
				<li>Situs klien / pengguna yang dimanage oleh pihak Grevia Networks tidak bisa diakses / down tanpa pemberitahuan selama 3x24jam</li>
				<li>Lingkup pekerjaan fitur website tidak sesuai dengan yang didiskusikan di awal(lebih banyak dan tidak tertulis di inquiry saat awal Request Proposal)</li>
				<li>Pelanggan berhak mendapat pengembalian dana 100% dipotong biaya administrasi sebesar 1 juta rupiah atau 10% dari total yang telah dibayar pengguna, jika terjadi kelalaian atas pihak Grevia.</li>
				<!--<li>Dalam kurun waktu 14 hari setelah website mulai dibuat, .</li>-->
			</ul>
			<br/>
			
			<h3>Penyimpanan dan Penghapusan Informasi</h3>
			<ul>
				<li>Pihak Grevia.com akan menyimpan informasi terkait materi yang telah diserahkan oleh pihak Klien, selama akun Pengguna tetap aktif dan dapat melakukan penyimpanan / penghapusan data sesuai dengan ketentuan peraturan hukum yang berlaku.</li>
				<li>Pihak Klien / pengguna membebaskan pihak Grevia Networks atas penggunaan data yang ada di server kami.</li>
				<li>Jika terjadi kelalaian oleh pihak Klien, sehingga sebagian data di server terhapus baik secara sengaja / tidak sengaja, tim Manajemen Grevia tidak bertanggung jawab.</li>
			</ul>
			<br/>

			<h3>Pembaruan Kebijakan Privasi</h3>
			<p>Pihak Grevia.com dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini. Grevia.com menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Situs maupun layanan Grevia.com lainnya, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.</p>
			<br/>

			<br/><br/>
			<i>Last Update by Admin 16 November 2020</i>
			<br/><br/>
			
		</div>
	</div>
</div>

<!--
<section class="" style="padding:35px 0 65px 0;background:#bae9ff">
	<div class="container" >
		<div class="row">
			<div class="col-sm-12">
				<div class="talCnt" id="faq"><h1>F.A.Q</h1><br/></div>
				<div class="col-sm-6">
					<span class="b">Mengapa harga paket website di Grevia mahal ?</span><br/>
					<span class="clrSftGry">Silakan anda bandingkan harga kami dengan harga kompetitor lain, boleh dikatakan harga kami adalah harga yang sangat kompetitif.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik, namun saya takut, apakah Grevia bisa dipercaya ?</span><br/>
					<span class="clrSftGry">Kami bekerja secara profesional dan sudah bergerak di bidang jasa teknologi ini selama 4 tahun. Silakan lihat portofolio klien kami di <a href="portofolio">sini</a>.</span>
					<br/><br/>
					
					<span class="b">Apakah anda menerima jasa pembuatan web berbasis CMS opensource, misal Wordpress, atau Blogspot ?</span><br/>
					<span class="clrSftGry">Tidak. Hal ini dikarenakan security CMS opensource yang rawan dibobol, sehingga kami mengembangkan pembuatan website dengan framework kami sendiri yang lebih secure, cepat dan mudah untuk di-custom(flexibel) sesuai kebutuhan anda.</span>
					
				</div>
				
				<div class="col-sm-6">
					<span class="b">Apa yang dimaksud Maintenance ?</span><br/>
					<span class="clrSftGry">Jasa maintenance termasuk jasa memperbaiki bug/error, menjamin uptime server agar bisa selalu diakses selama 24 jam non-stop.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik tapi saya bingung mau membuat website seperti apa ?</span><br/>
					<span class="clrSftGry">Silakan hubungi form atau kontak kami, kami akan berikan jasa konsultasi kapan saja, gratis.</span>
					<br/><br/>
					
					<span class="b">Dimana lokasi server Grevia ? Bagaimana dengan kecepatan aksesnya ?</span><br/>
					<span class="clrSftGry">Server kami semua terletak di Singapura, dan kecepatan akses tidak kalah dengan server lokal atau IIX.</span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="" style="padding:35px 0 65px 0;background:#fff;min-height: 300px">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="b clrRed">PEMESANAN</h2>
				
				<br/><br/>
			</div>
			
			<div class="col-sm-2">

			</div>
			<div class="8">
				<div class="talCnt">
					Untuk pemesanan silakan hubungi Rusdi di 0838.9199.8825 (telepon / Whatsapp)<br/><br/>
					atau <br/><br/>
					<a class="btn btn-success btn-sm" href="<?php echo base_url().'contact?subject=jasa%20webdesign'?>">Isi form kontak kami disini</a>
				</div>
			</div>
			<div class="col-sm-2">
			</div>
			
		</div>
	</div>
</section>
-->
<!--
<div class="col-sm-12">
	<h1 class="talCnt">Fitur</h1>
	<div class="col-sm-4 col-sm-offset-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-2">
	</div>
	<br/><br/>
</div>
-->