<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = ABOUT_US . ' - Grevia';
$PAGE_HEADER = NULL;
$BREADCRUMB = $this->common_model->breadcrumb(NULL, ABOUT_US);
?>

<div class="col-sm-12">
<img class="img img-responsive wdtFul" src="<?php echo base_url().'asset/images/grevia-top-banner.jpg'?>" />
<br/>
</div>

<div class="col-sm-6 col-xs-12">
<h1 style="padding:0;margin:0">VISI MISI</h1><hr/>
Grevia adalah singkatan dari <span class="b i">Group Evangelist Internet Indonesia</span>, sebuah situs yang menyediakan artikel seputar entrepreneurship, startup dan dunia digital. Seluruh artikel kami berdasarkan dari pengalaman pribadi dan pengalaman para pengusaha digital dari Asia dan Sillicon Valley.<br/><br/>

Grevia adalah wadah untuk membantu para individu / teknopreneur yang ingin memulai ide bisnis mereka, dengan memberikan informasi seputar startup, cerita sukses dari Founder-Founder dan analisa mendalam membangun bisnis.<br/><br/>

<span class="b">Misi</span> : Membantu para individu yang ingin memulai bisnis di dunia digital di Indonesia<br/><br/>
<span class="b">Visi</span> : Menciptakan lebih banyak para pekerja kreatif & teknopreneur di dunia digital Indonesia<br/><br/>

Silakan hubungi kami <a href="<?php echo base_url()?>contact">disini</a><br/><br/>

<table>
<tr>
	<td>
	<!-- START FACEBOOK -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=1457093531196351";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<!-- END FACEBOOK -->
	<div class="fb-like" data-href="https://www.facebook.com/greviacom" data-colorscheme="light" data-layout="button" data-show-faces="false"></div>
	</td>
	<td style="padding-left:15px"></td>
	<td>
	<!-- START TWITTER -->
	<a class="twitter-follow-button"
	  href="https://twitter.com/greviacom"
	  data-show-count="false"
	  data-lang="en">
	Follow @greviacom
	</a>
	<script>window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));</script>
	<!-- END TWITTER -->
	</td>
</tr>
</table><br/><br/>
</div>

<div class="col-sm-6 col-xs-12">
<h1 style="padding:0;margin:0">CERITA</h1><hr/>
Semua bermula dari passion saya, yaitu mempelajari model bisnis startup dan dunia digital, ditambah pengalaman berkarir sebagai programmer beberapa tahun, saya menjadi sangat antusias dengan dunia startup dan ingin sekali memulai perusahaan saya sendiri hingga mengikuti seminar-seminar yang diadakan oleh <span class="b i">Incubator</span>, <span class="b i">Venture Capital</span> dan para <span class="b i">Founder</span> terkenal untuk belajar bagaimana mereka berhasil membangun bisnis mereka dari nol. <br/><br/>

Banyaknya startup yang mulai bermunculan di Indonesia, akhirnya membuat saya terpanggil untuk membuat Grevia, sebagai wadah sekaligus projek saya untuk membantu para calon <i>founder</i> lewat artikel-artikel <i>entrepreneurship</i> di Grevia.<br/><br/>

Silakan kontak saya jika anda memiliki visi & passion yang sama untuk membangun startup. :) <br/><br/>

Rusdi.<br/>
<span class="b">Founder & CTO</span><br/><br/>

<span class="fa-stack fa-lg">
<a href="https://www.facebook.com/rusdi.lim" target="_blank">
	<i class="fa fa-circle fa-stack-2x"></i>
	<i class="fa fa-facebook fa-stack-1x clrWht"></i>
</a>
</span>&nbsp;

<span class="fa-stack fa-lg">
<a href="https://twitter.com/rusdikarsandi" target="_blank">
	<i class="fa fa-circle fa-stack-2x"></i>
	<i class="fa fa-twitter fa-stack-1x clrWht"></i>
</a>
</span>&nbsp;

<span class="fa-stack fa-lg">
<a href="https://www.linkedin.com/in/rusdikarsandi" title="Linkedin" target="_blank">
	<i class="fa fa-circle fa-stack-2x"></i>
	<i class="fa fa-linkedin fa-stack-1x clrWht"></i>
</a>
</span>&nbsp;

<br/><br/>

</div>
<!--
<div class="col-xs-12">
<br>
<h1 class="" style="padding:0;margin:0">VISI MISI GREVIA</h1><hr/>

<span class="b">Mengapa saya membangun Grevia ? </span><br/>
Grevia adalah situs tentang tips dan artikel startup, yang saya gunakan untuk meningkatkan personal branding para author (individu yang menulis artikel di Grevia) untuk menciptakan branding profesional dan membuat nama kontributor lebih dikenal di dunia startup melalui artikel - artikel yang mereka tulis. <br/><br/>

<span class="b">Mengapa personal branding di Grevia itu penting? </span><br/>
- Untuk mengenalkan hasil karya tulis / artikel anda ke publik.<br/>
- Membuat anda dikenal sebagai orang yang expert di bidang yang anda kuasai dari portofolio anda.<br/>
- Meningkatkan kecepatan leveling up karir anda (dalam hal ini, startup).<br/>
- Meraih kepercayaan dan mencari pendanaan melalui venture capital.<br/><br/>

<span class="b">Visi misi</span><br/>
Visi saya membangun Grevia adalah untuk memudahkan orang untuk mencari co-founder dan membantu para individu yang ingin memulai startup, agar memiliki panduan.<br/><br/>


<span class="b">Alasan lain ?</span><br/>
Indonesia, saat ini tumbuh berkembang dan mengalami pertumbuhan yang sangat baik,<br/><br/><br/>
</div>
-->
