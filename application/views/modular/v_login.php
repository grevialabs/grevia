<?php 
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = MENU_LOGIN . ' - Grevia';
$PAGE_HEADER = NULL;

if (is_member()) redirect(base_url().'member/profile');

/*|
  | LOGIN
*/

$email = $password = NULL;

if (isset($_POST['hdnLogin'])) 
{
	$gInfo = '';
	$email = $_POST['l_email'];
	$password = $_POST['l_password'];

	if (isset($email) && isset($password))
	{
		// debug($email);
		// echo "<hr/>";
		// debug($password);
		$member = $this->member_model->get(array(
			'email' => $email
		));
		if (!empty($member))
		{
			// echo "checkpoint";
			// die;
			if ($member['password'] == encrypt($password))
			{
				$encrypt = encrypt($member['member_id']."#".$member['full_name']."#".$member['email']."#".$member['is_admin']);
				
				// 2 days expired cookie on default
				$expiredCookie = time() + 2 * 86400;
				
				// if (post('l_remember_me')) $expiredCookie = time() + 30 * 86400;
				
				setcookie('hash', $encrypt, $expiredCookie, '/');
				
				// redirect to reference url if exist
				if ($this->input->get('url')) 
				{
					$url = $this->input->get('url');
					redirect(base_url().urldecode($url));
				}
				else
				{
					redirect('member/profile');
				}
				$gInfo = 'berhasil login.';
			}
			else
			{
				$gInfo = 'Email / Password tidak sesuai';
			}
		}
		else
		{
			$gInfo = 'Email tidak ditemukan';
		}
	}
}
/*|
  | UPDATE
*/
if ($_POST) {
	$gInfoReg = NULL;
	if(post('hdnRegister') == 1) {
		
		if(!is_filled($_POST['full_name'])) {
			$gInfoReg.= 'Nama harus diisi';
		} else if(!is_filled($_POST['email'])) {
			$gInfoReg.= 'Email harus diisi';
		} else if(!is_filled($_POST['password'])) {
			$gInfoReg.= 'Password harus diisi';
		} else if(!is_filled($_POST['Gender'])) {
			$gInfoReg.= 'Gender harus diisi';
		} else if(!is_filled($_POST['dob'])) {
			$gInfoReg.= 'Tgl lahir harus diisi';
		} else {
			$objMember = NULL;
			$objMember = $this->member_model->get(
				array('email' => $_POST['email'])
			);
			if (empty($objMember)) {
				$email = $_POST['email'];
				$full_name = $_POST['full_name'];
				$active_code = genRandomString(10);
					
				$param = array(
					'full_name' => $_POST['full_name'],
					'name' => $_POST['full_name'],
					'email' => $_POST['email'],
					'password' => encrypt($_POST['password']),
					'gender' => $_POST['gender'],
					'dob' => $_POST['dob'],
					'active_code' => $active_code,
					'is_admin' => 0,
					'is_subscribe' => 1,
					'is_active' => 0,
					'is_deleted' => 0,
					'creator_date' => get_datetime(),
				);
				$save = $this->member_model->save($param);
				
				if($save){			
					
					// benerin lagi nih
					$email_member.='<html><head></head><body><div style="padding:15px">Dear '.$full_name.',<br/><br/>Anda mendapatkan email ini karena anda telah mendaftar di Grevia.com.';
					$email_member.='Silakan klik <a href="'.base_url().'activation?do=submit&email='.$email.'&code='.$active_code.'">Link ini</a> atau ketik url ini ke browser anda '.base_url().'activation?do=submit&email='.$email.'&code='.$active_code.' untuk konfirmasi akun anda.<br/><br/>';
					$email_member.='<p>Salam.<br/>Tim Grevia</p></div></body></html>';
					if(!is_internal()){
						// SEND EMAIL HERE
						$to = $email;
						$subject = "Selamat bergabung di Grevia.com";
						$headers = "From: Grevia Team.<noreply@grevia.com> \r\n";
						$headers .= "Reply-To: \r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$send = mail($to,$subject,$email_member,$headers);
						//$gInfoReg.= 'Registrasi berhasil. Silakan periksa akun email <b>'.$to.'</b> untuk link aktivasi akun Grevia anda';
						$gInfoReg.= 'Registrasi berhasil. Silakan login akun email <b>'.$to.'</b> ';
					}else{
						$gInfoReg.= 'Registrasi berhasil.'.br().$email_member;
					}
				}else{
					$gInfoReg = 'Registrasi gagal, Silakan coba lagi atau hubungi administrator';
				}
			}else{
				$gInfoReg = 'Email <b>'.$_POST['email'].'</b> sudah terdaftar. Silakan gunakan email lain';
			}
		}
	}
}
?>

<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB, $META_KEYWORDS, $META_DESCRIPTION, $META_AUTHOR;
 
if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;

if (!isset($META_AUTHOR)) $META_AUTHOR = META_AUTHOR;
if (!isset($META_KEYWORDS)) $META_KEYWORDS = META_KEYWORDS;
if (!isset($META_DESCRIPTION)) $META_DESCRIPTION = META_DESCRIPTION; 
if (isset($OG_TITLE)) $PAGE_TITLE = $OG_TITLE;
if (!isset($TWITTER_URL)) $TWITTER_URL = current_url(); 
?>
<!DOCTYPE html>
<html lang="id">
<head itemscope itemtype="http://schema.org/WebSite">
	<meta name="google-site-verification" content="NM1j6BG3n_-7PcXeKwQnS6R-JABiEe2PL7Lm7M_Tbjo" />
	<meta name="keywords" content="<?php echo $META_KEYWORDS ?>" />
	<meta name="description" content="<?php echo $META_DESCRIPTION ?>" />
	<meta name="robots" content="all, index, follow"/>
	<?php if (isset($META_AUTHOR)) { ?>
	<meta name="author" content="<?php echo $META_AUTHOR ?>" />
	<?php } ?>
	
	<!-- Android Phone -->
	<meta name="theme-color" content="#0089C9">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#0089C9">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	
	<!-- META PROPERTY -->
	<meta property="fb:admins" content="<?php echo FACEBOOK_ADMIN_ID?>" />
	<meta property="fb:app_id" content="<?php echo FACEBOOK_APP_ID?>" />
	
	<?php if (isset($OG_TITLE)) { ?>
	<meta property="og:title" content="<?php echo $OG_TITLE ?>" />
	<?php } ?>
	<meta property="og:url" content="<?php echo current_url() ?>" />
	
	<?php if (isset($OG_IMAGE)) { ?>
	<meta property="og:image" content="<?php echo $OG_IMAGE ?>" />
	<?php } ?>
	
	<?php if (isset($OG_DESCRIPTION)) { ?>
	<meta property="og:description" content="<?php echo $OG_DESCRIPTION ?>" />
	<?php } ?>
	
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="<?php echo DEFAULT_WEBSITE_DOMAIN?>" />
	<meta property="article:publisher" content="https://www.facebook.com/greviacom" />
	<!-- META PROPERTY -->
	
	<meta name="twitter:title" content="<?php if (isset($PAGE_TITLE)) echo $PAGE_TITLE ?>" />
	<meta name="twitter:url" content="<?php echo $TWITTER_URL ?>" />
	<meta name="twitter:description" content="<?php echo $META_DESCRIPTION ?>" />
	<meta name="twitter:card" content="article" />
	
	<!--<meta charset="utf-8">-->
	<title itemprop="name"><?php echo $PAGE_TITLE ?></title>
	<link rel="canonical" href="<?php echo current_url()?>" itemprop="url">
	<link rel="shortcut icon" href="<?php echo base_url()?>favicon.png">
	
	<?php
	if (!is_internal()) {
	?>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56560352-1', 'auto');
	  ga('send', 'pageview');

	</script>

	<?php } ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->
	
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-animation.min.css?v=20160708"/>
	
	<!-- Include required JS files -->
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/shCore.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>asset/js/shAutoloader.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/shBrushPhp.js"></script>
	 
	<!-- Include *at least* the core style and default theme -->
	<link href="<?php echo base_url()?>asset/css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>asset/css/shThemeDefault.css" rel="stylesheet" type="text/css" />
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css?v=20160708"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=20160708"/>
	<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->
	
    <link href="<?php echo base_url()?>asset/css/font-awesome-4.6.3.min.css?=20160708" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.min.css?v=20160708"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css"/>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.1.11.2.min.js"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.min.js?v=20160708"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery.shiftcheckbox.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			$("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			//$('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
		});
		
		/* INSERT class parentcheckbox in wrapper checkbox to activate */
		$('.parentcheckbox').shiftcheckbox({
			checkboxSelector : ':checkbox',
			//selectAll        : $('.chkbox '),
			ignoreClick      : 'a',
		});
	});
	window.sr = new scrollReveal();
	</script>
	
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/colorbox.min.css?v=20160708" />
	<script src="<?php echo base_url()?>asset/js/jquery.colorbox.min.js?v=20160708"></script>
	<?php
	// SHOW POP UP SUBSCRIBE IF NO COOKIE
	if (isset($_COOKIE['is_show_subscribe']) && $_COOKIE['is_show_subscribe'] != '1')
	{
	?>
	<script>
	// $(document).ready(function(){
		// $.colorbox({inline:true, href:"#inline_content"});
	// });
	</script>
	<?php 
	}
	else
	{
		/* SHOW FLOATING SUBSCRIBE IF MORE THAN 100 SECONDS IN SHOWARTICLE*/
		if (strpos(current_url(), 'article/') == TRUE ) {

	?>
	<script>
	// $(document).ready(function(){
		// setTimeout(function() {
			// $.colorbox({inline:true, href:"#inline_content"});
		// },100000);
	// });
	</script>
	<?php
		}
	}

	if (!is_internal())
	{
	?>
	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3HbGjRjKeR4MlaDsKWfV0XXQLYgtN6k6";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
	</script>
	<!--End of Zopim Live Chat Script-->
	<?php
	}	
	?>
	
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body class="bgBlu">
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "http://www.grevia.com/",
  "logo": "http://www.grevia.com/asset/images/logo_grevia_small.png",
  "contactPoint" : [
	{ "@type" : "ContactPoint",
	  "telephone" : "+6283891998825",
	  "contactType" : "sales",
	  "areaServed" : "ID"
	} , {
	  "@type" : "ContactPoint",
	  "telephone" : "+6283891998825",
	  "contactType" : "reservations"
	} ],
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://www.grevia.com/search?q={search_term_string}",
    "query-input": "required name=search_term_string"
  },
  "sameAs" : [ "https://www.facebook.com/greviacom",
	"https://www.twitter.com/greviacom",
	// "https://plus.google.com/+jakartanotebook",
	"https://instagram.com/grevianetworks"]
}
</script>

<div class="container">	
	<div class="row">
		<div class="col-md-12" style="min-height:500px">

<div class='col-md-3'>
</div>
<div class='col-md-6 bgWht talCnt' style="margin-top:30px;padding: 20px 10px 10px 10px">
	<div class="talCnt"><img src="<?php echo base_url().'asset/images/logo-grevia.png'?>" class="" /></div><br/>

	<h1 class="title-header talCnt">&nbsp;<?php echo LOGIN?></h1><hr/>
	<!-- 
	<div id='fblogin'><fb:login-button scope="public_profile,email" size="large" onlogin="checkLoginState();"></fb:login-button></div><br/>
	
	<a href="forgotpassword"><?php echo MENU_FORGOT_PASSWORD?></a> 
	| <a href="activation"><?php echo MENU_RESEND_ACTIVATION_EMAIL?></a>
	-->
	<br/>
	<div class="errLog"></div>
	<form class='form-horizontal f18' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='text' style="padding: 25px" class='f18 form-control' name='l_email' id='l_email' placeholder='<?php echo EMAIL?>' value='<?php if (isset($email)) echo $email?>' required ></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><input type='password' style="padding: 25px" class='f18 form-control clrBlk' name='l_password' id='l_password' placeholder='<?php echo PASSWORD?>' value='' required></div>
		</div>
		<div class='form-group form-group-md'>
			<div class='col-sm-1 checkbox talCnt' style='padding-left:30px'>
				<label><input type='checkbox' name='l_remember_me' id='l_remember_me' value='1' /></label>
			</div>
			<div class='col-sm-5 talLft' style='padding-top:8px'><label for='l_remember_me'><?php echo INFO_REMEMBER_ME?></label></div>
			<div class='col-sm-6 talCnt' style='padding-top:12px'><a class="" href="forgotpassword" title="Reset Password">Reset Password</a></div>
		</div>
		<div class='form-group'>
			<div class='col-sm-12'><input type="hidden" name="hdnLogin" value="1"/><button type="submit" value="" class="wdtFul btn btn-success btn-lg"/><?php echo LOGIN?></button></div>

			<div class='col-sm-12 talCnt'><br/>Belum punya account ? <a class="" href="register" title="Register member Grevias">Register disini</a></div>
		</div>
	</form>
	<?php
	if (isset($gInfo)) echo print_message($gInfo);
	?>
</div>

<div class='col-md-3'>
</div>

</div>
	</div>
</div>

<?php //include_once 'navfooter.php';?>

</body>
</html>

<!--
<div class='col-md-6'>
	<h1 class="title-header">&nbsp;<?php echo REGISTRATION?></h1><hr/>
	<div class='col-sm-2'>
		<div id='fblogin'><fb:login-button scope="public_profile,email" size="large" onlogin="checkLoginState();"></fb:login-button></div>
	</div>
	
	<div class='col-sm-2'>
		<div id="gConnect">
			<button class="g-signin"
				data-scope="https://www.googleapis.com/auth/plus.profile.emails.read"
				data-requestvisibleactions="http://schemas.google.com/AddActivity"
				data-clientId="<?php echo GOOGLE_CLIENT_ID ?>"
				data-callback="onSignInCallback"
				data-theme="dark"
				data-cookiepolicy="single_host_origin">
			</button>
		</div>
	</div>
	<div class='clearfix'></div>
	
	<form method="post" id="frmReg" class="form-group">
		<div class="errReg"></div>
		<?php if (isset($gInfoReg))echo message($gInfoReg); ?>
		<div class="input-group margin-bottom-sm br">
			<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
			<input class="form-control" type="text" placeholder="<?php echo FULL_NAME?>" name="f_full_name" date-toggle="senpai" required/>
			<input type='hidden' name='f_FacebookID' id='f_FacebookID'/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
		  <input class="form-control" type="text" placeholder="<?php echo EMAIL?>" name="f_Email" required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
		  <input class="form-control" type="password" placeholder="<?php echo PASSWORD?>" name="f_Password" minlength="4" required/>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-neuter fa-fw"></i></span>
		  <select name="f_Gender" id='f_Gender' placeholder="Gender" title="<?php echo SELECT_GENDER?>" class="form-control" required>
			<option></option>
			<option value="1" <?php //if($gender == 1)echo 'selected'?>><?php echo MALE?></option>
			<option value="2" <?php // if($gender == 2)echo 'selected'?>><?php echo FEMALE?></option>
		  </select>
		</div>
		<div class="input-group margin-bottom-sm br">
		  <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
		  <input class="datepicker form-control" type="text" placeholder="Tgl lahir" name="f_DOB" required/>
		</div>
		<div class='form-group form-group-md'>
			<div class='col-sm-1 checkbox ' style='padding-left:30px'>
				<label><input type='checkbox' name='f_IsAgree' id='f_IsAgree' value='1' checked /></label>
			</div>
			<div class='col-sm-11' style='padding-top:8px'><label for='f_IsAgree'> <?php echo INFO_I_AGREE_TERM_CONDITION?> <strong>Grevia</strong></label></div>
		</div>
		<div class="form-group form-group-md br">
			<div class="col-sm-12"><input type='hidden' name='hdnRegister' value='1'/><button value="Register" class="btn btn-info"/><?php echo REGISTER?></button></div>
		</div>
		<div class='clearfix'></div><br/>
		
		
		<div class="errReg"></div>
		<p><input type="" placeholder="Nama lengkap" name="f_full_name" id="f_full_name" maxlength="50" value="<?php //echo $full_name;?>" minlength="4" required /></p>
		<input type='hidden' name='f_FacebookID' id='f_FacebookID'/>
		<p><input type="email" placeholder="Email valid" name="f_Email" id="f_Email" maxlength="50" value="<?php //echo $email;?>" required /></p>
		<p><select name="f_Gender" id='f_Gender' placeholder="Gender" title="Pilih Gender" class="wdtFul" required>
		<option></option>
		<option value="1" <?php //if($gender == 1)echo 'selected'?>>Pria</option>
		<option value="2" <?php // if($gender == 2)echo 'selected'?>>Wanita</option></select></p>
		<p><input type="password" placeholder="Kata Sandi" name="f_Password" minlength="4" required/></p>
		<p><input type="text" name="f_BirthDate" placeholder="Tgl Lahir" class="datepicker" id='f_BirthDate' value="<?php //echo $birthDate;?>"/></p>
		<p><input type="checkbox" class="checkbox w10" name="f_IsAgree" value="1" checked required/> Saya setuju dengan syarat dan ketentuan dari <strong>Grevia</strong></p>
		<p><input type='hidden' name='hdnRegister' value='1'/><button value="Register" class="btn btn-info"/>Register</button></p>
		<?php if (isset($gInfoReg))echo print_message($gInfoReg); ?>
		
	</form>
</div>
-->


<!-- START FACEBOOK -->

<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
	  //$('#fblogin').hide();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      //document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      // document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo FACEBOOK_APP_ID?>',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    //console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      //alert(JSON.stringify(response));
	  //console.log('Successful login for: ' + response.name);
      //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
	  <?php if (!is_filled(get('data'))) { ?>
	  //document.getElementById('status').innerHTML = JSON.stringify(response);
	  //window.location = '<?php echo base_url().'login?facebook=1'?>' + '&data=' + JSON.stringify(response);
	  <?php } ?>
	  document.getElementById('f_FacebookID').value = response.id;

	  document.getElementById('f_full_name').value = response.name;
	  document.getElementById('f_Email').value = response.email;
	  var userGender = response.gender;
	  if (response.gender == 'male') userGender = '1';
	  else if( response.gender == 'female') userGender = '2';
		var dd = document.getElementById('f_Gender');
		for (var i = 0; i < dd.options.length; i++) {
			if (dd.options[i].value == userGender) {
				dd.selectedIndex = i;
				break;
			}
		}
    });
  }
</script>
<!-- GOOGLE + LOGIN -->
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
-->
<!-- GOOGLE + LOGIN -->


<script type="text/javascript">
var helper = (function() {
  var BASE_API_PATH = 'plus/v1/';

  return {
    /**
     * Hides the sign in button and starts the post-authorization operations.
     *
     * @param {Object} authResult An Object which contains the access token and
     *   other authentication information.
     */
    onSignInCallback: function(authResult) {
      gapi.client.load('plus','v1').then(function() {
        $('#authResult').html('Auth Result:<br/>');
        for (var field in authResult) {
          $('#authResult').append(' ' + field + ': ' +
              authResult[field] + '<br/>');
        }
        if (authResult['access_token']) {
          $('#authOps').show('slow');
          $('#gConnect').hide();
          helper.profile();
          helper.people();
        } else if (authResult['error']) {
          // There was an error, which means the user is not signed in.
          // As an example, you can handle by writing to the console:
          console.log('There was an error: ' + authResult['error']);
          $('#authResult').append('Logged out');
          $('#authOps').hide('slow');
          $('#gConnect').show();
        }
        console.log('authResult', authResult);
      });
    },

	/**
     * Calls the OAuth2 endpoint to disconnect the app for the user.
     */
    disconnect: function() {
      // Revoke the access token.
      $.ajax({
        type: 'GET',
        url: 'https://accounts.google.com/o/oauth2/revoke?token=' +
            gapi.auth.getToken().access_token,
        async: false,
        contentType: 'application/json',
        dataType: 'jsonp',
        success: function(result) {
          console.log('revoke response: ' + result);
          $('#authOps').hide();
          $('#profile').empty();
          $('#visiblePeople').empty();
          $('#authResult').empty();
          $('#gConnect').show();
        },
        error: function(e) {
          console.log(e);
        }
      });
    },

    /**
     * Gets and renders the list of people visible to this app.
     */
    people: function() {
      gapi.client.plus.people.list({
        'userId': 'me',
        'collection': 'visible'
      }).then(function(res) {
        var people = res.result;
        // $('#visiblePeople').empty();
        // $('#visiblePeople').append('Number of people visible to this app: ' +
            // people.totalItems + '<br/>');
        // for (var personIndex in people.items) {
          // person = people.items[personIndex];
          // $('#visiblePeople').append('<img src="' + person.image.url + '">');
        // }
      });
    },

    /**
     * Gets and renders the currently signed in user's profile data.
     */
    profile: function(){
      gapi.client.plus.people.get({
        'userId': 'me'
      }).then(function(res) {
        var profile = res.result;
		var email_google;
		//alert(res.result.emails.value);
		<?php if (is_filled(get('data')) ) { ?>
		//window.location = '<?php echo base_url().'login?data=' ?>' + JSON.stringify(profile);
		<?php } ?>
        $('#f_Email').val(profile.email);
		$('#f_full_name').val(profile.displayName);
		email_google = JSON.parse(profile.emails);
		//alert(email_google[0].value);
		$('#profile').empty();
		$('#profile').append(JSON.stringify(profile));
        // $('#profile').append(
            // $('<p><img src=\"' + profile.image.url + '\"></p>'));
        // $('#profile').append(
            // $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
            // profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
        // if (profile.cover && profile.coverPhoto) {
          // $('#profile').append(
              // $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
        // }
      }, function(err) {
        var error = err.result;
        $('#profile').empty();
        $('#profile').append(error.message);
      });
    }
  };
})();

/**
 * jQuery initialization
 */
$(document).ready(function() {
  $('#disconnect').click(helper.disconnect);
  // if ($('[data-clientId="<?php echo GOOGLE_CLIENT_ID?>"]').length > 0) {
    // alert('This sample requires your OAuth credentials (client ID) ' +
        // 'from the Google APIs console:\n' +
        // '    https://code.google.com/apis/console/#:access\n\n' +
        // 'Find and replace YOUR_CLIENT_ID with your client ID.'
    // );
  // }
});

/**
 * Calls the helper method that handles the authentication flow.
 *
 * @param {Object} authResult An Object which contains the access token and
 *   other authentication information.
 */
function onSignInCallback(authResult) {
  helper.onSignInCallback(authResult);
}
</script>
<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->


<!-- END FACEBOOK -->

	