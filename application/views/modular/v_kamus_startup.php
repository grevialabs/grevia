<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = 'Jasa pembuatan web dan situs Company Profile - Grevia Webdesign';
$PAGE_HEADER = NULL;
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, 'Webdesign');

$dataq = $datas = array();
$dataq[] = 'Angel Investor';
$datas[] = 'Pemodal / investor awal yang mendanai startup, biasanya berupa individu';

$dataq[] = 'Burn rate';
$datas[] = 'Pengeluaran bulanan yang dihabiskan dalam sekali periode waktu tertentu secara rata-rata';

$dataq[] = 'Bootstrap';
$datas[] = 'Pendanaan operasional startup dengan menggunakan uang pribadi';

$dataq[] = 'VC / Venture Capital';
$datas[] = 'Badan / organisasi yang memberikan pendanaan biasanya terbagi menjadi investor awal, investor lanjutan';

$dataq[] = 'Pivot';
$datas[] = 'Merubah core bisnis model startup. Biasanya dilakukan jika tidak ada traction / pangsa pasar kurang baik.';

$dataq[] = 'Traction';
$datas[] = 'Animo atau ketertarikan user terhadap suatu produk.';

$dataq[] = 'Valuation / Valuasi';
$datas[] = 'Perkiraan atau estimasi nilai jual dari sebuah startup.';

$dataq[] = 'Accelerator';
$datas[] = 'Badan atau perusahaan yang membantu startup di fase yang telah berkembang dan mempercepat akselerasi atau growth .';

$dataq[] = 'Valuation / Valuasi';
$datas[] = 'Perkiraan atau estimasi nilai jual dari sebuah startup.';

$dataq[] = 'A B testing';
$datas[] = 'metodologi teknis yang digunakan untuk mengukur produk yang lebih diminati pelanggan dengan menampilkan beberapa varian.';

?>
<div class="container" style="padding-bottom:55px;">
	<div class="row">
		<div class="col-sm-12 bgWht">
			<h2 class="clrBlu b text-uppercase">Kamus startup</h2><hr/>

			Kamus istilah Startup yang sering digunakan.<br/>
			<span class="b">(Konten akan kami update secara berkala.)</span>
			<br/><br/>

			<div class="accordion" id="accordionExample">
				<?php 
				if (!empty($dataq)) 
				{
					// foreach ($dataq as $rsk => $rsq)
					for ($i=0; $i<count($dataq); $i++)
					{
				?>
				<div class="card">
					<div class="card-header" id="headingOne">
					<h2 class="mb-0">
						<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i?>" aria-expanded="true" aria-controls="collapse<?php echo $i?>">
						<?php echo $dataq[$i] ?>
						</button>
					</h2>
					</div>

					<div id="collapse<?php echo $i?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							<?php echo $datas[$i] ?>
						</div>
					</div>
				</div>
				<?php 
					}
				}
			?>
			</div>
				<!--
				<div class="card">
					<div class="card-header" id="headingThree">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						Collapsible Group Item #3
						</button>
					</h2>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					<div class="card-body">
						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
					</div>
					</div>
				</div>
				</div>
				-->
			
			<div class="col-sm-12"><hr/></div>	
			
		</div>
	</div>
</div>

<!--
<section class="" style="padding-bottom:55px;background:#fff">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="talCnt"><h1>F.A.Q</h1><br/></div>
				<div class="col-sm-6">
					<span class="b">Mengapa harga paket website di Grevia mahal ?</span><br/>
					<span class="clrSftGry">Silakan anda bandingkan harga kami dengan harga kompetitor lain, boleh dikatakan harga kami adalah harga yang sangat kompetitif.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik, namun saya takut, apakah Grevia bisa dipercaya ?</span><br/>
					<span class="clrSftGry">Kami bekerja secara profesional dan sudah bergerak di bidang jasa teknologi ini selama 4 tahun. Silakan lihat portofolio klien kami.</span>
					<br/><br/>
					
					<span class="b">Apakah anda menerima jasa pembuatan web berbasis CMS opensource, misal Wordpress, atau Blogspot ?</span><br/>
					<span class="clrSftGry">Tidak. Hal ini dikarenakan security CMS opensource yang rawan dibobol, sehingga kami mengembangkan pembuatan website dengan framework kami sendiri yang lebih secure, cepat dan mudah untuk di-custom(flexibel) sesuai kebutuhan anda.</span>
					
				</div>
				
				<div class="col-sm-6">
					<span class="b">Apa yang dimaksud Maintenance ?</span><br/>
					<span class="clrSftGry">Jasa maintenance termasuk jasa memperbaiki bug/error, menjamin uptime server agar bisa selalu diakses selama 24 jam non-stop.</span>
					<br/><br/>
					
					<span class="b">Saya tertarik tapi saya bingung mau membuat website seperti apa ?</span><br/>
					<span class="clrSftGry">Silakan hubungi form atau kontak kami, kami akan berikan jasa konsultasi kapan saja, gratis.</span>
					<br/><br/>
					
					<span class="b">Dimana lokasi server Grevia ? Bagaimana dengan kecepatan aksesnya ?</span><br/>
					<span class="clrSftGry">Server kami semua terletak di Singapura, dan kecepatan akses tidak kalah dengan server lokal atau IIX.</span>
				</div>
				<br/><br/>
			</div>
		</div>
	</div>
</section>
-->
<!--
<div class="col-sm-12">
	<h1 class="talCnt">Fitur</h1>
	<div class="col-sm-4 col-sm-offset-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-3">
		<i class="fa fa-envelope"></i> Mail Spam <br/>
		<i class="fa fa-tachometer"></i> Uptime Server <br/>
	</div>
	<div class="col-sm-2">
	</div>
	<br/><br/>
</div>
-->
<div class="col-sm-12">
	<br/><br/>
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body" style="width:100%">
        <img src="" id="imagepreview" class="img-responsive" >
      </div>
    </div>
  </div>
</div>

<script>
$(".pop").on("click", function() {
   $('#imagepreview').attr('src', $(this).find('img').attr('src'));
   // here asign the image to the modal when the user click the enlarge link
   
   $('#imagemodal').modal('show');
   // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
</script>