<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE_TITLE = ORDER . ' - Grevia';
$PAGE_HEADER = 'Order website';
$BREADCRUMB = $this->common_model->breadcrumb(NULL, ORDER);

$gInfo = '';

// // if($_POST){
	// // print_r($_POST);die;
// // }
// //BROWSER KEY AIzaSyCwZ7xWc3bPeuyGwLbJIf23lak599HunvM
// // SERVER KEY AIzaSyBcvHZffHXEK4dCxXC_HGCk4s6huhP6E4Q 
// $ch = curl_init();

// //$url_target = base_url().'contact';
// $url_target = "https://www.googleapis.com/urlshortener/v1/url";

// $post_data = array(
	// 'longUrl' => 'http://www.grevia.com/article/40/3-marketing-campaign-super-efektif-untuk-early-stage-startup/',
	// 'key' => 'AIzaSyBcvHZffHXEK4dCxXC_HGCk4s6huhP6E4Q'
// );
// //$post_data = http_build_query($post_data);
// $post_data = json_encode($post_data);

// curl_setopt($ch, CURLOPT_URL, $url_target);
// curl_setopt($ch, CURLOPT_POST, 1);
// //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
// curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
// curl_setopt($ch, CURLOPT_HEADER, 0);
// curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    // 'Content-type:application/json')                                                                       
// ); 
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// $result = curl_exec($ch);
// curl_close($ch);
// $result = json_decode($result);

// var_dump($result);die;
// if ($result) 
// { 
	// var_dump($result);die;
// }


if($this->input->post('hdnSubmit') == '1'){
	
	$name = post('tName');
	$email = filter($this->input->post('tEmail'));
	$phone = filter($this->input->post('tPhone'));
	$subject = 'Orderweb '.filter($this->input->post('tSubject'));
	$budget = filter($this->input->post('tBudget'));
	$deadline = filter($this->input->post('tDeadline'));
	$message = htmlentities($this->input->post('tMessage'));
	if(!is_filled($name)){
		$gInfo .= "Name must be filled.";
	}elseif(!is_filled($email)){
		$gInfo .= "Email must be filled.";
	}elseif(!is_filled($subject)){
		$gInfo .= "Subject must be filled.";
	}elseif(!is_filled($message)){
		$gInfo .= "Message must be filled.";
	}else{
		
		$save = array(
			'fullname' => $name,
			'email' => $email,
			'subject' => $subject,
			'message' => $message,
		);
		$save = $this->inbox_model->save($save);
		
		$info_message = '
		<html>
		<body>
		<table style="" cellpadding="15px" cellspacing="0" border="1">
		<tr>
			<td width="80px">Name</td>
			<td>'.$name.'</td>
		</tr>
		<tr>
			<td width="80px">Email</td>
			<td>'.$email.'</td>
		</tr>
		<tr>
			<td width="80px">Phone</td>
			<td>'.$phone.'</td>
		</tr>
		<tr>
			<td width="80px">Subject</td>
			<td>'.$subject.'</td>
		</tr>
		<tr>
			<td width="80px">Budget</td>
			<td>'.$budget.'</td>
		</tr>
		<tr>
			<td width="80px">Deadline</td>
			<td>'.$deadline.' hari</td>
		</tr>
		<tr>
			<td width="80px">Message</td>
			<td>'.$message.'</td>
		</tr>
		</table>
		</body>
		</html>
		';
		// echo($info_message);die;
		$to = 'rusdi.karsandi@gmail.com';
		$headers = "From: Admin Grevia<noreply@grevia.com> \r\n";
		$headers .= "Reply-To: \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		if (!is_internal()) {
			$send = mail($to,$subject,$info_message,$headers);
		} else {
			$send = TRUE;
		}
		
		if ($send)
			$gInfo = "Thank you for your email, we will respond in 2 x 24 hour.";
		else
			$gInfo = "Sorry, error connection. Please try again.";
	}
}

?>

<style>
 #map {
	height: 300px;
	width : 100%;
	padding: 15px ;
 }

</style>
<div class="row">
	<div class="col-sm-12">
		Silakan mengisi form dibawah ini untuk menghubungi tim kami. <i class="fa fa-question-circle pointer" data-toggle="popover" data-trigger="hover" title="Panduan Mengisi" data-content="Silakan mengisi seluruh field dengan lengkap"></i>
		<br/><br/><?php if (is_filled($gInfo)) echo message($gInfo)."";?>
		<form method="post">
		
		
		<?php 
		$list_category_website = array('E-commerce','Company Profile','Personal','Lainnya');
		?>
		<select name="tCategoryWebsite" class="input wdtFul" placeholder="Jenis website yang ingin dibuat">
			<option>Jenis website yang ingin dibuat</option>
			<?php 
			foreach ($list_category_website as $cw) {
				?>
			<option><?php echo $cw?></option>
				<?php 
			}
			?>
		</select><br/><br/>
		
		<div class="row">
			<div class="col-sm-6">
				<input type="text" name="tName" class="input wdtFul" placeholder="Nama anda" required />
			</div>
			<div class="col-sm-6">
				<input type="email" name="tEmail" class="input wdtFul" class="input round bdrGry" placeholder="Email anda"/>
			</div>
			<div class="col-sm-12"><br/></div>
			
			<div class="col-sm-6">
				<input type="text" name="tPhone" class="input wdtFul numeric" class="input round bdrGry" placeholder="No Hp anda" required />
			</div>
			<div class="col-sm-6">
				<input type="text" name="tSubject" class="input wdtFul" placeholder="Topik email"/>
			</div>
			<div class="col-sm-12"><br/></div>
			
			<!--
			<div class="col-sm-6">
			</div>
			<div class="col-sm-6">
			</div>
			<div class="col-sm-12"><br/></div>
			-->
			
			<div class="col-sm-6">
			<select name="tBudget" class="input wdtFul">
				<option>Budget</option>
				<?php 
				for($rp=2500000;$rp<=25000000;$rp++) {
					$start = $rp;
					$rp += 1000000;
					?>
				<option><?php echo format_money($start).' - '.format_money($rp)?></option>
				<?php 
					$rp--;
				}
				?>
			</select>
			</div>
			
			<div class="col-sm-6">
				<input type="number" name="tDeadline" class="input" placeholder="tenggat waktu" required /> hari
			</div>
			
			<div class="col-sm-12"><br/></div>
		</div>
		
		<textarea placeholder="Pesan *Opsional" name="tMessage" rows="8" class="input wdtFul"></textarea><br/><br/>
		
		<input type="hidden" name="hdnSubmit" value="1"/>
		<input type="submit" class="btn btn-success" value="Submit"/>
		</form><br/>
	</div>
</div>

<script>
$(document).ready(function(){
	$('img').addClass('img-responsive');
})
</script>