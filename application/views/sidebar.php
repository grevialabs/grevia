<div>
	<?php 
	$attr['paging'] = TRUE;
	$attr['limit'] = 0;
	$attr['offset'] = 5;
	$data = $this->article_model->get_list($attr);
	$list_new_article = $data['data'];

	// GRAB PAKE API NIH
	// $data = $this->common_model->curl('api/forumtopic');
	// $data = (array) json_decode($data);
	// var_dump($data['total_rows']);die;
	
	if (!empty($list_new_article))
	{
	?>
	<div class="panel panel-primary">
		<div class="panel-heading">Artikel Terbaru</div>
		<div segment="new-article" source="sidebar">
			<ul class="list-group" style="margin-bottom:0">
			<?php
			$i = 1;
			foreach ($list_new_article as $key => $rs)
			{
			?>
			<li class="list-group-item" ><a tracker_name="url-<?php echo $i?>" class="adsTrack" href="<?php echo base_url().'article/'.$rs['article_id'].'/'.$rs['slug']?>?ref=top_newart" title="Artikel <?php echo $rs['title']?>"><i class="fa fa-book fa-fw"></i>&nbsp; <?php echo $rs['title']?></a></li>
			<?php
				$i++;
			}
			?>
			</ul>
		</div>
	</div>
	<?php
	}
	?>
</div>

<?php
	$list_quotes = array(
		array("Tidak pernah ada waktu yang tepat untuk memulai selain sekarang.","ANONIM"), 
		array("Life isn't about finding yourself...Life is about creating yourself.","ANONIM"),
		array("Forgiveness is not something we do for other people..It's something we do for ourselves to move on.","ANONIM"),
		array("Challenges are what make life interesting, overcoming them is what makes life meaningfull.","ANONIM"),
		array("Life is about laughing and living in a good and bad times, getting through whatever comes our way and looking back with a smile.","ANONIM"),
		array("Learn from yesterday..Live for today..Hoper for tomorrow.","ANONIM"),
		array("Do what you love and love what you do.","Confucius"),
		array("A friend is one who has the same enemies as you have.","ANONIM"),
		array("Character is like a tree and reputation like a shadow. The shadow is what we think of it; the tree is the real thing.","ANONIM"),
		array("Choose a job you love, and you will never have to work a day in your life","Confucius"),
		array("Everything has beauty, but not everyone sees it.","ANONIM"),
		array("If you make a mistake and do not correct it, this is called a mistake.","ANONIM"),
		array("Wherever you go, go with all your heart.","ANONIM"),
		array("It does not matter how slowly you go as long as you do not stop.","ANONIM"),
		array("I hear and I forget. I see and I remember. I do and I understand","Confucius"),
		array("Respect yourself and others will respect you.","ANONIM"),
		array("Forget injuries, never forget kindnesses.","Confucius"),
		array("Give a bowl of rice to a man and you will feed him for a day. Teach him how to grow his own rice and you will save his life.","ANONIM"),
		array("Hold faithfulness and sincerity as first principles.","Confucius"),
		array("When you see a good person, think of becoming like her/him. When you see someone not so good, reflect on your own weak points.","ANONIM"),
		array("The gem cannot be polished without friction, nor man perfected without trials.","ANONIM"),
		array("Don't pray for easy lives. Pray for stronger heart.","ANONIM")
	);
	$random_int = rand(0,count($list_quotes)-1);
?>

<div>
	<h4 style="background-color:#0089C9;color:white" class="padLrg fntMed">StartUp Quotes by Grevia</h4>
	<div id="sidebar-quote" class="i" style="border-left: 1px solid grey;color:grey; padding-left:8px">
	<i class="clrGry fa fa-quote-left"></i>
	<?php echo $list_quotes[$random_int][0]?>
	<div class="b padMed talRgt">-<?php echo $list_quotes[$random_int][1]?>-</div>
	</div>
</div><br/>

<div class=" clrWht fntBld padLrg" style="background-color:#0089C9">Artikel Kategori</div>
<div class="bg-info">
	<div class="padMed <?php if (!$this->uri->segment(2) || $this->uri->segment(2)=='all') echo 'bg-warning';?>">
	<a class="" href="<?php echo base_url()?>articlecategory/all"><i class="glyphicon glyphicon-tag"></i>&nbsp; All</a>
	</div>
<?php 
  $list_article_category = $this->articlecategory_model->get_list();
  $list_article_category = $list_article_category['data'];
  foreach($list_article_category as $rsr) 
  {
  ?>
	<div class="padMed <?php if($this->uri->segment(2) == $rsr['slug']) echo 'bg-warning'?>">
	<a class="" alt="Artikel Kategori <?php echo $rsr['category_name']?>" title="Artikel Kategori Startup" href="<?php echo base_url().'articlecategory/'.$rsr['slug']?>?ref=side_art"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> <?php echo $rsr['category_name']?></a>
	</div>
<?php } ?>
</div>
<!---
<div class="list-group">
	<a class="list-group-item <?php if (!$this->uri->segment(2) || $this->uri->segment(2)=='all') echo 'active';?>" href="<?php echo base_url()?>articlecategory/all"><i class="fa fa-home fa-fw"></i>&nbsp; All</a>
  <?php 
  // $list_article_category = $this->articlecategory_model->get_list();
  // $list_article_category = $list_article_category['data'];
  foreach($list_article_category as $rsr) 
  {
  ?>
  <a class="list-group-item <?php if($this->uri->segment(2) && $this->uri->segment(2) == $rsr['slug']) echo 'active';?>" href="<?php echo base_url().'articlecategory/'.$rsr['slug'];?>"><i class="fa fa-home fa-fw"></i>&nbsp; <?php echo $rsr['category_name']?></a>
  <?php } ?>
</div>
-->
<br/>