<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Demo Page';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-9 bgWht min-height-250 mockup">
				<div>
					<h2>Tracking Form</h2>
				</div>

				<table class="">
				<thead>
					<tr>
						<td>Nama</td>
						<td></td>
					</tr>
					<tr>
						<td class="b">Destination</td>
						<td>
						<select class="input select w250">
							<option>-Please Select-</option>
							<option>Admin</option>
							<option>Purchasing</option>
							<option>Driver</option>
						</select><br/><br/>
						<img class="img img-responsive" src="<?php echo base_url().'asset/images/'?>gmap-route1.png"/>
						</td>
					</tr>
				</thead>
				</table>
			
			</div>
			
			<div class="col-sm-3">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>