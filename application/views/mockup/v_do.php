<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Demo Page';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-10 bgWht min-height-250 mockup">
				<div>
					<h2>DO - Delivery Order</h2>
				</div>

				<table class="table table-striped">
				<thead>
					<tr>
						<td width="150px" class="b">DO Number</td>
						<td><input type="text" class="input w250" placeholder="DO number..." /></td>
					</tr>
					<tr>
						<td class="b">Role</td>
						<td>
						<select class="input select w250">
							<option>-Please Select-</option>
							<option>Admin</option>
							<option>Purchasing</option>
							<option>Driver</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="b">Driver</td>
						<td>
						<select class="input select w250">
							<option>-Please Select-</option>
							<option>Anto</option>
							<option>Budi</option>
							<option>Charlie</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="b">Shipment</td>
						<td>
						<select class="input select w250">
							<option>PT ABC JAKARTA</option>
							<option>PT XXX JOGJA</option>
							
						</select><br/><br/>
						<img class="img img-responsive" src="<?php echo base_url().'asset/images/'?>gmap-route1.png"/>
						</td>
					</tr>
					<tr>
						<td class="b">StartDate</td>
						<td><input type="text" class="input calendar w250" placeholder="Calendar show" /></td>
					</tr>
					<tr>
						<td class="b">PIC Client</td>
						<td>
						<select class="input select w250">
							<option>Didi</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="b">Approval User</td>
						<td>
						<select class="input select w250">
							<option>Director Andi</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="b talCnt"><button class="btn btn-sm btn-danger w250"><i class="fa fa-arrow-left"></i> Back </button></td>
						<td class="b talRgt"><button class="btn btn-sm btn-success w300">Save & Submit <i class="fa fa-arrow-right"></i></button></td>
					</tr>
				</thead>
				</table>
			
			</div>
			
			<div class="col-sm-2">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>