<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Demo Page';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-10 bgWht min-height-250 mockup">
				<div>
					<h2>Customer Form</h2>
				</div>

				<table class="table ">
				<thead>
					<tr>
						<td class="b">Company Name</td>
						<td><input type="text" class="input w250" placeholder="" /></td>
					</tr>
					<tr>
						<td class="b">NPWP</td>
						<td><input type="text" class="input w250" placeholder="" /></td>
					</tr>
					<tr>
						<td width="150px" class="b">PIC Name</td>
						<td><select class="input select w100">
							<option>Mr</option>
							<option>Ms</option>
						</select>
						
						<input type="text" class="input w250" placeholder="" /></td>
					</tr>
					<tr>
						<td class="b">PIC Phone</td>
						<td><input type="text" class="input w250" placeholder="" /></td>
					</tr>
					<tr>
						<td width="150px" class="b">Active</td>
						<td><input class="pointer" type="checkbox" id="chk_show_cashback" data-size="small" data-toggle="toggle" data-on="yes" data-off=" no" data-onstyle="primary" tabindex="3" /><br/></td>
					</tr>
					<tr>
						<td class="b talCnt"><button class="btn btn-sm btn-danger w250"><i class="fa fa-arrow-left"></i> Back </button></td>
						<td class="b talRgt"><button class="btn btn-sm btn-success w300">Save & Submit <i class="fa fa-arrow-right"></i></button></td>
					</tr>

				</thead>
				</table>
			
			</div>
			
			<div class="col-sm-2">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>