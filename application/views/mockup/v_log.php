<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Demo Page';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-10 bgWht min-height-250 mockup">
				<div>
					<h2>User Form</h2>
				</div>

				<table class="">
				<thead>
					<tr>
						<td>Nama</td>
						<td></td>
					</tr>
					<tr>
						<td>Role</td>
						<td>
						<select>
							<option>-Please Select-</option>
							<option>Admin</option>
							<option>Purchasing</option>
							<option>Driver</option>
						</select>
						</td>
					</tr>
				</thead>
				</table>
			
			</div>
			
			<div class="col-sm-2">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>