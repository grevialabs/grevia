<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Demo Page';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-10 bgWht min-height-250 mockup">
				<div>
					<h2>Product List</h2>
				</div>
				
				<table class="table table-bordered table-striped">
					<thead>
						<tr class="alert alert-warning">
							<td width="1px" class="b">#</td>
							<td>Name<br/> <input type="text" class="input" placeholder="..." /> <button class="btn btn-sm btn-info"><i class="fa fa-search"></i></button></td>
							<td>Phone<br/> <input type="text" class="input" placeholder="..." /> <button class="btn btn-sm btn-info"><i class="fa fa-search"></i></button></td>
							<td>Role<br/> <input type="text" class="input" placeholder="..." /> <button class="btn btn-sm btn-info"><i class="fa fa-search"></i></button></td>
							<td width="80px" class="talCnt">Option</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>blabla</td>
							<td>blabla</td>
							<td>blabla</td>
							<td class="talCnt">
							<a href="#" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-lg" onclick="return doConfirm()"></i></a> 
							<a href="#" onclick=""><i class="clrRed fa fa-times fa-lg" title="Delete data" alt="Delete data"></i></a></td>
						</tr>
						<tr>
							<td>2</td>
							<td>blabla</td>
							<td>blabla</td>
							<td>blabla</td>
							<td class="talCnt">
							<a href="#" title="Edit data" alt="Edit data"><i class="clrBlu fa fa-pencil-square-o fa-lg" onclick="return doConfirm()"></i></a> 
							<a href="#" onclick=""><i class="clrRed fa fa-times fa-lg" title="Delete data" alt="Delete data"></i></a></td>
						</tr>
					</tbody>
				</table>
				
				<hr/>
				<h2>User Form</h2>
				<table class="table ">			
					<tr>
						<td width="150px" class="b">Nama</td>
						<td><input type="text" class="input w250" placeholder="" /></td>
					</tr>
					<tr>
						<td class="b">Phone</td>
						<td><input type="text" class="input w250" placeholder="" /></td>
					</tr>
					<tr>
						<td class="b">Role</td>
						<td>
						<select class="input select w250">
							<option>-Please Select-</option>
							<option>Admin</option>
							<option>Purchasing</option>
							<option>Driver</option>
						</select>
						</td>
					</tr>
					<tr>
						<td width="150px" class="b">Active</td>
						<td><input class="pointer" type="checkbox" id="chk_show_cashback" data-size="small" data-toggle="toggle" data-on="yes" data-off=" no" data-onstyle="primary" tabindex="3" /><br/></td>
					</tr>
					<tr>
						<td class="b talCnt"><button class="btn btn-sm btn-danger w250"><i class="fa fa-arrow-left"></i> Back </button></td>
						<td class="b talRgt"><button class="btn btn-sm btn-success w300">Save & Submit <i class="fa fa-arrow-right"></i></button></td>
					</tr>
				</table>
			
			</div>
			
			<div class="col-sm-2">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>