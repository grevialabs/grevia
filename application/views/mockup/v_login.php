<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Login User';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-10 bgWht min-height-250 mockup">
				<div>
					<h2><?php echo $PAGE_HEADER ?></h2>
					<form class='form-horizontal' role='form' method='post'>
					<div class='form-group form-group-sm'>
						<div class='col-sm-12'><?php echo EMAIL;?><input type='email' class='form-control' name='l_email' id='l_email' placeholder='<?php echo EMAIL?>' value='<?php if ($this->input->post('l_email')) echo filter(post('l_email'))?>' required></div>
					</div>
					<div class='form-group form-group-sm'>
						<div class='col-sm-12'><?php echo PASSWORD;?><br/><input type='password' class='form-control' name='l_password' id='l_password' placeholder='<?php echo PASSWORD?>' required></div>
					</div>
					<div class='form-group'>
						<div class='col-sm-12'>
							<div class='talRgt'><a href="<?php echo base_url()?>#"><?php echo MENU_FORGOT_PASSWORD?></a> | <a href="<?php echo base_url()?>#"><?php echo MENU_REGISTER?></a></div><br/>
						
							<input type="hidden" name="hdnLogin" value="1"/>
							<button type="submit" value="" class="btn btn-success wdtFul"/><?php echo LOGIN?></button>
						</div>
					</div>
				</form>
				</div>
			
			</div>
			
			<div class="col-sm-2">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>