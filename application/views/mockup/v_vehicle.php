<?php
global $PAGE, $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB;

$PAGE = 'Demo Page';
// $BREADCRUMB = $this->common_model->breadcrumb(NULL, $PAGE);
$PAGE_TITLE = $PAGE.' - '.SITE_NAME;
$PAGE_HEADER = $PAGE;
?>
<div class="jumbotron bgPch" >
	<div class="container">
		<div class="row">
			
			<div class="col-sm-10 bgWht min-height-250 mockup">
				<div>
					<h2>Vehicle Master</h2>
				</div>

				<table class="table table-striped">
				<thead>
					<tr>
						<td class="b" width="200px">Name</td>
						<td><input type="text" class="input w250" placeholder="Joni..." /></td>
					</tr>
					<tr>
						<td class="b">Plate No</td>
						<td><input type="text" class="input w250" placeholder="ex: B 8852 xxx" /></td>
					</tr>
					<tr>
						<td class="b">PIC Client</td>
						<td>
							<select class="input select w250">
								<option>Didi</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="b">Next Yearly tax Reminder</td>
						<td><input type="text" class="input w250" placeholder="Show Calendar" /></td>
					</tr>
					<tr>
						<td class="b">Next Yearly tax Reminder</td>
						<td><input type="text" class="input w250" placeholder="Show Calendar" /></td>
					</tr>
					<tr>
						<td class="b">Next Service Date</td>
						<td><input type="text" class="input w250" placeholder="Show Calendar" /></td>
					</tr>
					<tr>
						<td class="b">Role</td>
						<td>
						<select class="input select w250">
							<option>-Please Select-</option>
							<option>Truck1</option>
							<option>Truck1</option>
							<option>Driver</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="b">Dimension</td>
						<td><input type="text" class="input w50" placeholder="P" /> X <input type="text" class="input w50" placeholder="L" /> X <input type="text" class="input w50" placeholder="T" /></td>
					</tr>
					<tr>
						<td class="b talCnt"><button class="btn btn-sm btn-danger w250"><i class="fa fa-arrow-left"></i> Back </button></td>
						<td class="b talRgt"><button class="btn btn-sm btn-success w300">Save & Submit <i class="fa fa-arrow-right"></i></button></td>
					</tr>
				</thead>
				</table>
			
			</div>
			
			<div class="col-sm-2">
				<?php
				// $this->load->view('mockup/sidebar',NULL,TRUE)
				echo $SIDEBAR;
				?>
			</div>
		</div>
	</div>
</div>