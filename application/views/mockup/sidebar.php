<?php 
$list_master = array(
		array("Login Area","login","phone"), 
		array("Role","role","pencil"), 
		array("User","user","user"), 
		array("Customer","customer","building"), 
		array("Vehicle","vehicle","car"), 
		array("Product","product","book"),
		array("Vendor","vendor","users"),
		array("Finance&Tax","finance","money"),
);

$list_trx = array(
		array("PurchaseOrder","po","folder"),
		array("DeliveryOrder","do","truck"),
		array("Tracking","tracking","train"),
		array("Log Activity","log","tasks"),
);

$list_dashboard = array(
		array("Global","rpt_global","globe"), 		
		array("Sales","rpt_sales","money"), 
		array("Vehicle","rpt_vehicle","car"), 		
);
?>
<div class="panel ">
	<div class="panel-heading alert-primary">
		MasterData
	</div>
	<div class="panel-content">
		<ul class="list-group" style="margin-bottom:0">
		<?php
		$i = 1;
		foreach ($list_master as $k => $rs)
		{
			$val = $rs[0];
			$slug = $rs[1];
			$icon = '';
			if (isset($rs[2])) $icon = $rs['2'];
		?>
		<li class="list-group-item <?php if ($this->uri->segment(2) == $slug) echo " alert alert-info"; ?>" ><a href="<?php echo base_url().'mockup/'.$slug?>" title="Artikel <?php echo $val?>"><i class="fa fa-<?php echo $icon?> fa-fw"></i>&nbsp; <?php echo $val?></a></li>
		<?php
			$i++;
		}
		?>
		</ul>
	</div>
</div>

<div class="panel ">
	<div class="panel-heading alert-success">
		Trx
	</div>
	<div class="panel-content">
		<ul class="list-group" style="margin-bottom:0">
		<?php
		$i = 1;
		foreach ($list_trx as $k => $rs)
		{
			$val = $rs[0];
			$slug = $rs[1];
			$icon = '';
			if (isset($rs[2])) $icon = $rs['2'];
		?>
		<li class="list-group-item <?php if ($this->uri->segment(2) == $slug) echo " alert alert-info"; ?>" ><a href="<?php echo base_url().'mockup/'.$slug?>" title="Artikel <?php echo $val?>"><i class="fa fa-<?php echo $icon?> fa-fw"></i>&nbsp; <?php echo $val?></a></li>
		<?php
			$i++;
		}
		?>
		</ul>
	</div>
</div>

<div class="panel ">
	<div class="panel-heading alert-danger">
		Dashboard
	</div>
	<div class="panel-content">
		<ul class="list-group" style="margin-bottom:0">
		<?php
		$i = 1;
		foreach ($list_dashboard as $k => $rs)
		{
			$val = $rs[0];
			$slug = $rs[1];
			$icon = '';
			if (isset($rs[2])) $icon = $rs['2'];
		?>
		<li class="list-group-item <?php if ($this->uri->segment(2) == $slug) echo " alert alert-info"; ?>" ><a href="<?php echo base_url().'mockup/'.$slug?>" title="Artikel <?php echo $val?>"><i class="fa fa-<?php echo $icon?> fa-fw"></i>&nbsp; <?php echo $val?></a></li>
		<?php
			$i++;
		}
		?>
		</ul>
	</div>
</div>