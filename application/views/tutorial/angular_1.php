<!DOCTYPE html>
<html lang="en-US">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<body>

<div ng-app="">
  <p>Name : <input type="text" ng-model="name"></p>
  <h1>Hello {{name}}</h1>

  <div ng-init="names=['Jani','Hege','Kai']">
    <ul>
        <li ng-repeat="x in names">
        {{ x }}
        </li>
    </ul>
  </div>

</div>

</body>
</html>