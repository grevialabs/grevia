<div class="row">
	<div class="col-md-10 talJst">
      <h3>About Load Balancing</h3>
	  <a href="https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-6-with-yum">https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-6-with-yum</a>
	  <a href="https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7">https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7</a>

Install Nginx 

<pre>
sudo yum install epel-release
</pre><br/>

<p>Loadbalancing is a useful mechanism to distribute incoming traffic around several capable Virtual Private servers.By apportioning the processing mechanism to several machines, redundancy is provided to the application -- ensuring fault tolerance and heightened stability. The Round Robin algorithm for load balancing sends visitors to one of a set of IPs. At its most basic level Round Robin, which is fairly easy to implement, distributes server load without implementing considering more nuanced factors like server response time and the visitors’ geographic region.</p>

<div name="setup" data-unique="setup"></div><h2>Setup</h2>

<p>The steps in this tutorial require the user to have root privileges on your VPS. You can see how to set that up in the  <a href="https://www.digitalocean.com/community/articles/how-to-add-and-delete-users-on-ubuntu-12-04-and-centos-6">Users Tutorial</a>.</p> 

<p>Prior to setting up nginx loadbalancing, you should have nginx installed on your VPS. You can install it quickly with apt-get:</p>

<pre>sudo apt-get install nginx</pre>

<div name="upstream-module" data-unique="upstream-module"></div><h2>Upstream Module</h2>

<p>In order to set up a round robin load balancer, we will need to use the nginx upstream module. We will incorporate the configuration into the nginx settings.</p> 

<p>Go ahead and open up your website’s configuration (in my examples I will just work off of the generic default virtual host):</p>

<pre>sudo nano /etc/nginx/conf.d/default.conf</pre>
or
<pre>sudo nano /etc/nginx/sites-available/default</pre>

<p>We need to add the load balancing configuration to the file.</p>

<p>First we need to include the upstream module which looks like this:</p>

<pre>upstream backend  {
  server duitlo.com;
  server grevia.com;
}</pre>

<p>We should then reference the module further on in the configuration:</p>

<pre> server {
  location / {
    proxy_pass  http://backend;
  }
}</pre>

<p>Restart nginx:</p>

<pre>sudo service nginx restart</pre>

<p>As long as you have all of the virtual private servers in place you should now find that the load balancer will begin to distribute the visitors to the linked servers equally.</p>

<div name="directives" data-unique="directives"></div><h2>Directives</h2>

<p>The previous section covered how to equally distribute load across several virtual servers. However, there are many reasons why this may not be the most efficient way to work with data. There are several directives that we can use to direct site visitors more effectively. </p> 

<h3>Weight</h3>

<p>One way to begin to allocate users to servers with more precision is to allocate specific weight to certain machines. Nginx allows us to assign a number specifying the proportion of traffic that should be directed to each server.</p> 

<p>A load balanced setup that included server weight could look like this:</p>

<pre>upstream backend  {
  server backend1.example.com weight=1;
  server backend2.example.com weight=2;
  server backend3.example.com weight=4;
}</pre>

<p>The default weight is 1. With a weight of 2, backend2.example will be sent twice as much traffic as backend1, and backend3, with a weight of 4, will deal with twice as much traffic as backend2 and four times as much as backend 1.</p> 

<h3>Hash</h3>

<p>IP hash allows servers to respond to clients according to their IP address, sending visitors back to the same VPS each time they visit (unless that server is down). If a server is known to be inactive, it should be marked as down. All IPs that were supposed to routed to the down server are then directed to an alternate one.</p> 

<p>The configuration below provides an example:</p>

<pre>upstream backend {
  ip_hash;
  server   backend1.example.com;
  server   backend2.example.com;
  server   backend3.example.com  down;
 }</pre>

<h3>Max Fails</h3>
<p>According to the default round robin settings, nginx will continue to send data to the virtual private servers, even if the servers are not responding. Max fails can automatically prevent this by rendering unresponsive servers inoperative for a set amount of time.</p> 

<p>There are two factors associated with the max fails: max_fails and fall_timeout. Max fails refers to the maximum number of failed attempts to connect to a server should occur before it is considered inactive. 
Fall_timeout specifies the length of that the server is considered inoperative. Once the time expires, new attempts to reach the server will start up again. The default timeout value is 10 seconds.</p>

<p>A sample configuration might look like this:</p> 

<pre>upstream backend  {
  server backend1.example.com max_fails=3  fail_timeout=15s;
  server backend2.example.com weight=2;
  server backend3.example.com weight=4;</pre>

<div name="see-more" data-unique="see-more"></div><h2>See More</h2>

<p>This has been a short overview of simple Round Robin load balancing. Additionally, there are other ways to speed and optimize a server:</p>

<ul><li><a href="https://www.digitalocean.com/community/articles/how-to-configure-nginx-as-a-front-end-proxy-for-apache">How to Configure Nginx as a Front End Proxy for Apache</a></li>

<li><a href="https://www.digitalocean.com/community/articles/how-to-install-and-configure-varnish-with-apache-on-ubuntu-12-04--3">How to Install and Configure Varnish with Apache on Ubuntu 12.04</a></li>

<li><a href="https://www.digitalocean.com/community/articles/how-to-install-and-use-memcache-on-ubuntu-12-04">How to Install and Use Memcache on Ubuntu 12.04</a></li>

<li><a href="https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-6-with-yum">How to Install Nginx centos 6</a></li>
</ul>

<div class="author">By Etel Sverdlov</div>

	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>