<div class="row">
	<div class="col-md-10 talJst">
Reference
<a href='http://idroot.net/tutorials/how-to-install-redis-on-centos-6/'>Redis</a>

Step 1 : enable repo for redis
<pre>
## RHEL/CentOS 6 64-Bit ##
# wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
# rpm -ivh epel-release-6-8.noarch.rpm
</pre>

Step 2 : update and install redis with pecl (driver extension)
<pre>
yum -y update
yum install redis php-pecl-redis
</pre>

jalankan server redis & set autostart
<pre>
service redis start
chkconfig redis on
</pre>

untuk cek redis sudah berjalan atau tidak, jalankan command ini, jika sukses maka outputnya akan berupa : <b>PONG</b>
<pre>
redis-cli ping
</pre>

Step 3 : Install Redis Extension
<pre>
pecl install redis
</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>