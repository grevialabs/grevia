<div class="row">
	<div class="col-md-10 talJst">
	
    <h2>Setup server Centos</h2>

	<a href="http://www.cyberciti.biz/faq/linux-unix-nginx-redirect-all-http-to-https/" target="_blank" /> Setup Nginx http to https</a>

<h3 class="">Basic command</h3>
shutdown server (halt)
<pre>
shutdown -h now
</pre>

restart server now (reload)
<pre>
shutdown -r now
</pre>

<h3 class="">Pre requisite - Install Nano & Subversion</h3>
Install nano & subversion dulu
<pre>
sudo yum install nano subversion -y
</pre><br/>

<h3>Set Nano as default crontab</h3>
buka file berikut
<pre>
nano /etc/bashrc
</pre><br/>

tambahkan line berikut di line paling bawah<br/>
<pre>
export EDITOR="nano"
</pre><br/>

lalu logout dan login lagi. untuk lihat hasilnya<br/>
<pre>
crontab -e
</pre>
dan voilla... crontab sudah menggunakan nano<br/>


<!-- ------------------------------------------------------------------------------------------ -->
<h2 class="clrRed">ADD Login ssh dengan Public Key</h2>

Jika server sudah running dan server belum/ ingin ditambahkan login dengan Private Key silakan baca tutorial ini<br/>

Login SSH dan jalankan command ini
<pre>
nano /root/.ssh/authorized_keys
</pre>

lalu copaskan isi dari script public key yang berawalan -> <b>rsa_blablablabalbla</b> ke file ini
<pre>
nano /etc/ssh/sshd_config
</pre>

rsa key ini bisa lebih dari 1, dipisahkan dengan new line per key seperti berikut
<pre>
rsa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
rsa yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
</pre>

Untuk melihat hasilnya, jangan lupa restart service
<pre>
/etc/init.d/sshd restart
</pre>

<br/>

<h2 class="clrRed">DISABLE login SSH dengan password</h2>

Jika anda telah menggunakan private key untuk login ke SSH, maka sebaiknya login dengan password diblok untuk mengamankan server dari akses dan menghindari brute force.<br/>

Buka settingan config SSH
<pre>
nano /etc/ssh/sshd_config
</pre>

tekan ctrl+w, lalu cari PermitRootLogin, uncomment dan ubah valuenya jadi <br/>
<pre>
PermitRootLogin <span class="clrRed">without-password</span>
</pre>

Untuk ubah port default misalnya menjadi 222
<pre>
Port <span class="clrRed">222</span>
</pre>
<br/>

jika sudah maka reload service ssh
<pre>
/etc/init.d/sshd restart
</pre>

<h3>Install Plugin Image GD</h3>
<pre>
yum install gd gd-devel php-gd -y
</pre>

lalu restart nginx
<pre>
/etc/init.d/nginx restart
</pre>
<!-- ------------------------------------------------------------------------------------------ -->
<h2 class="clrRed">Instalasi Webserver NGINX, PHP-FPM, & Timezone</h2>

<h3 class="">Step One - Install EPEL</h3>
EPEL stands for Extra Package Entreprise Linux<br/>
<pre>
sudo yum install epel-release
</pre>

<h3 class="">Step Two - Install Nginx</h3>
<pre>
sudo yum install nginx
</pre>

<h3 class="">Step Three - Start Nginx</h3>
<pre>
sudo /etc/init.d/nginx start
</pre>
<br/>
Congratulations you have start your Nginx in your domain.<br/><br/>

<!---->
<hr/>

<h3 class="">Step Four - Install PHP-FPM</h3>
PHP-FPM is a php render in server. You have to install it first<br/><br/>

<pre>
sudo yum install php-fpm php-mysql -y
</pre>

<h3 class="">Step Five - Start PHP-FPM</h3>
Start you php-fpm<br/>
<pre>
sudo /etc/init.d/php-fpm start
</pre>

<h3 class="">Step Six - Checkout repository</h3>
Checkout in your root nginx directory to setup you script
<pre>
cd /usr/share/nginx/html
svn co http://www.grevia.com/repository .
</pre>
you will be asked for username & password for subversion<br/>

*tambahkan jika perlu extended_config.php

<h3 class="">Step Six - Checkout repository</h3>

Buka 
<pre>
nano /etc/nginx/conf.d/default.conf
</pre>

isi dengan ini
<pre>
#
server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  s1.grevia.com;
    error_log /var/log/nginx/error_nginx.log;

    # Load configuration files for the default server block.
    include /etc/nginx/default.d/*.conf;

    location / {
        root   /usr/share/nginx/html;
        index index.php index.html index.htm;
        try_files $uri $uri/ /index.php;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    location ~ \.php$ {
        root /usr/share/nginx/html;
        index index.php index.html index.htm;
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include        fastcgi_params;
        try_files $uri $uri/ /index.php;
    }

    #location ~ \.php {
    #    include /etc/nginx/common/php.conf;
    #}

    #sementara jangan pake dlu
    #error_page 404 /404.html;
    #    location = /40x.html {
    #}

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }
}
</pre>


<!-- HELP CONFIGURATION -->
<hr/>

<h3 class="">Create bash restart nginx PHP-FPM</h3>
buat file bernama <b>restart_nginx_fpm.sh</b>
<pre>
nano /home/restart_nginx_fpm.sh
</pre>

Isi dengan ini lalu save
<pre>
/etc/init.d/php-fpm restart && /etc/init.d/nginx restart
</pre>

Run command for executable
<pre>
chmod +x /home/restart_nginx_fpm.sh
</pre>

<h3 class="">Error Code Igniter Session</h3>
If showing error Session then run this command<br/>
<pre>
mkdir /var/lib/php/session
chmod -R 777 /var/lib/php/session
</pre>

<h3 class="">Set PHP-FPM untuk merunning Nginx</h3>
Buka konfigurasi dengan
<pre>
nano /etc/php-fpm.d/www.conf
</pre>

Jika sudah, ketik ctrl+W, lalu cari "http" atau "apache"
<pre>
; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
; RPM: apache Choosed to be able to access some dir as httpd
user = nginx
; RPM: Keep a group allowed to write in log dir.
group = nginx
</pre>

dan ganti dengan user = <b>nginx</b> dan group = <b>nginx</b><br/>

lalu buka
<pre>
nano /etc/nginx/nginx.conf
</pre>

ganti line ini:
<pre>
worker_processes auto;
</pre>
Ganti <b>Auto</b> dengan <b>4</b> untuk blabla

Restart Nginx
<pre>
/home/restart_nginx_fpm.sh
</pre>
And voila... Server sudah berjalan normal<br/>


<h3 class="">Error Timezone</h3>
if error timezone show then run this
<pre>
nano /etc/php.ini
</pre>

Then tekan ctrl+w : cari "<b>timezone</b>" ubah jadi Asia/Jakarta
<pre>
[Date]; Defines the default timezone used by the date functions                           
; http://www.php.net/manual/en/datetime.configuration.php#ini.date.timezone
date.timezone = Asia/Jakarta                                                        
</pre>

<h3 class="">Set Timezone MYSQL</h3>
Untuk mengatur timezone UTC di mysql
<pre>
nano /etc/my.cnf
</pre>

Ketikan ini di dalam sintaks <b>[mysqld]</b>
<pre>
default_time_zone='+07:00'
</pre>

Set Autostart of Nginx, Php FPM and Mysql
<pre>
sudo chkconfig --levels 235 mysqld on
sudo chkconfig --levels 235 nginx on
sudo chkconfig --levels 235 php-fpm on
</pre>

<h3 class="">Create bash auto svn up</h3>
Create file <b>update.sh</b> in your home directory
<pre>
nano /home/update.sh
</pre>

Filled <b>update.sh</b> with this for svn update
<pre>
svn up /usr/share/nginx/html/
</pre>

Run command for executable
<pre>
chmod +x /home/update.sh
</pre>

<h3>Set Cron auto update subversion</h3>
Untuk mengupdate script tiap ada perubahan / commit ke server repository:
<pre>
crontab -e
</pre>

Set update script tiap 2 menit sekali via cron
<pre>
#cron
*/2 * * * * /home/update.sh
</pre>

Voilla, script akan mengupdate otomatis

<h3>Install imagick</h3>
Imagick untuk image manipulation atau cropping
<pre>
 -- install agar bisa membaca pecl
 yum install php-pear
</pre>

<pre> 
 -- install untuk membaca error phpize
 sudo yum --noplugins install php-devel
</pre>
 
<pre>
 -- install untuk mengsolve error imagick c dependency
 yum install ImageMagick-devel
 yum intall gcc
</pre>

<pre> 
 -- install imagick php 5.3
 pecl install imagick-3.1.2
</pre>
	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>