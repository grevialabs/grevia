<style>
pre{background:#ededed;color:#000}
</style>

<div class="row">
	<div class="col-md-10 talJst">
      <h3>Tutorial MYSQL</h3>

http://www.cyberciti.biz/tips/how-do-i-enable-remote-access-to-mysql-database-server.html<br/><br/>
https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql<br/><br/>
http://xmodulo.com/how-to-allow-remote-access-to-mysql.html<br/>

<h2> Mysql Installation</h2>

install mysql dengan command:
<pre>
sudo yum install mysql-server
</pre>

Jika sudah maka jalankan servicenya : 
<pre>
sudo /etc/init.d/mysqld restart
</pre>

Untuk mengamankan dan settting password:
<pre>
sudo /usr/bin/mysql_secure_installation
</pre>

setelah itu silakan ikuti panduan nya<br/>

Untuk mysql bisa diakses remote via Heidi SQL<br/>
buka file config mysql
<pre>
nano /etc/my.cnf
</pre>

comment line berikut sebelum <b>[mysqld safe]</b>
<pre>
#bind-address           = 127.0.0.1
#skip-networking

atau 

port=3306
#bind-address=0.0.0.0
bind-address=139.59.253.205
# skip-networking
</pre>

Login ke dalam mysql via cmd
<pre>
mysql -u root -p mysql
</pre>

lalu create database misal: anu
<pre>
CREATE DATABASE anu;
</pre>

lalu ketikkan ini untuk memberi akses ke user berdasarkan password
<pre>
GRANT ALL ON anu.* TO 'root'@'%' IDENTIFIED BY 'PASSWORDKAMU';
</pre>

Jikasudah reload dengan
<pre>
mysql> FLUSH PRIVILEGES;
</pre>

<!-- ================================= -->
<h2>Create user baru di mysql</h2>

Untuk membuat user baru, login dahulu ke mysql via console dan jalankan ini.
<pre>
CREATE USER '<span class="clrRed">newuser</span>'@'localhost' IDENTIFIED BY '<span class="clrRed">password</span>';
</pre>

Lalu, bukakan aksesnya dengan ini
<pre>
GRANT ALL PRIVILEGES ON * . * TO '<span class="clrRed">newuser</span>'@'localhost';
</pre>

Reload konfigurasi baru
<pre>
FLUSH PRIVILEGES;
</pre>

dan anda sudah bisa login mysql via Heidi

<h2> Mysql Replication</h2>

<p>
<a href="http://www.tecmint.com/how-to-setup-mysql-master-slave-replication-in-rhel-centos-fedora/">http://www.tecmint.com/how-to-setup-mysql-master-slave-replication-in-rhel-centos-fedora/</a><br/><br/>

Tutorial ini akan membantu membuat replikasi Mysql server dengan 2 IP yang berbeda misal :<br/><br/>

192.168.1.1 => MASTER<br/>
192.168.1.2 => SLAVE<br/>

https://www.digitalocean.com/community/tutorials/how-to-set-up-mysql-master-master-replication<br/>

<h3>Install MYSQL di Master server</h3><br/>

Instal mysql dengan command ini.<br/>
<pre># yum install mysql-server mysql</pre><br/>

Buka config mysql<br/>
<pre># nano /etc/my.cnf</pre><br/>

Dalam kolom <b>[mysqld]</b> ganti <span class="b clrRed">grevia</span> dengan nama database yang mau di replicate ke <b>Slave</b>
<pre>
server-id = 1
binlog-do-db=<span class="clrRed">grevia</span>
relay-log = /var/lib/mysql/mysql-relay-bin
relay-log-index = /var/lib/mysql/mysql-relay-bin.index
log-error = /var/lib/mysql/mysql.err
master-info-file = /var/lib/mysql/mysql-master.info
relay-log-info-file = /var/lib/mysql/mysql-relay-log.info
log-bin = /var/lib/mysql/mysql-bin
</pre> <br/>

Restart Mysql<br/>
<pre># /etc/init.d/mysqld restart</pre><br/>

Jika sudah, login ke mysql via cli dengan
<pre>
# mysql -u root -p
</pre><br/>

Berikan akses instruksi ke master db, dan ganti <b class="clrRed">root_user</b> ganti <b class="clrRed">your_password</b> dengan password mysql di master DB<br/>
<pre>
mysql> GRANT REPLICATION SLAVE ON *.* TO '<b class="clrRed">root_user</b>'@'%' IDENTIFIED BY '<b class="clrRed">your_password</b>';
mysql> FLUSH PRIVILEGES;
mysql> FLUSH TABLES WITH READ LOCK;
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000003 | 990      | grevia		 |                  |
+------------------+----------+--------------+------------------+
1 row in set (0.00 sec)
mysql> quit;
</pre><br/>

Catat File <b class="clrRed">mysql-bin.000003</b>& Position<b class="clrRed">990</b> karena nanti akan digunakan di server slave.<br/>
Lakukan export data untuk nanti server slave bisa meload data yang sama.
<pre>#  mysqldump -u root -p --all-databases --master-data > /root/dbdump.db</pre><br/>

Jika sudah, lakukan unlock<br/>
<pre>
mysql> UNLOCK TABLES;
mysql> quit;
</pre>

<!-- 
--------------------------------------------------------------------------
FASE 2 
--> 
<h3>Fase 2 Setting Slave server untuk replication</h3>

Jika di slave belum ada mysql, instal dahulu dengan yum<br/>
<pre>
# yum install mysql-server mysql
</pre><br/>

Buka mysql config<br/>
<pre>
# nano /etc/my.cnf
</pre><br/>

<pre>
server-id = 2
master-host=192.168.1.1
master-connect-retry=60
master-user=slave_user
master-password=yourpassword
replicate-do-db=tecmint
relay-log = /var/lib/mysql/mysql-relay-bin
relay-log-index = /var/lib/mysql/mysql-relay-bin.index
log-error = /var/lib/mysql/mysql.err
master-info-file = /var/lib/mysql/mysql-master.info
relay-log-info-file = /var/lib/mysql/mysql-relay-log.info
log-bin = /var/lib/mysql/mysql-bin
</pre><br/>


<!-- ------------------------------------- FASE 3 SLAVE TESTING -->
Setelah Slave berjalan, coba running query di DB Master untuk test apakah query sudah jalan.<br/>
<pre>
CREATE TABLE mahonara(
id int AUTO_INCREMENT PRIMARY KEY,
nama varchar(100) NULL
);

INSERT INTO mahonara(nama) VALUES('benga'), ('charlie'), ('dumdum');
</pre>
</p>
<div class="author">By Rusdi</div><br/>

	  </div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>