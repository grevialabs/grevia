<div class="row">
	<div class="col-md-10 talJst">

<h3>Crop Image</h3>
http://stackoverflow.com/questions/16778801/how-to-crop-image-using-ajax-jquery-in-codeigniter<br/>

Setup image permission<br/><br/>

Jalankan image temp writable di server script
<pre>
chmod 0755 /usr/share/nginx/html/temp/
chown nginx:nginx /usr/share/nginx/html/temp/ 
</pre>

lalu di server static enable permission di
<pre>
chmod 0755 /usr/share/nginx/html/temp/
chmod 0755 /usr/share/nginx/html/temp/files

chown nginx:nginx /usr/share/nginx/html/temp
chown nginx:nginx /usr/share/nginx/html/temp/files
</pre>

Untuk meng-crop image
<pre>
//crop it
$data['x'] = $this->input->post('x');
$data['y'] = $this->input->post('y');
$data['w'] = $this->input->post('w');
$data['h'] = $this->input->post('h');

$config['image_library'] = 'gd2';
//$path =  'uploads/apache.jpg';
$config['source_image'] = 'uploads/'.$data['user_data']['img_link']; //http://localhost/resume/uploads/apache.jpg
// $config['create_thumb'] = TRUE;
//$config['new_image'] = './uploads/new_image.jpg';
$config['maintain_ratio'] = FALSE;
$config['width']  = $data['w'];
$config['height'] = $data['h'];
$config['x_axis'] = $data['x'];
$config['y_axis'] = $data['y'];

$this->load->library('image_lib', $config); 

if(!$this->image_lib->crop())
{
	echo $this->image_lib->display_errors();
}  
redirect('profile');
</pre>

	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>