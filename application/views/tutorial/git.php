<style>
.comment{ color:red; font-weight:bold}
</style>

<div class="row">
	<!-- CONTENT START -->
	<div class="col-md-10 talJst">
    
	<h2>Setup server Centos</h2>


<h3 class="">Cara install git di Centos 6.4</h3>
buka terminal dan ketik:
<pre>
sudo yum install git
</pre>

setelah muncul sukses, maka command 'git' sudah bisa digunakan untuk checkout.<br/>
Untuk checkout pertama kali di server VPS, gunakan command dibawah ini 
<pre>
git clone https://github.com/GreviaLabs/git.grevia.com.git /
</pre>

Jika sudah berhasil maka akan ada tulisan done.

Untuk melakukan update, gunakan command pull seperti berikut.
<pre>
git pull origin master
</pre>

Help:<br/>
<a href="https://help.github.com/articles/adding-an-existing-project-to-github-using-the-command-line/" target="_blank">https://help.github.com/articles/adding-an-existing-project-to-github-using-the-command-line/</a><br/>
<a href="https://www.siteground.com/tutorials/siteground-git/clone-git-repository.htm" target="_blank">https://www.siteground.com/tutorials/siteground-git/clone-git-repository.htm</a>

<h3>Cara menyimpan credentials username dan password di linux</h3>	

Saat anda menggunakan git dan melakukan git command, biasanya git akan meminta credential username dan password, setiap session / command. Untuk menghindari hal tsb, kita bisa melakukan store credential di dalam cache server dengan cara melakukan command berikut.<br/><br/>
Tahap 1<br/>
<pre>
git config credential.helper store
</pre>
<br/>
Tahap 2<br/>
lalu lakukan command update biasa, dan anda akan diminta password dan disimpan secara permanen.
<pre>
git pull
</pre>

Voila.

Jika anda ingin melakukan reset credentials maka ulangi lagi tahap 1.<br/>
Sumber:  <a href="https://stackoverflow.com/questions/35942754/how-to-save-username-and-password-in-git">https://stackoverflow.com/questions/35942754/how-to-save-username-and-password-in-git</a>


<h3>Git Config</h3>

sesudah menginstall Git, ada baiknya kita mengeset informasi kita ke dalam config agar tracking info history tersimpan.
<pre>
git config --global user.name "<b class="clrRed">John Smith</b>" 
git config --global user.email <b class="clrRed">john@example.com</b></pre><br/>

<h3>Setting Shortcut command untuk Git</h3>

Git bisa disetting agar memiliki shortcut command seperti sintax svn 
<pre>
git config --global alias.st status
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.up rebase
git config --global alias.ci commit
</pre>

<br/><br/>

<!-- Git start -->
<h3>Tutorial Git</h3>

Buatlah Git repository, jika sudah, bukalah cmd dan arahkan ke directory yg diinginkan<br/>
<pre>
$ cd D:
$ cd wamp/www
</pre><br/>

<ul>
<li>
	Jika repository belum ada file, Lakukan init ke repository anda
	<pre>$ git init
$ git remote add origin https://repository.klgsys.com/rusdi/cerberus.git</pre><br/>
</li>
<li>
	atau <br/>
	Jika repository sudah ada file gunakan clone
	<pre>$ git clone https://repository.klgsys.com/rusdi/cerberus.git</pre>
</li>
</ul>
<br/>

Untuk mengadd file banyak gunakan asterisk titik . atau spesifik nama file 
<pre>$ git add .</pre><br/>

Untuk commit lakukan <br/>
<pre>$ git commit -m "Initial commit"
</pre><br/>

Untuk push lakukan
<pre>$ git push -u origin master</pre><br/>



<!-- Git end -->

<h3>Merestore file versioned yang dihapus</h3>
Ada kalanya kita ingin hanya mengupdate 1 file dari repository ke folder, caranya adalah
<pre>
git status

#format: git checkout origin/master -- path/to/file
git checkout origin/master -- path/to/file
</pre>

<br/><br/> 

<h3>Melakukan update single file di git</h3>
Pastikan tidak ada file menggantung
<pre>
git status
</pre>
<br/>

Update file yang diinginkan dengan ketik '<b>path/to/file</b>' sesuai file path.
<pre>
git checkout origin/master -- path/to/file
</pre>

<h3>Membuat Branch baru</h3>
<pre>
git checkout -b modul_api
# git checkout -b <branch-name> 
# Create a new branch and check it out

git add .
#add new file global

git commit -m "commit ke cabang"
#commit to branch

git push
<span class="clrRed">#branch BARU akan bisa terbuat dan terlihat di repository git, jika anda sudah melakukan push</span>
</pre><br/>

<h3>Merge antar branch</h3>
Jika anda sudah membuat branch (asumsi branch dengan nama 'module_member' dan branch 'master')<br/>
<pre>
# pastikan anda sedang di repo master
git checkout master

# pastikan anda usdah update
git pull

# melakukan merge dari master dengan branch 'module_member' secara local
# tanpa melakukan commit dan fast forward
# format : git merge origin/NAMABRANCH --no-commit --no-ff
git merge origin/module_member --no-commit --no-ff

# jika tidak ada masalah maka bisa di commit 
git 
</pre><br/>
<pre></pre><br/>

<h3>Git stash</h3>

<pre>
# jika anda sedang mengedit script namun ingin update gunakan git stash
# command ini akan men-save semua local changes anda ke sebuah tempat sementara
git stash save

# lakukan update 
git pull 

#jika sudah yakin tidak ada masalah, kembalikan ke latest local changes anda
git stash pop
</pre><br/>


<h3>Git Check Url Repository</h3>
untuk memunculkan url repository dari git saat ini jalankan:
<pre>git config --get remote.origin.url</pre>

untuk memunculkan full output<br/>
<pre>git remote show origin</pre>
<br/>

<h3>Git Reset</h3>
Untuk file yang sudah di stage <b>Commit</b>, anda bisa menghapus file tsb dengan cara git reset
<pre>
git reset filesaya.txt
# git reset filename
</pre><br/>

<h3>Git Checkout</h3>
adalah command untuk <b>switch</b> atau pindah source antar branch<br/>
<pre>
# format: git checkout <b>&lt;branch_name&gt;</b>

git checkout master 
# pindah ke branch master 

git checkout modul_api
#pindah ke branch modul_api
</pre>

<h3>Git Rebase</h3>
Notes: <b class="clrRed">Jangan lakukan rebase di repo public seperti di master</b><br/>Karena repo public akan kehilangan history dan semua user yang terhubung akan langsung tersambung ke repo baru tanpa diketahui.

<pre></pre>

<h3>Interactive Rebase</h3>
adalah command untuk mengubah commit, dan mengontrol history commit dari sebuah branch<br/>

<pre>
git checkout feature
git rebase -i master
</pre>

Outputnya adalah
<pre>
pick 33d5b7a Message for commit #1
pick 9480b3d Message for commit #2
pick 5c67e61 Message for commit #3
</pre>

Listing diatas digunakan 
<pre>
pick 33d5b7a Message for commit #1
<b class="clrRed">fixup</b> 9480b3d Message for commit #2
pick 5c67e61 Message for commit #3
</pre>

Source<br/>
<a href="https://www.atlassian.com/git/tutorials/merging-vs-rebasing#the-golden-rule-of-rebasing">https://www.atlassian.com/git/tutorials/merging-vs-rebasing#the-golden-rule-of-rebasing</a>


<h3>Git Ignore</h3>

<pre>
<span class="comment"># To untrack a single file that has already been added/initialized to your repository, i.e., 
# stop tracking the file but not delete it from your system use: 
# To untrack every file that is now in your .gitignore:
# First commit any outstanding code changes, and then, run this command:
# format: git rm --cached filename
</span>
git rm -r --cached .

<span class="comment"># This removes any changed files from the index(staging area), then just run:</span>
git add .

<span class="comment"># Commit it:</span>
git commit -m ".gitignore is now working"

<span class="comment"># To undo</span>
git rm --cached filename, use git add filename.

<span class="comment"># Make sure to commit all your important changes before running  
git add . Otherwise, you will lose any changes to other files.</span>
</pre>

<h3>Git Resolve Conflict</h3>
<pre>
# resolve conflict using ours 
git checkout --ours PATH/FILE

# resolve conflict using theirs 
git checkout --theirs PATH/FILE
</pre>

	</div>
	<!-- CONTENT CLOSE -->
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>