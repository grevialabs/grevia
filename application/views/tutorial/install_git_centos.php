<div class="row">
	<!-- CONTENT START -->
	<div class="col-md-10 talJst">
    
	<h2>Setup server Centos</h2>


<h3 class="">Cara install git di Centos 6.4</h3>
buka terminal dan ketik:
<pre>
sudo yum install git
</pre>

setelah muncul sukses, maka command 'git' sudah bisa digunakan untuk checkout.<br/>
Untuk checkout pertama kali di server VPS, gunakan command dibawah ini 
<pre>
git clone https://github.com/GreviaLabs/git.grevia.com.git /
</pre>

Jika sudah berhasil maka akan ada tulisan done.

Untuk melakukan update, gunakan command pull seperti berikut.
<pre>
git pull origin master
</pre>

Help:<br/>
<a href="https://help.github.com/articles/adding-an-existing-project-to-github-using-the-command-line/" target="_blank">https://help.github.com/articles/adding-an-existing-project-to-github-using-the-command-line/</a><br/>
<a href="https://www.siteground.com/tutorials/siteground-git/clone-git-repository.htm" target="_blank">https://www.siteground.com/tutorials/siteground-git/clone-git-repository.htm</a>

<h3>Cara menyimpan credentials username dan password di linux</h3>	

Saat anda menggunakan git dan melakukan git command, biasanya git akan meminta credential username dan password, setiap session / command. Untuk menghindari hal tsb, kita bisa melakukan store credential di dalam cache server dengan cara melakukan command berikut.<br/><br/>
Tahap 1<br/>
<pre>
git config credential.helper store
</pre>
<br/>
Tahap 2<br/>
lalu lakukan command update biasa, dan anda akan diminta password dan disimpan secara permanen.
<pre>
git pull
</pre>

Voila.

Jika anda ingin melakukan reset credentials maka ulangi lagi tahap 1.<br/>
Sumber:  <a href="https://stackoverflow.com/questions/35942754/how-to-save-username-and-password-in-git">https://stackoverflow.com/questions/35942754/how-to-save-username-and-password-in-git</a>

<br/><br/>

<!-- Git start -->
<h4>Tutorial Git</h4>
<a class="btn btn-sm btn-info" href="<?php echo base_url()?>tutorial/git"><?php echo base_url()?>tutorial/git</a>
<br/><br/>

	</div>
	<!-- CONTENT CLOSE -->
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>