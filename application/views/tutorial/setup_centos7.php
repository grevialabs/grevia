<div class="row">
	<div class="col-md-10 talJst">
	
<h2>Server Centos 7</h2>

Lakukan update repository Linux
<pre>
sudo yum install epel-release
</pre>

Jika sudah selesai install Nginx dengan command :
<pre>
sudo yum install nginx -y
</pre>

Untuk menyalakan nginx, ketik command
<pre>
sudo systemctl start nginx
</pre>

Biasanya jika url / IP masih belum bisa menampilkan halaman hello world Nginx, anda harus menambahkan by pass firewall dengan command ini.
<pre>
sudo firewall-cmd --permanent --zone=public --add-service=http 
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload
</pre>

Jika sudah, anda harusnya sudah bisa melihat <b>Welcome to Nginx</b><br/>

Jangan lupa untuk membuat service nginx autostart setiap kali ada shutdown / reboot dengan command:
<pre>
sudo systemctl enable nginx
</pre>

<!-- ----------------------------------------- -->

<h2>Install MariaDB / Mysql</h2>
Lokasi konfigurasi server ada di 

<pre>
sudo yum install mariadb-server mariadb -y 

#untuk menjalankan
sudo systemctl start mariadb

# untuk auto start 
sudo systemctl enable mariadb
</pre>

Mysql sudah berjalan, maka jalankan command ini untuk setup master password
<pre>
sudo mysql_secure_installation
</pre>

<h2>Install PHP</h2>

Untuk menginstal PHP dan Php-Fpm
<pre>
sudo yum install php php-mysql php-fpm
</pre>

Jika sudah, jalankan
<pre>
sudo nano /etc/php.ini
</pre>

di config ini ubah parameter agar sama seperti ini
<pre>
cgi.fix_pathinfo=0
</pre>

Lanjut ke 
<pre>
/etc/php-php.d/www.conf 
</pre>

ubah 3 param berikut:
<pre>
#1
listen = /var/run/php-fpm/php-fpm.sock

#2
listen.owner = nobody
listen.group = nobody

#3
user = nginx
group = nginx
</pre>

Jika sudah maka
<pre>
sudo systemctl start php-fpm
sudo systemctl enable php-fpm
</pre>

Lanjut 
<pre>
sudo nano /etc/nginx/conf.d/default.conf
</pre>

Jika sudah maka restart nginx
<pre>
sudo systemctl restart nginx
</pre>

Untuk mengetes, buka
<pre>
sudo nano /usr/share/nginx/html/info.php
</pre>

dan isi dengan 
<pre>
phpinfo();
</pre>
<!-- ----------------------------------------- -->

<h2>Install Php Fpm</h2>
Lokasi konfigurasi server ada di 
<pre>
/usr/share/nginx/html
</pre>

dengan Lokasi conf.d di 
<pre>
/etc/nginx/conf.d
</pre>

Untuk pengaturan conf global nginx ada di 
<pre>
/etc/nginx/nginx.conf
</pre>

Untuk codeigniter, 
<pre class="bgSftGry">
server {
	listen       80 default_server;
	#listen      443 ssl;
	listen       [::]:80 default_server;
	
	server_name  testing.muahunter.com;
	error_log /var/log/nginx/error_nginx.log;
	# set client body size to 10M 
	#    client_max_body_size 10M;    
	
	#ssl on;    
	#ssl_certificate /etc/nginx/ssl/muahunter.com.chained.crt;    
	#ssl_certificate_key /etc/nginx/ssl/muahunter.com.key;    
	#set session timeout ssl    
	#ssl_session_cache shared:SSL:20m;    
	#ssl_session_timeout 10m;    
	# Load configuration files for the default server block.    include /etc/nginx/defa$    # session security ssl    
	#add_header Strict-Transport-Security "max-age=31536000";   
	
	location / {
		root   /usr/share/nginx/html;
		index index.php index.html index.htm;
		try_files $uri $uri/ /index.php;
		}    
		# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
		location ~ \.php$ {
			root /usr/share/nginx/html;
			index index.php index.html index.htm;
			fastcgi_pass   127.0.0.1:9000;
			fastcgi_index  index.php;
			fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;        include        fastcgi_params;
			try_files $uri $uri/ /index.php;
	}
	
	# force https-redirects    
	#if ($scheme = http) {    
	#    return 301 https://$host$request_uri;    
	#}    #location ~ \.php {    
	#    include /etc/nginx/common/php.conf;    
	#}    
	#sementara jangan pake dlu    
	#error_page 404 /404.html;    
	#    location = /40x.html {    
	#}    error_page 500 502 503 504 /50x.html;        
	location = /50x.html {    }
}
</pre>

<h3>Reload ssh</h3>
<pre>
systemctl reload sshd
</pre>
	</div>

	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>