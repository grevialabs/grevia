<div class="row">
	<div class="col-md-10">		
		<div class="talJst" >
			<div>
			<h2>Setup SSL</h2>
				Referensi<br/>
				<ul>
					<li><a href="https://www.digitalocean.com/community/tutorials/how-to-install-an-ssl-certificate-from-a-commercial-certificate-authority">https://www.digitalocean.com/community/tutorials/how-to-install-an-ssl-certificate-from-a-commercial-certificate-authority</a></li>
					<li><a href="https://jadoel.info/2015/07/cara-redirect-http-ke-https-pada-nginx/">https://jadoel.info/2015/07/cara-redirect-http-ke-https-pada-nginx/</a></li>
					<li><a href="https://www.namecheap.com/support/knowledgebase/article.aspx/794/67/how-to-activate-ssl-certificate">https://www.namecheap.com/support/knowledgebase/article.aspx/794/67/how-to-activate-ssl-certificate</a></li>
					<li><a href="http://www.cyberciti.biz/faq/linux-unix-nginx-redirect-all-http-to-https/">http://www.cyberciti.biz/faq/linux-unix-nginx-redirect-all-http-to-https/</a></li>
				</ul>

				Jika anda belum menginstall nginx, silakan baca panduan berikut <a href="<?php echo base_url().'tutorial/setup_centos'; ?>" target="_blank">Men-setup server Centos 6.8</a><br/><br/>

				Jika sudah, mulailah dengan membuat folder bernama <b>ssl</b> di folder nginx
				<pre>
				sudo mkdir /etc/nginx/ssl
				</pre>

				pindah ke directory <b>ssl</b> yang baru dibuat
				<pre>
				cd /etc/nginx/ssl
				</pre>

				berikutnya buatlah key untuk sertifikat SSL, <b>example.com.key</b>
				<pre>
				sudo openssl genrsa -des3 -out <span class="clrRed">example.com.key</span> 1024
				</pre>

				lalu anda akan diminta memasukan <b>passphrase</b>, ketikan keyword yang diinginkan<br/>
				*notes: Keyword ini sebaiknya dicatat di suatu tempat / notes dan jangan sampai lupa karena jika tidak sertifikat ini tidak akan bisa digunakan lagi.<br/><br/>

				Jika sudah, maka mulai 
				<pre>
				sudo openssl req -new -key <span class="clrRed">example.com.key</span> -out <span class="clrRed">example.com</span>.csr
				</pre>

				anda akan diminta mengisi <br/>
				- kode negara 2 digit => isi dengan <b>ID</b><br/>
				- Nama province / state => isi dengan <b>DKI Jakarta</b><br/>
				- City => Jakarta<br/>
				- Organization name<br/>
				- Organization Unit name<br/>
				- Common name (isi dengan domain atau IP server)<br/>

				Untuk 2 poin berikutnya kosongkan<br/>
				- Challenge password<br/>
				- Optional company name<br/>

				<h3>Tahap ketiga - Hilangkan passphrase</h3>

				<pre>
				sudo cp server.key server.key.org
				sudo openssl rsa -in server.key.org -out server.key
				</pre>

				<h3>Tahap keempat - Sign certificate</h3>
				Sertifikat SSL sudah selesai, hanya tinggal disign oleh kita: angka 365 adalah durasi ssl aktif
				<pre>
				sudo openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
				</pre>
				<br/>============================================================================<br/>
				Jika sudah divalidasi, Penerbit Certificate akan mengirim email berisi CRT File, download dan simpan.<br/>
				Extract file zip tersebut, akan berisi 1 file <b>bundle</b> dan 1 file <b>crt</b><br/>
				Untuk file
				<pre>

				</pre>
				Gabungkan intermediate.crt dan youserver.crt menjadi :
				<pre>
				cat makeuphunter.com.crt intermediate.crt > makeuphunter.com.chained.crt
				</pre>

				<hr/>
				<!------------------------------------------------------------------------------------------------------------------->
				<h2>SSL on Nginx with Load Balancer</h2>

				Buat folder ssl di config nginx
				<pre>
				mkdir /etc/nginx/ssl
				</pre>

				<h3>Membuat CSR</h3>
				CSR adalah key yang dibutuhkan sebagai identitas dasar sebuah server. Flownya<br/>
				<ol>
					<li>Pemilik server membuat CSR terlebih dahulu</li>
					<li>Beli SSL di vendor dan registrasi dengan CSR yang sudah dibuat</li>
					<li>Aktivasi ssl (email / DNS / webfile validation)</li>
					<li>Menunggu konfirmasi dari pihak vendor</li>	
					<li>Certificate Authority akan mengirim file crt dan bundle(nanti akan direname menjadi intermediate.crt)</li>
					<li>Menggabungkan crt dengan intermediate.crt menjadi chained.crt di Config server</li>
					<li>Voila ssl kamu sudah jadi</li>
					<li>Setup config di Nginx</li>
				</ol>
				jalankan command ini dan ganti <span class="b clrRed btn btn-xs bg-info">example.com</span> dengan domain anda
				<pre>
				openssl req -newkey rsa:2048 -nodes -keyout <span class="b clrRed btn btn-xs bg-info">example.com</span>.key -out <span class="b clrRed btn btn-xs bg-info">example.com</span>.csr
				</pre>

				Setelah menjalanan command diatas, anda akan diminta menginput beberapa hal terkait server. Yang harus diperhatikan adalah saat input <span class="b clrRed btn btn-xs bg-info">Common Name</span> contohnya
				<pre>
				Country Name (2 letter code) [AU]: <span class="b clrRed">ID</span>
				State or Province Name (full name) [Some-State]: <span class="b clrRed">DKI Jakarta</span> 
				Locality Name (eg, city) []: <span class="b clrRed">Jakarta</span>
				Organization Name (eg, company) [Internet Widgits Pty Ltd]: <span class="b clrRed">Example Company</span>
				Organizational Unit Name (eg, section) []: <span class="b clrRed">IT Division</span>
				Common Name (e.g. server FQDN or YOUR name) []: <span class="b clrRed">example.com</span>
				Email Address []: <span class="b clrRed">domo@example.com</span>
				</pre>

				<pre>

				</pre>

				<pre>

				</pre>

				<pre>

				</pre>

				<pre>

				</pre>

				<pre>

				</pre>

				<pre>

				</pre>


			</div>
		</div>

	</div>
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>