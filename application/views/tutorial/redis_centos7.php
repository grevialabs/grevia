<div class="row">
	<div class="col-md-10 talJst">

	Redis Centos7

	Referensi: 
	<a href="https://redis.io/topics/quickstart">https://redis.io/topics/quickstart</a><br/>
	<a href="https://redis.io/topics/quickstart">https://redis.io/topics/quickstart</a><br/>
	
	<a href="https://stackoverflow.com/questions/36126170/how-to-implement-redis-in-codeigniter">https://stackoverflow.com/questions/36126170/how-to-implement-redis-in-codeigniter</a><br/>
	<a href="https://stackoverflow.com/questions/36196117/how-do-i-install-redis-extention-to-windows-apache-server">https://stackoverflow.com/questions/36196117/how-do-i-install-redis-extention-to-windows-apache-server</a><br/>	
	https://stackoverflow.com/questions/19091087/open-redis-port-for-remote-connections

	
Untuk mengatur setingan redis config
<pre>
nano /etc/redis.conf
</pre>

Untuk mengakses redis secara public
<pre>
# untuk keperluan testing gunakan
bind 0.0.0.0
protected-mode no

# untuk live production, setting 127.0.0.1 agar redis hanya bisa diakses via server internal anda
bind 127.0.0.1
protected-mode yes
</pre>

Untuk menambah secure, gunakan password dengan menambahkan "requirepass", karena biasanya redis akan mengalami brute force 150k dalam semenit.

Untuk code igniter 3, tambahkan file redis.php di folder config dan masukan
<pre>
$config['socket_type'] = 'tcp'; //`tcp` or `unix`
// $config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
$config['host'] = '<b class="clrRed">IP</b>';
$config['password'] = NULL; // isi dengan requirepass jika ada
$config['port'] = 6000; // default 6379
$config['timeout'] = 0;
</pre>

Untuk redis di windows dengan XAMPP / WAMP stack, anda harus tambahkan .dll dan menambah php extension untuk redis.
Carannya: carilah PECL sesuai dengan php version anda dengan versi Thread Safety

Untuk php 5.3 - 5.6 anda bisa gunakan url berikut
<a href="https://pecl.php.net/package/redis/2.2.7/windows">https://pecl.php.net/package/redis/2.2.7/windows</a>


Jika redis masih belum bisa terbuka, gunakan bantuan web http://ping.eu/port-chk/ untuk mengecek port redis server anda sudah terbuka atau belum

Jika port belum dibuka, maka untuk membuka port 
<pre>
firewall-cmd --zone=public --permanent --add-port=8080/tcp
</pre>

<pre>
firewall-cmd --reload
</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

<pre>

</pre>

</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>