<div class="row">
	<div class="col-md-10 talJst">
<h3>Notes</h3>
<!-- START 1 -->
<h2>Disable Password Authentication</h2>

Disable password auth, berguna untuk mengamankan server agar tidak di brute force.
<pre>
sudo nano /etc/ssh/sshd_config
</pre>
<br/>

Cari line dengan value <b>"PasswordAuthentication"</b> dan uncomment, tambahkan value <b>"no"</b> menjadi seperti ini <br/>
<pre class="bgSftGry">
PasswordAuthentication no
</pre>

Jika sudah, maka lakukan reload service
<pre>
sudo systemctl reload sshd
</pre>
<hr/>
<!-- END 1 -->

<!-- START -->
<h2>Buatlah user baru</h2>

Defaultnya linux, memiliki user dengan nama root yang digunakan untuk hak akses penuh, namun penggunaan akses root ini, rawan dieksploitasi dan menyebabkan chaos, karena itu kita perlu membuat user baru. <br/><br/>
Untuk membuat user baru ketik
<pre>
adduser <b class="red">rusdi</b>
</pre>

User baru dengan nama "rusdi" sudah dibuat, saatnya kita mendaftar user tesebut ke dalam superuser agar bisa menggunakan perintah sudo dan tidak perlu login berulang kali
<pre>
usermod -aG sudo <b class="clrRed">rusdi</b>
</pre>

Hore user baru <b class="clrRed">rusdi</b> telah dibuat dengan group super user
<!-- END -->

<!-- START -->
<h2>Install Nginx</h2>

Nginx dibutuhkan untuk web server yang menampilkan web kita
<pre>
sudo apt-get update
sudo apt-get install nginx
</pre>

Jika sudah, untuk test nginx apakah sudah jalan atau belum, gunakan curl
<pre>
curl ardgery.com
</pre>

Jika ada response http get maka nginx akan muncul response.
<!-- END -->

<!-- START -->
<h2>MYSQL</h2>

Untuk install mysql 
<pre>
sudo apt-get install mysql-server
</pre>

Jika sudah maka 
<pre>
mysql_secure_installation
</pre>
<!-- END -->

<!-- START PHP -->
<h2>PHP</h2>

Untuk menjalankan php, maka kita perlu menginstal php-fpm
<pre>
sudo apt-get install php-fpm php-mysql
</pre>

Jika sudah, maka buka settingan php fpm di
<pre>
sudo nano /etc/php/7.0/fpm/php.ini
</pre>

Buka dan find set value "cgi.fix_pathinfo" menjadi
<pre>
cgi.fix_pathinfo=0
</pre>

Jika sudah maka restart 
<pre>
sudo systemctl restart php7.0-fpm
</pre>

<hr/>

<h2>Ubah config Nginx</h2>

Ubah setting Nginx agar menggunakan PHP
<pre>
sudo nano /etc/nginx/sites-available/default
</pre>

Untuk confignya, harus kita rubah default menjadi 

<pre>
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/html;
	index <b class="clrRed">index.php</b> index.html index.htm index.nginx-debian.html;

	server_name <b class="clrRed">server_domain_or_IP</b>;

	location / {
		try_files $uri $uri/ =404;
	}
	<b class="clrRed">
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
	}

	location ~ /\.ht {
		deny all;
	}
	</b>
}
</pre>

<br/>
*Notes : <br/>
Jika anda ingin mengeset folder laravel di dalam domain gunakan settingan berikut:<br/>
*asumsi folder yang anda pakai adalah laravel(tinggal diganti sesuai nama folder anda di route nginx diatas<br/>
<br/>
<pre>
	# setting subfolder url to overwrite index htacces in nginx
	location /laravel {
		#try_files $uri/ /index.php;                
		try_files $uri $uri/ /laravel/index.php?$query_string;        
	} 
</pre>

Untuk mengetes setting nginx sudah benar
<pre>
sudo nginx -t
</pre>

Jika config sudah dirubah, anda bisa reload
<pre>
sudo sytemctl service nginx
</pre>

Untuk testing php sudah berjalan
<pre>
sudo nano /var/www/html/info.php 
</pre>

tambahkan script 
<pre>
phpinfo();
</pre>

lalu buka 
<pre>
http://<b class="clrRed">server_domain</b>/info.php
</pre>

Jika benar maka akan muncul settingan info php yang dimaksud.
<!-- END PHP -->

<!-- START PHP 5 -->
<h2>PHP 7 ke php 5.6</h2>
Ubuntu 16 secara default akan menginstall php 7, untuk mengganti PHP 7 ke 5.6

Tambahkan repo untuk 5.6
<pre>
sudo add-apt-repository ppa:ondrej/php
</pre>

jalankan
<pre>
sudo apt-get update
sudo apt-get install php5.6-fpm
</pre>

untuk mereplace service php 7 ke 5.6
<pre>
sudo update-alternatives --set php /usr/bin/php5.6
</pre>

<pre>
</pre>

<pre>
</pre>


Untuk mencari tahu versi php di server buka php -> phpinfo atau ketik:
<pre>
php -v
</pre>

Jika benar maka akan muncul
<pre class="bgSftGry">
PHP 5.6.23-2+deb.sury.org~xenial+1 (cli) 
Copyright (c) 1997-2016 The PHP Group
Zend Engine v2.6.0, Copyright (c) 1998-2016 Zend Technologies
with Zend OPcache v7.0.6-dev, Copyright (c) 1999-2016, by Zend Technologies
</pre>


restart nginx 
<pre>
sudo systemctl restart nginx
</pre>

Dan voila, anda sudah bisa melihat hasilnya
<!-- END PHP 5 -->
			  <br/><br/>
		<div class="author">By Rusdi</div><br/>
	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
	
</div>