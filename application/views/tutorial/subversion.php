<div class="row">
	<div class="col-md-10 talJst">
	<h3>About Subversion</h3>
<p>Subversion adalah module repository / tempat penyimpanan source code untuk menaruh file script di repository linux. Subversion populer karena digunakan para programmer untuk mencatat history atau perubahan script secara terpadu.</p>

<h3>Tortoise SVN</h3>
<p>adalah program dari Windows yang berguna untuk menghubungkan repository dan menjalankan command-command ke dalam komputer windows kamu.</p>

<h3>Repository</h3>
<p>Repository atau repo adalah tempat file script kamu disimpan. Kamu harus membuat nama repository terlebih dahulu, baru kamu bisa mengupload file script kamu ke sini.</p>

<h3>Add User</h3>
<p>Jika repository subversion sudah dibuat, maka tahap selanjutnya adalah membuat user atau jika sudah ada langsung memberikan akses agar user bisa mengakses (readonly / full access read write).</p>

<h3>Svn Checkout</h3>
<p> Svn Checkout adalah command untuk inisiasi awal sinkronisasi file di server dengan repository. </p>
<pre>
svn co http://yourdomain.com/repo/grevia /home/blabla/public_html/ .
</pre>

<p>Setelah menjalankan command di atas maka kamu akan diminta username dan password repository. Ketikkan dan voila, file kamu sudah tersinkronisasi dengan repository.</p>

<h3>Svn Update</h3>
<p> Svn update adalah command yang digunakan untuk mengupdate file di server(Linux) agar ter sinkronisasi dengan file yang ada di repository yang sudah dilakukan proses checkout sebelumnya. Command ini bisa diterapkan untuk satu file tertentu atau ke seluruh directory yang bersangkutan.</p>

<pre>
svn update /home/blabla/public_html/scriptku -r30
</pre>

<p>Command diatas akan mengupdate server untuk menggunakan script dengan revisi ke 30</p>

<div class="author">By Rusdi</div><br/>
	
	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>