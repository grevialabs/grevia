<div class="row">
	<div class="col-md-10 talJst">
		<h2>Tips memperbesar limit attachment size di roundcube dan postfix</h2>
	Referensi<br/>
	<ul>
		<li><a href="https://www.electrictoolbox.com/postfix-email-size-limit/">https://www.electrictoolbox.com/postfix-email-size-limit/</a></li>
	</ul>
	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>


Secara default, roundcube hanya menyediakan 2MB untuk attachment, yang juga merupakan limit dari .htaccess di php.
Carilah folder php.ini di server anda dengan menggunakan keterangan dari php_info() dan find "php.ini"

Jika sudah membuka file "php.ini", maka cari lah keyword berikut ini dan ganti valuenya sesuai dengan kebutuhan anda.
<pre>
post_max_size = 16M
upload_max_filesize = 16M
max_input_time = 180
memory_limit = 128M
max_execution_time = 180
</pre>

Jika sudah melakukan command diatas, jangan lupa untuk merestart service dan mengecek perubahan di phpinfo dengan .

<pre>
/etc/init.d/httpd restart
/etc/init.d/postfix restart
</pre>

Jika setelah merubah parameter diatas, namun masih tidak bisa juga, maka bukalah ssh dan ketik:
<pre>
# postconf | grep message_size_limit
</pre>

maka anda akan melihat berapa limit file yang diset oleh postfix<br/><br/>

Jika anda masih mengalami eror semacam "SMTP Error: Message size exceeds server limit", maka coba lakukan langkah berikut via SSH.

Mengatur setingan max attachment di postfix, untuk set jadi 10MB:
<pre>
# postconf -e message_size_limit=10240000
</pre>

untuk set jadi 20MB:
<pre>
# postconf -e message_size_limit=20480000
</pre>

Setelah execute command di atas, jangan lupa melakukan restart <b>webserver</b> dan <b>postfix</b> untuk bisa melihat perubahan.
<pre>
/etc/init.d/httpd restart
/etc/init.d/postfix restart
</pre>

Cobalah login ke roundcube anda dan voila, limit attachment anda sudah berubah.
</div>