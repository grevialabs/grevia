<?php 
$kalimat = "
Selamat siang {panggilan}, <br/><br/>

saya adalah {nama_lengkap}
";

/**
 * A function to fill the template with variables, returns filled template.
 * 
 * @param string $template A template with variables placeholders {$varaible}.
 * @param array $variables A key => value store of variable names and values.
 * 
 * @return string  
 */

function replace_variables($template, array $variables)
{

 return preg_replace_callback('#{(.*?)}#',
       function($match) use ($variables){
            $match[1]=trim($match[1],'$');
            return $variables[$match[1]];
       },
       ' '.$template.' ');

}

$param = array();
$param['panggilan'] = 'bapak';
$param['nama_lengkap'] = 'ujang';

?>
<div class="row">
	<div class="col-md-10 talJst">
    <h3>About Template Engine</h3>
<p>Template Engine digunakan untuk membuat variabel / data di php dengan menggunakan sintax tertentu.</p>

<h3>Template</h3>
<p>Jika anda memiliki string seperti berikut:</p>

<pre>
Selamat siang {panggilan}, <br/><br/>

saya adalah {nama_lengkap}
</pre>

<p></p>
<pre>
<?php 
echo replace_variables($kalimat,$param);
?>
</pre>

<p>Sisipkan fungsi bernama <b>replace_variables()</b> berikut ke dalam script php anda.</p>
<pre>
function replace_variables($template, array $variables){

 return preg_replace_callback('#{(.*?)}#',
       function($match) use ($variables){
            $match[1]=trim($match[1],'$');
            return $variables[$match[1]];
       },
       ' '.$template.' ');

}
</pre>

<p>Secara Full, berikut script yang anda jalankan.</p>
<pre>

$kalimat = "
Selamat siang {panggilan}, <br/><br/>

saya adalah {nama_lengkap}
";

$param = array();
$param['panggilan'] = 'bapak';
$param['nama_lengkap'] = 'ujang';

echo replace_variables($kalimat,$param);

// =============== HASIL OUTPUT ===============
// Selamat siang bapak, 
//
//
// saya adalah ujang
</pre>

<p>Command diatas akan mengupdate server untuk menggunakan script dengan revisi ke 30</p>

<div class="author">By Rusdi</div><br/>
	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>