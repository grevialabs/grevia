<div class="row">
	<div class="col-md-10 talJst">

      <h3>About memcached</h3>
	  <a href="https://www.digitalocean.com/community/tutorials/how-to-store-php-sessions-in-memcached-on-a-centos-vps" target="_blank">Store php session with memcached Centos</a>
	  <a href="https://www.digitalocean.com/community/tutorials/how-to-share-php-sessions-on-multiple-memcached-servers-on-ubuntu-14-04" target="_blank">Integrate Memcached to php session Ubuntu 14</a>
	  
<h3>Memcached</h3>
<p>adalah platform cache server berbasis linux yang cukup populer. Kompetitor sejenis adalah redis dan solr</p>

Setup session with memcached<br/>
Server 1 <br/>
128.199.223.81 ; <br/>
<span class="clrRed">IP local : 10.130.18.16</span><br/><br/>

Server 2<br/>
188.166.212.57 ;<br/>
IP local : <span class="clrRed">10.130.41.103</span><br/><br/>

Step 1 : Install memcached :<br/>
<pre>
yum install memcached
</pre>

Lalu jalankan servicenya
<pre>
service memcached start
</pre>

Ubah settingan config memcached
<pre>
nano /etc/sysconfig/memcached
</pre>

Tambahkan command listen (yang berwarna merah) untuk menjelaskan bahwa tiap IP listen ke IP internal masing2.
<pre>
PORT="11211"
USER="memcached"
MAXCONN="1024"
CACHESIZE="64"
OPTIONS="<span class="clrRed">-l 10.130.41.103</span>"
</pre>

Jika sudah, restart memchaced
<pre>
service memcached restart
</pre>

*notes : GANTI SEMUA SETTING DIATAS UNTUK SETIAP SERVER<br/><br/>

<!------------------------------------------------ STEP 2 -------------------------------------------------->
<h3 class="b">Step 2: Set Memcached sebagai PHP SESSION</h3>

Bukalah config php.ini
<pre>
nano /etc/php.ini
</pre>

find / tekan ctrl + w di nano editor, untuk cari teks berikut
<pre>
session.save_handler = 
session.save_path =
</pre>

ganti save_handler dengan memcache, save_path dengan ip internal server1,server2,dst
<pre>
session.save_handler = memcache 
session.save_path = 'tcp://<span class="clrRed">10.130.18.16:11211</span>,tcp://<span class="clrRed">10.130.41.103:11211</span>'
</pre>
<span class="b clrRed">*Jangan lupa port 11211</span><br/><br/>

Lalu buka memcache.ini
<pre>
nano /etc/php.d/memcache.ini
</pre>

Tambahkan config ini
<pre>
memcache.allow_failover=1
memcache.session_redundancy=3
</pre>
untuk mengisi angka <span class="b clrRed">memcache.session_redundancy</span> harus diisi dengan jumlah memcached server aktif + 1 (dalam contoh kali ini adalah 2 server + 1 yaitu 3) dikarenakan <a href="https://bugs.php.net/bug.php?id=58585">bug dari PHP</a><br/><br/>

Jika sudah, reset server memcached dan Nginx
<pre>
service memcached restart 
/etc/init.d/nginx restart
/etc/init.d/php-fpm restart
</pre>
<br/>
*lakukan ini di setiap server script yang ada<br/>

<h3 class="b">Step 3 : Mengetes login </h3>
Jika sudah lakukan pengetesan dengan membuat file <span class="clrRed b">session.php</span> dengan isi:
<pre>

header('Content-Type: text/plain');
session_start();
if(!isset($_SESSION['visit']))
{
	echo "This is the first time you're visiting this server\n";
	$_SESSION['visit'] = 0;
}
else
		echo "Your number of visits: ".$_SESSION['visit'] . "\n";

$_SESSION['visit']++;

echo "Server IP: ".$_SERVER['SERVER_ADDR'] . "\n";
echo "Client IP: ".$_SERVER['REMOTE_ADDR'] . "\n";
print_r($_COOKIE);

</pre>

Lalu ambil PHPSESSID untuk mensimulasikan seakan-akan kita user yang sama
<pre>
curl -v -s http://s1.makeuphunter.com/session.php 2>&1 | grep 'Set-Cookie:'
</pre>

maka akan memunculkan
<pre>
< Set-Cookie: <span class="clrRed">PHPSESSID=8lebte2dnqegtp1q3v9pau08k4</span>; path=/
</pre>

Copy value dari <span class="clrRed">PHPSESSID</span> yaitu <span class="clrRed">8lebte2dnqegtp1q3v9pau08k4</span> dan coba lakukan request curl ke server lain
<pre>
curl --cookie "<span class="clrRed">PHPSESSID=8lebte2dnqegtp1q3v9pau08k4</span>" http://s1.makeuphunter.com/session.php http://s2.makeuphunter.com/session.php
</pre>


Jika sudah, coba matikan satu server untuk melihat efek dan IP server apakah sudah berubah 


<h3></h3>
Install dulu package development & agar pecl bisa diinstall
<pre>
yum groupinstall "Development Tools"
yum install php php-pear php-devel
</pre>

Update dulu system
<pre>
yum update
</pre>

Lalu install pecl memcache
<pre>
pecl install memcached
</pre>

tambahkan module memcache
<pre>
echo "extension=memcache.so" >> /etc/php.ini
</pre>

lalu restart php-fpm
<pre>
/etc/init.d/php-fpm restart
</pre>
<div class="author">By Rusdi</div><br/>

	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>