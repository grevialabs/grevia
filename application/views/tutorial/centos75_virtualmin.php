<div class="row">
	<div class="col-md-10 talJst">
	<h3>Install Virtualmin on Centos 7.5</h3>
    Virtualmin adalah software yang digunakan untuk memanage server dan sangat populer karena fitur-fiturnya yang sangat powerful dan free. Berikut adalah tutorial menginstall Virtualmin. <br/>

Requirement : <br/>
<ul>
    <li>Centos 7.0 atau 7.5</li>
</ul>
https://www.hostwinds.com/guide/install-virtualmin-centos-7/

<pre>
# update yum first to get latest repo
yum update -y

# we need perl to install
yum install perl -y

# get shell for virtualmin
wget http://software.virtualmin.com/gpl/scripts/install.sh

#make sh executable
chmod +x install.sh

# execute shell 
sh install.sh

# after this, you will be asked and showed list of OS that supported 
# press y and continue

# voila, you can access your virtualmin in port 10000 
# YOURIPADDRESS:10000

# secara default, virtualmin belum diset password
# set password for vmin login
passwd

#Notes : folder path html di virtualmin
<pre>
#root file parent and subdomain
cd /home/grevia/public_html/
cd /home/grevia/domains/excalibur.grevia.win/public_html/

#URL https://159.65.13.206:10000/

@ install virtualmin
https://hostpresto.com/community/tutorials/how-to-install-virtualmin-on-centos-7-0/

</pre>

<hr/>
<h3>Installing PHP 71, 72, etc</h3>
<pre>
# install epel
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# install remi 
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm

# enable repo
yum-config-manager --enable epel

#  pointing repo to php version using Remi
# format : yum-config-manager --enable remi-php72 or 71, etc
yum-config-manager --enable remi-php72

# install yum-utils
yum install yum-utils

 #  ———————— IMPORTANT NOTES ————————
# run yum update and write these result to your notepad
# modules that said deprecated, must be uninstalled for 
yum check-update  

# If you have PHP modules that are obsolete i would suggest to remove them with yum remove 
# php-* (e.g. yum remove php-mysql).

# Obsoleting Packages
# php-mysqlnd.x86_64                7.2.8-1.el7.remi                  remi-php72
# php-mysql.x86_64              5.4.16-45.el7                     @base
# we will install these modules later
Systemctl restart httpd 

# Upgrade php with
yum update 

# Hope you saved the results from step 7 because now we need them. Check what modules were # obsolete and removed and now we will bring them back with yum install php-** (e.g. yum install # php-mysql). Pay attention that some modules could have different names in never PHP versions # so you should check (Google or Remi website) before proceeding with this task.

#restart 
systemctl restart httpd 
yum clean all

# you will open Virtualmin and show POST Installation Wizard 
</pre>
—————————————————
<h3>How to upgrade to other PHP versions (7.0, 7.1, 7.2)</h3>
# after this you can start install php version
yum install php72-php-gd php72-php-json php72-php-mbstring php72-php-mysqlnd 
php72-php-xml php72-php-xmlrpc php72-php-opcache



<hr/>

<!-- NEW MDDULE -->
<h3>Installing php 7.2</h3>
Secara default, Virtualmin ini akan menginstal LAMP package dengan php 5.4 dan 7.0. Secara fitur, memang virtualmin bisa dibuat untuk menjalankan beberapa versi php tertentu dalam 1 server, jika anda ingin melakukan instalasi php 7.2 berikut tutorialnya:<br/>

<pre>
@article upgrade virtualmin to php 72
https://www.virtualmin.com/node/48280

@removed package
yum remove php-mysql.x86_64

# install package 7.2
yum install php72-php-gd php72-php-json php72-php-mbstring php72-php-mysqlnd 
php72-php-xml php72-php-xmlrpc php72-php-opcache
</pre>

<h3>Composer dengan versi php tertentu</h3>
Jika anda sudah install composer, secara default server akan pointing ke php 5.4, walau di panel virtualmin anda pointing ke 7.0<br/>
Untuk pointing composer dari 5.4 ke 7.0 jalankan : 
<pre>
# when composer not working to php as version in phpinfo use this
# jika terjadi error, sesuaikan path dengan file yang ada di pesan error
scl enable rh-php70 bash

# Jika anda telah melakukan update ke php7.2 gunakan:
# allow composer to update php version
scl enable php72 bash

# lalu check php version sekarang
php -v
</pre> 

<h3>Remote Mysql GUI</h3>


Source<br/>
<a href=""></a><br/>
<a href=""></a>

<div class="author">By Rusdi</div><br/>
	
	</div>
	
	<div class="col-md-2">
	<?php $this->load->view('tutorial/sidemenu') ?>
	</div>
</div>