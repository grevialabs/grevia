<!--
<div class="container" style="bottom:0">
	<div class="col-md-12" style="bottom:0">
	<ul class="list-inline topList">
		<li style="border-left:1px solid #000">Test</li>
		<li style="border-left:1px solid #000">AA</li>
		<li style="border-left:1px solid #000">AABB</li>
	</ul>
	</div>
</div>
-->
<!-- Static navbar -->
	<nav class="navbar navbar-fixed-top bgOrg">
      <div class="container">
        <div class="navbar-header lnkLogo">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            
            <span class="fa fa-bars" style="max-height:10px;padding:none"></span>
          </button>
          <a style="padding-top:6px" class="navbar-brand no-u" href="<?php echo base_url()?>"><img src="<?php echo base_url().'asset/images/'?>logo_grevia_small.png" alt="Group Evangelist Internet Indonesia" title="Group Evangelist Internet Indonesia"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav lnkMenu">
			<!--<li><a href="<?php echo base_url()?>"><i class="fa fa-home" aria-hidden="true"></i> Home <?php echo show_active_menu('index')?></a></li>
			<li><a href="<?php echo base_url()?>forum"><i class="fa fa-comments"></i> Forum</a></li>-->
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-newspaper-o"></i> <?php echo ARTICLE?> <span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li class=""><a href="<?php echo base_url()?>/articlecategory"></a></li>
					<?php 
					$list_article_category = $this->articlecategory_model->get_list();
					$list_article_category = $list_article_category['data'];
					foreach($list_article_category as $rsr) 
					{
					?>
					<li class=""><a class="<?php if($this->uri->segment(2) && $this->uri->segment(2) == $rsr['slug']) echo 'active';?>" href="<?php echo base_url().'articlecategory/'.$rsr['slug'];?>"><i class="fa fa-rocket fa-fw"></i>&nbsp;<?php echo $rsr['category_name']?></a></li>
					<?php } ?>
		
				</ul>
			</li>
			<li><a href="<?php echo base_url()?>about"><?php echo ABOUT_US?></a></li>
            <li><a href="<?php echo base_url()?>contact"><?php echo CONTACT?></a></li>
			<li>
				
				<form class="navbar-form navbar-left" role="search" action="<?php echo base_url()?>search" method="get">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search" name="q" value="<?php if (is_filled(get('q'))) echo get('q')?>">
					<button type="submit" class="btn btn-default"> <span class="glyphicon glyphicon-search"></span></button>
				</div>
				</form>
				<!--
				<form class="navbar-form input-group" method="get" action="<?php echo base_url()?>search" value="<?php if (is_filled(get('q'))) echo get('q')?>">
				
				<div class="input-group-btn">
					<input class="form-control" type="text" placeholder="Search..." name="q">
					<button class="btn btn-info">
					<span class="glyphicon glyphicon-search"></span>
					</button>
				</div>
				</form>
				-->
			</li>
			
          </ul>
		  <?php 
		  //var_dump(decrypt($_COOKIE['hash']));die;
		  //$hash = explode(',',$_COOKIE['hash']);
		  
		  ?>
		  <ul class="nav navbar-nav navbar-right clrWht lnkMenu">
			<?php if (is_member()) { ?>
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> <?php echo MENU_MY_ACCOUNT?> <span class="caret"></span></a>
			  <ul class="dropdown-menu " role="menu">
                <li><a href="<?php echo base_url()?>member/profile"><?php echo MENU_MY_PROFILE?></a></li>
                <li><a href="<?php echo base_url()?>logout"><?php echo MENU_LOGOUT?></a></li>
              </ul>
			</li>
            <?php } else { ?>
			<!-- <li><a href="<?php echo base_url()?>login"><i class="fa fa-user"></i> <?php echo MENU_LOGIN_REGISTER?></a></li> -->
			<?php } ?>
			
		  </ul>
          <!--
		  <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Default</a></li>
            <li class="active"><a href="./">Static top <span class="sr-only">(current)</span></a></li>
            <li><a href="../navbar-fixed-top/">Fixed top</a></li>
          </ul>
		  -->
        </div><!--/.nav-collapse -->
      </div>
    </nav><br/><br/><br/>
	
	<!-- START BALOON SUBSCRIBE -->
	<div style="display:none">
		<div id="inline_content" class="padLrg" style="max-height:480px;max-width:400px;line-height:170%">
			<form action="subscribe" method="post"><span class="fntBig fntBld upper">Subscribe</span><hr/>
			<div class="w550">
				<div class='talCnt'><img src="<?php echo base_url()?>asset/images/logo-grevia.png" width='230px'/></div>
				<div class='w300'>Dapatkan berita terbaru seputar startup dan inspirasi technopreneurship dengan berlangganan newsletter Grevia.
				<p><input type="text" class="wdtFul" maxlength="54" placeholder="Nama..." name="txtSubscribeName" value="<?php //echo $subscribeName?>" required /></p>
				<p><input type="text" class="wdtFul" maxlength="54" placeholder="Email..." name="txtSubscribeEmail" value="<?php //echo $subscribeEmail?>" required /></p>
				<p><button class="btn btn-success wdtFul" name='btnSubscribe' value="Subscribe"/>Subscribe</button></p>
				<?php //if(isFilled($sMessage)) echo message($sMessage);?>
				<div class="clrBth"></div>
				</div>
			</div>
			</form>
		</div>
	</div>
	<!-- END BALOON SUBSCRIBE -->
	
	
