<?php
global $PAGE_TITLE, $PAGE_HEADER, $BREADCRUMB, $META_KEYWORDS, $META_DESCRIPTION;
 
if (!isset($PAGE)) $PAGE = DEFAULT_PAGE_TITLE . ' - '.DEFAULT_PAGE_DESCRIPTION;
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE;

if (!isset($META_KEYWORDS)) $META_KEYWORDS = META_KEYWORDS;
if (!isset($META_DESCRIPTION)) $META_DESCRIPTION = META_DESCRIPTION; 
if (isset($OG_TITLE)) $PAGE_TITLE = $OG_TITLE;
if (!isset($TWITTER_URL)) $TWITTER_URL = current_url();

?>
<!DOCTYPE html>
<html lang="id">
<head itemscope itemtype="http://schema.org/WebSite">
	<meta name="google-site-verification" content="NM1j6BG3n_-7PcXeKwQnS6R-JABiEe2PL7Lm7M_Tbjo" />
	
	<meta property="fb:admins" content="<?php echo FACEBOOK_ADMIN_ID?>" />
	<meta property="fb:app_id" content="<?php echo FACEBOOK_APP_ID?>" />
	
	<?php if (isset($OG_TITLE)) { ?>
	<meta property="og:title" content="<?php echo $OG_TITLE ?>" />
	<?php } ?>
	<meta property="og:url" content="<?php echo current_url() ?>" />
	
	<?php if (isset($OG_IMAGE)) { ?>
	<meta property="og:image" content="<?php echo $OG_IMAGE ?>" />
	<?php } ?>
	
	<?php if (isset($OG_DESCRIPTION)) { ?>
	<meta property="og:description" content="<?php echo $OG_DESCRIPTION ?>" />
	<?php } ?>
	
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="<?php echo DEFAULT_WEBSITE_DOMAIN?>" />
	
	<?php if (isset($META_ARTICLE_AUTHOR)) { ?>
	<meta property="article:author" content="https://www.facebook.com/<?php echo $META_ARTICLE_AUTHOR?>" />
	<?php } ?>	
	<meta property="article:publisher" content="https://www.facebook.com/greviacom" />
	
	<meta name="twitter:title" content="<?php if (isset($PAGE_TITLE)) echo $PAGE_TITLE ?>" />
	<meta name="twitter:url" content="<?php echo $TWITTER_URL ?>" />
	<meta name="twitter:description" content="<?php echo $META_DESCRIPTION ?>" />
	<meta name="twitter:card" content="article" />
	
	<meta name="keywords" content="<?php echo $META_KEYWORDS ?>" />
	<meta name="description" content="<?php echo $META_DESCRIPTION ?>" />
	<!--<meta charset="utf-8">-->
	<title itemprop="name"><?php echo $PAGE_TITLE ?></title>
	<link rel="canonical" href="<?php echo current_url()?>" itemprop="url">
	<link rel="shortcut icon" href="<?php echo base_url()?>favicon.ico">
	
	<?php
	if(!is_internal()) {
	?>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-56560352-1', 'auto');
	  ga('send', 'pageview');

	</script>

	<?php } ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->
	
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160708"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/font-awesome-animation.min.css?v=20160708"/>
	
	<!-- Include required JS files -->
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/shCore.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>asset/js/shAutoloader.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/shBrushPhp.js"></script>
	 
	<!-- Include *at least* the core style and default theme -->
	<link href="<?php echo base_url()?>asset/css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>asset/css/shThemeDefault.css" rel="stylesheet" type="text/css" />
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map"/>
	<!--<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css"/>-->
	
    <link href="<?php echo base_url()?>asset/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.css"/>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script>
	<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery.shiftcheckbox.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			$("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			//$('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
		});
		
		/* INSERT class parentcheckbox in wrapper checkbox to activate */
		$('.parentcheckbox').shiftcheckbox({
			checkboxSelector : ':checkbox',
			//selectAll        : $('.chkbox '),
			ignoreClick      : 'a',
		});
	});
	window.sr = new scrollReveal();
	</script>
	
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/colorbox.css" />
	<script src="<?php echo base_url()?>asset/js/jquery.colorbox.js"></script>
	<?php
	// SHOW POP UP SUBSCRIBE IF NO COOKIE
	if (isset($_COOKIE['is_show_subscribe']) && $_COOKIE['is_show_subscribe'] != '1')
	{
	?>
	<script>
	// $(document).ready(function(){
		// $.colorbox({inline:true, href:"#inline_content"});
	// });
	</script>
	<?php 
	}
	else
	{
		/* SHOW FLOATING SUBSCRIBE IF MORE THAN 100 SECONDS IN SHOWARTICLE*/
		if (strpos(current_url(), 'article/') == TRUE ) {

	?>
	<script>
	// $(document).ready(function(){
		// setTimeout(function() {
			// $.colorbox({inline:true, href:"#inline_content"});
		// },100000);
	// });
	</script>
	<?php
		}
	} 
	?>
	
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}

?>
<body>
<?php include_once 'navheader_admin.php';?>

<div class="container">	
	<div class="row">
		<div class="col-xs-12" style="min-height:500px">
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2>'.$PAGE_HEADER.'</h2>' ?>
			<!--<h1>Selamat datang</h1>
			<div class="alert alert-warning"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>&nbsp;Email / Password tidak valid.</div>-->
			<?php echo $CONTENT; ?>
		</div>
	</div>
</div>

<?php include_once 'navfooter.php';?>


</body>
</html>