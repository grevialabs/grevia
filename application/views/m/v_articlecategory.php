<div class="col-xs-6 ">
	<?php 
	$bg = 'bg-info';
	if (!$this->uri->segment(2) || $this->uri->segment(2)=='all') 
		$bg = 'bg-primary lnkWht';
	?>
	<div class="padMed <?php echo $bg ?>">
	<a class="" href="<?php echo base_url()?>articlecategory/all"><i class="glyphicon glyphicon-tag"></i>&nbsp; All</a>
	</div>
	<?php 
	  $list_article_category = $this->articlecategory_model->get_list();
	  $list_article_category = $list_article_category['data'];
	  foreach($list_article_category as $rsr) 
	  {
	  $bg = 'bg-primary lnkWht';
	  ?>
		<div class="padMed <?php if($this->uri->segment(2) == $rsr['slug']) echo 'bg-primary lnkWht'; else echo 'bg-info'?>">
		<a class="" alt="Artikel Kategori <?php echo $rsr['category_name']?>" title="Artikel Kategori Startup" href="<?php echo base_url().'articlecategory/'.$rsr['slug']?>"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> <?php echo $rsr['category_name']?></a>
		</div>
	<?php } ?>
	
	<br/>
</div>
<div class="col-xs-9">
<?php

//$data = (array) json_decode($data);
if (!empty($data))
{
	$str = "";
	$total_rows = $data['total_rows'];
	$list_data = $data['data'];
	?>
	<div class="row">
	<?php
	$i = 0;
	if (is_numeric($page) && $page > 0) 
	{
		$i = ($page - 1) * $offset;
	}
	foreach($list_data as $key => $rs)
	{
		$rs = (array) $rs;
		$i += 1;
		$title = $rs['title'];
		$image = base_url().'asset/images/article/'.$rs['image'] ;
		$url = base_url().'article/'.$rs['article_id'].'/'.$rs['slug'];
		$alt = "Artikel ".$rs['title'];
		?>
		<div class="col-xs-12" source="category" segment="category">
			<a tracker_name="image-<?php echo $key+1?>" class="adsTrack fntMd" href="<?php echo $url ?>" alt="<?php echo $alt?>" title="<?php echo $alt?>"><img data-original="<?php echo $image ?>" src="<?php echo $image; ?>" class="lazy img-responsive wdtFul"/>
			</a>
			<a tracker_name="article-<?php echo $key+1?>" class="adsTrack fntMd" href="<?php echo $url ?>" alt="<?php echo $alt?>" title="<?php echo $alt?>"><?php echo $title?></a><br/><br/>
			<?php if(isset($rs['short_description'])) echo $rs['short_description'].'</br>'; ?><br/>
			<div source="category" segment="category" class="talRgt lnkBlu" style="padding:5px"><a tracker_name="readmore-<?php echo $key+1?>" class="adsTrack" href="<?php echo $url?>" alt="<?php echo $alt?>" title="<?php echo $alt?>">Baca selengkapnya</a></div>
			<div style="border-top: 3px #0089C9 solid"></div><br/>
		</div>
		<?php if($i%2 == 0) { ?>
		<div class="clearfix"></div>
		<?php
		}		
	}
	?>
	</div>
	<?php
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
}


?>
</div>