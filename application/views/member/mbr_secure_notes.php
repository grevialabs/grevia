<?php 
global $BREADCRUMB, $PAGE_TITLE, $MODULE;

$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, SECURE_NOTES);
$MODULE = SECURE_NOTES;
$PAGE_TITLE = SECURE_NOTES.' - '.DEFAULT_PAGE_TITLE;

if (isset($_POST['btnUpdate'])) {
	$member_id = member_cookies('member_id');
	
	$secure_notes = NULL;
	if (isset($_POST['secure_notes'])) $secure_notes = $_POST['secure_notes'];
	$param = array(
		'secure_notes' => encrypt($secure_notes)
	);
	// debug($param);die;
	if ($update) {
		$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
	}
	redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
}

if (is_member()) {
	$data = $this->member_model->get(array('member_id' => member_cookies('member_id')));
	
	// $momo = "
	// ah 'sugoi'<script>
	// </script>?
	// momotaro
	// ";
	// $momo = $this->db->escape_str($momo);
	
	// debug($momo);
	// die;
}

?>
<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h2 class="b"><?php echo $MODULE?></h2>
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	if ($this->session->flashdata('message')) echo message($this->session->flashdata('message')).BR;
	?>
	<?php 
	if (!empty($data)) {
		$obj = $data;
		
		$obj['secure_notes'] = decrypt($obj['secure_notes']);
		$obj['secure_notes'] = to_html($obj['secure_notes']); 
	?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'>
				<h3 class="b">Secure Notes</h3>
				Notes yang di-enkripsi<br/>
				<textarea class='form-control mceEditor' name='f_secure_notes' id='f_secure_notes' placeholder='Enter Secure Notes' style='height:400px'><?php echo $obj['secure_notes']?></textarea>
			</div>
		</div>
		<div class='form-group padLeft'>
			<button class='btn btn-success' name='btnUpdate' value='1'><?php echo UPDATE?></button>
		</div>
	</form>
	<?php
	}
	?>
</div>

<!-- TINYMCE -->
<script src="<?php echo base_url()?>asset/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
        mode : "specific_textareas",
        editor_selector : "mceEditor",
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste textcolor youtube"
        ],

        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify | styleselect fontselect fontsizeselect | link unlink image | forecolor backcolor | youtube | bullist numlist | outdent indent | table | hr | subscript superscript | charmap emoticons | ",
        toolbar2: " ",

        menubar: false,
        toolbar_items_size: 'medium'
});</script>
<!-- TINYMCE -->