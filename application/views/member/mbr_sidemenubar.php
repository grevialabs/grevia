	<?php
	global $current_segment;
	if ($this->uri->segment(2)) $current_segment = $this->uri->segment(2);
	
	function member_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	
	function admin_active($page) 
	{
		global $current_segment;
		$str = '';
		if ($current_segment === $page)
		{
			$str = 'active';
		}
		return $str;
	}
	$member_email = member_cookies('email');
	?>
	Hola, <?php echo member_cookies('full_name')?><hr>

	<div class="panel panel-info">
		<div class="list-group">
			<a class="list-group-item <?php echo member_active('editprofile').member_active('profile')?>" href="<?php echo base_url().'member/profile'?>"><i class="fa fa-child fa-fw" ></i>&nbsp; <?php echo MENU_MY_PROFILE?></a>
			<?php if ($member_email == 'rusdi.karsandi@gmail.com') { ?>
			<a class="list-group-item <?php echo member_active('article')?>" href="<?php echo base_url().'member/article'?>"><i class="fa fa-book fa-fw"></i>&nbsp; <?php echo MENU_MY_ARTICLE?></a>
			<?php } ?>
			<a class="list-group-item <?php echo member_active('secure_notes')?>" href="<?php echo base_url().'member/secure_notes'?>"><i class="fa fa-folder-o fa-fw"></i>&nbsp; Secure Notes</a>
			<!--<a class="list-group-item <?php echo member_active('forum')?>" href="<?php echo base_url().'member/forum'?>"><i class="fa fa-pencil fa-fw"></i>&nbsp; <?php echo MENU_MY_FORUM?></a>-->
			<!--<a class="list-group-item <?php echo member_active('setting')?>" href="<?php echo base_url().'member/setting'?>"><i class="fa fa-cog fa-fw"></i>&nbsp; <?php echo MENU_MY_SETTING?></a>-->
			<a class="list-group-item <?php echo member_active('changepass')?>" href="<?php echo base_url().'member/changepass'?>"><i class="fa fa-lock fa-fw clrYlw"></i>&nbsp; <?php echo MENU_CHANGE_PASSWORD?></a>
			<a class="list-group-item <?php echo member_active('video')?>" href="<?php echo base_url().'member/video'?>"><i class="fa fa-video-camera fa-fw clrYlw"></i>&nbsp; <?php echo MENU_VIDEO?></a>
			<a class="list-group-item <?php echo member_active('todo')?>" href="<?php echo base_url().'member/todo'?>"><i class="fa fa-pencil-square-o fa-fw clrYlw"></i>&nbsp; <?php echo 'Todo list'?></a>
		</div>
	</div>