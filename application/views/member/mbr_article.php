<?php 
global $BREADCRUMB, $PAGE_TITLE, $MODULE;

$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, ARTICLE);
$MODULE = ARTICLE;
$PAGE_TITLE = ARTICLE.' - '.DEFAULT_PAGE_TITLE;

global $get_do, $get_article_id,$message;
$message = array();
$message['message'] = '';

if (is_filled(get('article_id'))) $get_article_id = get('article_id');
if (is_filled(get('do'))) $get_do = get('do');
if (post('insert')) {
	
	$article_category_id = post("article_category_id");
	if (isset($article_category_id)) $article_category_id = implode(',', post("article_category_id"));
	$title = filter( post("title") );
	$slug = filter( post("slug") );
	$content = post("content");
	$short_description = filter( post("short_description") );
	$quote = filter(post("quote"));
	$tag = filter( post("tag") );
	//$image = filter( post("f_image") );
	$publish_date = filter( post("publish_date") );
	$image_description = filter( post("image_description") );
	$is_publish = 0;
	if (post('is_publish') == 1 ) $is_publish = 1 ;
	//$short_url = filter( post("short_url") );
	
	if (is_filled($title)) {
		if (post('is_publish') == 1 ) $is_publish = 1 ;else $is_publish = 0;
		
		// IMAGE
		if (is_filled($_FILES["f_image"]["name"])) 
		{
			$list_allowed_ext = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["f_image"]["name"]);
			$file_image_extension = end($temp);
			$file_image_type = $_FILES["f_image"]["type"];
			$file_image_size = $_FILES["f_image"]["size"];
			$file_image_name = $_FILES["f_image"]["name"];
			
			$str = "";
			$is_image_valid = FALSE;
			
			if (($file_image_type == "image/gif") || ($file_image_type == "image/jpeg") || ($file_image_type == "image/jpg")	|| ($file_image_type == "image/pjpeg") || ($file_image_type == "image/x-png")	|| ($file_image_type == "image/png"))
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Tipe bukan gambar.<br/>";
			}
			
			if ($file_image_size < 1000000)
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Size tidak cukup.<br/>";
			}
			
			if (in_array($file_image_extension, $list_allowed_ext))
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$message['message'].= "- Extension salah.<br/>";
			}
			
			if ($is_image_valid) 
			{
				$image = $file_image_name;
				if ($_FILES["f_image"]["error"] > 0) 
				{
					$message['message'].= "Image failed upload because error.";
				} 
				else 
				{
					if (is_internal())$upload_directory = "D:/xampp/htdocs/grevia.com/asset/images/article/";
					else $upload_directory = "/home/grevia/public_html/asset/images/article/";
					
					if (file_exists($upload_directory . $file_image_name)) 
					{
						echo $file_image_name . " already exists. ";
					} 
					else 
					{
						$is_move_success = move_uploaded_file($_FILES["f_image"]["tmp_name"],$upload_directory . $file_image_name);
						if ($is_move_success) 
						{
							$image = $file_image_name;
						} 
						else 
						{
							$message['message'].= "Image failed upload.";
						}
					}
				}
			}
		}
		else
		{
			$image = "";
		}
		
		$objArticle = array();
		$param['article_category_id'] = $article_category_id;
		$param['title'] = $title;
		$param['slug'] = $slug;
		$param['content'] = $content;
		$param['short_description'] = $short_description;
		$param['quote'] = $quote;
		$param['tag'] = $tag;
		$param['view'] = 0;
		if(is_filled($image)) $param['Image'] = $image;
		$param['image_description'] = $image_description;
		$param['publish_date'] = $publish_date;
		$param['is_publish'] = $is_publish;
		$param['short_url'] = $short_url;
		
		$save = $this->article_model->save($param);
		
		// UPDATE TO SHORT URL API
		if ($save && !is_internal()) 
		{
			$get_last_article = $this->article_model->get(array('last' => TRUE));
			
			$longUrl = base_url().'article/'.$get_last_article['article_id'].'/'.$get_last_article['slug'];

			// Get API key from : http://code.google.com/apis/console/
			// $apiKey = 'AIzaSyBcvHZffHXEK4dCxXC_HGCk4s6huhP6E4Q';
			$apiKey = GOOGLE_SERVER_API_KEY;

			$postData = array('longUrl' => $longUrl);
			$jsonData = json_encode($postData);

			$curlObj = curl_init();

			curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.$apiKey);
			curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curlObj, CURLOPT_HEADER, 0);
			curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
			curl_setopt($curlObj, CURLOPT_POST, 1);
			curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

			$response = curl_exec($curlObj);

			// Change the response json string to object
			$json = json_decode($response);

			curl_close($curlObj);

			$shortLink = get_object_vars($json);
			
			// UPDATE
			$update = $this->article_model->update($get_last_article['article_id'],array('short_url' => $shortLink['id']));
		}
		
		if ($save) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?do=edit&article_id='.$get_last_article['article_id']);
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

if (post('update') && is_numeric($get_article_id)) {
	$objArticle = array();
	$objArticle = $this->article_model->get(array('article_id' => $get_article_id));
	if (!empty($objArticle)) {
		
		// IMAGE
		if (is_filled($_FILES["f_image"]["name"])) 
		{
			$list_allowed_ext = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["f_image"]["name"]);
			$file_image_extension = end($temp);
			$file_image_type = $_FILES["f_image"]["type"];
			$file_image_size = $_FILES["f_image"]["size"];
			$file_image_name = $_FILES["f_image"]["name"];
			
			$str = "";
			$is_image_valid = FALSE;
			
			if (($file_image_type == "image/gif") || ($file_image_type == "image/jpeg") || ($file_image_type == "image/jpg")	|| ($file_image_type == "image/pjpeg") || ($file_image_type == "image/x-png")	|| ($file_image_type == "image/png"))
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Tipe bukan gambar.<br/>";
			}
			
			if ($file_image_size < 1000000)
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Size tidak cukup.<br/>";
			}
			
			if (in_array($file_image_extension, $list_allowed_ext))
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Extension salah.<br/>";
			}
			
			if ($is_image_valid) 
			{
				$image = $file_image_name;
				if ($_FILES["f_image"]["error"] > 0) 
				{
					$message['message'].= "Image failed upload because error.";
				} 
				else 
				{
					if (is_internal())$upload_directory = "D:/xampp/htdocs/grevia.com/asset/images/article/";
					else $upload_directory = "/home/grevia/public_html/asset/images/article/";
					if (file_exists($upload_directory . $file_image_name)) 
					{
						$message['message'].= "Image already exists. ";
					} 
					else 
					{
						$is_move_success = move_uploaded_file($_FILES["f_image"]["tmp_name"],$upload_directory . $file_image_name);
						if ($is_move_success) 
						{
							$image = $file_image_name;
						} 
						else 
						{
							$message['message'].= "Image failed upload.";
						}
					}
				}
			}
		}
		else
		{
			$image = "";
		}
		
		$article_category_id = post("article_category_id");
		if (isset($article_category_id)) $article_category_id = implode(',', post("article_category_id"));
		
		$title = filter( post("title") );
		$slug = filter( post("slug") );
		$content = post("content");
		$short_description = filter( post("short_description") );
		$quote = filter( post("quote") );
		$tag = filter( post("tag") );
		$view = filter( post("view") );
		$image_description = filter( post("image_description") );
		$publish_date = filter( post("publish_date") );
		
		$is_publish = 0;
		if (post('is_publish') == 1 ) $is_publish = 1 ;

		//$short_url = filter( post("short_url") );
		
		$param['article_category_id'] = $article_category_id;
		$param['title'] = $title;
		$param['slug'] = $slug;
		$param['content'] = $content;
		$param['short_description'] = $short_description;
		$param['quote'] = $quote;
		$param['tag'] = $tag;
		if(is_filled($image)) $param['Image'] = $image;
		$param['image_description'] = $image_description;
		$param['is_publish'] = $is_publish;
		$param['publish_date'] = $publish_date;
		//$param['short_url'] = $short_url;
		// debug($param);die;
		$update = $this->article_model->update($get_article_id, $param);
		
		if ($update) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::UPDATE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?do=edit&article_id='.$get_article_id);
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?do=edit&article_id='.$get_article_id);
	}
}

if ($this->uri->segment(3)) { 
	$data = $this->member_model->get(array('member_id' => $this->uri->segment(3)));
}
if (is_member()) {
	$data = $this->member_model->get(array('member_id' => member_cookies('member_id')));
}

?>
<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class='col-md-9'>
	<h1 class="title-header">&nbsp;<?php echo $MODULE?></h1><br/>
	<?php
if (is_filled($get_article_id) || $get_do == 'insert' || $get_do == 'edit') 
{
	$obj = NULL;
	if ($get_do == 'edit') { 
		$obj = $this->article_model->get(array('article_id' => $get_article_id));
	}

	if (!empty($obj) || $get_do == 'insert') {

?>
	<?php if(is_filled($message['message']))echo print_message($message['message']).BR?>
	<form class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>
		<div class='form-group form-group-sm form-group form-group-sm-sm'>
			<label for='f_article_category_id' class='col-sm-2'>Category</label>
			<div class='col-sm-10'>
			<?php 
			$data_category = $this->articlecategory_model->get_list();
			$obj_list_article_category = $data_category['data'];
			
			$arr_article_category = explode(',', $obj['article_category_id']);
			?>
			<select class='form-control multiple-select' name='f_article_category_id[]' multiple="multiple" size="<?php echo count($obj_list_article_category)?>" style="height:100%" required><?php
			
			foreach ($obj_list_article_category as $category) {
				$selected = '';
				if (in_array($category['article_category_id'], $arr_article_category, true)) $selected = ' selected';;
				echo '<option value="'.$category['article_category_id'].'" '.$selected.'>'.$category['category_name'].'</option>';
			}
			?></select>
			

			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_title' class='col-sm-2'>Title <sup class="b clrRed">* required</sup></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_title' id='f_title' placeholder='Enter title' value='<?php if (isset($obj['title'])) echo $obj['title']?>' required>
			<span id="helpBlock" class="help-block f12">Judul artikel</span>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_slug' class='col-sm-2'>Slug</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_slug' id='f_slug' placeholder='Enter Slug' value='<?php if (isset($obj['slug'])) echo $obj['slug']; ?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'>Content<br/><textarea style='height:500px' class='form-control mceEditor' name='f_content' id='f_content' cols='40' rows='45'><?php if (isset($obj['content'])) echo $obj['content']; ?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_short_description' class='col-sm-2'>ShortDescription</label>
			<div class='col-sm-10'><textarea class='form-control' name='f_short_description' id='f_short_description' placeholder='Enter Short Description' style='height:80px'><?php if (isset($obj['short_description'])) echo $obj['short_description']; ?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_quote' class='col-sm-2'>Quote</label>
			<div class='col-sm-10'><textarea class='form-control' name='f_quote' id='f_quote' placeholder='Enter Quote' style='height:80px' ><?php if (isset($obj['quote'])) echo $obj['quote']; ?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_tag' class='col-sm-2'>Tag</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_tag' id='f_tag' placeholder='Enter Tag' value='<?php if (isset($obj['tag'])) echo $obj['tag']; ?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_image' class='col-sm-2'>Image</label>
			<div class='col-sm-10'>
				<div class='input-group'>
				<span class="input-group-addon">
					<i class="glyphicon glyphicon-picture"></i>
				</span>
				<input type='file' class='form-control' id='f_image' name='f_image' onchange="$('#previewImage')[0].src = window.URL.createObjectURL(this.files[0]);$('#previewImage').show();" /></div>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><?php if (is_filled($obj['image'])) echo '<img src="'.base_url().'asset/images/article/'.$obj['image'].'" class="wdtFul"/>'.BR; ?><img class='previewImage' id='previewImage' /></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_image_description' class='col-sm-2'>Image Description</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_image_description' id='f_image_description' placeholder='Enter Image Description' value='<?php if (isset($obj['image_description'])) echo $obj['image_description']; ?>'/></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_publish_date' class='col-sm-2'>Publish Date</label>
			<div class='col-sm-10'>
			<input type='text' class='form-control datepicker' name='f_publish_date' id='f_publish_date' placeholder='Enter Publish Date' value='<?php if (isset($obj['publish_date'])) echo $obj['publish_date']; ?>'/>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_is_publish' class='col-sm-2'>Is Publish</label>
			<div class='col-sm-10'>
			<input type='checkbox' name='f_is_publish' id='f_is_publish' value='1' <?php if ((isset($obj['is_publish']) && $obj['is_publish'] == '1') || $do == "insert") echo 'checked';?> />
			
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_short_url' class='col-sm-2'>Short URL</label>
			<div class='col-sm-10'>
			<?php 
			if ($get_do != "insert") 
			{
				?>
			<input class='form-control' name='f_short_url' id='f_short_url' placeholder='Enter ShortUrl' value='<?php if (isset($obj['short_url'])) echo $obj['short_url']; ?>' <?php if (isset($obj['short_url'])) echo 'disabled'?>>
				<?php
			}
			else echo "*Auto generate";
			?>
			</div>
		</div>
		
		<div class='form-group form-group-sm'>
		<?php if($get_do == 'insert') { ?>
		<button class='btn btn-success' name='f_insert' value='1'><?php echo SAVE?></button>
		<?php } else if ($get_do == 'edit'){ ?>
		<button class='btn btn-success' name='f_update' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
	</form>
	<?php
	} else {
		print_message('Data not found');
	}
} else {
	?>
	<?php if (is_filled($message['message']))echo print_message($message['message']).BR?>
	<?php if ($get_do != "insert") { ?>
	<form class=''><button class='btn btn-success' name='do' value='insert'><?php echo ADD.' '.ARTICLE;?></button></form><br/>
	<?php } ?>
	<table class='table table-hover table-striped sm'>
	<tr class='fntBld'>
		<td style='width:1px'>#</td>
		<td style='width:140px '>Date</td>
		<td style=''>Title</td>
		<td style='width:1px'>View</td>
		<td style='width:40px'>Publish</td>
		<td style='width:180px' class='talCnt'>Option</td>
	</tr>
	<?php
	$page = 1;
	$limit = 0;
	$i = 1;
	$offset = OFFSET;
	//$offset = 20;
	if (is_numeric(get('page'))) $page = get('page');
	if($page > 1) {
		$limit = ($page - 1 )* $offset ;
		$i = $limit+1;
	}

	$objList = NULL;
	$filter = array('paging' => TRUE, 
					'limit' => $limit, 
					//'Author' => 'rusdi', 
					'is_publish' => 'all',
					'order' => 'article_id DESC',
					'creator_id' => member_cookies('member_id'), 
					'offset' => $offset
					);
	
	$result = $this->article_model->get_list($filter);
	$total_rows = $result['total_rows'];
	$data = $result['data'];

	if (!empty($data)) {
		foreach ($data as $obj) {
			$id = $obj['article_id'];
			$is_publish = '<i class="fa fa-remove text-danger"></i>';
			if ($obj['is_publish']) $is_publish = '<i class="fa fa-check text-success"></i>';
	?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo date(DATE_FORMAT, strtotime($obj['creator_date']));?></td>
		<td class=''><?php echo '<span class="fntBld">'.$obj['category_name'].'</span>'.BR.$obj['title'];?></td>
		<td class='talCnt'><?php echo $obj['view'];?></td>
		<td class='talCnt'><?php echo $is_publish;?></td>
		<td class='talCnt'><a href='<?php echo base_url().'article/'.$obj['article_id'].'/'.$obj['slug'];?>'><button class='btn btn-sm btn-info'>Preview</button></a> <a href='?do=edit&article_id=<?php echo $id;?>'><button class='btn btn-sm btn-info'>Edit</button></a></td>
	</tr>
	<?php  
			$i++;
		}
		?>
	<tr><td colspan='100%' class='fntBld'>Total <?php echo $result['total_rows']; ?> row(s)</td></tr>
		<?php
	} else {
		?>
	<tr><td colspan='100%' align='center'>No data</td></tr>
		<?php
	}
	?>
	</table>
	
<?php 
	if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
}
?>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- TINYMCE -->
<script src="<?php echo base_url()?>asset/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
        mode : "specific_textareas",
        editor_selector : "mceEditor",
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste textcolor youtube"
        ],

        toolbar1: "bold italic underline | alignleft aligncenter alignright alignjustify | styleselect fontselect fontsizeselect | link unlink image | forecolor backcolor | youtube | bullist numlist | outdent indent | table | hr | subscript superscript | charmap emoticons | ",
        toolbar2: " ",

        menubar: false,
        toolbar_items_size: 'medium'
});</script>
<!-- TINYMCE -->
<script>
$("#f_title").keyup(function () {
    var textValue = $(this).val();
    textValue = textValue.toLowerCase();
	textValue = textValue.replace(/[^a-zA-Z0-9]+/g,"-");
    $('#f_slug').val(textValue);
});
$(document).ready(function(){
	//$(".multiple-select").height("auto");
});
</script>