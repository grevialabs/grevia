<?php 
global $BREADCRUMB, $PAGE_TITLE, $MODULE;

$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, VIDEO);
$MODULE = VIDEO;
$PAGE_TITLE = VIDEO.' - '.DEFAULT_PAGE_TITLE;

$get_video_id = $obj_prev_next = $obj_next = $obj_prev = NULL;
if (isset($_GET['video_id'])) $get_video_id = $_GET['video_id'];

$prm['creator_id'] = member_cookies('member_id');
$prm['video_id'] = $get_video_id;
$obj_prev_next = $this->video_model->get_list_prev_next($prm);
if ( ! empty($obj_prev_next['prev'])) $obj_prev = $obj_prev_next['prev'];
if ( ! empty($obj_prev_next['next'])) $obj_next = $obj_prev_next['next'];
// debug($obj_prev_next);
// debug($obj_next);
// debug($obj_prev);
// die;

// Strip v value from URL if contain like "https://www.youtube.com/watch?v=jBCDVlGdAKQ"
function get_url_embed($url,$is_full_url = FALSE)
{
	$v = NULL;
	if ($is_full_url) 
	{
		// return full url video youtube. ex : https://www.youtube.com/watch?v=9yaBAzEee
		if (strpos($url,'?v=') != FALSE) {
			$v = explode('?v=',$url);
			$v = $v[1];
		} else {
			// Full url
			$v = $url;
		}
		$v = 'https://www.youtube.com/watch?v='.$v;
	}
	else 
	{
		// return only v of youtube. ex : 9yaBAzEee
		if (strpos($url,'?v=') != FALSE) {
			$v = explode('?v=',$url);
			$v = $v[1];
		} else {
			$v = $url;
		}
	}
	return $v;
}

function get_image_src($src)
{
	$img = 'https://img.youtube.com/vi/'.$src.'/mqdefault.jpg';
	return $img;
}

/*
Each YouTube video has 4 generated images. They are predictably formatted as follows:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/0.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/1.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/2.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/3.jpg
The first one in the list is a full size image and others are thumbnail images. The default thumbnail image (ie. one of 1.jpg, 2.jpg, 3.jpg) is:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/default.jpg
For the high quality version of the thumbnail use a url similar to this:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg
There is also a medium quality version of the thumbnail, using a url similar to the HQ:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/mqdefault.jpg
For the standard definition version of the thumbnail, use a url similar to this:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/sddefault.jpg
For the maximum resolution version of the thumbnail use a url similar to this:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/maxresdefault.jpg
*/

$base_url = base_url();

// Save
if ($_POST) 
{
	$post = $_POST;
	
	if (isset($post['name']) && isset($post['url']))
	{
		$paramv = NULL;
		$paramv['name'] = $post['name'];
		if (isset($post['videolist_id'])) $paramv['videolist_id'] = $post['videolist_id'];
		$paramv['description'] = $post['description'];
		$paramv['author'] = $post['author'];
		$paramv['url'] = $post['url'];
		$paramv['creator_id'] = 1;
		$paramv['creator_ip'] = get_ip();
		$paramv['creator_date'] = get_datetime();
		// debug($paramv);
		// die;
		$save = $this->video_model->save($paramv);
		if ($save) 
		{
			$print_message = 'berhasil';
			$this->session->set_flashdata('message',$print_message);
			redirect(current_full_url());
			die;
		}
		else
		{
			$print_message = 'Mohon maaf terjadi error.';
		}
	}
	else 
	{
		$print_message = 'Data harus diisi';
	}
}

$get_video_id = NULL;
if (isset($_GET['video_id'])) $get_video_id = $_GET['video_id'];

$obj_video = $tmp = NULL;
if (isset($get_video_id))
{
	$tmp['video_id'] = $get_video_id;
	$obj_video = $this->video_model->get($tmp);
}

//----------------------------------
// Get all video
// $param = NULL;
// $param['creator_id'] = member_cookies('member_id');
// $obj_list_video = $this->video_model->get_list($param);
// $obj_list_video = $obj_list_video['data'];

//----------------------------------
// Get playlist video
$param = NULL;
$param['creator_id'] = member_cookies('member_id');
$obj_list_videolist = $this->videolist_model->get_list($param);
$obj_list_videolist = $obj_list_videolist['data'];
?>
<style>
.slidevi {
    width: 96%;
    overflow: scroll;  
	display: block;
	position: relative;
	margin: 0 auto 0 auto;
}

.slidevi-outer
{
    width: 4000px;
    max-height:200px;
    padding-bottom:5px;
    padding-top:5px;
    background: #fff;
    font-family:Verdana, Tahoma, Sans-Serif;
	display: block;
}

.slidevi-outer .slidevi-item{
float: left;
width: 250px; /*width of each menu slidevi-item*/
height: 180px;
margin-left:10px;

}

.slidevi-outer .slidevi-item:first-child{
margin-left:5px;
}

.slidevi-outer .slidevi-item ul{
margin: 0;
padding: 0;
list-style-type: none;
}

.slidevi-outer .slidevi-item ul li{
padding: 5px;
}

.slidevi-outer .slidevi-item h3{
color: White;
font-weight:bold;
font-family:Verdana, Tahoma, Sans-Serif;
margin: 0 0 5px 0;
padding:5px;
text-shadow: 0px 2px 3px #555;
border:1px dashed #eee;
-webkit-border-radius: 20px;
-moz-border-radius: 20px;
border-radius: 20px;     
}

.slidevi-outer .slidevi-item ul li a{
text-decoration: none;
padding:2px;
}

.slidevi-outer .slidevi-item ul li a:hover{
color:white;
border-left:2px solid #FF9F21;
}

</style>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class='col-md-9'>
	<h1 class="title-header">&nbsp;<?php echo $MODULE?></h1><br/>
	<?php 
	if (isset($print_message)) 
		echo "<div class='alert alert-info'>".$print_message."</div>";
	if ($this->session->flashdata('message')) 
		echo "<div class='alert alert-info'>".$this->session->flashdata('message')."</div>";	
	?>
	<a id="new_video" href="javascript:void(0)" class="btn btn-info btn-sm">New Video</a><br/><br/>
	<div id="new_form_video" class="alert alert-info hide row">
		<form method="post" action="">
			<div class="col-md-12 b text-uppercase fntHdr text-primary">New Video</div>
			
			<div class="col-md-6">
			<input type="text" class="input br wdtFul" name="f_name" placeholder="Nama video" required />
			</div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6">
			<?php 
			if ( ! empty($obj_list_videolist))
			{
				?>
				<select name="f_videolist_id" class="input br wdtFul">
				<?php 
				foreach ($obj_list_videolist as $vl)
				{
				?>
				<option value="<?php echo $vl['videolist_id']?>"><?php echo $vl['name']?></option>
				<?php 
				}
				?>
				</select>
				<?php 
			}
			?>
			</div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6"><input type="text" class="input br wdtFul" name="f_author" placeholder="Author" /></div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6"><textarea class="input br wdtFul" name="f_description" placeholder="Description"></textarea></div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6"><input type="text" class="input br wdtFul" name="f_url" placeholder="URL video" /></div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-12"><button type="submit" class="btn btn-sm btn-success">SUBMIT</button><hr/></div>
		</form>
	</div>
	
	<div>
		<?php 
		// ---------------------------
		// When data found show embed video
		if (isset($obj_video['video_id'])) 
		{
			// debug($obj_video);
			$url = $img = NULL;
			// Strip v value from URL if contain like "https://www.youtube.com/watch?v=jBCDVlGdAKQ"
			$obj_video['url'] = get_url_embed($obj_video['url']);
			$url = 'http://www.youtube.com/embed/'.$obj_video['url'];
			// echo $url;
			// debug($obj_video);
		?>
		<!-- open iframe when needed -->
		<!--
		-->
		<div id="player">
			<iframe width="560" height="349" src="<?php echo $url?>" frameborder="0" allowfullscreen></iframe>
		</div>
		
			<h1 class="b "><?php echo $obj_video['name']?> - <?php echo $obj_video['author']?></h1>
			<h3 class="b ">Playlist <?php echo $obj_video['videolist_name']?></h3>
			<div class="clrBlu f18">Url: <?php echo $obj_video['url']?></div>
			<div class="clrBlu f18">Full Url: <a href="<?php echo get_url_embed($obj_video['url'],TRUE)?>"><?php echo get_url_embed($obj_video['url'],TRUE)?></a></div>
			<?php if (isset($obj_video['description'])) echo 'Deskripsi:'.BR.nl2br($obj_video['description']);?>
			<div class="videoWrapper">
				<div id="playera"></div>
			</div>

			<script src="http://www.youtube.com/player_api"></script>

			<script>
				// create youtube player
				var player;
				function onYouTubePlayerAPIReady() {
					player = new YT.Player('player', {
					  width: '640',
					  height: '390',
					  videoId: '<?php echo $obj_video['url']?>',
					  events: {
						onReady: onPlayerReady,
						onStateChange: onPlayerStateChange
					  }
					});
				}

				// autoplay video
				function onPlayerReady(event) {
					<?php if (isset($_GET['autoplay']) && $_GET['autoplay']) { ?>
					event.target.playVideo();
					<?php } ?>
				}

				// when video ends
				function onPlayerStateChange(event) {        
					if(event.data === 0) {          
						// alert('done');
						<?php if ( ! empty($obj_next)) { ?>
						// redirect to next video
						window.location.replace("<?php echo $base_url.'member/video?video_id='.$obj_next['video_id'];?>");
						<?php } ?>
					}
				}

			</script>
			
			

			<?php 
			// debug($obj_prev['url']);
			
			// get prev v
			$prev_v = $next_v = NULL;
			if ( ! empty($obj_prev)) $prev_v = get_url_embed($obj_prev['url']);
			if ( ! empty($obj_next)) $next_v = get_url_embed($obj_next['url']);
			?>
			
			<!-- Prev Video Start -->
			<div class="row padMed">
				<div class="col-md-6 alert-warning">
					<div class="f36 b clrGry"><< Previous video</div><br/>
					<?php if ( ! empty($obj_prev)) { ?>
					<span class="f24"><?php echo $obj_prev['name']?> by <?php echo $obj_prev['author']?></span>
					<a href="<?php echo $base_url.'member/video?video_id='.$obj_prev['video_id'] ?>"><img src="<?php echo get_image_src($prev_v)?>" class="img img-responsive" /></a>
					<?php } else { ?>
					<div class="f24 b" style="min-height:150px">No video</div>
					<?php } ?>
					<br/>
				</div>
				
				<!-- Next Video -->
				<div class="col-md-6 alert-info">
					<div class="f36 b clrBlu talRgt">Next video >></div><br/>
					<?php if ( ! empty($obj_next)) { ?>
					<span class="f24"><?php echo $obj_next['name']?> by <?php echo $obj_next['author']?></span>
					<a href="<?php echo $base_url.'member/video?video_id='.$obj_next['video_id'] ?>"><img src="<?php echo get_image_src($next_v)?>" class="img img-responsive" /></a>
					<?php } else { ?>
					<div class="f24 b" style="min-height:150px">No video</div>
					<?php } ?>
					<br/>
				</div>
			</div>
			<!-- Prev Video End -->
			<div class="panel panel-info">
				<div class="panel-heading"><h2>Relate Playlist</h2></div>
				<div class="panel-body">
					<div id="relate-playlist"></div>
					<?php 
					// Show list video by videolist
					$param_relate = NULL;
					$param_relate['creator_id'] = member_cookies('member_id');
					$param_relate['videolist_id'] = $obj_video['videolist_id'];
					$param_relate['order'] = 'video_id DESC';
					$param_relate['paging'] = TRUE;
					$param_relate['limit'] = '0';
					$param_relate['offset'] = '10';
					$list_video_relate = $this->video_model->get_list($param_relate);
					$list_video_relate = $list_video_relate['data'];
					// $param_relate['']
					$j_relate = NULL;
					if ( ! empty($list_video_relate)) 
					{
						$j_relate = '<div class="slidevi"><div class="slidevi-outer">';
						foreach($list_video_relate as $key => $vr)
						{
							$active = NULL;
							if ($vr['video_id'] == $obj_video['video_id']) $active = 'bgBlk lnkWht';
							$vr['url'] = get_url_embed($vr['url']);
							$j_relate .= '<div class="slidevi-item '.$active.'"> <a href="'.base_url().'member/video?video_id='.$vr['video_id'].'"><img class="img img-responsive" src="'.get_image_src($vr['url']).'"/>'.$vr['name'].' - '.$vr['author'].'</a></div>';
						}
						$j_relate .= '</div></div>';
						?>
						<script>
						$(document).ready(function(){
							$('#relate-playlist').append('<?php echo $j_relate?>');
						})
						</script>
						<?php
					}
					?>
					</div>
				</div>
		
		<?php 
		}
		// ---------------------------
		?>		
		
		<h1 class="b fntHdr">MY PLAYLIST</h1><br/>
		
		<?php 
		// Show all playlist
		if ( ! empty($obj_list_videolist))
		{
			foreach($obj_list_videolist as $vl)
			{
				?>
			<div class="panel panel-warning">
				<div class="panel-heading"><h2><?php echo $vl['name']?></h2></div>
				<div class="panel-body">
				<span><?php echo $vl['description']?></span><hr/>
				<?php
				// Show list of video by category_id
				$list_video = $param_video = NULL;
				$param_video['creator_id'] = member_cookies('member_id');
				$param_video['videolist_id'] = $vl['videolist_id'];
				$param_video['order'] = 'RAND()';
				$param_video['paging'] = TRUE;
				$param_video['limit'] = '0';
				$param_video['offset'] = '10';
				$list_video = $this->video_model->get_list($param_video);
				$list_video = $list_video['data'];
				
				if ( ! empty($list_video))
				{
					?>
					Show <?php echo count($list_video); ?> data<br/>
					<div class="slidevi">
						<div id="" class="slidevi-outer">
					<?php
					foreach ($list_video as $rsv)
					{
						$url = $img = NULL;
						// Strip v value from URL if contain like "https://www.youtube.com/watch?v=jBCDVlGdAKQ"
						// if (strpos($rsv['url'],'?v=') != FALSE) {
							// $url = explode('?v=',$rsv['url']);
							// $rsv['url'] = $url[1];
						// }
						$rsv['url'] = get_url_embed($rsv['url']);
						$url = 'http://youtube.com/embed/'.$rsv['url'];
				
						// https://img.youtube.com/vi/L54q_24LvY8/hqdefault.jpg.jpg
						// $img = 'https://img.youtube.com/vi/'.$rsv['url'].'/hqdefault.jpg';
						
						?>
						<div class="slidevi-item">
							<a href="<?php echo $base_url.'member/video?video_id='.$rsv['video_id']; ?>" alt="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" title="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" ><img class="img img-responsive" src="<?php echo get_image_src($rsv['url']); ?>" />
							<?php echo $rsv['name'].' - '.$rsv['author']; ?></a>
						</div>
						<!-- Copy & Pasted from YouTube -->
						<!--<iframe width="280" height="200" src="<?php echo $url?>" frameborder="0" allowfullscreen onclick="alert('dame <?php echo $url?>')"></iframe>-->
						<?php
					}
					?>
						</div>
					</div>
					<?php
				}
				else 
				{
					echo "No data".BR;
				}
				?>
				</div>	
			</div>	
			<?php
			}
		}
		else 
		{
			echo "No data".BR;
		}
		// End all playlist
		?>
		<br/><br/>
		
		<h2>UNCATEGORIZED</h2><br/>
		<?php 
		// Uncategorized
		$obj_list_video_uncategorized = $tmp4 = NULL;
		$tmp4['uncategorized'] = TRUE;
		$tmp4['order'] = 'RAND()';
		$obj_list_video_uncategorized = $this->video_model->get_list($tmp4);
		$obj_list_video_uncategorized = $obj_list_video_uncategorized['data'];
		// debug($obj_list_video_uncategorized);
		if ( ! empty($obj_list_video_uncategorized))
		{
			?>
			<div class="slidevi">
				<div id="" class="slidevi-outer">
			<?php
			foreach($obj_list_video_uncategorized as $rsv)
			{
				$url = $img = NULL;
				$rsv['url'] = get_url_embed($rsv['url']);
				?>
				<div class="slidevi-item">
					<a href="<?php echo $base_url.'member/video?video_id='.$rsv['video_id']; ?>" alt="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" title="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" ><img class="img img-responsive" src="<?php echo get_image_src($rsv['url']); ?>" /><?php echo $rsv['name'].' - '.$rsv['author']?></a>
				</div>
				<!-- <iframe width="280" height="200" src="<?php //echo $url?>" frameborder="0" allowfullscreen onclick="alert('dame <?php // echo $url?>')"></iframe> -->
		
			<?php 
			}
			?>
				</div>
			</div>
			<?php
		}
		else 
		{
			echo "No data".BR;
		}
		?>
	</div>
</div>


<style>
.videoWrapper {
	position: relative;
	padding-bottom: 56.25%; /* 16:9 */
	padding-top: 25px;
	height: 0;
}
.videoWrapper iframe {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
</style>

<script src="<?php echo base_url().'asset/js/jquery.iframetracker.js'; ?>">
</script>

<script>
$(document).ready(function(){
	$('#new_video').click(function(){
		$(this).addClass('hide');
		$('#new_form_video').removeClass('hide');
	});
	// new_form_video
	
	var iframeDoc = $('#player').contents().get(0);
	$(iframeDoc).bind('click', function( event ) {
		// do something
		alert('jalan nih');
	});
	
	// $('#player').iframeTracker({
        // blurCallback: function(){
            // // Do something when iframe is clicked (like firing an XHR request)
			// alert('jalan nih');
        // }
    // });
});

// jQuery(document).ready(function($){
	// $('#player').iframeTracker({
		// blurCallback: function(){
			// // Do something when the iframe is clicked (like firing an XHR request)
			// alert('jalan nih');
		// }
	// });
// });
</script>