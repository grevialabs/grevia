<?php 
global $BREADCRUMB, $PAGE_TITLE, $MODULE;

$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, TODO);
$MODULE = TODO;
$PAGE_TITLE = TODO.' - '.DEFAULT_PAGE_TITLE;

global $get_do, $get_todo_id,$message;
$message = array();
$message['message'] = '';

if (is_filled(get('todo_id'))) $get_todo_id = get('todo_id');
if (is_filled(get('do'))) $get_do = get('do');
if (post('insert')) {
	
	$priority = filter( post("title") );
	$slug = filter( post("slug") );
	$content = post("content");
	$short_description = filter( post("short_description") );
	$quote = filter(post("quote"));
	$tag = filter( post("tag") );
	//$image = filter( post("f_image") );
	$publish_date = filter( post("publish_date") );
	$image_description = filter( post("image_description") );
	$is_publish = 0;
	if (post('is_publish') == 1 ) $is_publish = 1 ;
	//$short_url = filter( post("short_url") );
	
	if (is_filled($title)) {
		if (post('is_publish') == 1 ) $is_publish = 1 ;else $is_publish = 0;
		
		// IMAGE
		if (is_filled($_FILES["f_image"]["name"])) 
		{
			$list_allowed_ext = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["f_image"]["name"]);
			$file_image_extension = end($temp);
			$file_image_type = $_FILES["f_image"]["type"];
			$file_image_size = $_FILES["f_image"]["size"];
			$file_image_name = $_FILES["f_image"]["name"];
			
			$str = "";
			$is_image_valid = FALSE;
			
			if (($file_image_type == "image/gif") || ($file_image_type == "image/jpeg") || ($file_image_type == "image/jpg")	|| ($file_image_type == "image/pjpeg") || ($file_image_type == "image/x-png")	|| ($file_image_type == "image/png"))
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Tipe bukan gambar.<br/>";
			}
			
			if ($file_image_size < 1000000)
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$str.= "- Size tidak cukup.<br/>";
			}
			
			if (in_array($file_image_extension, $list_allowed_ext))
			{
				$is_image_valid = TRUE;
			}
			else
			{
				$message['message'].= "- Extension salah.<br/>";
			}
			
			if ($is_image_valid) 
			{
				$image = $file_image_name;
				if ($_FILES["f_image"]["error"] > 0) 
				{
					$message['message'].= "Image failed upload because error.";
				} 
				else 
				{
					if (is_internal())$upload_directory = "D:/xampp/htdocs/grevia.com/asset/images/article/";
					else $upload_directory = "/home/grevia/public_html/asset/images/article/";
					
					if (file_exists($upload_directory . $file_image_name)) 
					{
						echo $file_image_name . " already exists. ";
					} 
					else 
					{
						$is_move_success = move_uploaded_file($_FILES["f_image"]["tmp_name"],$upload_directory . $file_image_name);
						if ($is_move_success) 
						{
							$image = $file_image_name;
						} 
						else 
						{
							$message['message'].= "Image failed upload.";
						}
					}
				}
			}
		}
		else
		{
			$image = "";
		}
		
		$obj_article = array();
		$param['article_category_id'] = $article_category_id;
		$param['title'] = $title;
		$param['slug'] = $slug;
		$param['content'] = $content;
		$param['short_description'] = $short_description;
		$param['quote'] = $quote;
		$param['tag'] = $tag;
		$param['view'] = 0;
		if(is_filled($image)) $param['Image'] = $image;
		$param['image_description'] = $image_description;
		$param['publish_date'] = $publish_date;
		$param['is_publish'] = $is_publish;
		$param['short_url'] = $short_url;
		
		$save = $this->todo_model->save($param);
		
		// UPDATE TO SHORT URL API
		if ($save && !is_internal()) 
		{
			$get_last_article = $this->todo_model->get(array('last' => TRUE));
			
			$longUrl = base_url().'article/'.$get_last_article['todo_id'].'/'.$get_last_article['slug'];

			// Get API key from : http://code.google.com/apis/console/
			// $apiKey = 'AIzaSyBcvHZffHXEK4dCxXC_HGCk4s6huhP6E4Q';
			$apiKey = GOOGLE_SERVER_API_KEY;

			$postData = array('longUrl' => $longUrl);
			$jsonData = json_encode($postData);

			$curlObj = curl_init();

			curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.$apiKey);
			curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curlObj, CURLOPT_HEADER, 0);
			curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
			curl_setopt($curlObj, CURLOPT_POST, 1);
			curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

			$response = curl_exec($curlObj);

			// Change the response json string to object
			$json = json_decode($response);

			curl_close($curlObj);

			$shortLink = get_object_vars($json);
			
			// UPDATE
			$update = $this->todo_model->update($get_last_article['todo_id'],array('short_url' => $shortLink['id']));
		}
		
		if ($save) {
			$this->session->set_flashdata('message', getMessage(MESSAGE::SAVE));
		} else {
			$this->session->set_flashdata('message', getMessage(MESSAGE::ERROR));
		}
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?do=edit&todo_id='.$get_last_article['todo_id']);
	} else {
		$this->session->set_flashdata('message', getMessage(MESSAGE::NOT_FOUND));
		redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2));
	}
}

// Ajax update
if ($_POST) 
{
	$post = $_POST;
	if (isset($post['ajax'])) 
	{
		$param = NULL;
		// debug($post);die;
		
		if (isset($post['todo_id']) && $post['todo_id'] != 'null') {
			// update
			if (isset($post['title'])) $param['title'] = $post['title'];
			if (isset($post['details'])) $param['details'] = $post['details'];
			// $param['priority'] = 1;
			if (isset($post['priority'])) $param['priority'] = $post['priority'];
			$param['status'] = 1;
			if (isset($post['delete'])) $param['status'] = '-1';

			$update = $this->todo_model->update($post['todo_id'],$param);
			if ($update) echo "success";
			else echo "failed";
		} else {
			// save
			$param['title'] = $post['title'];
			$param['details'] = $post['details'];
			$param['priority'] = 1;
			if (isset($post['priority'])) $param['priority'] = $post['priority'];
			$param['status'] = 1;

			$save = $this->todo_model->save($param);
			if ($save) echo "success";
			else echo "failed";
		}
		die;
	}
	else 
	{
		echo "failed";
		die;
	}
}

if ($this->uri->segment(3)) { 
	$data = $this->member_model->get(array('member_id' => $this->uri->segment(3)));
}
if (is_member()) {
	$data = $this->member_model->get(array('member_id' => member_cookies('member_id')));
}

?>

<style>
.block{
	margin-right : 15px;
	margin-top: 10px;
	margin-bottom: 10px;
}
.bdrGry{
	border: 1px solid #c8c8c8;
	padding: 10px;
	border-radius: 3.5px;
}
.bgBlock{
	background: #f5f5f5;
}
.btn-round-lg{
border-radius: 22.5px;
}
.btn-round{
border-radius: 17px;
}
.btn-round-sm{
border-radius: 15px;
}
.btn-round-xs{
border-radius: 11px;
padding-left: 10px;
padding-right: 10px;
}
</style>

<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class='col-md-9'>
	<h1 class="title-header">&nbsp;<?php echo $MODULE?></h1><br/>
	<?php
if (is_filled($get_todo_id) || $get_do == 'insert' || $get_do == 'edit') 
{
	$obj = NULL;
	if ($get_do == 'edit') { 
		$obj = $this->todo_model->get(array('todo_id' => $get_todo_id));
	}

	if (!empty($obj) || $get_do == 'insert') {

?>
	<?php if(is_filled($message['message']))echo print_message($message['message']).BR?>
	<form class='form-horizontal' role='form' method='post' enctype='multipart/form-data'>
		<div class='form-group form-group-sm form-group form-group-sm-sm'>
			<label for='f_article_category_id' class='col-sm-2'>Category</label>
			<div class='col-sm-10'>
			<?php 
			$data_category = $this->articlecategory_model->get_list();
			$obj_list_article_category = $data_category['data'];
			
			$arr_article_category = explode(',', $obj['article_category_id']);
			?>
			<select class='form-control multiple-select' name='f_article_category_id[]' multiple="multiple" size="<?php echo count($obj_list_article_category)?>" style="height:100%" required><?php
			
			foreach ($obj_list_article_category as $category) {
				$selected = '';
				if (in_array($category['article_category_id'], $arr_article_category, true)) $selected = ' selected';;
				echo '<option value="'.$category['article_category_id'].'" '.$selected.'>'.$category['category_name'].'</option>';
			}
			?></select>
			

			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_title' class='col-sm-2'>Title <sup class="b clrRed">* required</sup></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_title' id='f_title' placeholder='Enter title' value='<?php if (isset($obj['title'])) echo $obj['title']?>' required>
			<span id="helpBlock" class="help-block f12">Judul artikel</span>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_slug' class='col-sm-2'>Slug</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_slug' id='f_slug' placeholder='Enter Slug' value='<?php if (isset($obj['slug'])) echo $obj['slug']; ?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'>Content<br/><textarea style='height:500px' class='form-control mceEditor' name='f_content' id='f_content' cols='40' rows='45'><?php if (isset($obj['content'])) echo $obj['content']; ?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_short_description' class='col-sm-2'>ShortDescription</label>
			<div class='col-sm-10'><textarea class='form-control' name='f_short_description' id='f_short_description' placeholder='Enter Short Description' style='height:80px'><?php if (isset($obj['short_description'])) echo $obj['short_description']; ?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_quote' class='col-sm-2'>Quote</label>
			<div class='col-sm-10'><textarea class='form-control' name='f_quote' id='f_quote' placeholder='Enter Quote' style='height:80px' ><?php if (isset($obj['quote'])) echo $obj['quote']; ?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_tag' class='col-sm-2'>Tag</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_tag' id='f_tag' placeholder='Enter Tag' value='<?php if (isset($obj['tag'])) echo $obj['tag']; ?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_image' class='col-sm-2'>Image</label>
			<div class='col-sm-10'>
				<div class='input-group'>
				<span class="input-group-addon">
					<i class="glyphicon glyphicon-picture"></i>
				</span>
				<input type='file' class='form-control' id='f_image' name='f_image' onchange="$('#previewImage')[0].src = window.URL.createObjectURL(this.files[0]);$('#previewImage').show();" /></div>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<div class='col-sm-12'><?php if (is_filled($obj['image'])) echo '<img src="'.base_url().'asset/images/article/'.$obj['image'].'" class="wdtFul"/>'.BR; ?><img class='previewImage' id='previewImage' /></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_image_description' class='col-sm-2'>Image Description</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_image_description' id='f_image_description' placeholder='Enter Image Description' value='<?php if (isset($obj['image_description'])) echo $obj['image_description']; ?>'/></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_publish_date' class='col-sm-2'>Publish Date</label>
			<div class='col-sm-10'>
			<input type='text' class='form-control datepicker' name='f_publish_date' id='f_publish_date' placeholder='Enter Publish Date' value='<?php if (isset($obj['publish_date'])) echo $obj['publish_date']; ?>'/>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_is_publish' class='col-sm-2'>Is Publish</label>
			<div class='col-sm-10'>
			<input type='checkbox' name='f_is_publish' id='f_is_publish' value='1' <?php if ((isset($obj['is_publish']) && $obj['is_publish'] == '1') || $do == "insert") echo 'checked';?> />
			
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_short_url' class='col-sm-2'>Short URL</label>
			<div class='col-sm-10'>
			<?php 
			if ($get_do != "insert") 
			{
				?>
			<input class='form-control' name='f_short_url' id='f_short_url' placeholder='Enter ShortUrl' value='<?php if (isset($obj['short_url'])) echo $obj['short_url']; ?>' <?php if (isset($obj['short_url'])) echo 'disabled'?>>
				<?php
			}
			else echo "*Auto generate";
			?>
			</div>
		</div>
		
		<div class='form-group form-group-sm'>
		<?php if($get_do == 'insert') { ?>
		<button class='btn btn-success' name='f_insert' value='1'><?php echo SAVE?></button>
		<?php } else if ($get_do == 'edit'){ ?>
		<button class='btn btn-success' name='f_update' value='1'><?php echo UPDATE?></button>
		<?php } ?>
		</div>
	</form>
	<?php
	} else {
		print_message('Data not found');
	}
} else {
	?>
	<?php if (is_filled($message['message']))echo print_message($message['message']).BR?>

	<button class='btn btn-info' id="btn_insert" type='button'><?php echo ADD.' '.TODO;?></button><br/><br/>
	
	Sort By Priority <a href="#" class="btn btn-primary btn-xs btn-round-xs">Low</a> <a href="#" class="btn btn-primary btn-xs btn-round-xs">Normal</a> <a href="#" class="btn btn-primary btn-xs btn-round-xs">High</a><br/><br/>
	
	<div id="parent" class="">

	<?php
	// $page = 1;
	// $limit = 0;
	$i = 1;
	// $offset = OFFSET;
	// $offset = 20;
	// if (is_numeric(get('page'))) $page = get('page');
	// if($page > 1) {
		// $limit = ($page - 1 )* $offset ;
		// $i = $limit+1;
	// }

	$filter = array(//'paging' => TRUE, 
					// 'limit' => $limit, 
					//'Author' => 'rusdi', 
					// 'is_publish' => 'all',
					'status' => '1',
					'order' => 'todo_id DESC',
					'creator_id' => member_cookies('member_id'), 
					// 'offset' => $offset
					);
	
	$result = $this->todo_model->get_list($filter);
	$total_rows = $result['total_rows'];
	$data = $result['data'];
	
	// return array key with todo_id for javascript
	$dataurut = NULL;

	if (!empty($data)) {
		foreach ($data as $obj) {
			
			$dataurut[$obj['todo_id']] = $obj;
			
			$id = $obj['todo_id'];
	?>
	<div class="col-sm-4 bdrGry bgBlock pointer btn_edit record_<?php echo $id?> block" data-id="<?php echo $id?>">
		<div class="talRgt">
			<span class="btn btn-xs btn-info btn-round-xs"><?php echo todo_priority($obj['priority']);?></span> #<?php echo $i;?><br/>
			<span class="f12"><?php echo date(DATE_FORMAT, strtotime($obj['creator_date']));?></span>
		</div>
		<div class='' id='title_<?php echo $id?>' >
			<span class="b"><?php echo $obj['title']?></span><br/><br/>
			<?php echo $obj['details'];?>
		</div><br/>
		<div class='talCnt' id=''></div>
		<!-- <td class='talCnt'><a href='?do=edit&todo_id=<?php echo $id;?>'><button class='btn btn-sm btn-info'>Edit</button></a> </td>-->
	</div>
	<?php  
			$i++;
		}
		?>
		<?php
	} else {
		?>
	<div align='center'>No data</div>
		<?php
	}
	?>
	</div>
	
<?php 
	//if (!empty($data)) echo $this->common_model->common_paging($total_rows, $offset);
}
?>
</div>

<!-- Start modal block -->
<div id="modalForm" class="modal fade" role="dialog" style="padding-top:200px;z-index:9999;background-color:rgba(0, 0, 0, 0.4)">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
		<div class="talCnt" style="padding:5px 10px">
			<div class="row">
				<div class="col-sm-12">
					<span class="form_action f24 b"></span>
					<input type="hidden" id="e_todo_id" name="e_todo_id" />
					<select class="input br wdtFul" type="text" id="e_priority" name="e_priority" required/>
					<?php $arr_todo_priority = todo_priority('get')?>
					<?php foreach ($arr_todo_priority as $key => $pr) { ?>
					<option value="<?php echo $key?>"><?php echo $arr_todo_priority[$key]?></option>
					<?php } ?>
					</select>
					<br/>
					<input class="input br wdtFul" type="text" id="e_title" name="e_title" placeholder="Title" required /><br/>
					<textarea class="input br wdtFul" id="e_details" name="e_details" placeholder="Details" rows="10" required></textarea><br/>
					<button class="btn btn-success btn-sm btn_action" type="button" value="dosubmit">Save</button> &nbsp;
					<button class="btn btn-danger btn-sm btn_action" type="button" value="dodelete">Delete</button>
					<br/><br/>
					<div id="loading-response"></div>
				</div>
			</div>
		</div>
      </div>
    </div>

  </div>
</div>
<!-- End modal block -->

<script>

</script>


<!-- TINYMCE -->

<!-- TINYMCE -->
<script>
$(document).ready(function(){
	//$(".multiple-select").height("auto");
	
	var datajson = <?php echo json_encode($dataurut)?>;
	
	$('.btn_edit').click(function(){
		
		$('.form_action').html('Edit');
		$('#modalForm').modal({
			show: 'false',
		});
		
		$('#e_todo_id').val($(this).data('id'));
		$('#e_title').val(datajson[$(this).data('id')].title);
		$('#e_details').val(datajson[$(this).data('id')].details);
		$('.btn_action[value=dodelete]').show();
		$('#e_priority option[value=' + datajson[$(this).data('id')].priority + ']').attr('selected','selected');
		
	});

	$('#btn_insert').click(function(){
		
		$('.form_action').html('Insert');
		$('#modalForm').modal({
			show: 'false',
		});
		
		$('#e_todo_id').val('');
		$('#e_title').val('');
		$('#e_details').val('');
		$('.btn_action[value=dodelete]').hide();
		$('#e_priority option[value=0]').attr('selected','selected');
	});

	$('.btn_action').click(function(){
		// $('.btn_action').attr('disabled',true);
		$('.btn_action').hide();
		
		var paramdata = null;
		var dodelete = null;
		var todo_id = null;
		var message = null;
		
		if ($('#e_todo_id').val() != '') todo_id = $('#e_todo_id').val();
		if ($(this).val() == 'dodelete') {
			dodelete = 1;
			paramdata = "ajax=1&creator_id=" + <?php echo member_cookies('member_id')?> + "&todo_id=" + todo_id + "&delete=" + dodelete;
		} else {
			var title = $('#e_title').val();
			var details = $('#e_details').val();
			var priority = $('#e_priority').val();
			
			paramdata = "ajax=1&title=" + title +"&details=" + details + "&priority=" + priority + "&creator_id=" + <?php echo member_cookies('member_id')?> + "&todo_id=" + todo_id;
		}
		
		var is_action_success = false;
		var action_message = null;
		$.ajax({
			type: "POST",
			data: paramdata,
			cache: false,
			success: function(resp) {

				$('#loading-response').html('<div class="alert alert-info talCnt"><i class="fa fa-cog fa-spin fa-lg fa-fw"></i> Mohon menunggu</div>');
				setTimeout(function(){
					if (resp == "success") {
						// alert(dodelete);
						if (dodelete) {
							action_message = 'Data telah terhapus.';
							
							$('.record_' + todo_id).remove();

						} else {
							action_message = 'Data telah tersimpan.';
							
							if (!todo_id || todo_id == null) {
								// save new
								setTimeout(function(){
									location.reload();
								}, 1000)
							} else {
								// update
								$('#title_' + todo_id).html(title + '<br/>' + details);
							}
						}						

						is_action_success = true;
						
						// untuk redirect 
						// setTimeout(function(){
							// location.reload();
						// }, 2000)
						
					} else if (resp == "failed") {
						action_message = 'Mohon maaf, data gagal disimpan.';
					} else {
						action_message = 'Mohon maaf, terjadi kesalahan, mohon ulangi beberapa saat lagi.';
					}
					// $('#loading-response').html('<div class="alert alert-info talCnt">' + action_message + '</div>');
					
					
				}, 1000);
				
				setTimeout(function(){
					if (is_action_success) {
						$('#loading-response').html('');
						$('#modalForm').modal('hide');
						show_balloon_bottom(action_message,12);
						
					} else {
						// do save
						if (!todo_id || todo_id == null) 
							$('.btn_action[value=dosubmit]').show();						
						else 
							$('.btn_action').show();
					}
					
				}, 2000);
			}
		});

		
	});
	
	// $(document).ready(function(){
		// show_balloon('ayam',12);
	// });
	
});
</script>

