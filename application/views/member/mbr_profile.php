<?php 
global $BREADCRUMB, $PAGE_TITLE, $MODULE;

$bread['member'] = 'Member';
$BREADCRUMB = $this->common_model->breadcrumb($bread, PROFILE);
$MODULE = PROFILE;
$PAGE_TITLE = PROFILE.' - '.DEFAULT_PAGE_TITLE;

if (isset($_POST['btnUpdate'])) {
	$is_subscribe = 0;
	$dob = NULL;
	if (isset($_POST['is_subscribe'])) {
		$is_subscribe = 1;
	}
	if (isset($_POST['dob'])) {
		$tempdob = explode('-',$_POST['dob']);
		$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
	}
	$member_id = member_cookies('member_id');
	$param = array(
		'full_name' => $_POST['full_name'],
		'email' => $_POST['email'],
		'gender' => $_POST['gender'],
		'dob' => $dob,
		'facebook' => $_POST['facebook'],
		'twitter' => $_POST['twitter'],
		'linkedin' => $_POST['linkedin'],
		'website' => $_POST['website'],
		'about_me' => $_POST['about_me'],
		'is_subscribe' => $is_subscribe,
	);
	// debug($param);die;
	$update = $this->member_model->update($member_id, $param);
	($update)?$message['message'] = MESSAGE::UPDATE : $message['message'] = MESSAGE::ERROR;
	$message['message'] = getMessage($message['message']);
}

if ($this->uri->segment(3)) { 
	$data = $this->member_model->get(array('member_id' => $this->uri->segment(3)));
}
if (is_member()) {
	$data = $this->member_model->get(array('member_id' => member_cookies('member_id')));
}

?>
<div class="col-md-3">
	<?php echo $SIDEMENUBAR?>
</div>

<div class="col-md-9">
	<h1><?php echo $MODULE?></h1><br/>
	<?php 
	if (isset($message['message'])) echo message($message['message']).BR;
	?>
	<?php 
	if(!empty($data)) {
		$obj = $data;
		if (is_filled($obj['dob'])) {
			$tempdob = explode('-',$obj['dob']);
			$dob = $tempdob[2].'-'.$tempdob[1].'-'.$tempdob[0];
		}
	?>
	<form class='form-horizontal' role='form' method='post'>
		<div class='form-group form-group-sm'>
			<label for='f_full_name' class='col-sm-2'><?php echo FULL_NAME?></label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_full_name' id='f_full_name' placeholder='Enter <?php echo FULL_NAME ?>' value='<?php echo $obj['full_name']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_email' class='col-sm-2'><?php echo EMAIL?></label>
			<div class='col-sm-10'><input type='email' class='form-control' name='f_email' id='f_email' placeholder='Enter Email' value='<?php echo $obj['email']?>'></div>
		</div>		
		<div class='form-group form-group-sm'>
			<label for='f_gender' class='col-sm-2'><?php echo GENDER?></label>
			<div class='col-sm-10'>
			<select class='form-control' name='f_gender'>
			<option value='1' <?php if ($obj['gender'] == 1) echo 'selected'?>><?php echo MALE?></option>
			<option value='2' <?php if ($obj['gender'] == 2) echo 'selected'?>><?php echo FEMALE?></option>
			</select>
			</div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_dob' class='col-sm-2'><?php echo DOB?></label>
			<div class='col-sm-10'><input type='text' class='form-control datepicker' name='f_dob' id='f_dob' placeholder='Enter Date Of birth' value='<?php echo $dob?>'/></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_facebook' class='col-sm-2'>Facebook</label>
			<div class='col-sm-2' style="font-size:12px;padding-top:8px">http://www.facebook.com/</div>
			<div class='col-sm-8'><input type='text' class='form-control' name='f_facebook' id='f_facebook' placeholder='Enter Facebook' value='<?php echo $obj['facebook']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_twitter' class='col-sm-2'>Twitter</label>
			<div class='col-sm-2' style="font-size:12px;padding-top:8px">http://twitter.com/</div>
			<div class='col-sm-8'><input type='text' class='form-control' name='f_twitter' id='f_twitter' placeholder='Enter Twitter' value='<?php echo $obj['twitter']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_linkedin' class='col-sm-2'>Linkedin</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_linkedin' id='f_linkedin' placeholder='http://www.linkedin.com/in/yourname' value='<?php echo $obj['linkedin']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_website' class='col-sm-2'>Website</label>
			<div class='col-sm-10'><input type='text' class='form-control' name='f_website' id='f_website' placeholder='http://yourwebsite.com' value='<?php echo $obj['website']?>'></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_about_me' class='col-sm-2'><?php echo ABOUT_ME?></label>
			<div class='col-sm-10'><textarea class='form-control' name='f_about_me' id='f_about_me' placeholder='Enter AboutMe' style='height:95px'><?php echo $obj['about_me']?></textarea></div>
		</div>
		<div class='form-group form-group-sm'>
			<label for='f_is_subscribe' class='col-sm-2'><?php echo SUBSCRIBE_NEWSLETTER?></label>
			<div class='col-sm-10 checkbox'>
				<label><input type='checkbox' name='f_is_subscribe' id='f_is_subscribe' value='1' <?php if ($obj['is_subscribe']) echo 'checked'?>></label>
			</div>
		</div>
		<div class='form-group padLeft'>
			<button class='btn btn-success' name='btnUpdate' value='1'><?php echo UPDATE?></button>
		</div>

		<!--
		<p><input type="file" name="f_image"/><br/>
		<?php if(is_filled($image)) echo '<img src="'.base_url().'/data/image/'.$image.'"/>'?>
		</p>
		-->
	</form>
	<!--
	<form class='form-horizontal'><div class='form-group padLeft'><button class="btn btn-danger" onclick="return confirm('apakah anda yakin ? \n Aksi ini akan menghapus permanen akun anda. ');"><?php echo DELETE.' '.ACCOUNT?></button></div></form>
	-->
	<?php
	}
	?>
</div>