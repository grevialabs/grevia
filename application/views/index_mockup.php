<?php
global $PAGE, $PAGE_TITLE, $BREADCRUMB , $PAGE_HEADER;
// global $config;

// $config = $this->config_model->get();

if (!isset($PAGE)) $PAGE = SITE_NAME . ' - '.SITE_DESCRIPTION;
// if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE.' - '.$config['site_name'];
if (!isset($PAGE_TITLE)) $PAGE_TITLE = $PAGE.' - demo';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $PAGE_TITLE ?></title>	
	<?php 
	if (!is_internal()) 
	{
	?>
	<!-- GA Analytics -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136676376-1"></script>
	  <script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-136676376-1');
	  </script>
	<!-- GA Analytics -->
	<?php 
	}
	?>
	<meta name="description" content="<?php if (isset($config['meta_description_global'])) echo $config['meta_description_global']?>"/>
	<meta name="keywords" content="<?php if (isset($config['meta_keywords_global'])) echo $config['meta_keywords_global']?>"/>
	<meta property="og:site_name" content="<?php if (isset($config['site_name'])) echo $config['site_name'] ?>"/>
	<meta property="og:title" content="<?php if (isset($config['site_description'])) echo $config['site_description'];?>"/>
	<meta property="og:description" content="<?php if (isset($config['meta_description_global'])) echo $config['meta_description_global']?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php if (isset($config['site_url'])) echo $config['site_url']?>"/>
    <meta property="og:image" content="<?php if (isset($config['logo_url'])) echo base_url().'asset/images/'.$config['logo_url'] ; ?>"/>
	
	<meta property="fb:admins" content="<?php echo FACEBOOK_ADMIN_ID?>"/>
	<meta property="fb:app_id" content="<?php echo FACEBOOK_APP_ID?>"/>
	
	<?php if (isset($config['header_embed_global'])) echo $config['header_embed_global']; ?>
	
	<?php if (isset($config['favicon_url'])) { ?>
	<link rel="shortcut icon" href="<?php echo base_url().$config['favicon_url']?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url().$config['favicon_url']?>" type="image/x-icon">
	<?php } ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Latest compiled and minified CSS 
		<link rel="stylesheet" href="http://localhost/ci/assets/css/bootstrap.min.css">
		-->

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css?v=20160714"/>
	
	<!-- Include required JS files -->
	<link rel='stylesheet' href='<?php echo base_url(); ?>asset/css/ui-lightness/jquery-ui-1.10.4.css' />

	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.min.css?v=20160714"/>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-theme.css.map?v=20160714"/>
	
    <link href="<?php echo base_url()?>asset/css/font-awesome.min.css?v=20160714" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap-social.min.css?v=20160714"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.min.css?v=20160714"/>
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/animate.min.css?v=20160714"/>

	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.1.11.2.min.js?v=20160714"></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery-ui-1.10.4.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/jquery.validate.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>asset/js/ui/jquery.ui.core.js'></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/lazyload/jquery.lazyload.js?v=1.9.1"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/colorbox.min.css?v=20160714" />
	<script src="<?php echo base_url()?>asset/js/jquery.colorbox.js"></script>
	<script src="<?php echo base_url()?>asset/js/wow.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){	
		$(function() {	
			//$.backstretch("http://dl.dropbox.com/u/515046/www/garfield-interior.jpg");
			$("img.lazy").lazyload({ effect: "fadeIn" });
			
			$('[data-toggle="popover"]').popover();
			//$('.popover').popover(options);
			$( ".datepicker").datepicker({
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showAnim: "slideDown",
				yearRange: '1950:+10' 
			});
			
			$(".numeric").keypress(function(e){
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
		});
		
	});
	
	window.sr = new scrollReveal();
	new WOW().init();
	</script>
	
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css?v=170812" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js?v=170812"></script>
	
	<link rel="stylesheet" href="<?php echo base_url().'asset/css/chosen/'?>chosen.min.css">
	<style type="text/css" media="all">
	/* fix rtl for demo */
	.chosen-rtl .chosen-drop { left: -9000px; }
	
	.mockup{ border-radius: 4px; padding:25px; border: 1px solid #FEFEFE}
	.w50{width: 50px}
	.w100{width: 100px}
	.w200{width: 200px}
	.w250{width: 250px}
	.w300{width: 300px}
	
	
	</style>
</head>
<?php 
global $fetch_class, $fetch_method;
$fetch_class = $this->router->fetch_class();
$fetch_method = $this->router->fetch_method();
function show_active_menu($menu_title) 
{
	global $fetch_class, $fetch_method;
	$str = '';
	if ($fetch_class == 'modular') 
	{
		if ($fetch_method == $menu_title) 
		{
			$str = 'btn btn-info';
			$str = '<span class="sr-only">(current)</span>';
		}
	}
	return $str;
}
// $out = shell_exec("svn up /home/grevia/public_html/");
// var_dump($out);
?>
<body>
<?php include_once 'navheader.php';?>
<!--
<div class="container">	
	<div class="row">
		
		<div class="col-md-12" style="min-height:500px"><br/>
			<?php if (isset($BREADCRUMB)) echo $BREADCRUMB.'' ?>
			<?php if (isset($PAGE_HEADER)) echo '<h2 class="f20 b">'.$PAGE_HEADER.'</h2>' ?>
			<div class="row">
				<div class="col-sm-3">
					<?php // $this->load->view('mockup/sidebar',NULL,TRUE) ?>
				</div>
				<div class="col-sm-9">
				</div>
					
			</div>
		</div>
	</div>
</div>
-->

<?php echo $CONTENT; ?>

<?php include_once 'navfooter.php';?>

<script src="<?php echo base_url().'asset/js/'?>chosen.jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
var config = {
  '.chosen-select'           : {allow_single_deselect:true},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
</body>
</html>