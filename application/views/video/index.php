<?php 
/*
Each YouTube video has 4 generated images. They are predictably formatted as follows:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/0.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/1.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/2.jpg
https://img.youtube.com/vi/<insert-youtube-video-id-here>/3.jpg
The first one in the list is a full size image and others are thumbnail images. The default thumbnail image (ie. one of 1.jpg, 2.jpg, 3.jpg) is:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/default.jpg
For the high quality version of the thumbnail use a url similar to this:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg
There is also a medium quality version of the thumbnail, using a url similar to the HQ:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/mqdefault.jpg
For the standard definition version of the thumbnail, use a url similar to this:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/sddefault.jpg
For the maximum resolution version of the thumbnail use a url similar to this:

https://img.youtube.com/vi/<insert-youtube-video-id-here>/maxresdefault.jpg
*/

$base_url = base_url();

// Save
if ($_POST) 
{
	$post = $_POST;
	
	if (isset($post['name']) && isset($post['url']))
	{
		$paramv = NULL;
		$paramv['name'] = $post['name'];
		if (isset($post['videolist_id'])) $paramv['videolist_id'] = $post['videolist_id'];
		$paramv['description'] = $post['description'];
		$paramv['author'] = $post['author'];
		$paramv['url'] = $post['url'];
		$paramv['creator_id'] = 1;
		$paramv['creator_ip'] = get_ip();
		$paramv['creator_date'] = get_datetime();
		// debug($paramv);
		// die;
		$save = $this->video_model->save($paramv);
		if ($save) 
		{
			$print_message = 'berhasil';
			$this->session->set_flashdata('message',$print_message);
			redirect(current_full_url());
			die;
		}
		else
		{
			$print_message = 'Mohon maaf terjadi error.';
		}
	}
	else 
	{
		$print_message = 'Data harus diisi';
	}
}

$get_video_id = NULL;
if (isset($_GET['video_id'])) $get_video_id = $_GET['video_id'];

$obj_video = $tmp = NULL;
if (isset($get_video_id))
{
	$tmp['video_id'] = $get_video_id;
	$obj_video = $this->video_model->get($tmp);
}

//----------------------------------
// Get all video
$param = NULL;
// $param['creator_id'] = 1;
$obj_list_video = $this->video_model->get_list($param);
$obj_list_video = $obj_list_video['data'];

//----------------------------------
// Get playlist video
$param = NULL;
// $param['creator_id'] = 1;
$obj_list_videolist = $this->videolist_model->get_list($param);
$obj_list_videolist = $obj_list_videolist['data'];
?>
<style>

*{
    font-size:10px;
}

.parent {
    min-height:300px;
    border: 2px solid #ccc;
    width: 100%;
    overflow: scroll;  
}

.ayam
{
    width: 1500px;
    overflow-x: scroll;
    max-height:300px;
    -webkit-box-shadow: 0px -1px 12px rgba(0, 0, 0, 0.48);
    -moz-box-shadow:    0px -1px 12px rgba(0, 0, 0, 0.48);
    box-shadow:         0px -1px 12px rgba(0, 0, 0, 0.48);
    padding-bottom:45px;
    padding-top:5px;
    background: #ccc;
    border-top:1px solid #767676;
    font-family:Verdana, Tahoma, Sans-Serif;
    z-index: 100;
}


.ayam .item{
float: left;
width: 300px; /*width of each menu item*/
margin-left:5px;
margin:15px;
}

.ayam .item:first-child{
margin-left:20px;
}

.ayam .item ul{
margin: 0;
padding: 0;
list-style-type: none;
}

.ayam .item ul li{
padding: 5px;
}

.ayam .item h3{
color: White;
font-weight:bold;
font-family:Verdana, Tahoma, Sans-Serif;
margin: 0 0 5px 0;
padding:5px;
text-shadow: 0px 2px 3px #555;
border:1px dashed #eee;
-webkit-border-radius: 20px;
-moz-border-radius: 20px;
border-radius: 20px;

-webkit-box-shadow: 0px 0px 9px rgba(0, 0, 0, 0.48);
-moz-box-shadow:    0px 0px 9px rgba(0, 0, 0, 0.48);
box-shadow:         0px 0px 9px rgba(0, 0, 0, 0.48);

        
}

.ayam .item ul li a{
text-decoration: none;
padding:2px;

}

.ayam .item ul li a:hover{

color:white;

border-left:2px solid #FF9F21;


}

</style>

<div>
	<?php 
	if (isset($print_message)) 
		echo "<div class='alert alert-info'>".$print_message."</div>";
	if ($this->session->flashdata('message')) 
		echo "<div class='alert alert-info'>".$this->session->flashdata('message')."</div>";	
	?>
	<a id="new_video" href="javascript:void(0)" class="btn btn-info btn-sm">New Video</a><br/><br/>
	<div id="new_form_video" class="alert alert-info hide row">
		<form method="post" action="">
			<div class="col-md-12 b text-uppercase fntHdr text-primary">New Video</div>
			
			<div class="col-md-6">
			<input type="text" class="input br wdtFul" name="f_name" placeholder="Nama video" required />
			</div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6">
			<?php 
			if ( ! empty($obj_list_videolist))
			{
				?>
				<select name="f_videolist_id" class="input br wdtFul">
				<?php 
				foreach ($obj_list_videolist as $vl)
				{
				?>
				<option value="<?php echo $vl['videolist_id']?>"><?php echo $vl['name']?></option>
				<?php 
				}
				?>
				</select>
				<?php 
			}
			?>
			</div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6"><input type="text" class="input br wdtFul" name="f_author" placeholder="Author" /></div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6"><textarea class="input br wdtFul" name="f_description" placeholder="Description"></textarea></div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-6"><input type="text" class="input br wdtFul" name="f_url" placeholder="URL video" /></div>
			<div class="col-md-6">&nbsp;</div>
			<div class="clearfix"></div>
			
			<div class="col-md-12"><button type="submit" class="btn btn-sm btn-success">SUBMIT</button><hr/></div>
		</form>
	</div>
	<div >
		
		<?php 
		// ---------------------------
		// When data found show embed video
		if (isset($obj_video['video_id'])) 
		{
			$url = $img = NULL;
			// Strip v value from URL if contain like "https://www.youtube.com/watch?v=jBCDVlGdAKQ"
			if (strpos($obj_video['url'],'?v=') != FALSE) {
				$url = explode('?v=',$obj_video['url']);
				$obj_video['url'] = $url[1];
			}
			$url = 'https://youtube.com/embed/'.$obj_video['url'];
		?>
		<!--
		<div class="videoWrapper">
			<iframe width="560" height="349" src="<?php echo $url?>" frameborder="0" allowfullscreen></iframe>
		</div>
		-->
		<h1 class="b "><?php echo $obj_video['name']?> - <?php echo $obj_video['author']?></h1>
		<?php if (isset($obj_video['description'])) echo nl2br($obj_video['description']); ?>
		<div class="videoWrapper">
			<div id="player"></div>
		</div>

		<script src="http://www.youtube.com/player_api"></script>

		<script>
			// create youtube player
			var player;
			function onYouTubePlayerAPIReady() {
				player = new YT.Player('player', {
				  width: '640',
				  height: '390',
				  videoId: '<?php echo $obj_video['url']?>',
				  events: {
					onReady: onPlayerReady,
					onStateChange: onPlayerStateChange
				  }
				});
			}

			// autoplay video
			function onPlayerReady(event) {
				event.target.playVideo();
			}

			// when video ends
			function onPlayerStateChange(event) {        
				if(event.data === 0) {          
					// alert('done');
					window.location.replace("<?php echo $base_url.'video?video_id='.($obj_video['video_id'] + 1);?>");
				}
			}

		</script>
			
		<?php 
		}
		// ---------------------------
		?>		
		
		<h1 class="b fntHdr">MY PLAYLIST</h1><br/>
		
		<?php 
		// Show all playlist
		if ( ! empty($obj_list_videolist))
		{
			foreach($obj_list_videolist as $vl)
			{
				?>
				<h2><?php echo $vl['name']?></h2>
				<span><?php echo $vl['description']?></span><hr/>
				<?php
				$list_video = $param_video = NULL;
				// $param_video['creator_id'] = 1;
				$param_video['videolist_id'] = $vl['videolist_id'];
				$param_video['order'] = 'RAND(), v.video_id';
				$list_video = $this->video_model->get_list($param_video);
				// debug($list_video);
				$list_video = $list_video['data'];
				
				if ( ! empty($list_video))
				{
					?>
					<div class="parent">
						<div id="" class="ayam">
					<?php
					foreach ($list_video as $rsv)
					{
						$url = $img = NULL;
						// Strip v value from URL if contain like "https://www.youtube.com/watch?v=jBCDVlGdAKQ"
						if (strpos($rsv['url'],'?v=') != FALSE) {
							$url = explode('?v=',$rsv['url']);
							$rsv['url'] = $url[1];
						}
						$url = 'https://youtube.com/embed/'.$rsv['url'];
				
						// https://img.youtube.com/vi/L54q_24LvY8/hqdefault.jpg.jpg
						$img = 'https://img.youtube.com/vi/'.$rsv['url'].'/hqdefault.jpg';
						
						?>
						<div class="item">
							<a href="<?php echo $base_url.'video?video_id='.$rsv['video_id']; ?>" alt="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" title="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" ><img class="img img-responsive" src="<?php echo $img; ?>" /></a>
						</div>
						<!-- Copy & Pasted from YouTube -->
						<!--<iframe width="280" height="200" src="<?php echo $url?>" frameborder="0" allowfullscreen onclick="alert('dame <?php echo $url?>')"></iframe>-->
						<?php
					}
					?>
						</div>
					</div>
					<?php
				}
				else 
				{
					// echo "No data".BR;
				}
			}
		}
		else 
		{
			echo "No data".BR;
		}
		// End all playlist
		?>
		<br/><br/>
		
		<h2>UNCATEGORIZED</h2><br/>
		<?php 
		// Uncategorized
		$obj_list_video_uncategorized = $tmp4 = NULL;
		$tmp4['uncategorized'] = TRUE;
		$tmp4['order'] = 'RAND(), v.video_id';
		$obj_list_video_uncategorized = $this->video_model->get_list($tmp4);
		$obj_list_video_uncategorized = $obj_list_video_uncategorized['data'];
		// debug($obj_list_video_uncategorized);
		if ( ! empty($obj_list_video_uncategorized))
		{
			?>
			<div class="parent">
				<div id="" class="ayam">
			<?php
			foreach($obj_list_video_uncategorized as $rsv)
			{
				$url = $img = NULL;
				// Strip v value from URL if contain like "https://www.youtube.com/watch?v=jBCDVlGdAKQ"
				if (strpos($rsv['url'],'?v=') != FALSE) {
					$url = explode('?v=',$rsv['url']);
					$rsv['url'] = $url[1];
				}
				// $url = 'https://youtube.com/embed/'.$video['url'];
				$img = 'https://img.youtube.com/vi/'.$rsv['url'].'/hqdefault.jpg';
				?>
				<div class="item">
					<a href="<?php echo $base_url.'video?video_id='.$rsv['video_id']; ?>" alt="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" title="<?php echo $rsv['name']?> by <?php echo $rsv['author']?>" ><img class="img img-responsive" src="<?php echo $img; ?>" /></a>
				</div>
				<!-- <iframe width="280" height="200" src="<?php echo $url?>" frameborder="0" allowfullscreen onclick="alert('dame <?php echo $url?>')"></iframe> -->
		
			<?php 
			}
			?>
				</div>
			</div>
			<?php
		}
		else 
		{
			echo "No data".BR;
		}
		?>
	</div>
</div>


<style>
.videoWrapper {
	position: relative;
	padding-bottom: 56.25%; /* 16:9 */
	padding-top: 25px;
	height: 0;
}
.videoWrapper iframe {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
</style>

<script>
$(document).ready(function(){
	$('#new_video').click(function(){
		$(this).addClass('hide');
		$('#new_form_video').removeClass('hide');
	});
	// new_form_video
});
</script>