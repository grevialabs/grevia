<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['socket_type'] = 'tcp'; //`tcp` or `unix`
$config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
$config['host'] = '45.63.7.14';
$config['password'] = NULL;
$config['port'] = 6379;
$config['timeout'] = 0;

/* End of file routes.php */
/* Location: ./application/config/routes.php */