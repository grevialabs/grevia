<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('OFFSET', '30');
define('DEFAULT_PAGE_TITLE', 'Grevia');
define('DEFAULT_PAGE_DESCRIPTION', 'Jasa pembuatan aplikasi web dan artikel Teknopreneurship Startup');
define('DEFAULT_WEBSITE_NAME', 'GREVIA');
define('DEFAULT_WEBSITE_DOMAIN', 'GREVIA.COM');
define('IMAGE_ARTICLE_DIR', 'asset/images/');

// define('META_KEYWORDS', 'grevia,artikel,startup,teknologi,founder,pengusaha,entrepreneur,internetpreneur,digital kreatif,teknopreneur.');
define('META_KEYWORDS', 'jasa website,website murah,company profile,website wedding,artikel,startup,teknologi,pengusaha,entrepreneur,digital kreatif,teknopreneur.');
define('META_DESCRIPTION', 'Jasa pembuatan aplikasi web custom, company profile, dan artikel Teknopreneurship terbaik di Jakarta.');
define('META_AUTHOR', 'Grevia Networks');


define('FACEBOOK_APP_ID','1457093531196351' );
define('FACEBOOK_ADMIN_ID','1262709090');
define('FACEBOOK_SECRET_ID','4fec5bacb5f3bb7fe6235a4d6c5607b5' );

define('FACEBOOK_PAGE', 'greviacom');
define('TWITTER_PAGE', 'greviacom');

// SERVER KEY FOR grevianet@gmail.com
define('GOOGLE_SERVER_API_KEY','AIzaSyBcvHZffHXEK4dCxXC_HGCk4s6huhP6E4Q');
define('GOOGLE_CLIENT_ID', '115356414498-nl4fnt9gu76dcmp5m0ao14mlj22cvs40.apps.googleusercontent.com');
define('GOOGLE_SECRET_ID', 'Nhu1S2uI_aD_hvnizU_UIVPR');
define('GOOGLE_REDIRECT_URI', 'http://127.0.0.1/grevia.com/login');
//---------------------------------------------------------------------------------------------
define('INFO', 'info');
define('ERROR', 'error');
define('BR', '<br/>');
define('HR', '<hr/>');

//----------------------------------------------------------------------------------------------
// BUTTON 
// define('SAVE', 'Save');
// define('EDIT', 'Edit');
// define('ADD', 'Add');
// define('UPDATE' ,'Update');
// define('DELETE', 'Delete');

//-----------------------------------------------------------------------------------------------
define('DATE_FORMAT', 'd M Y, H:i');

interface MESSAGE{
	const ERROR = 0;
	const SAVE = 1;
	const UPDATE = 2;
	const DELETE = 3;
	const CONNECTION_ERROR = 4;
	const NOT_FOUND = 5;
}
function getMessage($val){
	switch($val){
		case MESSAGE::ERROR:
			$str="<span class='fntBld'>Data Process Error</span>";break;
		case MESSAGE::SAVE:
			$str="<span class='fntBld'>Save Success</span>";break;
		case MESSAGE::UPDATE:
			$str="<span class='fntBld'>Update Success</span>";break;
		case MESSAGE::DELETE:
			$str="<span class='fntBld'>Delete Success</span>";break;
		case MESSAGE::NOT_FOUND:
			$str="<span class='fntBld'>Data not found</span>";break;
	}
	return $str;
}


/* End of file constants.php */
/* Location: ./application/config/constants.php */