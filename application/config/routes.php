<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
// $route['translate_uri_dashes'] = TRUE;
$route['default_controller'] = "modular";
$route['404_override'] = 'modular/error404';
$route['member/(:any)'] = "member/$1";
$route['member'] = "member/index";
$route['admin/(:any)'] = "admin/$1";
$route['admin'] = "admin/index";
$route['tutorial/(:any)'] = "tutorial/$1";
$route['tutorial'] = "tutorial/index";
$route['forumtopic/(:any)'] = "forumtopic/$1";
$route['forumtopic'] = "forumtopic/index";
$route['map/(:any)'] = "map/$1";
$route['map'] = "map/index";
$route['api/(:any)'] = "api/$1";
$route['api'] = "api/index";
$route['page/(:any)'] = "page/$1";
$route['page'] = "page/index";
$route['belajar/(:any)'] = "belajar/$1";
$route['belajar'] = "belajar/index";
$route['video/(:any)'] = "video/$1";
$route['video'] = "video/index";
$route['m/(:any)'] = "m/$1";
$route['m'] = "m/index";
$route['booking/(:any)'] = "booking/$1";
$route['booking'] = "booking/index";
$route['tools/(:any)'] = "tools/$1";
$route['tools'] = "tools/index";
// $route['template'] = "theme";
// $route['theme/'] = "theme/company_one";
// $route['theme/(:any)'] = "theme/company_one";

// $route['article/(:any)/(:any)'] = "modular/showarticle/$1/$2";
$route['forumdetail/(:any)/(:any)'] = "modular/showforumdetail/$1/$2";
//$route['article/(:number)/(:any)'] = "modular/showarticle/$1/$2";
$route['mockup'] = "mockup/index";
$route['mockup/(:any)'] = "mockup/$1";



// New controller harus diatas ini !!!

$route['modular/(:any)'] = "modular/$1";
$route['(:any)'] = "modular/$1";
$route['translate_uri_dashes'] = TRUE;

//$route['(index|article|about|contact|forum)'] = "modular/$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */