<?php 
// include_once('mpdf.php');

$html = '<!DOCTYPE html>
<html lang="en">
<header>
	<title>Lakupon Coupon</title>
	<link rel="stylesheet" type="text/css" href="https://lakupon.com/asset/css/coupon.css" />
</header>

<body onLoad="print()">
	<div class="contCoupon">
		<img src="https://lakupon.com/assets/img/logo_n.png" style="vertical-align:top">
		<div class="codeCoupon">
			<b>3H5CC74024GWECV</b><br>
			Rp. 78,000<br>
			2015-04-27 15:48:40		</div>
		<div class="codeCoupon" style="margin-right:10px;">
			Kode Kupon: <br>
			Nilai Kupon: <br>
			Di Print Tangal: 
		</div>
		<div class="qcode">
			<img src="https://lakupon.com/qr/3H5CC74024GWECV">
		</div>
		<div class="hrCoupon" style="margin-top:20px"></div>
		<div class="titleCoupon">Classic Pizza Cone</div>
		<img class="imgCoupon" src="http://awan.lakupon.com/cdn/190/dl-1523-201503131709309235_190.jpg">
		<div class="descCoupon">
			<b>Ketentuan umum:</b>
			<ul>
<li>Kupon berlaku mulai 04 April 2015 sampai dengan 04 Mei 2015</li>
<li>Tidak ada batasan pembelian kupon per akun</li>
<li>Tidak ada batasan penggunaan kupon per transaksi</li>
<li>Kupon asli yang sudah dicetak harus diberikan kepada pihak Classic Pizza Cone sesuai dengan cabang yang dipilih</li>
<li>Kupon berlaku setiap hari, Jam 10.00 &ndash; 21.30 WIB</li>
<li>Kupon yang tidak digunakan sesuai dengan periode penukarannya dianggap hangus dan tidak dapat di refund</li>
<li>Kupon sudah termasuk tax dan service</li>
<li>Proses verifikasi pembayaran max. 1 24 jam (hari kerja)</li>
<li>Kupon tidak dapat digabungkan dengan promosi lainnya</li>
<li>Kupon dapat dipindahtangankan dan digunakan sebagai hadiah</li>
<li>Kupon tidak dapat diuangkan untuk nilai yang tidak digunakan</li>
<li>Kupon tidak dapat ditukar dengan uang dan dikembalikan ke pihak LaKupon</li>
</ul>		</div>
		<div class="ketCoupon">
			<b>Penerima:</b><br>
            Rusdi Karsandi<br><br>
            
            <b>Paket:</b><br>
			 Paket B : 3 pizza + 3 air mineral 600ml<br>
                               
			
			<br><br>
			
			<b>Lokasi Penukaran Kupon:</b><br>
			Mall Artha Gading - West Food Court Lantai 2 (Depan Koi) 			
			<br><br>
			<b>Jam Operasional:</b><br>
			10:00 am - 10:00 pm /senin-minggu			
			<br><br>
			<b>Masa Berlaku Kupon:</b><br>
			2015-04-04 00:00:00 - 2015-05-04 00:00:00					</div>
		<div style="clear:both"></div>
	</div>
</body>
</html>
';

// $mpdf=new Mpdf(); 

// $mpdf->WriteHTML($html);
// $mpdf->Output('a.pdf','D');
// exit;



/*
I: send the file inline to the browser. The plug-in is used if available. The name given by filename is used when one selects the "Save as" option on the link generating the PDF.
D: send to the browser and force a file download with the name given by filename.
F: save to a local file with the name given by filename (may include a path).
S: return the document as a string. filename is ignored.
*/
?>