<?php

Class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		/* ANTI CURL
		// Set validation cookies for cannot be curl 
		setcookie("csrf",$_SERVER['REMOTE_ADDR']);
		
		// Start session and reset counter
		session_start();

		if (array_key_exists('cookie_check_count', $_SESSION) == false) {
			$_SESSION['cookie_check_count'] = 0;
		}
		
		if(count($_COOKIE) > 0) {
			// echo "valid";
		} else {
			if ($_SESSION['cookie_check_count'] >= 3) {
				echo "banned";
				die;
			} else {
				$_SESSION['cookie_check_count']++;
				redirect(current_url());
				die;
			}
		}
		*/
		
		// SAVE REFERENCE MODEL, AND ALFA NUMERIC
		if (isset($_GET['ref']) && preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $_GET['ref']))
		{
			$this->load->model('reference_model');
			$param_ref['ref'] = $_GET['ref'];
			$param_ref['creator_ip'] = get_ip();
			$param_ref['creator_date'] = get_datetime();

			if (isset($_SERVER['HTTP_REFERER'])) 
			{
				if (strpos($_SERVER['HTTP_REFERER'],base_url()) !== FALSE) {
					// echo "kena";die;
					$param_ref['from_url'] = str_replace(array(base_url(),'?ref='.$_GET['ref']),'',current_full_url());
				
				} else {
					$param_ref['from_url'] = $_SERVER['HTTP_REFERER'];
				}
			}
			// debug_array($param_ref);
			// die;
			$save = $this->reference_model->save($param_ref);
		}
		
		// SAVE postgre
		//$data = $this->db->query("INSERT INTO member(name,address) VALUES('kang ujang', 'jalan absurd')");
		// $data = $this->db->query('SELECT * FROM member')->result_array();
		// var_dump($data);
		// die;
		// foreach ($data as $obj) 
		// {
			// echo $obj['member_id'];
		// }
		// //print_r($data);
		// die;
		
		$this->load->model('common_model');
		$this->load->model('articlecategory_model');
		
		$this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
		
		foreach ($_GET as $key => $value)
		{
			// $_GET[$key] = filter_input(INPUT_GET, $val, FILTER_SANITIZE_INPUT);
			// $_GET[$key] = filter_input(INPUT_GET, $val, FILTER_SANITIZE_ENCODED);
			// $_GET[$key] = filter_input(INPUT_GET, $val, FILTER_SANITIZE_ENCODED);
			$_GET[$key] =(is_array($this->input->get($key)))?$this->input->get($key) : preg_replace("/[^0-9a-zA-Z@ -._%:\/=?+]/", "", filter_var(strip_tags($this->input->get($key)), FILTER_SANITIZE_STRING));
		}
		
		if (isset($_GET['nocache'])) 
		{
			//$this->cache->delete('index');
			// clear all cache
			$this->cache->clean();
		}
		
		if ($_POST) {
			$tmp = array();
			foreach($_POST as $key => $val){
				// STRPOS use !== to accept 0 or TRUE
				if (isset($key) && strpos($key,'f_') !== FALSE) {
					$field = explode('f_',$key);
					// $val = mysql_real_escape_string($val);
					$val = $this->db->escape_str($val);
					$tmp[$field[1]] = $val;
					
				} else {
					if (is_array($val)) {
						foreach($val as $k => $dval) {
							// $dval = mysql_real_escape_string($dval);
							$dval = $this->db->escape_str($dval);
							$val[$k] = $dval;
						}
						$tmp[$key] = $val;
					} else {
						// $val = mysql_real_escape_string($val);
						$val = $this->db->escape_str($val);
						$tmp[$key] = $val;
					}
				}
			}
			$_POST = NULL;
			$_POST = $tmp;
		}
		
		/* START LANGUAGE */
		
		// $lang = NULL;
		// $allowed_lang = array('en','id');
		// if (!isset($_COOKIE['the_lang'])) {
			// if (isset($_GET['lang']) && in_array($_GET['lang'],$allowed_lang) == TRUE) 
				// $lang = $_GET['lang'];
			// else
				// $lang = 'en';
			// setcookie('the_lang',$lang, time()+86400, '/');
		// } else {

			// if (isset($_COOKIE['the_lang']) && in_array($_COOKIE['the_lang'],$allowed_lang) == TRUE) {
				// $lang = $_COOKIE['the_lang'];
			// } else {
				// $lang = "en";
			// }
			// setcookie('the_lang',$lang, time()+86400, base_url());
			
			// if (isset($_GET['lang']) && in_array($_GET['lang'],$allowed_lang) == TRUE) {
				// $lang = $_GET['lang'];
				// setcookie('the_lang',$lang, time()+86400, '/');
			// }
		
		// }
		$lang = 'id';
		$this->lang->load('general',$lang);
		
		if (isset($_GET['do']) && $_GET['do'] == "redir") {
			if (isset($_GET['url'])) {
				$url = urldecode($_GET['url']);
				redirect($url);
			} 
		}
	}
	
	public function active_fetch_class() 
	{
		return $this->router->fetch_class();
	}
	
	public function fetch_method() 
	{
		return $this->router->fetch_method();
	}
	
	public function curl($url,$method,$data)
	{
		$url = 'http://localhost/jobtalento.com/'.$url;
		
		$ch = curl_init( $url );
		
		# Setup request to send json via POST.
		if (isset($method) && $method == "post")
		{
			
			$data_post = '';
			//create name value pairs seperated by &
			foreach($data as $k => $v) 
			{ 
			  $data_post .= $k . '='.$v.'&'; 
			}
			$data_post = rtrim($data_post, '&');
		    
			curl_setopt( $ch, CURLOPT_POST, count($data_post));
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_post );

		}
		
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		# Send request.
		$result = curl_exec($ch);
		curl_close($ch);
		
		# Print response.
		// echo "<pre>";
		return $result;
		// echo "</pre>";
	}
	
	function paging($row_per_page,$total)
	{
		echo 'bisa';die;
		if((int) $total <= (int)$row_per_page ){return;}
		$ci=& get_instance();
		$ret=null;
		$_get=null;
		foreach($_GET as $key => $value){
			if($key=='page'){continue;}
			$_get[$key]=$value;
			
		}
		
		$url=base_url().$ci->uri->uri_string();
		$param=@http_build_query($_get);

		for($i=1;$i <= ceil($total/$row_per_page);$i++){
			$pages=($i<=1)?null:'page='.$i;
			$url_x=($pages || $param)?$url.'?'.$param.'&'.$pages:$url;
			$class=($i==$_GET['page'])?'class="selected"':null;
			
			if($i<=3 || $i>=ceil($total/$row_per_page)-2 || ($i>=$_GET['page']-1 && $i<=$_GET['page']+1) ){
				$ret[]='<a href="'.$url_x.'" '.$class.' ><span>'.$i.'</span></a>';
			}else{
				if($_GET['page']<=$i){
					$ret['pinaple']='..';
				}else{
					$ret['orange']='..';
				}
			}
			
		}
		
		return $ret='<div class="pagin">'.implode('',(array) $ret).'<form action="'.$url.'" style="display:inline-block"><input value="'.$_GET['page'].'" name="page" class="small"><button class="small">Go</button></form></div>';
	}
	
}