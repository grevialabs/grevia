<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function test_method($var = '')
    {
        $var = 'ahsenpai';
		return $var;
    }   
}

function cookie_order_id()
{
	$ret = NULL;
	if (isset($_COOKIE['order_id']) && is_numeric($_COOKIE['order_id']))
	{
		$ret = $_COOKIE['order_id'];
	}
	return $ret;
}

function cdn_url()
{
	return base_url().'asset/';
}

function asset_url()
{
	$base = NULL;
	// if (is_internal()) {
	// $base = BASE_URL;
	// $base = "http://shop.grevia.com/";
	// $base = "http://www.grevia.com/";
	if (isset($base)) {
		// SET 
		return $base.'asset/';
	} else {
		return base_url().'asset/';
	}
}

function sanitize($string)
{
    $string = filter_var($string, FILTER_SANITIZE_STRING);
    $string = trim($string);
    $string = stripslashes($string);
    $string = strip_tags($string);

    return $string;
}

function to_alpha($data){
    $alphabet =   array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
    $alpha_flip = array_flip($alphabet);
        if($data <= 25){
          return $alphabet[$data];
        }
        elseif($data > 25){
          $dividend = ($data + 1);
          $alpha = '';
          $modulo;
          while ($dividend > 0){
            $modulo = ($dividend - 1) % 26;
            $alpha = $alphabet[$modulo] . $alpha;
            $dividend = floor((($dividend - $modulo) / 26));
          } 
          return $alpha;
        }

}

function current_full_url()
{
    $CI =& get_instance();

    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}