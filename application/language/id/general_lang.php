<?php 

/* MENU */
DEFINE('MENU_HOME', 'Home');
DEFINE('MENU_ABOUT_US', 'Tentang Kami');
DEFINE('MENU_ABOUT', 'Tentang Kami');
DEFINE('MENU_CONTACT', 'Kontak');
DEFINE('MENU_LOGIN', 'Login');
DEFINE('MENU_SEARCH', 'Cari');
DEFINE('MENU_FORGOT_PASSWORD', 'Lupa Password');
DEFINE('MENU_RESEND_ACTIVATION_EMAIL', 'Kirim ulang email aktivasi.');

DEFINE('MENU_MY_ACCOUNT', 'Akun Saya');
DEFINE('MENU_MY_SETTING', 'Pengaturan');
DEFINE('MENU_MY_PROFILE', 'Profil');
DEFINE('MENU_MY_ARTICLE', 'Artikel');
DEFINE('MENU_MY_FORUM', 'Forum');
DEFINE('MENU_CHANGE_PASSWORD', 'Ubah kata sandi');
DEFINE('MENU_LOGOUT', 'Keluar');
DEFINE('MENU_LOGIN_REGISTER', 'Login / Registrasi');
DEFINE('MENU_SUBSCRIBE', 'Berlangganan');
DEFINE('MENU_CAREER', 'Karir');
DEFINE('MENU_TERMS', 'Ketentuan');
DEFINE('MENU_FAQ', 'FAQ');
DEFINE('MENU_SITEMAP', 'Sitemap');
DEFINE('MENU_PRIVACY', 'Privasi');
DEFINE('MENU_PRODUCT', 'Webdesign');
DEFINE('MENU_PORTOFOLIO', 'Portofolio');
DEFINE('MENU_VIDEO', 'Video');
//DEFINE('', '');

/* MESSAGE */
DEFINE('INFO_SAVE_SUCCESS', 'Data berhasil disimpan.');
DEFINE('INFO_DATA_INVALID', 'Data tidak valid.');
DEFINE('INFO_ERROR_OCCURED', 'Mohon maaf terjadi kesalahan.');
DEFINE('INFO_EMAIL_SUBSCRIBE_SUCCESS', 'Email berhasil berlangganan.');
DEFINE('INFO_EMAIL_SUBSCRIBE_EXIST', 'Email sudah terdaftar.');

DEFINE('INFO_I_AGREE_TERM_CONDITION', 'Saya setuju dengan syarat dan ketentuan');
DEFINE('INFO_REMEMBER_ME', 'Ingat saya');
/* END MESSAGE */

/* VOCAB */
// A
DEFINE('ACTIVE', 'Aktif');
DEFINE('ACTION', 'Aksi');
DEFINE('ARTICLE', 'Artikel');
DEFINE('ARTICLE_CATEGORY', 'Kategori artikel');
DEFINE('ACCOUNT', 'Akun');
DEFINE('ABOUT', 'Tentang');
DEFINE('ABOUT_US', 'Tentang kami');
DEFINE('ABOUT_ME', 'Tentang saya');

// B
DEFINE('BEFORE', 'Sebelum');
// C
DEFINE('CAREER', 'Karir');
DEFINE('CATEGORY', 'Kategori');
DEFINE('CONTACT', 'Kontak');
DEFINE('CONFIRM', 'Konfirmasi');
DEFINE('CHANGE_PASSWORD', 'Ubah Sandi');

// 
DEFINE('DATA', 'Data');
DEFINE('DESCRIPTION', 'Deskripsi');
DEFINE('DOB', 'Tgl Lahir');

// E
DEFINE('EMAIL', 'Email');

// F
DEFINE('FORUM', 'Forum');

// G
DEFINE('GENDER', 'Jenis kelamin');

// H
DEFINE('HOME', 'Beranda');

// I
DEFINE('INACTIVE', 'Tidak aktif');
DEFINE('INPUT', 'Masukan');

// J
DEFINE('FROM', 'dari');
DEFINE('FULL_NAME', 'Nama lengkap');
DEFINE('FEMALE', 'Wanita');

// K


// L


// M
DEFINE('MALE', 'Pria');

// N
DEFINE('NAME', 'Nama');

// L
DEFINE('LOGIN', 'Login');

// P
DEFINE('PAGE', 'Halaman');
DEFINE('PASSWORD', 'Kata sandi');
DEFINE('PROFILE', 'Profil');

// R
DEFINE('REGISTRATION', 'Registrasi');
DEFINE('REGISTER', 'Daftar');

// S
DEFINE('SEARCH', 'Cari');
DEFINE('SETTING', 'Pengaturan');
DEFINE('SELECTED', 'Terpilih');
DEFINE('SELECT_GENDER', 'Pilih jenis kelamin');
DEFINE('SUBSCRIBE_NEWSLETTER', 'Berlangganan newsletter');
DEFINE('STATISTIC', 'Statistik');
DEFINE('SECURE_NOTES', 'Secure Notes');

// T
DEFINE('THIS', 'ini');
DEFINE('TODO', 'Todo List');

DEFINE('OLD_PASSWORD', 'Kata sandi lama');
DEFINE('NEW_PASSWORD', 'Kata sandi baru');
DEFINE('CONFIRM_NEW_PASSWORD', 'Konfirmasi sandi baru');

// U


// V
DEFINE('VIDEO', 'Video');

// W

// X


// Y

// Z
DEFINE('ORDER', 'Pesan');


// 
/* */

/* */
DEFINE('ADD_NEW', 'Tambah baru');
DEFINE('ADD', 'Tambah');
DEFINE('EDIT', 'Mengedit');
DEFINE('INSERT', 'Tambahkan');
DEFINE('SAVE', 'Simpan');
DEFINE('UPDATE', 'Perbarui');
DEFINE('DELETE', 'Hapus');
//DEFINE('', '');


