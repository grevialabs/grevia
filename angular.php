<?php echo date('YmdHis', strtotime('2015-11-19 12:28:36')) ?>

<!doctype>
<html>
<head>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
</head>

<script>
$scope.add = function(){
  $http.get($scope.url).then(function(response) {
            $scope.newMessage = response.data.queries.request.totalResults;
            $scope.messages.push($scope.newMessage);
  });
};
</script>

<body>
<div ng-app="myApp" ng-controller="c_data">
	<table cellpadding="5" border="1">
	<tr>
		<td>#</td>
		<td>Name</td>
		<td>Email</td>
	</tr>
	<tr ng-repeat="x in list_member">
		<td>#</td>
		<td>{{ x.Name }}</td>
		<td>{{ x.Email }}</td>
	</tr>
	</table>
	<div id="label"></div>
</div>

<script>
var app = angular.module('myApp', []);
app.controller('c_data', function($scope, $http) {
    url = 'http://localhost/grevia.com/api/member';
	$http.get(url)
    .success(function(response) {
		$scope.list_member = response.data;
	});
	
	// url = 'http://localhost/grevia.com/api/member?MemberID=1';
	// $http.get(url)
    // .success(function(response) {
		// $scope.member = response.data;
		
	// });
});
</script>
</body>
</html>