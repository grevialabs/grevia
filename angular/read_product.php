<?php 
// include database and object files 
include_once 'config/database.php'; 
include_once 'class/product.php'; 
 
// get database connection 
$database = new Database(); 
$db = $database->getConnection();
 
// prepare product object
$product = new Product($db);
 
// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));     
 
// set ID property of product to be edited
// $product->product_id = 1;
$product->product_id = $data->product_id;
 
// read the details of product to be edited
$product->get_one();
 
// create array
$product_arr[] = array(
    "product_id" =>  $product->product_id,
    "name" => $product->name,
    "description" => $product->description,
    "price" => $product->price
);
 
// make it json format
print_r(json_encode($product_arr));
?>