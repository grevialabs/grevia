<?php 
class Product{ 
    // database connection and table name 
    private $conn; 
    private $table_name = "product"; 
 
    // object properties 
    public $product_id; 
    public $name; 
    public $description; 
    public $price; 
    public $creator_date; 
 
    // constructor with $db as database connection 
    public function __construct($db){ 
        $this->conn = $db;
    }
	
	// used when filling up the update product form
	function get_one(){
		 
		// query to read single record
		$query = "SELECT 
					name, price, description  
				FROM 
					" . $this->table_name . "
				WHERE 
					product_id = ? 
				LIMIT 
					0,1";
	 
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		 
		// bind id of product to be updated
		$stmt->bindParam(1, $this->product_id);
		 
		// execute query
		$stmt->execute();
	 
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		 
		// set values to object properties
		$this->name = $row['name'];
		$this->price = $row['price'];
		$this->description = $row['description'];
	}

	// read products
	function get_all(){
	 
		// select all query
		$query = "SELECT 
					product_id, name, description, price, creator_date 
				FROM 
					" . $this->table_name . "
				ORDER BY 
					product_id DESC";
	 
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		 
		// execute query
		$stmt->execute();
		return $stmt;
	}
	
	// create product
	function create(){
		 
		// query to insert record
		$query = "INSERT INTO 
					" . $this->table_name . "
				SET 
					name=:name, price=:price, description=:description, creator_date=:creator_date";
		 
		// prepare query
		$stmt = $this->conn->prepare($query);
	 
		// posted values
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->price=htmlspecialchars(strip_tags($this->price));
		$this->description=htmlspecialchars(strip_tags($this->description));
	 
		// bind values
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":creator_date", $this->creator_date);
		 
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			echo "<pre>";
				print_r($stmt->errorInfo());
			echo "</pre>";
	 
			return false;
		}
	}
	
	// update the product
	function update(){
	 
		// update query
		$query = "UPDATE 
					" . $this->table_name . "
				SET 
					name = :name, 
					price = :price, 
					description = :description 
				WHERE
					product_id = :product_id";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	 
		// posted values
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->price=htmlspecialchars(strip_tags($this->price));
		$this->description=htmlspecialchars(strip_tags($this->description));
	 
		// bind new values
		$stmt->bindParam(':name', $this->name);
		$stmt->bindParam(':price', $this->price);
		$stmt->bindParam(':description', $this->description);
		$stmt->bindParam(':product_id', $this->product_id);
		 
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the product
	function delete(){
	 
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE product_id = ?";
		 
		// prepare query
		$stmt = $this->conn->prepare($query);
		 
		// bind id of record to delete
		$stmt->bindParam(1, $this->product_id);
	 
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
}


?>