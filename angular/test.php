<?php 
if ($_POST) 
{
	var_dump($_POST);
	die;
}
?>
<!doctype>
<html>
<head>
	
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	
	<!-- Compiled and minified CSS -->
	<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">

	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	
	<!-- custom CSS -->
	<style>
	.width-30-pct{
		width:30%;
	}
	 
	.text-align-center{
		text-align:center;
	}
	 
	.margin-bottom-1em{
		margin-bottom:1em;
	}
	</style>
	
</head>

<script>
$scope.add = function(){
  $http.get($scope.url).then(function(response) {
            $scope.newMessage = response.data.queries.request.totalResults;
            $scope.messages.push($scope.newMessage);
  });
};
</script>

<body>
<?php //echo date('YmdHis', strtotime('2015-11-19 11:28:39')) ?>
<div ng-app="myApp" ng-controller="c_data">
	<table cellpadding="5" cellspacing="0" border="1" >
	<tr>
		<td>#</td>
		<td>Name</td>
		<td>Email</td>
		<td>Option</td>
	</tr>
	<tr ng-repeat="x in list_member" style="">
		<td ng-if="$odd" style="background:#000;color:#fff">{{ $index+1 }}</td>
		<td ng-if="$even">{{ $index+1 }}</td>
		<td>{{ x.name }}</td>
		<td>{{ x.email }}</td>
		<td><a href="?do=edit&member_id={{ x.member_id}}" alt="Edit data" title="Edit data">Edit</a></td>
	</tr>
	</table>
	<div id="label"></div>

	<!--<select ng-model="selectedCar">
	<option ng-repeat="rs in cars" value="{{rs.model}}">{{rs.model}}</option>
	
	<select ng-model="selectedCar" ng-options="rs.model for rs in cars">-->
	<select ng-model="selectedCar">
		<option ng-repeat="rs in cars" value="{{ rs.model }}">{{ rs.color }}</option>
	</select><hr/><br/>
	<div>You select {{ selectedCar.model }}</div>

</div>


<!-- Switch -->
  <div class="switch">
    <label>
      Off
      <input type="checkbox">
      <span class="lever"></span>
      On
    </label>
  </div>

<script>
var app = angular.module('myApp', []);
app.controller('c_data', function($scope, $http) {
    url = 'http://localhost/grevia.com/api/member';
	$http.get(url)
    .success(function(response) {
		$scope.list_member = response.data;
	});
	
	$scope.cars = [
        {model : "Ford Mustang", color : "red"},
        {model : "Fiat 500", color : "white"},
        {model : "Volvo XC90", color : "black"}
    ];
	
	// url = 'http://localhost/grevia.com/api/member?member_id=1';
	// $http.get(url)
    // .success(function(response) {
		// $scope.list_member = response.data;
		
	// });
});
</script>
</body>
</html>