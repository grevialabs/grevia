<?php 
// get database connection 
include_once 'config/database.php'; 
include_once 'class/product.php';

$database = new Database(); 
$db = $database->getConnection();
 
// instantiate product object

$product = new Product($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input")); 
 
// set product property values
$product->name = $data->name;
$product->price = $data->price;
$product->description = $data->description;
$product->creator_date = date('Y-m-d H:i:s');
     
// create the product
if($product->create()){
    echo "Product was created.";
}
 
// if unable to create the product, tell the user
else{
    echo "Unable to create product.";
}
?>