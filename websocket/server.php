<?php
$host = 'localhost'; //host
$port = '9000'; //port
$null = NULL; //null var

$email = '';
if (isset($_GET['email'])) $email = $_GET['email'];

//Create TCP/IP sream socket
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
//reuseable port
socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);

//bind socket to specified host
socket_bind($socket, 0, $port);

//listen to port
socket_listen($socket);

//create & add listning socket to the list
$clients = array($socket);

//start endless loop, so that our script doesn't stop
while (true) 
{
	//manage multipal connections
	$changed = $clients;
	
	//returns the socket resources in $changed array
	socket_select($changed, $null, $null, 0, 10);
	
	//check for new socket
	if (in_array($socket, $changed)) 
	{

		// $socket_new = socket_accept($socket); //accpet new socket
		// $clients[] = $socket_new; //add socket to client array
		
		$socket_new = socket_accept($socket);
		$header = socket_read($socket_new, 1024); //read data sent by the socket
		
		// $clients[] = $socket_new;
		
		$new_client = '';
		$new_client = end(explode('?session=',$header));
		$new_client = current(explode(' ',$new_client));
		$clients[$new_client] = $socket_new;
		// unset($clients[0]);
		// var_dump($new_client);
		// var_dump($clients);
		
		// var_dump($clients);
		// var_dump(perform_handshaking($header, $socket_new, $host, $port)); //perform websocket handshake
		perform_handshaking($header, $socket_new, $host, $port); //perform websocket handshake
		
		// socket_getpeername($socket_new, $ip); //get ip address of connected socket
		// $response = mask(json_encode(
			// array(
				// 'type'		=>	'system', 
				// 'message'	=>	'ahay someone connected'
			// )
		// )); 
		
		//notify all users about new connection
		// send_broadcast_message($response); 
		
		//make room for new socket
		$found_socket = array_search($socket, $changed);
		unset($changed[$found_socket]);
	}
	
	//loop through all connected sockets
	foreach ($changed as $changed_socket) 
	{
		
		//check for any incoming data
		while(socket_recv($changed_socket, $buf, 1024, 0) >= 1)
		{
			
			$received_text = unmask($buf); //unmask data
			$received_text = json_decode($received_text); //json decode 
			$user_name = $received_text->name; //sender name
			$user_message = $received_text->message; //message text
			$user_color = $received_text->color; //color
			
			// check if not broadcast
			// if (isset($received_text->is_broadcast))
			if (isset($received_text->to_id))
			{
				if ($received_text->to_id == "all")
				{
					$user_message = 'Broadcast ' .$received_text->from_id. ': ' . $user_message;
					// if contain ; then send to selected user
					
					//prepare data to be sent to client
					$response_text = mask(json_encode(
						array(
						'type'		=>	'usermsg', 
						'name'		=>	$user_name, 
						'message'	=>	$user_message, 
						'color'		=>	$user_color
						)
					));
					// echo "jalan send_broadcast_message";
					send_broadcast_message($response_text); //send data
				}
				else if($received_text->to_id)
				{
					// check to_id
					$from_id = $received_text->from_id;
					$to_id = $received_text->to_id;
					$user_message = 'Untuk: '.$to_id.'; message: '.$user_message;
					
					//prepare data to be sent to client
					$response_text = mask(json_encode(
						array(
						'type'		=>	'usermsg', 
						'name'		=>	$user_name, 
						'message'	=>	$user_message, 
						'color'		=>	$user_color
						)
					));
					if (strpos($to_id,';') !== FALSE)
					{
						send_several_message($from_id, $to_id, $response_text);
					}
					else 
					{
						send_single_message($from_id, $to_id, $response_text);
					}
				}
				
			}
			
			// check single message
			else
			{
				// gajelas nih
			}
			
			break 2; //exist this loop
		}
		
		$buf = @socket_read($changed_socket, 1024, PHP_NORMAL_READ);
		
		// check disconnected client
		if ($buf === false) 
		{
			// remove client for $clients array
			$found_socket = array_search($changed_socket, $clients);
			socket_getpeername($changed_socket, $ip);
			unset($clients[$found_socket]);
			
			//notify all users about disconnected connection
			// $response = mask(json_encode(
				// array(
					// 'type'		=>	'system',
					// 'message'	=>	$ip.' disconnected'
				// )
			// ));
			// send_broadcast_message($response);
		}
	}
}
// close the listening socket
socket_close($socket);

// send broadcast message
function send_broadcast_message($msg)
{
	global $clients;
	foreach($clients as $key => $changed_socket)
	{
		// if ($key == 0) {
			// continue 1; 
		// }
		// var_dump($changed_socket);
		echo "ngirim nih ke ".$key;
		@socket_write($changed_socket,$msg,strlen($msg));
	}
	return true;
}

// send single message to person
function send_several_message($from_id = NULL, $to_id = NULL, $msg)
{
	$list_target = array();
	
	if (isset($to_id) && strpos($to_id, ';') !== FALSE)
	{
		$list_target = explode(';',$to_id);
	}
	
	global $clients;
	foreach($clients as $key => $changed_socket)
	{
		
		// if to_id is in array 
		if (in_array($key,$list_target) || $key == $from_id)
		{
			echo 'User '.$from_id.' mengirim ke '.$to_id.'<br/>';
			@socket_write($changed_socket,$msg,strlen($msg));
		}
		
		// send to from and to user
		// if (isset($from_id) && isset($to_id) && ($from_id == $key || $to_id == $key) ) 
		// {
			// echo 'User '.$from_id.' mengirim ke '.$to_id.'<br/>';
			// @socket_write($changed_socket,$msg,strlen($msg));
		// }
	}
	return true;
}

// send single message to person
function send_single_message($from_id = NULL, $to_id = NULL, $msg)
{
	global $clients;
	foreach($clients as $key => $changed_socket)
	{
		// send to from and to user
		if (isset($from_id) && isset($to_id) && ($from_id == $key || $to_id == $key) ) 
		{
			echo 'User '.$from_id.' mengirim ke '.$to_id.'<br/>';
			@socket_write($changed_socket,$msg,strlen($msg));
		}
	}
	return true;
}


//Unmask incoming framed message
function unmask($text) 
{
	$length = ord($text[1]) & 127;
	if($length == 126) {
		$masks = substr($text, 4, 4);
		$data = substr($text, 8);
	}
	elseif($length == 127) {
		$masks = substr($text, 10, 4);
		$data = substr($text, 14);
	}
	else {
		$masks = substr($text, 2, 4);
		$data = substr($text, 6);
	}
	$text = "";
	for ($i = 0; $i < strlen($data); ++$i) {
		$text .= $data[$i] ^ $masks[$i%4];
	}
	return $text;
}

//Encode message for transfer to client.
function mask($text)
{
	$b1 = 0x80 | (0x1 & 0x0f);
	$length = strlen($text);
	
	if($length <= 125)
		$header = pack('CC', $b1, $length);
	elseif($length > 125 && $length < 65536)
		$header = pack('CCn', $b1, 126, $length);
	elseif($length >= 65536)
		$header = pack('CCNN', $b1, 127, $length);
	return $header.$text;
}

//handshake new client.
function perform_handshaking($received_header,$client_conn, $host, $port)
{
	$headers = array();
	$lines = preg_split("/\r\n/", $received_header);
	foreach($lines as $line)
	{
		$line = chop($line);
		if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
		{
			$headers[$matches[1]] = $matches[2];
		}
	}

	$secKey = $headers['Sec-WebSocket-Key'];
	$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
	//hand shaking header
	$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
	"Upgrade: websocket\r\n" .
	"Connection: Upgrade\r\n" .
	"WebSocket-Origin: $host\r\n" .
	"WebSocket-Location: ws://$host:$port/demo/shout.php\r\n".
	"Sec-WebSocket-Accept:$secAccept\r\n\r\n";
	socket_write($client_conn,$upgrade,strlen($upgrade));
}
