<?php 
include_once "connection/connect.php";
include_once "class/GlobalFunction.php";

//include_once "class/Article.php";

header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" ?>';

$root = "http://www.grevia.com";

function makeUrlString ($urlString) {
    return htmlentities($urlString, ENT_QUOTES, 'UTF-8');
}
?>
<urlset xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

	<url>
		<loc><?php echo $root?></loc>
	</url>
	<url>
		<loc><?php echo $root?>/about</loc>
	</url>
	<url>
		<loc><?php echo $root?>/contact_us</loc>
	</url>
	<url>
		<loc><?php echo $root?>/login</loc>
	</url>
	<?php 
	//ARTICLE
	$query = mysql_query("
	SELECT ArticleID, Slug
	FROM grv_article 
	");
	while($rs = mysql_fetch_array($query) ){
		echo "<url>\n";
		echo "		<loc>".makeUrlString( friendlyUrl("showarticle.php?aid=".$rs['ArticleID']."&slug=".$rs['Slug']) )."</loc>";
		echo "\n	</url>\n";
	}
	?>
	<?php
	// $query = mysql_query("
	// SELECT CategoryName
	// FROM grv_articlecategory
	// ");
	// while($rs = mysql_fetch_array($query) ){
		// echo "	<url>\n";
		// echo "	<loc>".makeUrlString( friendlyUrl("/showcategoryarticle.php?name=".$rs['CategoryName']) )."</loc>";
		// echo "\n	</url>\n";
	// }
	?>
</urlset>